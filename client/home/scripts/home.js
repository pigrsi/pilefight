function loadGameSelection() {
	$("#mainDiv").load("/home/game-select.html");
}

function clickedCreateGameButton(isPrivateGame=false) {
	let nickname = document.getElementById("nicknameBox").value;
	document.cookie = "nickname="+nickname+"; expires=Thu, 2 Apr 2101 12:00:00 UTC; path=/";
	if (nickname) {
		requestCreateGame(isPrivateGame);
		$("#mainDiv").load("/home/waiting.html");
	}
}