function checkJoiningGame() {
	// if (!window.location.search.includes('?')) return false;
	// let token = window.location.search.split('?')[1];
	
	// // Try to join game...
	// alert("Failed to join game!");
	// return false;
}

function requestCreateGame(isPrivateGame=false) {
	var sock = io(`${location.protocol}//${window.location.hostname}/home`);
	var sock = sock.connect();
	
	sock.on('connect', function() {
		console.log('Connected to server');
		sock.emit('requestgame', {"private": isPrivateGame});
	});
	
	sock.on('foundgame', function(info) {
		window.location.replace(`${location.protocol}//${window.location.hostname}/game?` + info.roomId);
		console.log(info);
	});
}