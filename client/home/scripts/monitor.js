// var socketIO = require('socket.io');
var socket = io(`${location.protocol}//${window.location.hostname}/monitor`);

var connected = false;
var interval;
socket.on('connect', function(data) {
    console.log("connected");
    connected = true;
    requestServerInfo();
    interval = setInterval(requestServerInfo, 1000);
});

socket.on('disconnect', function(data) {
    console.log("disconnected");
    connected = false;
    if (interval) {
        clearInterval(interval);
        interval = null;
    }
});

socket.on('monitorInfo', function(data) {
    updateInfo(data);
});

function requestServerInfo() {
    socket.emit('monitorRequest');
}

function updateInfo(data) {
    console.log(data);

    for (var i = 0; i < data.workers.length; ++i) {
        updateTableForWorker(data.workers[i]);
    }
}

function updateTableForWorker(workerInfo) {
    var tableId = "workerTable" + workerInfo.workerWid;
    var table = document.getElementById(tableId);
    if (!table) {
        var temp = document.getElementsByClassName("tableTemplate")[0];
        var clone = temp.content.cloneNode(true);
        clone.querySelector('table').id = tableId;
        clone.querySelector('.workerWid').innerText = "WorkerWID: " + workerInfo.workerWid;
        document.getElementById('tables').appendChild(clone);
        table = document.getElementById(tableId);
    }

    table.innerHTML = '';

    var temp = document.getElementsByClassName("headerTemplate")[0];
    var clone = temp.content.cloneNode(true);
    table.appendChild(clone);

    appendDataToTable(table, workerInfo.rooms);
}

function appendDataToTable(table, roomList) {
    var temp = document.getElementsByClassName("roomInfoTemplate")[0];
    for (var i = 0; i < roomList.length; ++i) {
        var clone = temp.content.cloneNode(true);

        var roomIdCell = clone.querySelector('.roomIdCell');
        var clientsCell = clone.querySelector('.clientsCell');
        var joinableCell = clone.querySelector('.joinableCell');
        var nextRoom = roomList[i];
        roomIdCell.innerText = `${location.protocol}/${window.location.hostname}/game/?${nextRoom.roomId}`;
        clientsCell.innerText = nextRoom.numCurrentClients;
        joinableCell.innerText = !nextRoom.joinable;

        table.appendChild(clone);
    }
}