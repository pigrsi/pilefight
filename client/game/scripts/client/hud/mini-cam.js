define([], function() {
class MiniCam {
	constructor(mainScene, hudScene, mainCam) {
		this.mainCam = mainCam;
		this.hudScene = hudScene;
		this.borderImages = [];
		this.cam = mainScene.cameras.add(0, 0, 0, 0).setZoom(0.5);
		this.cam.transparent = false;
		this.cam.roundPixels = true;
		this.cam.setBackgroundColor(this.mainCam.getCamera().backgroundColor);

		this.createBorder(hudScene);
		this.resetBounds();

		this.setVisible(false);
	}
	
	update(delta, hoveredOver) {
		if (this.following && !this.canFollow(this.following)) {
			if (this.following)
				this.stopFollowing();
		}

		if (hoveredOver) {
			let graphics = this.getHudGraphics();
			if (graphics) {
				graphics.fillStyle(0xffffff, 0.5);
				graphics.fillRect(this.cam.x, this.cam.y, this.cam.width, this.cam.height);
			}
		}
	}
	
	getHudGraphics() {
		if (this.hudGraphics) return this.hudGraphics;
		let gameObjects = this.hudScene.make.displayList.getChildren();
		for (let i = 0; i < gameObjects.length; ++i) {
			if (gameObjects[i].name === 'hudGraphics') {
				this.hudGraphics = gameObjects[i];
				return this.hudGraphics;
			}
		}
	}
	
	canFollow(item) {
		return !(this.mainCam.itemIsOnCamera(item) || item.isDisabledOrDead() || (item.isStill() && !item.beingForciblyMoved));
	}
	
	canStartFollowing(item) {
		return this.canFollow(item) && !(this.cam.visible && this.itemIsOnCamera(item));
	}
	
	resetBounds() {
		this.width = 400;
		this.height = 300;
		this.halfWidth = this.width / 2;
		this.halfHeight = this.height / 2;
		let margin = 32;
		let actualCam = this.mainCam.getCamera();
		this.cam.setPosition(actualCam.width - this.width - margin, actualCam.height - this.height - margin);
		this.cam.setSize(this.width, this.height);
		this.setBorderPositions();
	}
	
	itemIsOnCamera(item) {
		let width = this.cam.width;
		let height = this.cam.height;
		let widthDiff = (width/this.cam.zoom)-width;
		let heightDiff = (height/this.cam.zoom)-height;
		let left = this.cam.scrollX - width/2 - widthDiff/2;
		let right = this.cam.scrollX + width/2 + widthDiff/2;
		let top = this.cam.scrollY - height/2 - heightDiff/2;
		let bottom = this.cam.scrollY + height/2 + heightDiff/2;
		
		return item.x+item.width > left &&
		item.x < right &&
		item.y+item.height > top &&
		item.y < bottom;
	}
	
	createBorder() {
		let hudScene = this.hudScene;
		let images = [];
		let tex = 'mini-cam-border';
		let frameWidth = 4;
		let frameHeight = 4;
		let cam = this.cam;
		this.borderImages.push(hudScene.add.image(cam.x, cam.y, tex, 0));
		this.borderImages.push(hudScene.add.image(cam.x+frameWidth, cam.y, tex, 1));
		this.borderImages.push(hudScene.add.image(cam.x+cam.width-frameWidth, cam.y, tex, 2));
		
		this.borderImages.push(hudScene.add.image(cam.x, cam.y+frameHeight, tex, 3));
		this.borderImages.push(hudScene.add.image(cam.x+cam.width-frameWidth, cam.y+frameHeight, tex, 5));
		
		this.borderImages.push(hudScene.add.image(cam.x, cam.y+cam.height-frameHeight, tex, 6));
		this.borderImages.push(hudScene.add.image(cam.x+frameWidth, cam.y+cam.height-frameHeight, tex, 7));
		this.borderImages.push(hudScene.add.image(cam.x+cam.width-frameWidth, cam.y+cam.height-frameHeight, tex, 8));
		
		for (let i = 0; i < this.borderImages.length; ++i) {
			let image = this.borderImages[i];
			image.setScrollFactor(0);
			image.setOrigin(0);
		}
	}
	
	setBorderPositions() {
		let images = this.borderImages;
		let cam = this.cam;
		let frameWidth = 4;
		let frameHeight = 4;
		
		images[0].setPosition(cam.x, cam.y);
		images[1].setPosition(cam.x+frameWidth, cam.y);
		images[2].setPosition(cam.x+cam.width-frameWidth, cam.y);
		
		images[3].setPosition(cam.x, cam.y+frameHeight);
		images[4].setPosition(cam.x+cam.width-frameWidth, cam.y+frameHeight);
		
		images[5].setPosition(cam.x, cam.y+cam.height-frameHeight);
		images[6].setPosition(cam.x+frameWidth, cam.y+cam.height-frameHeight);
		images[7].setPosition(cam.x+cam.width-frameWidth, cam.y+cam.height-frameHeight);
		
		// Size
		images[1].displayWidth = cam.width - (frameWidth/2);
		images[3].displayHeight = cam.height - (frameHeight/2);
		images[4].displayHeight = cam.height - (frameHeight/2);
		images[6].displayWidth = cam.width - (frameWidth/2);
	}
	
	pointerIsOverCam(pointerPos) {
		return  pointerPos.x >= this.cam.x &&
				pointerPos.x <= this.cam.x + this.cam.width &&
				pointerPos.y >= this.cam.y &&
				pointerPos.y <= this.cam.y + this.cam.height;
	}
	
	setVisible(visible) {
		for (let i = 0; i < this.borderImages.length; ++i) {
			this.borderImages[i].setVisible(visible);
		}
		this.cam.setVisible(visible);
	}
	
	tryGoInvisible() {
		if (this.cam.visible && !this.following) this.setVisible(false);
	}
	
	getFollowing() {
		return this.following;
	}
	
	isVisible() {
		return this.cam.visible;
	}
	
	followItem(item) {
		if (this.following === item) return;
		this.following = item;
		this.setVisible(true);
		this.cam.stopFollow();
		this.cam.startFollow(item, true, 0.1, 0.1);
	}
	
	stopFollowing() {
		this.following = null;
		setTimeout(this.tryGoInvisible.bind(this), 250);
	}
	
	isFollowing() {
		return !!this.following;
	}
}

return MiniCam;
});