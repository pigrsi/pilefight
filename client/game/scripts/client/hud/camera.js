define([], function() {
class Camera {
	constructor(cam, scene) {
		this.cam = cam;
		this.scene = scene;
		this.cam.setBackgroundColor('#9BC297');
		this.cam.roundPixels = true;
		this.cam.setOrigin(0, 0);
		this.controlPressed('SET_ZOOM', 0, 0, 0.75);
		this.velocity = {x: 0, y: 0};
	}
	
	scrollCamera(x, y) {
		// camera follow null is debug
		this.cameraFollow = null;
		if (true) {
			this.cam.scrollX += x;
			this.cam.scrollY += y;
		}
	}
	
	setCameraPosition(x, y) {
		this.cam.scrollX = x;
		this.cam.scrollY = y;
	}
	
	tickMovement(delta) {
		if (this.cameraFollow) {
			if (this.followTween) {
				let tween = this.followTween;
				let follow = this.cameraFollow;
				tween.updateTo('scrollX', follow.x + follow.halfWidth - this.cameraHalfWidth, false);
				tween.updateTo('scrollY', follow.y + follow.halfHeight - this.cameraHalfHeight, false);
			} else {
				this.velocity = {x: 0, y: 0};
				let item = this.cameraFollow;
				this.setCameraPosition(item.x + item.halfWidth - this.cameraHalfWidth, item.y + item.halfHeight - this.cameraHalfHeight);
			}
			
			if (this.cameraFollow.disabled) this.cameraFollow = null;
		} else {
			this.scrollCamera(this.velocity.x, this.velocity.y);
			if (this.velocity.x != 0) this.velocity.x *= 0.9;
			if (this.velocity.y != 0) this.velocity.y *= 0.9;
			if (Math.abs(this.velocity.x) < 0.1) this.velocity.x = 0;
			if (Math.abs(this.velocity.y) < 0.1) this.velocity.y = 0;
		}
		this.updateCameraBounds();
	}
	
	scrollCamera(xVel, yVel) {
		this.cam.scrollX += xVel;
		this.cam.scrollY += yVel;
	}
	
	followItem(item, followAfterTween=true, tweenDuration) {
		if (!item) return;
		this.stopFollowing();
		this.cameraFollow = followAfterTween ? item : null;
		if (tweenDuration <= 0) return;
		this.followTween = this.scene.tweens.add({
			targets: this.cam,
			scrollX: item.x + item.halfWidth - this.cameraHalfWidth,
			scrollY: item.y + item.halfWidth - this.cameraHalfHeight,
			duration: tweenDuration,
			ease: 'Quad.easeOut',
			delay: 0,
			onComplete: this.setFollowTweenNull.bind(this),
		});
	}
	
	setFollowTweenNull() {
		this.followTween = null;
	}
	
	isFollowing(object) {
		return object === this.cameraFollow;
	}
	
	stopFollowing() {
		if (this.followTween) this.followTween.stop();
		this.followTween = null;
		this.cameraFollow = null;
	}
	
	setVelocity(x, y) {
		this.velocity.x = x;
		this.velocity.y = y;
	}
	
	controlPressed(cameraControlType, scrollX=0, scrollY=0, zoom=1, zoomFocusX=0, zoomFocusY=0) {
		this.stopFollowing();
		let zoomAmount = 0;
		switch (cameraControlType) {
			case 'ZOOM_IN':
				if (this.cam.zoom < 1) zoomAmount = 0.025;
				break;
			case 'ZOOM_OUT':
				if (this.cam.zoom > 0.25) zoomAmount = -0.025;
				break;
			case 'SCROLL_KEY':
				this.scrollCamera(scrollX, scrollY);
				this.cam.velocity = {x: 0, y: 0};
				break;
			case 'SET_ZOOM':
				zoomAmount = zoom - this.cam.zoom;
				break;
		}
			
		if (!zoomAmount) return;
		let distX = (this.cam.width/this.cam.zoom) - (this.cam.width/(this.cam.zoom + zoomAmount));
		let distY = (this.cam.height/this.cam.zoom) - (this.cam.height/(this.cam.zoom + zoomAmount));
		if (zoomFocusX != 0) {
			// let pointerMidVector = {x: (zoomFocusX-WorldViewer.cameraMidX), y: (zoomFocusY-WorldViewer.cameraMidY)};
			// let mag = Math.sqrt(pointerMidVector.x*pointerMidVector.x + pointerMidVector.y*pointerMidVector.y);
			// if (mag !== 0) unit = {x: pointerMidVector.x/mag, y: pointerMidVector.y/mag};
		}
		this.cam.scrollX += distX/2;
		this.cam.scrollY += distY/2;
		this.cam.zoom += zoomAmount;
	}
	
	shake(duration) {
		this.cam.shake(duration);
	}
	
	setSize(width, height) {
		this.cam.setSize(width, height);
	}
	
	// Camera properties adjusted for zoom
	updateCameraBounds() {
		this.cameraLeft = this.cam.scrollX;
		this.cameraTop = this.cam.scrollY;
		this.cameraWidth = (this.cam.width / this.cam.zoom)
		this.cameraHeight = (this.cam.height / this.cam.zoom);
		this.cameraHalfWidth = this.cameraWidth/2;
		this.cameraHalfHeight = this.cameraHeight/2;
		this.cameraRight = this.cameraLeft + this.cameraWidth;
		this.cameraBottom = this.cameraTop + this.cameraHeight;
		this.cameraMidX = this.cameraLeft + this.cameraHalfWidth;
		this.cameraMidY = this.cameraTop + this.cameraHalfHeight;
	}
	
	itemIsOnCamera(item) {
		return item.x+item.width > this.cameraLeft &&
		item.x < this.cameraRight &&
		item.y+item.height > this.cameraTop &&
		item.y < this.cameraBottom;
	}
	
	getCamera() {
		return this.cam;
	}
}

return Camera;
});