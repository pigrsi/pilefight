define(['shared/global-info', 'hud/aimer', 'shared/physics', 'client/game-status', 'hud/end-screen', 'buttons/move-buttons', 'buttons/pregame-buttons', 'items/pebble'],
    function (GlobalInfo, Aimer, Physics, GameStatus, EndScreen, MoveButtons, PreGameButtons, Pebble) {
        Hud = {};
        Hud.pointerPos = null;
        Hud.camera = null;
        var WorldNetwork;
        var World;

        Hud.setWorld = function (world) {
            World = world;
        };

        Hud.setWorldNetwork = function (worldNetwork) {
            WorldNetwork = worldNetwork;
        };

        Hud.setGameScene = function (gameScene) {
            Hud.gameScene = gameScene;
        };

        Hud.setUserActions = function (userActions) {
            Hud.userActions = userActions;
        };

        Hud.init = function () {
            Hud.hudScene = this;

            WorldNetwork.registerListener(Hud.onWorldNetworkEvent);
            World.registerListener(Hud.onWorldEvent);
            Hud.hudGraphics = Hud.hudScene.add.graphics();
            Hud.hudGraphics.name = 'hudGraphics';
            Hud.hudGraphics.depth = 40;
            // Hud.hudGraphics.setScrollFactor(0);
            Hud.gameGraphics = Hud.gameScene.add.graphics();
            Hud.gameGraphics.depth = 30;
            Aimer.init(WorldNetwork.applyAimer.bind(WorldNetwork), Hud.gameGraphics);
            Hud.moveButtons = new MoveButtons(Hud.hudScene, Hud.onButtonClicked);
            Hud.pregameButtons = new PreGameButtons(Hud.hudScene, Hud.onButtonClicked);
            Hud.pregameButtons.setVisible(true);
            // Buttons.init(Hud.hudScene, Hud.onButtonClicked);
            Hud.pointsTexts = [];

            Hud.buttonManagers = [Hud.moveButtons, Hud.pregameButtons];
        };

        Hud.setGameScene = function (gameScene) {
            Hud.gameScene = gameScene;
            Hud.camera = gameScene.cameras.main;
        };

        Hud.clearGraphics = function () {
            Hud.hudGraphics.clear();
            Hud.gameGraphics.clear();
        };

        Hud.draw = function () {
            Hud.checkAimerStatus();

            Hud.drawTurnPlayerIndicator();
            Hud.displayPlayerNames();
            Hud.displayPlayerDisconnected();
            Hud.displayTurnsLeftForCurrentPlayer();
            Hud.displayClientPoints();
            //Hud.displayClientLives();
            Hud.drawEndScreen();
            Hud.drawDebug();
            Hud.updatePointsTexts();
            Aimer.draw();
            Hud.manageButtons();
        };

        Hud.updatePointer = function (pointerPos) {
            Hud.pointerPos = pointerPos;
        };

        Hud.aimerPebble = new Pebble();
        Hud.checkAimerStatus = function () {
            if (Hud.shouldShowAimer()) {
                let turnPlayer = WorldNetwork.getTurnPlayer();
                let item = WorldNetwork.getSelectedThrowItem() ? WorldNetwork.getSelectedThrowItem() : turnPlayer;
                let moveType = WorldNetwork.getTurnPlayerMove().moveType;
                if (moveType === 'THROW' && item === turnPlayer) {
                    item = Hud.aimerPebble;
                    item.setPosition(turnPlayer.x + turnPlayer.halfWidth - item.halfWidth, turnPlayer.y + turnPlayer.halfHeight - item.halfHeight);
                }
                Aimer.setLaunchType(WorldNetwork.getTurnPlayerMove().moveType, item);
                Aimer.setStartPosition(item.x + item.halfWidth, item.y + item.halfHeight);
                if (!Aimer.waitingForRelease) Aimer.setEndPosition(item.x + item.halfWidth, item.y + item.halfHeight);
                Aimer.active = true;
            } else {
                Aimer.active = false;
            }
        };

        Hud.shouldShowAimer = function () {
            return WorldNetwork.localPlayerIsDecidingMove() &&
                WorldNetwork.getTurnPlayer() &&
                !WorldNetwork.getTurnPlayerMove().moveReady &&
                WorldNetwork.getTurnPlayerMove().moveType;
        };

        Hud.onPointerDown = function (pointerPosAdjusted, pointerPosUnadjusted) {
            for (let i = 0; i < Hud.buttonManagers.length; ++i)
                if (Hud.buttonManagers[i].onPointerDown(pointerPosUnadjusted)) return true;
            return Aimer.onPointerDown(pointerPosAdjusted);
        };

        Hud.onPointerUp = function (pointerPosUnadjusted) {
            for (let i = 0; i < Hud.buttonManagers.length; ++i)
                Hud.buttonManagers[i].onPointerUp(pointerPosUnadjusted);
            Aimer.onPointerUp();
            return false;
        };

        Hud.onPointerDrag = function (pointerPos) {
            for (let i = 0; i < Hud.buttonManagers.length; ++i)
                if (Hud.buttonManagers[i].onPointerDrag(pointerPos)) return true;
            return Aimer.onPointerDrag(pointerPos);
        };

        Hud.onButtonClicked = function (action) {
            switch (action) {
                case 'jump':
                    WorldNetwork.changeMoveType('JUMP');
                    break;
                case 'throw':
                    WorldNetwork.changeMoveType('THROW');
                    break;
                case 'skip':
                    WorldNetwork.changeMoveType('SKIP');
                    break;
                case 'next_player':
                    WorldNetwork.cycleToNextAllowedTurnPlayer();
                    break;
                case 'add_player':
                    Hud.userActions.addLocalPlayer();
                    break;
                case 'toggle-ready':
                    Hud.userActions.toggleReady();
                    Hud.pregameButtons.setVisible(false);
                    break;
            }
        };

        Hud.moveButtonPressed = function (moveType) {
            switch (moveType) {
                case 'JUMP':
                case 'THROW':
                case 'SKIP':
                    WorldNetwork.changeMoveType(moveType);
            }
        };

        Hud.drawTurnPlayerIndicator = function () {
            if (WorldNetwork.getTurnPlayer()) {
                Hud.gameGraphics.setAlpha(0.8);
                Hud.gameGraphics.lineStyle(2, 0xDDDA5F, 1.0);
                let rad = 45;
                let turnPlayer = WorldNetwork.getTurnPlayer();
                Hud.gameGraphics.strokeCircle(turnPlayer.x + turnPlayer.halfWidth, turnPlayer.y + turnPlayer.halfHeight, 45);
                Hud.gameGraphics.setAlpha(1);
            }
        };

        Hud.worldPositionToWindowPosition = function (position) {
            let camera = Hud.camera;
            return {x: camera.zoom * (position.x - camera.scrollX), y: camera.zoom * (position.y - camera.scrollY)};
        };

        Hud.displayPlayerNames = function () {
            // adds text objects to array of players and updates them
            WorldNetwork.getPlayersInfo().forEach(function (playerInfo) {
                let player = playerInfo.player;
                if (!player.alive) return;
                if (!player.text) {
                    player.text = Hud.gameScene.add.text(0, 0, playerInfo.nickname, {
                        font: 'bold 20pt Arial',
                        fill: "#000000"
                    });
                    player.text.angle = 9;
                    player.text.depth = 25;
                }

                player.text.setPosition(player.x + player.halfWidth - player.text.width / 2, player.y + player.height + 10);
            });
        };

        Hud.displayPlayerDisconnected = function () {
            WorldNetwork.getPlayersInfo().forEach(function (playerInfo) {
                let player = playerInfo.player;
                if (!player.disconnected) return;
                if (!player.dcText) {
                    player.dcText = Hud.gameScene.add.text(0, 0, 'DC', {font: 'bold 16pt Arial', fill: "#DD0000"});
                }

                player.dcText.setPosition(player.x + player.halfWidth - player.dcText.width / 2, player.y - 24);
            });
        };

        Hud.displayTurnsLeftForCurrentPlayer = function () {
            let turnsLeft = GameStatus.getTurnsLeftForCurrentPlayer();

            let strokeWidth = 4;
            Hud.hudGraphics.lineStyle(strokeWidth, 0xFFFF00, 1.0);
            Hud.hudGraphics.fillStyle(0xFF0000, 1.0);

            let radius = 16;
            Hud.hudGraphics.strokeCircle(8 + radius, 8 + radius, radius);
            Hud.hudGraphics.strokeCircle(8 + (radius * 3) + 8, 8 + radius, radius);

            for (let i = 0; i < turnsLeft; ++i)
                Hud.hudGraphics.fillCircle(8 + radius + (radius * 2 * i) + 8 * i, 8 + radius, radius - 2);
        };

        var pointsText;
        Hud.displayClientPoints = function () {
            /* Slow */
            let clientsInfo = GameStatus.getClientsInfo();
            if (!pointsText)
                pointsText = Hud.hudScene.add.text(GlobalInfo.WINDOW_WIDTH, GlobalInfo.WINDOW_HEIGHT, 'Points go here', {
                    font: "16px Consolas",
                    fill: "#FFFFFF"
                });

            pointsText.text = "-Points-\n";
            for (var i = 0; i < clientsInfo.length; ++i) {
                pointsText.text += clientsInfo[i].nickname + ": " + clientsInfo[i].points;
                if (i < clientsInfo.length - 1) pointsText.text += '\n';
            }

            pointsText.x = GlobalInfo.WINDOW_WIDTH - pointsText.width - 8;
            pointsText.y = GlobalInfo.WINDOW_HEIGHT - pointsText.height - 8;
        };

        var livesText;
        Hud.displayClientLives = function () {
            let clientsInfo = GameStatus.getClientsInfo();
            if (!livesText)
                livesText = Hud.hudScene.add.text(GlobalInfo.WINDOW_WIDTH, GlobalInfo.WINDOW_HEIGHT, 'Lives go here', {
                    font: "16px Consolas",
                    fill: "#000000"
                });

            livesText.text = "-Lives-\n";
            for (var i = 0; i < clientsInfo.length; ++i) {
                livesText.text += clientsInfo[i].nickname + ": " + clientsInfo[i].lives;
                if (i < clientsInfo.length - 1) livesText.text += '\n';
            }

            livesText.x = GlobalInfo.WINDOW_WIDTH - livesText.width - 8;
            livesText.y = GlobalInfo.WINDOW_HEIGHT - livesText.height - 8;
        };

        Hud.drawEndScreen = function () {
            if (GameStatus.gameOver) {
                EndScreen.draw(Hud.hudScene, Hud.hudGraphics);
            }
        };

        Hud.onWorldEvent = function(eventName, args) {
            switch (eventName) {
                case 'POINTS_ADDED':
                    Hud.addPointsText(args[0], args[1]);
                    break;
            }
        };

        Hud.onWorldNetworkEvent = function (eventName, args) {
            switch (eventName) {
                case 'DECIDING_PLAYER':
                    Hud.moveButtons.showNextPlayerButton(true);
                    break;
                case 'PERFORMING_MOVE':
                    Hud.moveButtons.showNextPlayerButton(false);
                    break;
                case 'CHANGED_MOVE_TYPE':
                    Hud.moveButtons.showNextPlayerButton(false);
                    Hud.moveButtons.showMoveButtons(false);
                    break;
                case 'PLAYER_SELECTED':
                    if (WorldNetwork.getTurnPlayerMove().moveType !== 'RESPAWN')
                        Hud.moveButtons.showMoveButtons(true);
                    break;
            }
        };

        Hud.manageButtons = function () {
            if (Aimer.waitingForRelease) Hud.moveButtons.showNextPlayerButton(false);
            if (Hud.moveButtons.turnButtonsVisible) {
                let pos = {
                    x: WorldNetwork.getTurnPlayer().x + WorldNetwork.getTurnPlayer().width / 2,
                    y: WorldNetwork.getTurnPlayer().y + WorldNetwork.getTurnPlayer().height / 2
                };
                let turnPlayerPos = Hud.worldPositionToWindowPosition(pos);
                Hud.moveButtons.setButtonPositions(turnPlayerPos);
                Hud.moveButtons.update(Hud.pointerPos);
            }
            Hud.pregameButtons.setButtonPositions();
        };

        Hud.addPointsText = function(item, numPoints) {
            let textSize = 32;
            let sign = numPoints < 0 ? '' : '+';
            let text = Hud.gameScene.add.text(0, 0, sign + numPoints.toString(), {
                font: `bold ${textSize}pt 'World of Water'`,
                fill: "#000000"
            });
            Hud.setPointsTextPosition(item, text);
            text.depth = 30;
            text.angle = 13;

            Hud.pointsTexts.push({text: text, item: item, ticks: 0});
        };

        Hud.updatePointsTexts = function() {
            for (let i = Hud.pointsTexts.length-1; i >= 0; --i) {
                let p = Hud.pointsTexts[i];
                let item = p.item;
                if (p.ticks++ >= 90 || !item) {
                    p.text.destroy();
                    Hud.pointsTexts.splice(i, 1);
                    continue;
                }
                Hud.setPointsTextPosition(item, p.text);
            }
        };

        Hud.setPointsTextPosition = function(item, text) {
            text.setPosition(item.x + item.width, item.y+item.halfHeight-12);
        };

        /* ********************** D ************************/
        /* ********************** E ************************/
        /* ********************** B ************************/
        /* ********************** U ************************/
        /* ********************** G ************************/
        Hud.drawDebug = function () {
            Hud.showCountdown();
            //Hud.showInterestingEdgesDebug();
            //Hud.drawFightOrThrowDebug();
            //Hud.showBoundingBoxDebug();
            Hud.drawExplosions();
            Hud.drawLasers();
            Hud.drawUncalmSquareDebug();
            // Hud.drawTileQuadsDebug();
            Hud.highlightTilesInQuadsDebug();
            Hud.highlightTilesContainingItems();
            Hud.connectHolderParentDebug();
            // Hud.drawItemIds();
            Hud.showDebugInfo();
            if (GlobalInfo.NEED_SAFETY) Hud.safetySquare();
            if (GlobalInfo.PAY_ATTENTION) Hud.attentionSquare();
        };
        var countdownText;

        Hud.showCountdown = function () {
            if (!countdownText) {
                countdownText = Hud.hudScene.add.text(GlobalInfo.WINDOW_WIDTH / 2, GlobalInfo.WINDOW_HEIGHT - 56, '', {
                    font: "20px Consolas",
                    fill: "#ffff00"
                });
            }
            let info = GameStatus.getCountdownInfo();

            if (Date.now() < info.endTime) {
                countdownText.text = 'Time left: ' + Math.floor((info.endTime - Date.now()) / 1000);
                countdownText.x = GlobalInfo.WINDOW_WIDTH / 2 - countdownText.width / 2;
                countdownText.y = GlobalInfo.WINDOW_HEIGHT - countdownText.height - 4;
            } else {
                countdownText.text = '';
            }
        };
        var debugText;

        Hud.showDebugInfo = function () {
            if (!debugText) {
                debugText = Hud.hudScene.add.text(8, GlobalInfo.WINDOW_HEIGHT - 56, 'debug text', {
                    font: "16px Consolas",
                    fill: "#ffffff"
                });
            }
            debugText.text = GlobalInfo.LOCAL_GAME ? "Offline game\n" : 'Remote game\n';
            debugText.text += 'Room name: ' + GlobalInfo.ROOM_NAME + '\n';
            debugText.text += 'Map name: ' + GlobalInfo.MAP_NAME + '\n';
            debugText.y = GlobalInfo.WINDOW_HEIGHT - 56;
        };
        var textThing;

        Hud.drawFightOrThrowDebug = function () {
            if (!textThing) {
                textThing = Hud.hudScene.add.text(10, 10, 'jump', {font: "30px Arial Black", fill: "#fff"});
                textThing.setScrollFactor(0);
            }
            let moveText = WorldNetwork.getTurnPlayerMove().moveType === '' ? 'CHEWS MOVE' : WorldNetwork.getTurnPlayerMove().moveType
            textThing.setText(moveText);
        };
        Hud.drawUncalmSquareDebug = function () {

            if (WorldNetwork.arenaIsCompletelyCalm()) {
                Hud.hudGraphics.setAlpha(0.5);
                Hud.hudGraphics.fillStyle(0x4321AB, 1.0);
                Hud.hudGraphics.fillCircle(GlobalInfo.WINDOW_WIDTH - 14, 14, 14);
                Hud.hudGraphics.setAlpha(1);
            }
        };
        Hud.showInterestingEdgesDebug = function () {

            Hud.gameGraphics.lineStyle(2, 0x000000, 1);
            for (var i = 0; i < World.tiles.length; ++i) {
                let tile = World.tiles[i];
                if (!tile) continue;
                if (tile.destroyed) continue;
                Hud.gameGraphics.beginPath();
                if (tile.interesting.left) {
                    Hud.gameGraphics.moveTo(tile.left, tile.bottom);
                    Hud.gameGraphics.lineTo(tile.left, tile.top);
                }
                if (tile.interesting.right) {
                    Hud.gameGraphics.moveTo(tile.right, tile.bottom);
                    Hud.gameGraphics.lineTo(tile.right, tile.top);
                }
                if (tile.interesting.top) {
                    Hud.gameGraphics.moveTo(tile.left, tile.top);
                    Hud.gameGraphics.lineTo(tile.right, tile.top);
                }
                if (tile.interesting.bottom) {
                    Hud.gameGraphics.moveTo(tile.left, tile.bottom);
                    Hud.gameGraphics.lineTo(tile.right, tile.bottom);
                }
                Hud.gameGraphics.closePath();
                Hud.gameGraphics.strokePath();
            }
        };
        Hud.drawTileQuadsDebug = function () {

            if (!World.quads) return;
            let quadSize = World.quads.quadSize;
            for (let i = 0; i < World.quads.length; ++i) {
                let quad = World.quads[i];
                Hud.gameGraphics.lineStyle(4, 0xFF0000, 1);
                Hud.gameGraphics.strokeRect(quad.x, quad.y, quadSize, quadSize);
            }
        };

        Hud.highlightTilesInQuadsDebug = function () {
            if (!World.quads) return;
            for (let i = 0; i < World.quads.length; ++i) {
                let quad = World.quads[i];
                for (let k = 0; k < quad.length; ++k) {
                    let tile = quad[k];
                    Hud.gameGraphics.fillStyle(0xFFFF00, 0.25);
                    Hud.gameGraphics.fillRect(tile.x, tile.y, tile.width, tile.height);
                }
            }
        };

        Hud.highlightTilesContainingItems = function() {
            for (let i = 0; i < World.tiles.length; ++i) {
                let tile = World.tiles[i];
                if (tile.items.length === 0) continue;
                let alpha = Math.min(1, 0.2 * tile.items.length);
                Hud.gameGraphics.fillStyle(0x0000FF, alpha);
                Hud.gameGraphics.fillRect(tile.x, tile.y, tile.width, tile.height);
            }
        };

        Hud.showBoundingBoxDebug = function () {

            for (var i = 0; i < World.items.length; ++i) {
                let item = World.items[i];
                Hud.gameGraphics.lineStyle(1, 0x0000FF, 1);
                Hud.gameGraphics.strokeRect(item.x, item.y, item.width, item.height);
                if (item.justBecameTangible) {
                    Hud.gameGraphics.fillStyle(0x00FFFF, 0.5);
                    let box = Physics.getSmallCollisionBox(item, 0.85);
                    Hud.gameGraphics.fillRect(box.x, box.y, box.width, box.height);
                }
            }
        };
        Hud.drawExplosions = function () {

            for (var i = 0; i < World.explosions.length; ++i) {
                let ex = World.explosions[i];
                Hud.gameGraphics.fillStyle(0x39FF14, 0.6);
                Hud.gameGraphics.fillCircle(ex.x, ex.y, ex.radius);
            }
        };
        Hud.drawItemIds = function () {
            for (let i = 0; i < World.items.length; ++i) {
                let item = World.items[i];
                if (!item.idText) {
                    item.idText = Hud.gameScene.add.text(item.x, item.y, item.id, {
                        font: "40px Consolas",
                        fill: "#00FFFF"
                    });
                    item.idText.depth = 100;
                }
                item.idText.setPosition(item.x + item.width / 2 - item.idText.width / 2, item.y + item.height / 2 - item.idText.height / 2);
            }
        };

        Hud.drawLasers = function () {
            for (var i = 0; i < World.lasers.length; ++i) {
                let laser = World.lasers[i];
                Hud.gameGraphics.lineStyle(laser.width, 0xFF0000, 1);
                let endPoint;
                if (laser.pointingUp()) {
                    endPoint = {x: laser.x, y: laser.y - 5000};
                } else if (laser.pointingDown()) {
                    endPoint = {x: laser.x, y: laser.y + 5000};
                }
                Hud.gameGraphics.lineBetween(laser.x, laser.y, endPoint.x, endPoint.y);
            }
        };

        Hud.connectHolderParentDebug = function() {
            for (var i = 0; i < World.items.length; ++i) {
                let item = World.items[i];

                for (let i = 0; i < item.itemsStandingOn.length; ++i) {
                    let other = item.itemsStandingOn[i];
                    Hud.gameGraphics.beginPath();
                    Hud.gameGraphics.lineStyle(3, 0xFF0000, 1);
                    Hud.gameGraphics.moveTo(item.x+item.width*0.75, item.y+item.height*0.75);
                    Hud.gameGraphics.lineTo(other.x+other.width*0.75, other.y+other.height*0.25);
                    Hud.gameGraphics.closePath();
                    Hud.gameGraphics.strokePath();
                }
                for (let i = 0; i < item.itemsOnHead.length; ++i) {
                    let other = item.itemsOnHead[i];
                    Hud.gameGraphics.beginPath();
                    Hud.gameGraphics.lineStyle(3, 0xFFFF00, 1);
                    Hud.gameGraphics.moveTo(item.x+item.width*0.25, item.y+item.height*0.25);
                    Hud.gameGraphics.lineTo(other.x+other.width*0.25, other.y+other.height*0.75);
                    Hud.gameGraphics.closePath();
                    Hud.gameGraphics.strokePath();
                }
            }
        };

        Hud.safetySquare = function () {
            Hud.hudGraphics.fillStyle(0x000000, 0.85);
            Hud.hudGraphics.fillRect(0, 0, GlobalInfo.WINDOW_WIDTH, GlobalInfo.WINDOW_HEIGHT);
        };

        Hud.attentionAlpha = 0;
        Hud.attentionSquare = function () {
            Hud.hudGraphics.fillStyle(0x000000, Math.min(1, Hud.attentionAlpha += 0.0005));
            Hud.hudGraphics.fillRect(0, 0, GlobalInfo.WINDOW_WIDTH, GlobalInfo.WINDOW_HEIGHT);
        };

        return Hud;
    });