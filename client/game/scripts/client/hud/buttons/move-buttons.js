define(['buttons/button-manager', 'buttons/rect-button', 'buttons/round-button'], function(ButtonManager, RectButton, RoundButton) {
class MoveButtons extends ButtonManager {
  constructor(scene, onPressCallback) {
      super(scene);
      this.create(onPressCallback);
  }

  create(onPressed) {
    this.jumpButton = this.addNewRoundButton('button-jump', 0.5, () => {onPressed('jump')});
    this.throwButton = this.addNewRoundButton('button-throw', 0.5, () => {onPressed('throw')});
    this.skipButton = this.addNewRoundButton('button-skip', 0.5, () => {onPressed('skip')});
    this.nextButton = this.addNewRectButton('red-arrow', 0.5, () => {onPressed('next_player')});

    for (let i = 0; i < this.buttons.length; ++i) {
        let button = this.buttons[i];
        button.registerEvent('pointerover', () => {button.setAlpha(1); });
        button.registerEvent('pointerout', () => {button.setAlpha(button.initialAlpha)});
    }

    this.turnButtonsVisible = false;
  }

  setButtonPositions(startPosition) {
    this.jumpButton.setPosition(startPosition.x - 100, startPosition.y - 100);
    this.throwButton.setPosition(startPosition.x, startPosition.y - 150);
    this.skipButton.setPosition(startPosition.x + 100, startPosition.y - 100);
    this.nextButton.setPosition(startPosition.x, startPosition.y + 32);
  }

  showMoveButtons(show) {
      this.jumpButton.setVisible(show);
      this.throwButton.setVisible(show);
      this.skipButton.setVisible(show);
      this.turnButtonsVisible = show;
  }

  showNextPlayerButton(show) {
      this.nextButton.setVisible(show);
  }

}

return MoveButtons;
});