define(['buttons/button-manager', 'buttons/rect-button', 'shared/global-info'], function(ButtonManager, RectButton, GlobalInfo) {
    class PreGameButtons extends ButtonManager {
        constructor(scene, onPressCallback) {
            super(scene);
            this.addPlayerButton = this.addNewRectButton('add-player', 0.5, () => {onPressCallback('add_player')});
            this.addPlayerButton.setSize(64, 64);
            this.addPlayerButton.setDepth(39);

            this.startButton = this.addNewRectButton('toggle-ready', 0.5, () => {onPressCallback('toggle-ready')});
            this.startButton.setSize(64, 64);
            this.startButton.setDepth(39);
        }

        setButtonPositions() {
            let button = this.addPlayerButton;
            button.setPosition(GlobalInfo.WINDOW_WIDTH - button.width/2, button.height/2);
            button = this.startButton;
            button.setPosition(GlobalInfo.WINDOW_WIDTH - button.width/2, button.height/2 + this.addPlayerButton.height);
        }
    }

    return PreGameButtons;
});