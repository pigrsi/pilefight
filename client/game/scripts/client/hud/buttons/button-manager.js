define(['util/util', 'buttons/button-base', 'buttons/rect-button', 'buttons/round-button'], function(Util, ButtonBase, RectButton, RoundButton) {
    class ButtonManager {
    constructor(scene) {
        this.scene = scene;
        this.buttons = [];
    }

    addNewRectButton(frame, alpha, action) {
        let button = new RectButton(this.scene, frame, alpha);
        button.setAction(action);
        this.buttons.push(button);
        return button;
    }

    addNewRoundButton(frame, alpha, action) {
        let button = new RoundButton(this.scene, frame, alpha);
        button.setAction(action);
        this.buttons.push(button);
        return button;
    }

    update(pointerPos) {
        if (!pointerPos) return;
        for (var i = 0; i < this.buttons.length; ++i) {this.buttons[i].emit('pointerout');}

        let buttonCollide = this.checkButtonsCollide(pointerPos);
        if (buttonCollide) buttonCollide.emit('pointerover');
    }

    onPointerUp(pointerPos) {
        let buttonCollided = this.checkButtonsCollide(pointerPos);
        if (buttonCollided)
            buttonCollided.emit('pointeruptouching');

        for (var i = 0; i < this.buttons.length; ++i) {this.buttons[i].emit('pointerupnottouching');}
    }

    onPointerDown(pointerPos) {
        let buttonCollided = this.checkButtonsCollide(pointerPos);
        if (buttonCollided) {
            buttonCollided.emit('pointerdown');
            return true;
        }

        return false;
    }

    onPointerDrag(pointerPos) {
        for (var i = 0; i < this.buttons.length; ++i) {if (this.buttons[i].isBeingHeldDown()) return true;}
        return false;
    }

    checkButtonsCollide(pointerPos) {
        for (let i = 0; i < this.buttons.length; ++i) {
            if (this.buttons[i].checkCollide(pointerPos)) return this.buttons[i];
        }
        return false;
    }

    setVisible(visible) {
        for (let i = 0; i < this.buttons.length; ++i)
            this.buttons[i].setVisible(visible);
    }
}

return ButtonManager;
});