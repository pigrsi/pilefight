define([], function() {
   class ButtonBase {
       constructor(scene, frame, alpha) {
           let image = scene.add.image(0, 0, frame);
           this.x = 0;
           this.y = 0;
           this.initialAlpha = alpha;
           this.width = image.displayWidth;
           this.height = image.displayHeight;
           this.image = image;

           this.setAlpha(alpha);
           this.setVisible(false);
           this.setDefaultEvents();
       }

       setDefaultEvents() {
           let image = this.image;
           let this_ = this;
           this.registerEvent('pointerdown', () => { this_.heldDown = true; });
           this.registerEvent('pointerupnottouching', () => {
               this_.heldDown = false;
           });
           this.registerEvent('pointeruptouching', () => {
               if (this_.heldDown) {
                   this_.heldDown = false;
                   if (image.visible && this_.action)
                       this_.action.call(this_, this_.args);
               }
           });
       }

       emit(event) {
           this.image.emit(event);
       }

       registerEvent(eventName, callback) {
            this.image.on(eventName, callback);
       }

       setAction(action, args) {
            this.action = action;
            this.args = args;
       }

       setVisible(visible) {
            this.image.visible = visible;
            this.visible = visible;
       }

       setAlpha(alpha) {
           this.image.alpha = alpha;
           this.alpha = alpha;
       }

       setPosition(x, y) {
           this.image.setPosition(x, y);
           this.x = x;
           this.y = y;
       }

       setSize(width, height) {
           this.image.displayWidth = width;
           this.image.displayHeight = height;
           this.width = width;
           this.height = height;
       }

       setDepth(depth) {
           this.depth = depth;
           this.image.depth = depth;
       }

       getDepth() {
           return this.image.depth;
       }

       getAlpha() {return this.alpha;}

       isVisible() {return this.visible;}

       isBeingHeldDown() {return this.heldDown;}

       checkCollide(pointerPos) {
           return false;
       }
   }

   return ButtonBase;
});