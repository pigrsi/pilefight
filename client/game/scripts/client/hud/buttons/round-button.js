define(['buttons/button-base', 'util/util'], function(ButtonBase, Util) {
    class RoundButton extends ButtonBase {
        constructor(scene, frame, alpha) {
            super(scene, frame, alpha);
            this.radius = this.width / 2;
        }

        checkCollide(pointerPos) {
            if (this.isVisible() && Util.pointInCircle({x: this.image.x, y: this.image.y, radius: this.radius}, pointerPos.x, pointerPos.y))
                return this;
        }
    }

    return RoundButton;
});