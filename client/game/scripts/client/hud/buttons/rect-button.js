define(['buttons/button-base', 'util/util'], function(ButtonBase, Util) {
    class RectButton extends ButtonBase {
        checkCollide(pointerPos) {
            if (this.isVisible() && Util.pointInRect({x: this.x-this.width/2, y: this.y-this.height/2, width: this.width, height: this.height}, pointerPos.x, pointerPos.y)) {
                return this;
            }
            return null;
        }
    }

    return RectButton;
});