define(['client/game-status', 'shared/global-info'], function(GameStatus, GlobalInfo) {
var EndScreen = {};
EndScreen.screenDarkened = false;
EndScreen.screenTintAlpha = 0;
EndScreen.tweenStarted = false;

EndScreen.draw = function(scene, graphics) {
    if (!EndScreen.screenDarkened) EndScreen.darkenScreen();
    
    graphics.fillStyle(0x000000, EndScreen.screenTintAlpha);
    graphics.fillRect(0, 0, GlobalInfo.WINDOW_WIDTH, GlobalInfo.WINDOW_HEIGHT);
    
    if (EndScreen.screenDarkened) EndScreen.showRankings(scene);
};

EndScreen.darkenScreen = function() {
    if (!EndScreen.tweenStarted) {
        EndScreen.tweenStarted = true;
        scene.tweens.add({
            targets: EndScreen,
            screenTintAlpha: 0.65,
            duration: 1000,
            delay: 500,
        });
        EndScreen.nothing = 0;
        scene.tweens.add({
            targets: EndScreen,
            nothing: 10,
            duration: 2000,
            onComplete: function() {EndScreen.screenDarkened = true;},
        });
    }
};

var flavorText = {top: null, bottom: null};
var winnerText = {rank: null, name: null};
var losersTexts = [];
var loserTextsTotalHeight = 0;
EndScreen.showRankings = function(scene) {
    if (!flavorText.top) EndScreen.createTextObjects(scene);
   
    let ceiling = 0;
    flavorText.top.setPosition(GlobalInfo.WINDOW_WIDTH/2 - flavorText.top.width/2, 8);
    ceiling = flavorText.top.y + flavorText.top.height;
    
    flavorText.bottom.setPosition(GlobalInfo.WINDOW_WIDTH/2 - flavorText.bottom.width/2, ceiling);
    ceiling = flavorText.bottom.y + flavorText.bottom.height;
    
    let rankGap = 16;
    winnerText.rank.x = GlobalInfo.WINDOW_WIDTH/2 - (winnerText.rank.width/2 + rankGap + winnerText.name.width/2);
    winnerText.rank.y = ceiling + 32;
    winnerText.name.x = winnerText.rank.x + winnerText.rank.width + rankGap;
    winnerText.name.y = winnerText.rank.y;
    ceiling = winnerText.name.y + winnerText.name.height;
    
    ceiling = Math.max(ceiling, (GlobalInfo.WINDOW_HEIGHT-16) - (loserTextsTotalHeight) - 8);
    for (var i = 0; i < losersTexts.length; ++i) {
        let rank = losersTexts[i].rank;
        let name = losersTexts[i].name;
        
        rank.x = GlobalInfo.WINDOW_WIDTH/2 - (rank.width/2 + rankGap + name.width/2);
        rank.y = ceiling + 8;
        name.x = rank.x + rank.width + rankGap;
        name.y = rank.y;
        ceiling = name.y + name.height + 8;
    }
};

EndScreen.createTextObjects = function(scene) {
    let fontNames = 'World of Water, Impact';
    flavorText.top = Hud.hudScene.add.text(0, 0, 'Game over!', {font: "40px " + fontNames, fill: "#0000ff" });
    flavorText.bottom = Hud.hudScene.add.text(0, 0, 'GGs in my opinions.', {font: "40px " + fontNames, fill: "#0000ff" });
    if (!GlobalInfo.NEED_SAFETY) {flavorText.top.depth = 100; flavorText.bottom.depth = 100;}
    
    let rankings = GameStatus.getRankingsInfo();
    winnerText.rank = Hud.hudScene.add.text(0, 0, 'Winner:', {font: "65px " + fontNames, fill: "#ff5400" });
    winnerText.name = Hud.hudScene.add.text(0, 0, rankings[0], {font: "65px " + fontNames, fill: "#FF00D8" });
    if (!GlobalInfo.NEED_SAFETY) {winnerText.rank.depth = 100; winnerText.name.depth = 100;}

    loserTextsTotalHeight = 0;
    for (var i = 1; i < rankings.length; ++i) {
        let size = Math.max(40 - (i * 5), 5);
        let rank = Hud.hudScene.add.text(0, 0, '#' + (i + 1), {font: size + "px " + fontNames, fill: "#873300"});
        let name = Hud.hudScene.add.text(0, 0, rankings[i], {font: size + "px " + fontNames, fill: "#82C407"});
        if (!GlobalInfo.NEED_SAFETY) {rank.depth = 100; name.depth = 100;}
        losersTexts.push({rank: rank, name: name});
        
        loserTextsTotalHeight += rank.height + 8;
    }
};

return EndScreen;
});