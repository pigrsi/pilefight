// Receives input and passes it on
define(['client/world-viewer', 'hud/hud'], function(WorldViewer, Hud) {
Input = {};
Input.pointerPrev = {x: 0, y: 0};
Input.pointerDelta = {x: 0, y: 0};
Input.pointerCameraAdjusted = {x: 0, y: 0};

Input.init = function(scene) {
    // scene.input.on('pointerdown', Input.onPointerDown);
    // scene.input.on('pointerup', Input.onPointerUp);
    Input.setKeyboardEvents();
    window.addEventListener('mouseup', Input.onWindowMouseUp, false);
    window.addEventListener('wheel', Input.onWindowMouseWheel, false);
}

Input.onWindowMouseUp = function() {
    Input.windowMouseUp = true;
}

Input.onWindowMouseWheel = function(wheelEvent) {
    let zoomType = wheelEvent.deltaY < 0 ? 'ZOOM_IN' : 'ZOOM_OUT';
    WorldViewer.cameraControlPressed(zoomType, 0, 0, 0, pointerX=Input.pointerCameraAdjusted.x, pointerY=Input.pointerCameraAdjusted.y);
    wheelEvent.preventDefault();
}

Input.update = function() {    
    //scene.input.keyboard.enabled = game.hasFocus;
    
    let pointer = scene.input.activePointer;
    let camera = WorldViewer.getCamera();
    Input.pointerDelta.x = Input.pointerPrev.x - pointer.x/camera.zoom;
    Input.pointerDelta.y = Input.pointerPrev.y - pointer.y/camera.zoom;
    let maxDelta = 30;
    if (Input.pointerDelta.x > maxDelta) Input.pointerDelta.x = maxDelta;
    if (Input.pointerDelta.x < -maxDelta) Input.pointerDelta.x = -maxDelta;
    if (Input.pointerDelta.y > maxDelta) Input.pointerDelta.y = maxDelta;
    if (Input.pointerDelta.y < -maxDelta) Input.pointerDelta.y = -maxDelta;
    Input.pointerPrev.x = pointer.x/camera.zoom;
    Input.pointerPrev.y = pointer.y/camera.zoom;
    
    Input.pointerCameraAdjusted.x = pointer.x/camera.zoom + camera.scrollX;
    Input.pointerCameraAdjusted.y = pointer.y/camera.zoom + camera.scrollY;

    Hud.updatePointer(scene.input.activePointer);
    WorldViewer.updatePointer(scene.input.activePointer);
    if (pointer.justDown) Input.onPointerDown();
    else if (pointer.justUp || Input.windowMouseUp) Input.onPointerUp();
    else if (pointer.isDown && !Input.windowMouseUp) Input.onPointerDrag();
    
    if (Input.cursorKeys) {
        if (Input.cursorKeys.up.isDown) WorldViewer.cameraControlPressed('SCROLL_KEY', 0, -30);
        if (Input.cursorKeys.down.isDown) WorldViewer.cameraControlPressed('SCROLL_KEY', 0, 30);
        if (Input.cursorKeys.left.isDown) WorldViewer.cameraControlPressed('SCROLL_KEY', -30, 0);
        if (Input.cursorKeys.right.isDown) WorldViewer.cameraControlPressed('SCROLL_KEY', 30, 0);
    }
}

Input.onPointerUp = function() {
    Hud.onPointerUp(scene.input.activePointer);
    WorldViewer.onPointerUp();
    return;
}

Input.onPointerDown = function() {
    Input.windowMouseUp = false;
    if (!Hud.onPointerDown(Input.pointerCameraAdjusted, scene.input.activePointer))
    if (!WorldViewer.onPointerDown(Input.pointerCameraAdjusted, scene.input.activePointer))
    return;
}

Input.onPointerDrag = function() {
    if (!Hud.onPointerDrag(Input.pointerCameraAdjusted))
    if (!WorldViewer.onPointerDrag(Input.pointerDelta.x, Input.pointerDelta.y))
    return;
}

var monkey;
Input.setKeyboardEvents = function() {
    
    Input.cursorKeys = scene.input.keyboard.createCursorKeys(); 
    scene.input.keyboard.removeKey('SPACE');
    scene.input.keyboard.removeKey('SHIFT');
    
    return;
    
    
    
    
    scene.input.keyboard.on('keydown_Z', function (event) {
        //PileFightArena.getTurnPlayerMove().moveType = 'JUMP';
        WorldViewer.moveButtonPressed('JUMP');
    });
    scene.input.keyboard.on('keydown_X', function (event) {
        //PileFightArena.getTurnPlayerMove().moveType = 'THROW';
        //PileFightArena.turnPlayer.prepareForThrow();
        WorldViewer.moveButtonPressed('THROW');
    });
    
    scene.input.keyboard.on('keydown_C', function (event) {
        // PileFightArena.getTurnPlayerMove().moveType = 'SKIP';
        // PileFightArena.getTurnPlayerMove().moveReady = true;
        WorldViewer.moveButtonPressed('SKIP');
    })
    // scene.input.keyboard.on('keydown_T', function (event) {
        // monkey.x = scene.input.activePointer.x - monkey.halfWidth;
        // monkey.y = scene.input.activePointer.y - monkey.halfHeight;
        // monkey.setVelocity(0);
    // });
    scene.input.keyboard.on('keydown_MINUS', function (event) {
        WorldViewer.cameraControlPressed('ZOOM_OUT');
    });
    scene.input.keyboard.on('keydown_PLUS', function (event) {
        WorldViewer.cameraControlPressed('ZOOM_IN');
    });
    
    return false;
}

return Input;
});