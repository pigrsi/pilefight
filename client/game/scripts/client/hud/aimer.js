define(['items/monkey', 'shared/global-info', 'drawing/draw-util'], function (Monkey, GlobalInfo, DrawUtil) {
    var Aimer = {
        active: false,
        graphics: null,
        startPointer: {x: 0, y: 0},
        endPosition: {x: 0, y: 0},
        maxAimLengthStandard: 125,
        maxAimLengthRespawn: 175,
        maxAimLength: this.maxAimLengthStandard,
        velocityMagnifierStandard: 0.13,
        velocityMagnifierRespawn: 0.13,
        velocityMagnifierThrow: 0.115,
        velocityMagnifier: this.velocityMagnifierStandard,
        waitingForRelease: false,
        aimingForThrow: false,
        aimingForRespawn: false,
        target: null,
        baseMass: new Monkey().mass,
        init: function (shotDeterminedCallback, graphics) {
            Aimer.graphics = graphics;
            Aimer.onShotDetermined = shotDeterminedCallback;
        }
    };

    Aimer.setStartPosition = function (x, y) {
        Aimer.startPointer.x = x;
        Aimer.startPointer.y = y;
    }

    Aimer.setEndPosition = function (x, y) {
        Aimer.endPosition.x = x;
        Aimer.endPosition.y = y;
    }

    Aimer.onPointerDown = function (pointerPos) {
        // Initial click
        if (!Aimer.active) return false;
        if (Aimer.pointerIsWithinRange(pointerPos)) {
            Aimer.waitingForRelease = true;
            Aimer.setEndPosition(pointerPos.x, pointerPos.y)
            return true;
        }
        return false;
    }

    Aimer.onPointerDrag = function (pointerPos) {
        // Only swallow input if active and mouse is within range
        if (!Aimer.active) return false;
        if (Aimer.waitingForRelease) {
            Aimer.setEndPosition(pointerPos.x, pointerPos.y);
            return true;
        }
        return false;
    }

    Aimer.onPointerUp = function () {
        if (!Aimer.active) return false;
        if (Aimer.waitingForRelease) {
            Aimer.waitingForRelease = false;
            Aimer.active = false;
            Aimer.onShotDetermined(Aimer.getAimVelocity());
            return true;
        }
        return false;
    }

    Aimer.reset = function () {
        Aimer.waitingForRelease = false;
        Aimer.active = false;
    }

    Aimer.pointerIsWithinRange = function (pointerPos) {
        let maxDistanceToAim = 24; /*Aimer.maxAimLength;*/
        let xDist = Math.abs(Aimer.startPointer.x - pointerPos.x);
        let yDist = Math.abs(Aimer.startPointer.y - pointerPos.y);
        return (xDist * xDist + yDist * yDist) <= maxDistanceToAim * maxDistanceToAim;
    }

    Aimer.getAimVelocity = function () {
        let start = Aimer.endPosition;
        let end = {x: Aimer.startPointer.x, y: Aimer.startPointer.y};
        let mag = Aimer.velocityMagnifier;
        let vex = {x: mag * (end.x - start.x), y: mag * (end.y - start.y)};
        if (Aimer.aimingForThrow)
            vex = Aimer.adjustThrowVelocityForMass(vex);

        return vex;
    }

    Aimer.adjustThrowVelocityForMass = function (vex) {
        let velocityModifier = 1;
        if (Aimer.target.mass < Aimer.baseMass) {
            velocityModifier = 1 + 0.25 * (Aimer.baseMass / Aimer.target.mass);
            vex.x *= velocityModifier;
            vex.y *= velocityModifier;
        }
        return vex;
    }

    Aimer.setEndPosition = function (endX, endY) {
        let start = {x: Aimer.startPointer.x, y: Aimer.startPointer.y};
        let startEndVector = {x: endX - start.x, y: endY - start.y};
        let magnitude = Math.sqrt(
            startEndVector.x * startEndVector.x + startEndVector.y * startEndVector.y);
        let unitVector = {x: startEndVector.x / magnitude, y: startEndVector.y / magnitude};

        let maxLen = Aimer.maxAimLength;
        Aimer.endPosition = magnitude > maxLen ? {
            x: start.x + unitVector.x * maxLen,
            y: start.y + unitVector.y * maxLen
        } : {x: endX, y: endY};
    }

    Aimer.setLaunchType = function (type, target) {
        Aimer.aimingForThrow = type === 'THROW';
        Aimer.aimingForRespawn = type === 'RESPAWN';
        Aimer.maxAimLength = Aimer.aimingForRespawn ? Aimer.maxAimLengthRespawn : Aimer.maxAimLengthStandard;
        Aimer.target = target;

        Aimer.velocityMagnifier = Aimer.velocityMagnifierStandard;
        if (Aimer.aimingForThrow) Aimer.velocityMagnifier = Aimer.velocityMagnifierThrow;
        else if (Aimer.aimingForRespawn) Aimer.velocityMagnifier = Aimer.velocityMagnifierRespawn;
    }

    Aimer.draw = function () {
        var graphics = Aimer.graphics;
        if (!Aimer.active) return;

        graphics.setAlpha(0.8);
        let color = 0xffffff;
        if (Aimer.aimingForThrow) color = 0xff0000;
        else if (Aimer.aimingForRespawn) color = 0x0000FF;
        graphics.fillStyle(color, 1);
        graphics.fillCircle(Aimer.endPosition.x, Aimer.endPosition.y, 12);

        Aimer.drawAimLine(color);
        if (!Aimer.aimingForRespawn && Aimer.waitingForRelease && !(Aimer.aimingForThrow && Aimer.target.preventShowTrajectory)) {
            //   Aimer.drawTrajectoryDots(color);
            Aimer.drawTrajectorySquares(color);
        }
        graphics.setAlpha(1);
    }

    Aimer.drawAimLine = function (color) {
        var graphics = Aimer.graphics;
        let endPosition = Aimer.endPosition;
        graphics.lineStyle(4, color, 1);
        graphics.beginPath();
        graphics.moveTo(Aimer.startPointer.x, Aimer.startPointer.y);
        graphics.lineTo(endPosition.x, endPosition.y);
        graphics.closePath();
        graphics.strokePath();
    }

    Aimer.drawTrajectoryDots = function (color) {
        var graphics = Aimer.graphics;
        let target = Aimer.target;
        let nub = {x: target.x, y: target.y};
        let gravity = target.gravity ? target.gravity : GlobalInfo.GRAVITY;
        let velocity = Aimer.getAimVelocity();

        velocity = {x: velocity.x, y: velocity.y};
        let numDots = Aimer.aimingForThrow ? 14 : 20;

        for (var i = 0; i < numDots; ++i) {
            graphics.fillStyle(color, 1);
            graphics.fillCircle(nub.x + target.width / 2, nub.y + target.height / 2, 4);

            graphics.fillStyle(0xaa00ff, 0.25);
            graphics.fillCircle(nub.x, nub.y, 4);
            graphics.fillStyle(0x8844ff, 0.25);
            graphics.fillCircle(nub.x + target.width, nub.y, 4);
            graphics.fillStyle(0x4488ff, 0.25);
            graphics.fillCircle(nub.x, nub.y + target.height, 4);
            graphics.fillStyle(0x00aaff, 0.25);
            graphics.fillCircle(nub.x + target.width, nub.y + target.height, 4);
            for (let k = 0; k < 3; ++k) {
                nub.x += velocity.x * 1;
                nub.y += velocity.y * 1;

                velocity.y += gravity * 1;

                // Check velocity doesn't go over max
                if (velocity.x > target.maxVelocity.x) velocity.x = target.maxVelocity.x;
                else if (velocity.x < -target.maxVelocity.x) velocity.x = -target.maxVelocity.x;
                if (velocity.y > target.maxVelocity.y) velocity.y = target.maxVelocity.y;
                else if (velocity.y < -target.maxVelocity.y) velocity.y = -target.maxVelocity.y;

            }
        }
    };

    Aimer.drawTrajectorySquares = function (color) {
        var graphics = Aimer.graphics;
        let target = Aimer.target;
        let nub = {x: target.x, y: target.y};
        let prevNub = {x: target.x, y: target.y};
        let gravity = target.gravity ? target.gravity : GlobalInfo.GRAVITY;
        let velocity = Aimer.getAimVelocity();

        velocity = {x: velocity.x, y: velocity.y};
        let numIterations = Aimer.aimingForThrow ? 14 : 20;
        let startingPoints = [{x: nub.x, y: nub.y},
            {x: nub.x + target.width, y: nub.y},
            {x: nub.x + target.width, y: nub.y + target.height},
            {x: nub.x, y: nub.y + target.height}];

        let originalAlpha = 0.65;
        let points;
        for (var i = 0; i < numIterations; ++i) {
            let alphaLess = (i/numIterations) * originalAlpha;
            graphics.fillStyle(color, originalAlpha - alphaLess);

            if (i === 0) {
                DrawUtil.drawFilledShape(graphics, startingPoints);
            } else {
                let points = [{x: nub.x, y: nub.y},
                    {x: nub.x + target.width, y: nub.y + target.height},
                    {x: prevNub.x + target.width, y: prevNub.y + target.height},
                    {x: prevNub.x, y: prevNub.y}];
                DrawUtil.drawFilledShape(graphics, points);

                points = [{x: nub.x + target.width, y: nub.y},
                    {x: nub.x, y: nub.y + target.height},
                    {x: prevNub.x, y: prevNub.y + target.height},
                    {x: prevNub.x + target.width, y: prevNub.y}];
                DrawUtil.drawFilledShape(graphics, points);
            }

            prevNub.x = nub.x; prevNub.y = nub.y;
            for (let k = 0; k < 3; ++k) {
                nub.x += velocity.x * 1;
                nub.y += velocity.y * 1;

                velocity.y += gravity * 1;

                // Check velocity doesn't go over max
                if (velocity.x > target.maxVelocity.x) velocity.x = target.maxVelocity.x;
                else if (velocity.x < -target.maxVelocity.x) velocity.x = -target.maxVelocity.x;
                if (velocity.y > target.maxVelocity.y) velocity.y = target.maxVelocity.y;
                else if (velocity.y < -target.maxVelocity.y) velocity.y = -target.maxVelocity.y;
            }
        }
    };
    return Aimer;
});