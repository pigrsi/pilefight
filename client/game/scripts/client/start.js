var scene;
var game;

define(['shared/world', 'client/load-assets', 'client/world-viewer', 'cgame/client-master', 'cgame/client-world', 'sgame/server-master', 'events/events', 'events/event-listener', 'events/external-listener',
'hud/input', 'hud/hud', 'sgame/points-game', 'sgame/lives-game', 'cgame/client-points', 'cgame/client-lives', 'shared/global-info', 'client/html-king', 'shared/world-network',
'shared/room-info', 'shared/config', 'client/user-actions'],
function(WorldClass, LoadAssets, WorldViewer, ClientMaster, ClientWorld, ServerMaster, Events, EventListener, ExternalListener, Input, Hud, PointsGame,
 LivesGame, ClientPoints, ClientLives, GlobalInfo, HtmlKing, WorldNetwork, RoomInfo, Config, UserActions){
var Start = {};
ExternalListener.logger = gameLogger;

Start.setupExternal = function(lobbyGameManager, gameId) {
	Start.initEvents(gameId);
	this.events.setEmitMessageExternalCallback(lobbyGameManager.emit.bind(lobbyGameManager));
	ExternalListener.addEventManager(this.events, gameId);
	this.eventListener.registerGameRunnerClient(lobbyGameManager);
};

Start.initEvents = function(gameId) {
	this.events = new Events(gameId);
	this.eventListener = new EventListener(this.events);
};

Start.start = function(localGame, gameMode, nickname, asSpectator=false, onGameComplete) {
	if (!this.events) Start.initEvents();
	Start.onGameComplete = onGameComplete;

	Start.World = new WorldClass();
    Start.World.init(60);
    setWindowSize();

    Start.localGame = localGame;
    GlobalInfo.GAME_MODE = gameMode;
    GlobalInfo.LOCAL_GAME = localGame;
    GlobalInfo.NEED_SAFETY = Config.safety;
    GlobalInfo.PAY_ATTENTION = Config.payAttention;

	Start.worldNetwork = new WorldNetwork(Start.World, gameLogger);
	if (Start.localGame) Start.serverMaster = new ServerMaster(this.events, Start.worldNetwork, gameLogger);

	let clientWorld = new ClientWorld(Start.worldNetwork);
	Start.clientMaster = new ClientMaster(this.events, clientWorld, Start.worldNetwork, nickname);
	Hud.setWorld(Start.World);
	Hud.setWorldNetwork(Start.worldNetwork);
	Start.userActions = new UserActions();
	Hud.setUserActions(Start.userActions);

	if (Start.localGame) this.eventListener.registerListenEventsServer(Start.serverMaster);
	this.eventListener.registerListenEventsClient(Start.clientMaster);

    if (Start.localGame) {
		let roomInfo = new RoomInfo('', false, 'POINTS', 1, 8, 99, 1, 31000, true);
        Start.serverGameMode = new PointsGame(Start.serverMaster, roomInfo, gameLogger);
	}
    Start.clientGameMode = new ClientPoints(Start.clientMaster, Start.userActions, asSpectator, Start.stop);

    scene = new Phaser.Scene('Scene');
    scene.preload = LoadAssets.loadImagesAndSounds;
    scene.create = create;
    scene.update = update;
    hudScene = new Phaser.Scene('HudScene');
    hudScene.create = Hud.init;
    //hudScene.update = Hud.draw;

    var Setup = {
        config: {
            type: Phaser.AUTO,
            width: GlobalInfo.WINDOW_WIDTH,
            height: GlobalInfo.WINDOW_HEIGHT,
            parent: 'game',
            //backgroundColor: '#f4ab39',
            // backgroundColor: '#ADD8E6',
            scene: [scene, hudScene],
            fps: {
                target: 60
            }
        }
    };

    game = new Phaser.Game(Setup.config);

    Start.started = true;
};

Start.stop = function() {
    Start.onGameComplete();
};

Start.resetGameMode = function(gameMode, nickname) {
    Start.clientMaster.reset(nickname);
    Start.clientGameMode = new ClientPoints(Start.clientMaster, Start.userActions, false, Start.stop);
};

Start.hasStarted = function() {
    return !!Start.started;
};

var create = function() {
    LoadAssets.loadAnimations(this);
    this.scene.launch('HudScene');
    WorldViewer.create.call(this, hudScene, Start.World, Start.worldNetwork);
    Input.init(scene);
    Hud.setGameScene(scene);
    HtmlKing.init();
};

var setWindowSize = function() {
    let subtractX = 128; let subtractY = 138;
    if (GlobalInfo.WINDOW_WIDTH !== window.innerWidth - subtractX || GlobalInfo.WINDOW_HEIGHT !== window.innerHeight - subtractY) {
        GlobalInfo.WINDOW_WIDTH = window.innerWidth - subtractX;
        GlobalInfo.WINDOW_HEIGHT = window.innerHeight - subtractY;
        if (game) game.resize(GlobalInfo.WINDOW_WIDTH, GlobalInfo.WINDOW_HEIGHT);
        WorldViewer.setCameraSize(GlobalInfo.WINDOW_WIDTH, GlobalInfo.WINDOW_HEIGHT);
		if (Hud.hudScene) Hud.hudScene.cameras.main.setSize(GlobalInfo.WINDOW_WIDTH, GlobalInfo.WINDOW_HEIGHT);
    }
};

var update = function(time, delta) {
    setWindowSize();
    if (Start.localGame) Start.serverGameMode.update();
    Start.clientGameMode.tick();
    WorldViewer.update.call(this, time, delta);
};
return Start;
});

let gameLogger = {
	debug(message) {console.log(message);},
	info(message) {console.log(message);},
	warn(message) {console.log(message);},
	error(message) {console.log(message);},
	fatal(message) {console.log(message);},
};