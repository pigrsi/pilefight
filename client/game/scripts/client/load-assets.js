define(['text!assets/images/animations.json'], function (animationsJSON) {
    LoadAssets = {};
    
    LoadAssets.loadImages = function(scene) {        
        // Buttons
        scene.load.image('button-jump', 'assets/images/button-jump.png');
        scene.load.image('button-throw', 'assets/images/button-throw.png');
        scene.load.image('button-skip', 'assets/images/button-skip.png');
        scene.load.image('red-arrow', 'assets/images/red-arrow.png');
        scene.load.image('add-player', 'assets/images/add-player-button.png');
        scene.load.image('toggle-ready', 'assets/images/windows-helper.png');

		// Images
		scene.load.image('ice-block', 'assets/images/ice-block.png');
		scene.load.image('destruction_deployed', 'assets/images/destruction_deployed.png');
		scene.load.image('orb-glow', 'assets/images/orb-glow.png');
		scene.load.image('grass', 'assets/images/grass.png');

        // Tilesets
        scene.load.image('green_square', 'assets/images/tilesets/green_square_big.png');
        scene.load.image('template', 'assets/images/tilesets/template.png');
        scene.load.image('grassy', 'assets/images/tilesets/grassy.png');
        scene.load.image('grassy-big', 'assets/images/tilesets/grassy-big.png');
        
        // Spritesheep
		LoadAssets.loadSpritesheets(scene);
		scene.load.spritesheet('mini-cam-border', 'assets/images/mini-cam-border.png', {frameWidth: 4, frameHeight: 4, spacing: 2});
    }
	
	LoadAssets.loadSounds = function(scene) {
		scene.load.audio('dingdong', 'assets/sounds/ding-dong.ogg');
	}
	
	LoadAssets.loadSpritesheets = function(scene) {
		let json = JSON.parse(animationsJSON);		
		for (var i = 0; i < json.length; ++i) {
			let info = json[i];
			info.frameWidth = parseInt(info.frameWidth);
			info.frameHeight = parseInt(info.frameHeight);
			scene.load.spritesheet(info.name, "assets/images/"+info.spritesheet, {frameWidth: info.frameWidth, frameHeight: info.frameHeight});
        }  
	};
    
    LoadAssets.loadAnimations = function(scene) {
        let json = JSON.parse(animationsJSON);
        for (var i = 0; i < json.length; ++i) {
            for (var k = 0; k < json[i].animations.length; ++k) {
                createAnim(json[i].name, json[i].animations[k], scene);
            }
        }        
    };
    
    function createAnim(spritesheetKey, anim, scene) {
		let frames = anim.frames;
		if (frames != undefined && frames != null)  {
			frames = frames.split(',');
			frames = scene.anims.generateFrameNumbers(spritesheetKey, {frames: frames});
		}
		else 
			frames = scene.anims.generateFrameNumbers(spritesheetKey, {start: anim.start, end: anim.end});
		
        let config = {
            key: spritesheetKey + '-' + anim.name,
            frames: frames
        }
        
        let keys = Object.keys(anim);
        for (var i = 0; i < keys.length; ++i) {
            let key = keys[i];
            if (key !== 'start' && key !== 'end' && key !== 'name' && key !== 'frames') {
                config[key] = anim[key];
            }
        }
        
        scene.anims.create(config);
    }
	
	LoadAssets.loadImagesAndSounds = function() {
		LoadAssets.loadImages(this);
		LoadAssets.loadSounds(this);
	}
    
    return LoadAssets;
});