// Starting point for client
requirejs.config({
    baseUrl: '',
    paths: {
        text: '/game/lib/text',
        shared: '/game/scripts/shared',
        client: '/game/scripts/client',
        entities: '/game/scripts/shared/entities',
        maps: '/game/assets/maps',
        drawing: '/game/scripts/client/drawing',
        util: '/game/scripts/shared/util',
        hud: '/game/scripts/client/hud',
        buttons: '/game/scripts/client/hud/buttons',
        cgame: '/game/scripts/client/cgame',
        events: '/game/scripts/shared/events',
        sgame: '/game/scripts/shared/sgame',
        items: '/game/scripts/shared/entities/items',
    }
});

requirejs(['client/lobby', 'client/start', 'events/external-listener', 'shared/config', 'client/lobby-game-manager', 'client/client-util'], function(Lobby, Start, ExternalListener, Config, LobbyGameManager, ClientUtil) {
    const BASE_PORT = Config.basePort;
	const LOCAL_GAME = Config.local;
    let gameId = ClientUtil.getGameId();
    let port = BASE_PORT + ClientUtil.getPortOffset(gameId);
	let enteredNickname = ClientUtil.getCookie('nickname');
	let nicknameElement = document.getElementById('setName');
	let lobbyElement = document.getElementById('lobby');
	let gameElement = document.getElementById('game');

	if (ClientUtil.showNicknameFormIfNecessary(enteredNickname)) {
		let nicknameSubmitButton = document.getElementById('nicknameSubmit');
		nicknameSubmitButton.onclick = function() {
			enteredNickname = document.getElementById('nicknameBox').value;
			ClientUtil.showElement(nicknameElement, false);
			setupLobbyGameManager();
		};
	} else {
		setupLobbyGameManager();
	}

	// Lobby start point
	function setupLobbyGameManager() {
		let lgm = new LobbyGameManager({nickname: enteredNickname, port: port, gameId: gameId, lobbyElement: lobbyElement, gameElement: gameElement, localGame: LOCAL_GAME});
		Start.setupExternal(lgm, gameId);
		lgm.init();
	}

	return;





	// const LOCAL_GAME = Config.local;
	// const BASE_PORT = Config.basePort;
	// let gameId;
	// let sock;
	// let nicknameElement = document.getElementById('setName');
	// let lobbyElement = document.getElementById('lobby');
	// let gameElement = document.getElementById('game');
	// document.getElementById('nicknameSubmit').onclick = function() {
	// 	nickname = document.getElementById('nicknameBox').value;
	// 	tryJoiningLobby();
	// };
	//
	// function emit(messageType, data, gameId) {
	// 	sock.emit(messageType, data);
	// }
	//
	// function startGame(asSpectator=false) {
	// 	if (!Start.hasStarted()) Start.start(LOCAL_GAME, 'POINTS', nickname, asSpectator, LOCAL_GAME ? function(){} : onGameComplete);
	// 	else Start.resetGameMode('POINTS', nickname);
	// }
	//
	// function showLobbyElements(isHost) {
	// 	Lobby.showLobbyElements(isHost);
	// }
	//
	// function onLobbyComplete(spectate=false) {
	// 	ClientUtil.showElement(lobbyElement, false);
	// 	ClientUtil.showElement(gameElement, true);
	// 	startGame(spectate);
	// }
	//
	// function onGameComplete() {
	// 	ClientUtil.showElement(gameElement, false);
	// 	ClientUtil.showElement(lobbyElement, true);
	// }
	//
	// function sendLobbySettings(lobbySettings) {
	// 	emit('START_PRIVATE', lobbySettings, gameId);
	// }
	//
    // function registerListen(messageType) {
    //     sock.on(messageType, function(data) {
    //         ExternalListener.messageReceived(messageType, data, gameId);
    //     });
    // }
	//
	// function connectToServer() {
	// 	let port = BASE_PORT + getPortOffset(gameId);
	// 	sock = io(`${location.protocol}//${window.location.hostname}:${port}/play`, {query: {token:gameId}});
	// 	sock = sock.connect();
	// 	setupServerEvents();
	// }
	//
	// function setupServerEvents() {
	// 	sock.on('connect', function() {
	// 		emit("CONNECTED", {nickname: nickname}, gameId);
	// 		if (isPrivateGame(gameId)) {
	// 			Lobby.init(sendLobbySettings, function() {onLobbyComplete(true)});
	// 			ClientUtil.showElement(lobbyElement, true);
	// 		} else {
	// 			ClientUtil.showElement(gameElement, true);
	// 			startGame();
	// 		}
	// 	});
	//
	// 	sock.on('error', function(error) {
	// 		joinError();
	// 	});
	//
	// 	registerListen('SIGNAL');
	// 	registerListen('LOBBY_COMPLETE');
	// }
	//
	// function tryJoiningLobby() {
	// 	if (nickname) {
	// 		ClientUtil.showElement(nicknameElement, false);
	// 		connectToServer();
	// 	} else {
	// 		ClientUtil.showElement(nicknameElement, true);
	// 	}
	// }
	//
	// // Set up
	// if (LOCAL_GAME) {
	// 	ClientUtil.showElement(gameElement, true);
	// 	Start.start(LOCAL_GAME, 'POINTS', getRandomName());
	// 	return;
	// }
	//
	// let nickname = getCookie('nickname');
	// gameId = getGameId();
    //
	// Lobby.showLobbyElements(false);
	//
	// if (isPrivateGame(gameId)) {
	// 	tryJoiningLobby();
	// } else {
	// 	connectToServer();
	// }
	//
	// // Set up external parts of game
	// let gameRunnerFunctions = {
	// 		onSignal: Lobby.onSignal.bind(Lobby),
	// 		onLobbyComplete: onLobbyComplete,
	// 		emit: emit
	// 	};
	// Start.setupExternal(gameRunnerFunctions, gameId);
});

let defaultNames = ['Huell', 'Gondy', 'Stan', 'Yugi', 'Storm', 'Loon', 'Carrie', 'Wanda', 'Rags', 'Bolegda', 'Davy', 'Jack', 'Fred', 'Linda', 'Josh', 'Obi', 'Koront', 'Kebab', 'Tog', 'Leo', 'Monkey', 'Cream', 'Homer', 'Mongolia', 'Lemeza', 'Tiphet', 'Cheeks', 'Frogs', 'McCloud', 'Fifi', 'Rafiki', 'Toffifee', 'Clicker', 'Tenter', 'Doug', 'Edmund', 'Tommy', 'Friend', 'Big', 'Griff', 'Kenny', 'Candy', 'Kick', 'Goofy', 'Guff', 'Donald', 'Bugs', 'Daffy', 'Dinglepop', 'Laura', 'Mundle', 'Wise', 'Frodo', 'Pimpin', 'Ax', 'Toad', 'Ostro', 'Gina', 'Slick', 'Pus', 'Dos', 'Doss', 'Funhent', 'Carton', 'Crush', 'Argon', 'Metro', 'Sylvia', 'Soccer', 'Curly', 'Turkey', 'Baclava', 'Pringle', 'Cubbage', 'Kubbage', 'Hammy', 'Jilda', 'Mindquaid', 'Jodie', 'Crabba', 'Tugga', 'Olivia', 'Kebora', 'Toggle', 'Crust', 'Cub', 'Cubbin', 'Cubbing', 'Cubba', 'Penguin', 'Wombat', 'Lava', 'Popopokki', 'Pubi', 'Alan', 'France', 'Jose', 'Becky'];