define(['drawing/item-animator'], function(ItemAnimator) {
class BombAnimator extends ItemAnimator {
    constructor(item, scene, viewer) {
        super(item, scene, viewer);
        this.injectCallIntoItem('explode');
    }

    explode() {
        this.viewer.suggestCameraFollow('EXPLOSION_SPAWNED', this.item);
    }
}

return BombAnimator;
});