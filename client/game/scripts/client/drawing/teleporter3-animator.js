define(['drawing/teleporter2-animator'], function(Teleporter2Animator) {
class Teleporter3Animator extends Teleporter2Animator {
    constructor(item, scene, viewer) {
        super(item, scene, viewer);
        this.image.setTint(0xFF00FF);
    }
	
	update() {
		super.update();
        this.image.setTint(0xFF00FF);
        this.checkOrbEffect();
	}
}

return Teleporter3Animator;
});