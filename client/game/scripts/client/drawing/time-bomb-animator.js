define(['drawing/item-animator'], function(ItemAnimator) {
class TimeBombAnimator extends ItemAnimator {
    constructor(item, scene, viewer) {
        super(item, scene, viewer);
        this.text = scene.add.text(item.x, item.y, item.turnCountdown, {font: 'bold 20pt Arial'});
        this.text.depth = 11;
        this.injectCallIntoItem('countdown');
        this.injectCallIntoItem('explode');
    }
    
    update() {
        super.update();
        if (this.text) {
            this.text.x = this.item.x + this.item.halfWidth - 8;
            this.text.y = this.item.y + this.item.halfHeight - 12;
        }
    }
    
    countdown() {
        if (!this.text) return;
        this.text.setText(this.item.turnCountdown);
    }
    
    explode() {
        if (this.text) {
            this.text.destroy();
            this.text = null;
            this.viewer.suggestCameraFollow('EXPLOSION_SPAWNED', this.item);
        }
    }
    
    destroy() {
        if (this.text) {
            this.text.destroy();
            this.text = null;
        }
    }
}

return TimeBombAnimator;
});