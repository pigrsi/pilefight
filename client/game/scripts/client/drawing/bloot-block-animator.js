define(['drawing/item-animator'], function(ItemAnimator) {
class BlootBlockAnimator extends ItemAnimator {
    constructor(item, scene, viewer) {
        super(item, scene, viewer);
        this.injectCallIntoItem('startBlootDrop');
        this.injectCallIntoItem('destroyTile');
        this.injectCallIntoItem('blootBounce');
        this.image.play(this.item.name + "-idle");
    }
    
    startBlootDrop() {
        this.viewer.suggestCameraFollow('BLOOT_DROP', this.item);
    }
	
	destroyTile() {
		this.viewer.shakeCamera(75);
	}
	
	blootBounce() {
		this.viewer.shakeCamera(50);
	}
}

return BlootBlockAnimator;
});