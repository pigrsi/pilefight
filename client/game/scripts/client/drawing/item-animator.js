define([], function() {
class ItemAnimator {
    constructor(item, scene, viewer) {
        this.item = item;
        this.originalSize = {width: item.width, height: item.height};
        this.scene = scene;
        this.viewer = viewer;
        this.image = scene.add.sprite(item.x, item.y);
		this.image.play(item.name + "-idle");
        // this.image.setOrigin(0);
		this.setScale();
		this.setPosition();
        this.injectCallIntoItem('onPlucked');
		this.injectCallIntoItem('launchAsAttack');
		this.injectCallIntoItem('setBeingForciblyMoved');
	}

    update() {
		if (this.image.isTinted) this.image.clearTint();
        this.checkRemoval();
        this.setFlip();
        this.setAlpha();
		if (!this.beingPlucked)
			this.setScale();
        this.setPosition();
		this.drawStatusEffects();
		this.checkOrbEffect();
    }
    
    checkRemoval() {
        if (this.item.readyForRemoval) {
            this.readyForRemoval = true;
            this.image.destroy();
        }
    }
    
    setPosition() {
        this.image.setPosition(this.item.x+this.item.width/2, this.item.y+this.item.height/2);
    }
    
    setFlip() {
        if (this.item.velocity.x !== 0)
            this.image.flipX = this.item.velocity.x < 0 ? true : false;
    }
    
    setAlpha() {
        if (this.item.disabled) this.image.alpha = 0;
        else if (!this.item.tangible) this.image.alpha = 0.4;
        else this.image.alpha = 1;
    }
    
    setScale() {
        if (this.item.width !== this.image.displayWidth)
           //this.image.scaleX = this.item.width / this.image.displayWidth;
			this.image.displayWidth = this.item.width;
        if (this.item.height !== this.image.displayHeight)
            //this.image.scaleY = this.item.height / this.image.displayHeight;
			this.image.displayHeight = this.item.height;
    }
    
    onPlucked() {
        let config = {duration: 300, ease: '', delay: 60, yoyo: true};
        this.viewer.addTween(this.image, ['scaleX', 'scaleY', 'depth'], [1.15, 1.15, this.image.depth+5], config, function() {this.beingPlucked = false}.bind(this));
		this.beingPlucked = true;
    }
    
    launchAsAttack() {
        this.viewer.suggestCameraFollow('ITEM_THROWN', this.item);
    }
    
    setBeingForciblyMoved() {
        this.viewer.suggestCameraFollow('FORCIBLY_MOVED', this.item);
    }
	
	drawStatusEffects() {
		let effects = this.item.statusEffects.effects;
		this.resetEffectImages();


		if (this.item.isStunned()) {
			this.rotateSpeed = this.item.velocity.x*2;
		} else {
			this.image.angle = 0;
			this.rotateSpeed = 0;
		}

		this.image.angle += this.rotateSpeed;
		
		for (let i = 0; i < effects.length; ++i) {
			switch (effects[i].name) {
				case 'Frozen':
					this.drawFrozen();
					break;
				case 'OnFire':
					this.drawOnFire();
					break;
				case 'Bread':
					this.drawBread();
					break;
				case 'Float':
					this.drawFloat();
					break;
				case 'Boosting':
					this.drawBoosting();
					break;
				case 'Shield':
					this.drawShield();
					break;
				case 'Zipping':
					this.drawZipping();
					break;
			}
		}
	}
	
	checkOrbEffect() {
		if (this.item.glowingFromOrb) {
			this.image.setTintFill(0xE4B73A);
		}
	}
	
	resetEffectImages() {
		if (this.frozenImage) this.frozenImage.visible = false;
		if (this.onFireImage) this.onFireImage.visible = false;
		if (this.breadImage) this.breadImage.visible = false;
		if (this.floatImage) this.floatImage.visible = false;
	}
	
	drawFrozen() {
		if (!this.frozenImage) {
			this.frozenImage = this.scene.add.image(0, 0, 'ice-block');
			this.frozenImage.depth = 14;
		}
		this.frozenImage.visible = true;
		this.frozenImage.setPosition(this.item.x+this.item.halfWidth, this.item.y+this.item.height-this.frozenImage.height/2);
	}
	
	drawOnFire() {
		if (!this.onFireImage) {
			this.onFireImage = this.scene.add.sprite(0, 0);
			this.onFireImage.play("Flame-inAir");
			this.onFireImage.depth = 14;
		}
		this.onFireImage.visible = true;
		this.onFireImage.setPosition(this.item.x+this.item.halfWidth, this.item.y+this.item.height/2-this.onFireImage.height/2);
	}
	
	drawBread() {
		if (!this.breadImage) {
			this.breadImage = this.scene.add.image(0, 0, 'Bread');
			this.breadImage.depth = 13;
		}
		this.breadImage.visible = true;
		this.breadImage.angle = this.image.angle;
		this.breadImage.setPosition(this.item.x+this.item.halfWidth, this.item.y+this.item.height-this.breadImage.height/2);
	}
	
	drawFloat() {
		//if (this.item.width > 48) return;
		if (!this.floatImage) {
			this.floatImage = this.scene.add.image(0, 0, 'Floatie', 1);
			this.floatImage.displayWidth = this.image.displayWidth;
			this.floatImage.displayHeight = this.image.displayHeight;
			this.floatImage.depth = 14;
			this.floatImage.setOrigin(0);
		}
		this.floatImage.visible = true;
		this.floatImage.setPosition(this.item.x-(this.image.displayWidth-this.item.width)/2, this.item.y);
	}

	drawShield() {
		this.image.setTint(0x00ffff);
	}

	drawBoosting() {
		this.image.setTint(0xFF0000);
	}

	drawZipping() {
    	let item = this.item;
    	let to = item.zippingTo;
		let graphics = this.viewer.getGameGraphics();
		graphics.lineStyle(2, 0xFFFFFF, 0.5);
		DrawUtil.drawLine(graphics, {x: item.x+item.halfWidth, y: item.y+item.halfHeight}, {x: to.x+this.item.halfWidth, y: to.y+this.item.halfHeight});
	}
    
    injectCallIntoItem(funcName) {
        let item = this.item;
        let anim = this;
        item[funcName] = (function() {
            var cached_function = item[funcName];
            return function() {
                let result = cached_function.apply(item, arguments);
                anim[funcName]();
                return result;
            };
        })();
    }
    
    injectCallIntoItemBefore(funcName) {
        let item = this.item;
        let anim = this;
        item[funcName] = (function() {
            var cached_function = item[funcName];
            return function() {
                anim[funcName]();
                return cached_function.apply(item, arguments);
            };
        })();
    }
    
    destroy() {
		if (this.frozenImage) this.frozenImage.destroy();
		if (this.onFireImage) this.onFireImage.destroy();
		if (this.breadImage) this.breadImage.destroy();
		if (this.floatImage) this.floatImage.destroy();
	}
}

return ItemAnimator;
});