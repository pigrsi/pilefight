define(['drawing/teleporter-animator'], function(TeleporterAnimator) {
class Teleporter2Animator extends TeleporterAnimator {
    constructor(item, scene, viewer) {
        super(item, scene, viewer);
        this.image.setTint(0x00FF00);
    }
	
	update() {
		super.update();
		this.image.setTint(0x00FF00);
        this.checkOrbEffect();
	}
}

return Teleporter2Animator;
});