define(['drawing/item-animator'], function(ItemAnimator) {
class BubbleGunAnimator extends ItemAnimator {
    constructor(item, scene, viewer) {
        super(item, scene, viewer);
        this.injectCallIntoItem('startSpawningBubbles');
    }

    startSpawningBubbles() {
        this.viewer.suggestCameraFollow('ITEM_FIRED', this.item);
    }
}

return BubbleGunAnimator;
});