define(['drawing/item-animator'], function(ItemAnimator) {
class ItemBubbleAnimator extends ItemAnimator {
    constructor(item, scene, viewer) {
        super(item, scene, viewer);
        this.injectCallIntoItemBefore('releaseItem');
        this.injectCallIntoItem('onStrengthChange');
		let strength = this.item.strength ? this.item.strength : 0;
		this.image.play(this.item.name + "-strength" + strength);
		this.image.depth = 14;
    }

    releaseItem() {
    }
	
	onStrengthChange() {
		let strength = this.item.strength ? this.item.strength : 0;
		this.image.play(this.item.name + "-strength" + strength);
	}
}

return ItemBubbleAnimator;
});