define(['drawing/item-animator'], function(ItemAnimator) {
class MonkeyAnimator extends ItemAnimator {
    constructor(item, scene, viewer) {
        super(item, scene, viewer);
		// this.image.play(this.item.name + "-idle");
        this.injectCallIntoItem('jump');
        this.injectCallIntoItem('bump');
        this.injectCallIntoItem('updated');
        this.injectCallIntoItem('die');
        this.injectCallIntoItem('pickUpItem');
        this.ticksSinceSuggestDeath = 0;
    }

    update() {
        super.update.apply(this, arguments);
        if (this.item.disconnected) this.image.setTint(0x666666);
    }

    jump() {
        this.viewer.suggestCameraFollow('PLAYER_JUMPED', this.item);
    }
        
    bump() {
        this.viewer.suggestCameraFollow('BUMP', this.item);
    }
    
    updated() {
		/* Currently switched off via the lovely "return" below*/
		return;
        this.ticksSinceSuggestDeath--;
        if (this.ticksSinceSuggestDeath <= 0 && this.item.y > this.item.world.dangerY) {
            if (!this.viewer.cameraIsFollowing(this.item)) this.viewer.pauseUpdatesFor(60);
            this.ticksSinceSuggestDeath = 120;
            this.viewer.suggestCameraFollow('PLAYER_DEATH', this.item, true);
        }
    }
    
    pickUpItem() {
        this.viewer.suggestCameraFollow('PLAYER_PICKUP', this.item);
    }
    
    die() {
        this.ticksSinceSuggestDeath = 0;
    }
}

return MonkeyAnimator;
});