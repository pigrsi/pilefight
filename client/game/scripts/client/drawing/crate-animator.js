define(['drawing/item-animator'], function(ItemAnimator) {
class CrateAnimator extends ItemAnimator {
    constructor(item, scene, viewer) {
        super(item, scene, viewer);
        //this.originalSize.width = item.width > item.height ? 64 : 32;
        this.originalSize.width = 32;
        this.originalSize.height = 32;
        this.injectCallIntoItem('setCrateReadyToOpen');
        //this.viewer.suggestCameraFollow('CRATE_SPAWNED', this.item);
    }
    
    setCrateReadyToOpen() {
        this.viewer.suggestCameraFollow('CRATE_OPENING', this.item);
    }
    
    update() {
        super.update();
    }
}

return CrateAnimator;
});