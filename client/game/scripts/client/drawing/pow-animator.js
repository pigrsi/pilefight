define(['drawing/item-animator'], function(ItemAnimator) {
class PowAnimator extends ItemAnimator {
    constructor(item, scene, viewer) {
        super(item, scene, viewer);
        this.injectCallIntoItem('bonkTheWorld');
    }

    bonkTheWorld() {
        this.viewer.shakeCamera(300);
    }
}

return PowAnimator;
});