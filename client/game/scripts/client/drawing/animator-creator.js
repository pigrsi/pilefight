let d = 'drawing/';
let a = '-animator';
define([d+'item'+a, d+'monkey'+a, d+'bloot-block'+a, d+'bubble-gun'+a, d+'teleporter'+a, d+'teleporter2'+a, d+'teleporter3'+a, d+'bomb'+a, d+'time-bomb'+a, d+'crate'+a, d+'item-bubble'+a, d+'booster'+a, d+'pow'+a,
		d+'destruction'+a, d+'drill'+a, d+'orb'+a, d+'goalhole'+a],
function(ItemAnimator, MonkeyAnimator, BlootBlockAnimator, BubbleGunAnimator, TeleporterAnimator, Teleporter2Animator, Teleporter3Animator, BombAnimator, TimeBombAnimator, CrateAnimator, ItemBubbleAnimator, BoosterAnimator,
PowAnimator, DestructionAnimation, DrillAnimator, OrbAnimator, GoalholeAnimator) {

var AnimatorCreator = {};

AnimatorCreator.init = function(viewer, scene, graphics) {
    this.viewer = viewer;
    this.scene = scene;
    this.graphics = graphics;
}

AnimatorCreator.create = function(object) {
    switch (object.name) {
        case 'Monkey': return new MonkeyAnimator(object, this.scene, this.viewer);
        case 'BlootBlock': return new BlootBlockAnimator(object, this.scene, this.viewer);
        case 'BubbleGun': return new BubbleGunAnimator(object, this.scene, this.viewer);
        case 'Teleporter': return new TeleporterAnimator(object, this.scene, this.viewer);
        case 'Teleporter2': return new Teleporter2Animator(object, this.scene, this.viewer);
        case 'Teleporter3': return new Teleporter3Animator(object, this.scene, this.viewer);
        case 'Bomb': return new BombAnimator(object, this.scene, this.viewer);
        case 'TimeBomb': return new TimeBombAnimator(object, this.scene, this.viewer);
        case 'Crate': return new CrateAnimator(object, this.scene, this.viewer);
        case 'ItemBubble': return new ItemBubbleAnimator(object, this.scene, this.viewer);
		case 'Booster': return new BoosterAnimator(object, this.scene, this.viewer);
		case 'Pow': return new PowAnimator(object, this.scene, this.viewer);
		case 'Destruction': return new DestructionAnimation(object, this.scene, this.viewer);
		case 'Drill': return new DrillAnimator(object, this.scene, this.viewer);
		case 'Orb': return new OrbAnimator(object, this.scene, this.viewer);
		case 'Goalhole': return new GoalholeAnimator(object, this.scene, this.viewer);
        default: return new ItemAnimator(object, this.scene, this.viewer);
    }
}

return AnimatorCreator;
});
