define(['drawing/draw-util', 'util/util'], function(DrawUtil, Util) {
PawmawSkin = {};
var graphics;
var World;

PawmawSkin.init = function(world, graphix) {
	World = world;
    graphics = graphix;
}

PawmawSkin.drawPawmaw = function() {
    if (!World.pawmaw) return;
    let pawmaw = World.pawmaw;
    let node = pawmaw.getTail();
    if (pawmaw.alpha < 1) {
        PawmawSkin.drawPawmawNode(pawmaw.head);
    } else {
        while (node) {
            PawmawSkin.drawPawmawNode(node);
            node = node.next;
        }
    }
    
    PawmawSkin.drawPawmawSkin(pawmaw);
}

PawmawSkin.drawPawmawNode = function(node) {
    graphics.fillStyle(0xFFD95E);
    graphics.fillCircle(node.x, node.y, node.radius);
}

PawmawSkin.drawPawmawSkin = function(pawmaw) {
    let node = pawmaw.getTail();
    //PawmawSkin.drawLinesConnectingNodeCentres(pawmaw);
    //PawmawSkin.drawVectorToNextNode(pawmaw);
    //PawmawSkin.drawPerpendicularsOfVectorToNextNode(pawmaw);
    //PawmawSkin.drawCirclesOnPerpPoints(pawmaw);
    PawmawSkin.drawSkin(pawmaw);
}

PawmawSkin.drawSkin = function(pawmaw) {
    graphics.fillStyle(0xFFD95E, pawmaw.alpha);
    let node = pawmaw.getTail();
    while (node.next) {
        let next = node.next;
        let bounds = PawmawSkin.calculateSkinRectBounds(node, next);
        DrawUtil.drawFilledShape(graphics, bounds);
        
        node = node.next;
    }
}

PawmawSkin.drawCirclesOnPerpPoints = function(pawmaw) {
    let node = pawmaw.getTail();
    while (node.next) {
        let next = node.next;
        let bounds = PawmawSkin.calculateSkinRectBounds(node, next);
        for (var i = 0; i < bounds.length; ++i) {
            i < 2 ? graphics.fillStyle(0xFFFF00) : graphics.fillStyle(0xFF69B4);
            DrawUtil.drawCircle(graphics, bounds[i].x, bounds[i].y, 8);
        }
        
        node = node.next;
    }
}

PawmawSkin.calculateSkinRectBounds = function(node1, node2) {
    let unit = Util.getUnitVectorTowards(node2, node1);
    let perps = Util.getPerpendicularVectors(unit);
    return [{x: node1.x + perps.left.x * node1.radius, y: node1.y + perps.left.y * node1.radius},
            {x: node2.x + perps.left.x * node2.radius, y: node2.y + perps.left.y * node2.radius},
            {x: node2.x + perps.right.x * node2.radius, y: node2.y + perps.right.y * node2.radius},
            {x: node1.x + perps.right.x * node1.radius, y: node1.y + perps.right.y * node1.radius}];
}

PawmawSkin.drawVectorToNextNode = function(pawmaw) {
    graphics.lineStyle(4, 0x00FF00, 1.0);
    let node = pawmaw.getTail();
    while (node.next) {
        let next = node.next;
        let unit = Util.getUnitVectorTowards(next, node);        
        DrawUtil.drawLines(graphics, [[node.x, node.y], [node.x + unit.x * node.radius, node.y + unit.y * node.radius]]);
        
        node = node.next;
    }
}

PawmawSkin.drawPerpendicularsOfVectorToNextNode = function(pawmaw) {
    let node = pawmaw.getTail();
    while (node.next) {
        let next = node.next;
        let unit = Util.getUnitVectorTowards(next, node);
        let perps = Util.getPerpendicularVectors(unit);
        graphics.lineStyle(4, 0xFF0000, 1.0);
        let leftLine = [[node.x, node.y], [node.x + perps.left.x * node.radius, node.y + perps.left.y * node.radius]];
        DrawUtil.drawLines(graphics, leftLine);
        graphics.lineStyle(4, 0x0000FF, 1.0);
        let rightLine = [[node.x, node.y], [node.x + perps.right.x * node.radius, node.y + perps.right.y * node.radius]];
        DrawUtil.drawLines(graphics, rightLine);
        node = node.next;
    }
}

PawmawSkin.drawLinesConnectingNodeCentres = function(pawmaw) {
    graphics.lineStyle(8, 0xFFFFFF, 1.0);
    let node = pawmaw.getTail();
    while (node.next) {
        let next = node.next;
        DrawUtil.drawLines(graphics, [[node.x, node.y], [next.x, next.y]]);
        node = node.next;
    }
}

return PawmawSkin;
});