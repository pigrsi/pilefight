define(['drawing/item-animator'], function(ItemAnimator) {
class DrillAnimator extends ItemAnimator {
    constructor(item, scene, viewer) {
        super(item, scene, viewer);
        this.injectCallIntoItem('startSpinning');
        this.injectCallIntoItem('stopSpinning');
    }
	
	startSpinning() {
		this.image.play('Drill-drilling');
	}
	
	stopSpinning() {
		this.image.play('Drill-idle');
	}
	
	setScale() {
		if (!this.item.drilling) super.setScale();
	}
}

return DrillAnimator;
});