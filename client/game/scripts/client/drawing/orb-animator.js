define(['drawing/item-animator'], function(ItemAnimator) {
class OrbAnimator extends ItemAnimator {
    constructor(item, scene, viewer) {
        super(item, scene, viewer);		
		this.glowImage = scene.add.image(item.x+item.width/2, item.y+item.height/2, 'orb-glow');
		this.image.depth = 10;
		this.glowImage.depth = this.image.depth - 1;
    }
	
	update() {
        super.update();
		this.glowImage.setPosition(this.item.x+this.item.width/2, this.item.y+this.item.height/2);
		this.glowImage.scaleX = this.image.scaleX;
		this.glowImage.scaleY = this.image.scaleY;
    }
}

return OrbAnimator;
});