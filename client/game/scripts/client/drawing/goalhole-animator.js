define(['drawing/draw-util'], function(DrawUtil) {
class GoalholeAnimator {
    constructor(goalhole, scene, viewer) {
		this.goalhole = goalhole;
		this.graphics = viewer.getGameGraphics();
    }
	
	update() {
    	let radius = this.goalhole.width/2;
    	let color = 0x000000;
    	if (this.goalhole.turnPlayerHasEnoughPoints) color = 0xDDDD00;
		this.graphics.fillStyle(color, this.goalhole.isActive() ? 1 : 0.1);
		DrawUtil.drawCircle(this.graphics, this.goalhole.x+radius, this.goalhole.y+radius, radius);
	}
}

return GoalholeAnimator;
});