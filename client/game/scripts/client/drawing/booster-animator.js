define([''], function() {
class BoosterAnimator {
    constructor(booster, scene, viewer) {
		this.booster = booster;
		this.graphics = viewer.getGameGraphics();
    }
	
	update() {
		this.graphics.fillStyle(this.booster.requiresBoosterEffect ? 0xFF0000 : 0x00FF00, 0.5);
		this.graphics.fillPoints(this.booster.points);
	}
}

return BoosterAnimator;
});