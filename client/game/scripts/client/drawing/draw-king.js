define(['util/util', 'drawing/animator-creator', 'drawing/pawmaw-skin'], function(Util, AnimatorCreator, PawmawSkin) {
var DrawKing = {};
DrawKing.animators = [];

DrawKing.init = function(world, scene, graphicsFG, gameGraphics) {
    DrawKing.scene = scene;
    DrawKing.graphicsFG = graphicsFG;
    DrawKing.gameGraphics = gameGraphics;
    world.registerListener(DrawKing.onWorldEvent);
    PawmawSkin.init(world, graphicsFG);
}

DrawKing.addAnimator = function(item) {
    DrawKing.animators.push(AnimatorCreator.create(item));
}

DrawKing.onTileDestroyed = function (tile) {
    if (tile.image) {
        tile.image.destroy();
        tile.image = null;
    }
}

DrawKing.groundItemAdded = function (tile, item) {
    if (!tile.grassImage) {
        tile.grassImage = scene.add.image(tile.x + tile.width / 2, tile.y - 12, 'grass');
        tile.grassImage.depth = 21;
    }
    if (item.name === 'Orb') tile.grassImage.setTintFill(0xFFFF00);
}

DrawKing.groundItemRevealed = function(tile) {
    if (tile.items.length > 0) return;
    if (tile.grassImage) tile.grassImage.destroy();
    tile.grassImage = null;
}

DrawKing.createTileset = function (scene, tilesetName, imageName, tilesetInfo) {
    let source = scene.textures.get(imageName).source[0];
    let texture = scene.textures.addImage('tileset-' + tilesetName, source.source);

    let info = tilesetInfo;
    let margin = 3;
    let spacing = 3;
    let frameCounter = info.firstgid;
    for (var y = margin; y + info.tileheight < source.height; y += (info.tileheight + spacing)) {
        for (var x = margin; x + info.tilewidth < source.width; x += (info.tilewidth + spacing)) {
            texture.add(frameCounter++, 0, x, y, info.tilewidth, info.tileheight);
        }
    }

}

DrawKing.applyTileImageToTiles = function (scene, tiles, tilesetName) {
    for (var i = 0; i < tiles.length; ++i) {
        let tile = tiles[i];
        tile.image = scene.add.image(tile.x, tile.y, tilesetName, tile.tileNumber);
        tile.image.setOrigin(0);
        tile.image.depth = 20;
        tile.image.scaleMode = 1;
    }
}

DrawKing.update = function() {
    for (var i = DrawKing.animators.length-1; i >= 0; --i) {
        DrawKing.animators[i].update();
        if (DrawKing.animators[i].readyForRemoval) {
            DrawKing.animators[i].destroy();
            DrawKing.animators.splice(i, 1);
        }
    }
    PawmawSkin.drawPawmaw();
}

DrawKing.onWorldEvent = function(eventName, args) {
    switch (eventName) {
        case 'ITEM_ADDED': case 'OBJECT_ADDED':
            DrawKing.addAnimator(args[0]);
            break;
        case 'TILE_DESTROYED':
            DrawKing.onTileDestroyed(args[0]);
            break;
        case 'GROUND_ITEM_ADDED':
            DrawKing.groundItemAdded(args[0], args[1]);
            break;
        case 'GROUND_ITEM_REVEALED':
            DrawKing.groundItemRevealed(args[0]);
            break;

    }
}



return DrawKing;
});