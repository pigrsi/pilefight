define(['drawing/item-animator'], function(ItemAnimator) {
class DestructionAnimator extends ItemAnimator {
    constructor(item, scene, viewer) {
        super(item, scene, viewer);
        this.injectCallIntoItem('updateDeployed');
        this.injectCallIntoItem('deploy');
		//this.image.play(this.item.name + "-strength" + strength);
		//this.image.depth = 5;
    }
	
	updateDeployed() {
		if (this.changeImage) {
			
			this.changeImage = false;
		}
	}
	
	deploy() {
		this.viewer.stopCameraFollow();
		this.changeImage = true;
		let _this = this;
		setTimeout(function() {_this.image.anims.stop(); _this.image.setTexture('destruction_deployed', 0);}, 1);
	}
	
	setPosition() {
		if (this.item.isDeployed())
			this.image.setPosition(this.item.x+this.item.width/2, this.item.y+this.item.halfHeight-(this.image.displayHeight-this.item.height)/2);
		else
			super.setPosition();
    }
	
	setScale() {
		if (this.item.isDeployed()) {
			this.image.scaleX = 1;
			this.image.scaleY = 1;
		} else {
			super.setScale();
		}
	}
	
}

return DestructionAnimator;
});