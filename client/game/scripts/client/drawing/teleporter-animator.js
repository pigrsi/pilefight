define(['drawing/item-animator', 'drawing/draw-util'], function(ItemAnimator, DrawUtil) {
class TeleporterAnimator extends ItemAnimator {
    constructor(item, scene, viewer) {
        super(item, scene, viewer);
        this.injectCallIntoItem('updated');
        //this.injectCallIntoItemBefore('teleportThrower');
    }

    updated() {
        let other = this.item.thrownBy;
        if (!other) return;
        let graphics = this.viewer.getGameGraphics();
        graphics.lineStyle(2, 0x0000CC, 0.5);

        DrawUtil.drawLine(graphics, {x: other.x+other.halfWidth, y: other.y+other.halfHeight}, {x: this.item.x+this.item.halfWidth, y: this.item.y+this.item.halfHeight});
    }

    teleportThrower() {
        //this.viewer.suggestCameraFollow('PLAYER_TELEPORT', this.item.thrownBy);
    }
}

return TeleporterAnimator;
});