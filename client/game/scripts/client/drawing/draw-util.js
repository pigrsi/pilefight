define([], function() {
DrawUtil = {};

DrawUtil.drawLine = function(graphics, from, to) {
    graphics.beginPath();
    graphics.moveTo(from.x, from.y);
    graphics.lineTo(to.x, to.y);
    graphics.closePath();
    graphics.strokePath();
}

DrawUtil.drawLines = function(graphics, points) {
    graphics.beginPath();
    graphics.moveTo(points[0][0], points[0][1]);
    for (var i = 1; i < points.length; ++i)
        graphics.lineTo(points[i][0], points[i][1]);
    graphics.closePath();
    graphics.strokePath();
}

DrawUtil.drawFilledShape = function(graphics, points, autoClose=true) {
    graphics.fillPoints(points, true);
}

DrawUtil.drawCircle = function(graphics, x, y, radius) {
    graphics.fillCircle(x, y, radius);
}

return DrawUtil;
});