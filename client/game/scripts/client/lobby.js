define([], function() {
	let Lobby = {};
	Lobby.currentPlayerColumn = 0;
	Lobby.players = [];

	Lobby.init = function() {
		let livesSlider = document.getElementById("livesPerPlayer");
		let livesValue = document.getElementById("livesPerPlayerValue");
		livesValue.innerHTML = livesSlider.value;
		livesSlider.oninput = function() {
			livesValue.innerHTML = this.value;
		};

		let numPlayersSlider = document.getElementById("playersPerClient");
		let numPlayersValue = document.getElementById("playersPerClientValue");
		numPlayersValue.innerHTML = numPlayersSlider.value;
		numPlayersSlider.oninput = function() {
			numPlayersValue.innerHTML = this.value;
		};
		
		let secondsPerTurnSlider = document.getElementById("secondsPerTurn");
		let secondsPerTurnValue = document.getElementById("secondsPerTurnValue");
		secondsPerTurnValue.innerHTML = secondsPerTurnSlider.value;
		secondsPerTurnSlider.oninput = function() {
			secondsPerTurnValue.innerHTML = this.value;
		};
		
		let startButton = document.getElementById("lobbyStartButton");
		startButton.onclick = function() {
			Lobby.emitEvent('HOST_START', {gameSettings: Lobby.getGameSettings()});
		};

		let spectateButton = document.getElementById("lobbySpectateButton");
		spectateButton.onclick = function() {
			Lobby.emitEvent('START_SPECTATE');
		}
	};

	Lobby.registerListener = function(listener) {
		Lobby.listener = listener;
	};

	Lobby.emitEvent = function(eventType, data) {
		if (!Lobby.listener) return;
		Lobby.listener({type: eventType, data: data});
	};

	Lobby.onSignal = function(signal) {
		switch (signal.signal) {
			case 'BECOME_HOST':
				this.showLobbyElements(signal.becomeHost);
				break;
			case 'PLAYER_JOINED':
				this.addPlayer(signal.nickname);
				break;
			case 'ROOM_INFO':
				this.addAllPlayers(signal.players);
				this.setSpectateButtonVisibility(signal.gameRunning);
				break;
			case 'CLIENT_DISCONNECTED_LOBBY':
				this.removePlayer(signal.nickname);
				break;
		}
	};

	Lobby.addPlayer = function(nickname) {
		Lobby.players.push({nickname: nickname});
		let player = document.createElement('div');
		player.className = 'lobbyPlayer';
		
		let image = document.createElement('img');
		image.src = '/home/assets/images/wizard.png';
		image.style = 'margin:10px';
		
		let label = document.createElement('label');
		label.innerHTML = nickname;
		
		player.appendChild(image);
		player.appendChild(label);
		
		document.getElementById('playerColumn' + this.currentPlayerColumn).appendChild(player);
		this.currentPlayerColumn = (this.currentPlayerColumn + 1) % 3;
	};
	
	Lobby.clearPlayers = function() {
		Lobby.players = [];
		for (let i = 0; i < 3; ++i) {
			let column = document.getElementById('playerColumn' + i);
			while (column.firstChild) {
				column.removeChild(column.firstChild);
			}
		}
		this.currentPlayerColumn = 0;
	};
	
	Lobby.addAllPlayers = function(playersInfo) {
		Lobby.clearPlayers();
		for (let i = 0; i < playersInfo.length; ++i) {
			Lobby.addPlayer(playersInfo[i].nickname);
		}
	};
	
	Lobby.removePlayer = function(nickname) {
		for (let i = 0; i < Lobby.players.length; ++i) {
			if (Lobby.players[i].nickname === nickname) {
				Lobby.players.splice(i, 1);
				break;
			}
		}
		
		Lobby.addAllPlayers(Lobby.players.slice());
	};
	
	Lobby.getGameSettings = function() {
		let gameModeSelector = document.getElementById('gamemode');
		let gameMode = gameModeSelector.options[gameModeSelector.selectedIndex].value;
		let livesPerPlayer = document.getElementById('livesPerPlayer').value;
		let playersPerClient = document.getElementById('playersPerClient').value;
		let secondsPerTurn = document.getElementById('secondsPerTurn').value;
		let mapName = 'random';
		
		return {gameMode: gameMode, livesPerPlayer: livesPerPlayer, 
				playersPerClient: playersPerClient, secondsPerTurn: secondsPerTurn,
				mapName: mapName};
	};
	
	Lobby.showLobbyElements = function(isHost) {
		let hostOnly = document.getElementsByClassName('hostOnly');
		let notHostOnly = document.getElementsByClassName('notHostOnly');
		for (let i = 0; i < hostOnly.length; ++i) {
			hostOnly[i].style = isHost ? 'display: flex;' : 'display: none;';
		}
		for (let i = 0; i < notHostOnly.length; ++i) {
			notHostOnly[i].style = isHost ? 'display: none;' : 'display: flex;';
		}
		
		let startButton = document.getElementById('lobbyStartButton');
		startButton.disabled = !isHost;
		startButton.innerHTML = isHost ? 'Start' : 'Waiting for host to start';
	};

	Lobby.setSpectateButtonVisibility = function(visible) {
		visible ? $('#spectateBox').removeClass("invisible") : $('#spectateBox').addClass("invisible");
	};
	
	return Lobby;
});