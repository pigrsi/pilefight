define(['client/game-status', 'shared/global-info'], function(GameStatus) {
class ClientGameBase {
    constructor(clientMaster, userActions, asSpectator, onGameComplete) {
        this.joinRequestDelay = 50;
        this.state = 'JOIN_ROOM';
        this.counter = 0;
        this.clientMaster = clientMaster;
        this.debug_name_counter = 0;
        this.local = {ready: false};
        this.spectating = asSpectator;
        this.onComplete = onGameComplete;
        userActions.registerListener(this);
    }
    
    tick() {
        this.checkFreeSignals();
        switch (this.state) {
            case 'IDLE': this.actIdle(); break;
            case 'DECIDING_MOVE': this.actDecidingMove(); break;
            case 'PREPARING_RESPAWN': this.actPrepareRespawn(); break;
            case 'PERFORMING_MOVE': this.actPerformingMove(); break;
            case 'MOVE_HAS_ENDED': this.actMoveEnd(); break;
            case 'GAME_OVER': this.actGameEnd(); break;
            case 'LOAD_MAP': this.actLoadMap(); break;
            case 'OBTAIN_GAME_INFO': this.actObtainGameInfo(); break;
            case 'JOIN_ROOM': this.actJoinRoom(); break;
            case 'WAITING_START': this.waitingToStart(); break;
            case 'COUNTING_POINTS': this.actCountingPoints(); break;
            default: console.log("ClientGameBase: Invalid client state, " + this.state);
        }
        this.updateGameStatus();
    }

    checkFreeSignals() {
        while (this.clientMaster.freeSignals.length > 0)
            this.processSignal(this.clientMaster.freeSignals.shift());
    }

    actIdle() {
        if (this.clientMaster.blockedSignals.length > 0) {
            this.processSignal(this.clientMaster.blockedSignals.shift());
        }
    }

    onUserAction(action, args) {
        switch (action) {
            case 'add-local-player':
                this.sendJoinRequest('Lameguest' + this.debug_name_counter++);
                break;
            case 'toggle-ready':
                this.clientMaster.sendNotion({notion: 'TOGGLE_READY'});
        }
    }

    actJoinRoom () {
        let this_ = this;
        this.sendReq = function() {
            if (this_.clientMaster.hasJoinedRoom()) return;
            this_.timer = null;
            this_.sendJoinRequest();
        };
        if (this.clientMaster.hasJoinedRoom()) {
            if (this.timer) this.timer.destroy();
            this.timer = null;
            this.changeState('OBTAIN_GAME_INFO');
        } else if (!this.timer) {
            this.timer = scene.time.addEvent({
                delay: this.joinRequestDelay,
                callback: this.sendReq
            });
        }
    }

    sendJoinRequest(nickname) {
        this.clientMaster.sendJoinRequest(nickname);
    }
	
    waitingToStart() {
        this.changeState('IDLE');
    }

    blockedSignalsIsEmpty() {return this.clientMaster.blockedSignals.length === 0;}

    processSignal(signal) {
        switch (signal.signal) {
            case 'SYNC':
                this.setStateFromAction(this.clientMaster.applySync(signal.sync)); break;
            case 'COUNT_POINTS':
                this.changeState('COUNTING_POINTS'); break;
            case 'CLIENT_DISCONNECT':
                this.clientMaster.flagClientAsDisconnected(signal.clientId); break;
            case 'YOUR_ID':
                this.clientMaster.addLocalClientId(signal.clientId);
                break;
            case 'GAME_INFO':
                this.processGameInfo(signal.gameInfo);
                break;
            case 'GAME_ENDED':
                this.clientMaster.clearBlockedSignalsForGameEnd();
                break;
        }
    }

    processGameInfo(gameInfo) {
        if (gameInfo.mapName) {
            this.clientMaster.roomJoined();
            this.clientMaster.setMapName(gameInfo.mapName);
            this.clientMaster.sendNotion({
                notion: 'JOINED_ROOM',
                nickname: this.clientMaster.getCurrentlyJoiningNickname(),
                spectating: this.spectating
            });
            console.log("ClientGameBase: Map name received: " + gameInfo.mapName);
        } else {
            console.log("ClientGameBase: Received gameInfo doesn't contain mapName!");
        }

        this.obtainedGameInfo = true;
    }

    setStateFromAction(actionInfo) {
        switch (actionInfo.actionType) {
            case '': return;
            case 'JUMP': case 'THROW': case 'RESPAWN': case 'SKIP':
                this.clientMaster.performMove(actionInfo);
                this.changeState('PERFORMING_MOVE');
            break;
            case 'DECIDE_STANDARD_MOVE': 
                this.clientMaster.informWorldOfNextTurnPlayers(actionInfo.ids);
                this.onConfirmTurnPlayer();
                if (this.clientMaster.playerBelongsToLocalClient(actionInfo.ids[0])) {
                    this.changeState('DECIDING_MOVE');
                }
                break;
            case 'DECIDE_RESPAWN_MOVE':
                this.clientMaster.informWorldOfNextTurnPlayers(actionInfo.ids);
                this.onConfirmTurnPlayer();
                this.changeState('PREPARING_RESPAWN');
                break;
            case 'GAME_OVER':
                this.updateGameStatusRankings(actionInfo.rankings);
                this.changeState('GAME_OVER');
                break;
        }
        console.log(actionInfo.actionType);
        GameStatus.setCountdownInfo(actionInfo.countdownEndTime);
    }

    updateGameStatusRankings(rankings) {
        let nameRanks = [];
        for (var i = 0; i < rankings.length; ++i) {
            nameRanks.push(this.clientMaster.getNickname(rankings[i]));
        }
        GameStatus.setRankingsInfo(nameRanks);
    }

    actLoadMap() {
        if (!!this.clientMaster.mapNameReceived()) {
            this.clientMaster.loadMap();
            this.changeState('WAITING_START');
        }
    }

    actObtainGameInfo() {
        if (this.obtainedGameInfo)
            this.changeState('LOAD_MAP');
    }

    actDecidingMove() {
        if (!this.blockedSignalsIsEmpty()) {
            this.changeState('IDLE');
        } else if (this.clientMaster.moveHasBeenDecided()) {
            this.clientMaster.sendMoveToServer();
            this.changeState('IDLE');
        }
    }

    actPrepareRespawn(){
        if (this.clientMaster.respawnIsPrepared()) {
            let respawnPlayerId = this.clientMaster.getTurnPlayerId();
            if (this.clientMaster.playerBelongsToLocalClient(respawnPlayerId))
                this.changeState('DECIDING_MOVE');
            else this.changeState('IDLE');
        } else {
            this.clientMaster.prepareForRespawn();
        }
    }

    actPerformingMove(){
		if (this.counter-- <= 0) {
			if (this.clientMaster.turnIsOver())
				this.changeState('MOVE_HAS_ENDED');
			else
				this.counter = 10;
		}
    }

    actMoveEnd(){
        this.clientMaster.moveEndCleanUp();
        this.changeState('IDLE');
    }

    actGameEnd(){
        if (this.counter++ >= 300) {
            if (this.onComplete) this.onComplete();
            this.onComplete = null;
        }
    }

    changeState(state) {
        this.state = state;
        this.counter = 0;
    }

    actCountingPoints(){}
    updateGameStatus(){};
    onConfirmTurnPlayer(){};
}
return ClientGameBase;  
});