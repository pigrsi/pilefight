// In between game world and network stuff
define(['shared/global-info'], function(GlobalInfo) {
class ClientWorld {
    constructor(worldNetwork) {
        this.worldNetwork = worldNetwork;
    }

    init(worldNetwork) {
        this.worldNetwork = worldNetwork;
    }

    loadMap(mapName) {
        this.worldNetwork.clientCreate(mapName);
        this.worldNetwork.spawnMapObjectsFromMap();
    }

    pushItems(items, groundItems) {
        this.worldNetwork.harmonizeItems(items);
        this.worldNetwork.harmonizeGroundItems(groundItems);
    }

    pushPlayersInfo(playersInfo) {this.worldNetwork.harmonizePlayersWithData(playersInfo);}

    tellPlayersDecideMove(playerIds) {
        this.worldNetwork.playersDecideMove(playerIds);
    }

    flagPlayersAsDisconnected(playerIds) {
        for (let i = 0; i < playerIds.length; ++i) {
            let player = this.worldNetwork.getItemById(playerIds[i]);
            if (player) player.disconnected = true;
        }
    }

    setRemoteTurnPlayer(playerId) {
        this.worldNetwork.setRemoteTurnPlayer(playerId);
    }

    startAddingOrbPoints() {
        this.worldNetwork.signalAddOrbPoints();
    }

    finishedAddingOrbPoints() {
        return this.worldNetwork.finishedAddingOrbPoints();
    }

    getPlayerMoveInfo() {
        return this.worldNetwork.grabMoveInfo();
    }

    checkGoalholesActive(anyClientMeetsPointsRequired, turnClientMeetsPointsRequired) {
        this.worldNetwork.checkGoalholesActive(anyClientMeetsPointsRequired, turnClientMeetsPointsRequired);
    }

    canMoveOntoNextTurn() {
        if (!GlobalInfo.LOCAL_GAME) this.worldNetwork.checkArenaIsCalm();
        return this.worldNetwork.arenaIsCompletelyCalm();
    }

    setSyncLatenessDegree(latenessDegree) {
        this.worldNetwork.setDesiredSpeedUpDegree(latenessDegree);
    }

    syncDestroyedTiles(destroyedTileIds) {
        this.worldNetwork.harmonizeDestroyedTilesWithIds(destroyedTileIds);
    }

    updateMisc(lastGeneratedItemId, respawnIndex) {
        this.worldNetwork.harmonizeMisc(lastGeneratedItemId, respawnIndex);
    }

    moveEndCleanUp() {this.worldNetwork.onMoveEnd();}

    performMove(playerId, actionType, velocity, targetId) {
        this.worldNetwork.performMove(playerId, actionType, velocity, targetId);
    }

    getPointsForPlayerIds(playerIds) {
        let points = 0;
        for (let i = 0; i < playerIds.length; ++i) {
            let item = this.worldNetwork.getItemById(playerIds[i]);
            if (item) points += item.clientPoints;
        }
        return points;
    }

    playerIsAlive(playerId) {
        let player = this.worldNetwork.getItemById(playerId);
        return !!player && player.alive && !player.disabled;
    }

    prepareForRespawn() {
        return this.worldNetwork.prepareForRespawn();
    }

    getTurnPlayer() {
        return this.worldNetwork.getTurnPlayer();
    }
}

return ClientWorld;
});