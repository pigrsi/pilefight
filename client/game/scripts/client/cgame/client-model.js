define([], function() {
class ClientModel {
    constructor() {
        /* clientInfo contains: lives, nickname, playerIds, points, numPlayersAlive */
        this.clientInfoMap = {};
        // IDs of clients local to this instance of the game (the players are using this computer)
        this.localClientIds = [];

        this.joinedRoom = false;
        this.turnClientId = null;
    }

    // Sync with data from server
    syncClientsInfo(syncWith) {
        for (let i = 0; i < syncWith.length; ++i) this.syncSingleClientInfo(syncWith[i]);
    }

    syncSingleClientInfo(remoteData) {
        let clientId = remoteData.clientId;
        if (!this.getClientInfo(clientId)) this.createClientInfo(clientId);
        this.updatePlayerIds(clientId, remoteData.playerIds);
        this.updateNickname(clientId, remoteData.nickname);
        this.updatePoints(clientId, remoteData.points);
        this.updateLives(clientId, remoteData.lives);
        // Don't need to sync clientId or numPlayersAlive
    }

    createClientInfo(clientId) {
        console.log('ClientModel: Creating client info for ID: ' + clientId);
        this.clientInfoMap[clientId] =
        {
            clientId: clientId,
            playerIds: [],
            nickname: 'Noname',
            points: 0,
            lives: 0,
            numPlayersAlive: 0
        };
    }

    playerBelongsToLocalClient(playerId) {
        let localClients = this.getLocalClientsList();
        for (let i = 0; i < localClients.length; ++i) {
            let playerIds = this.getPlayerIds(localClients[i]);
            if (playerIds && playerIds.includes(playerId)) return true;
        }
        return false;
    }

    playerBelongsToClient(playerId, clientId) {
        let playerIds = this.getPlayerIds(clientId);
        return playerIds.indexOf(playerId) !== -1;
    }

    setClientIdAsLocal(clientId) {
        this.localClientIds.push(clientId);
    }

    /******************************************************************/
    /**************************** Updates *****************************/
    /******************************************************************/
    updateClientAttribute(clientId, attributeName, value) {
        let clientInfo = this.getClientInfo(clientId);
        if (clientInfo) clientInfo[attributeName] = value;
    }
    updatePlayerIds(clientId, playerIds) {
        this.updateClientAttribute(clientId, 'playerIds', playerIds);
    }
    updateNickname(clientId, nickname) {
        this.updateClientAttribute(clientId, 'nickname', nickname);
    }
    updatePoints(clientId, numPoints) {
        this.updateClientAttribute(clientId, 'points', numPoints);
    }
    updateLives(clientId, numLives) {
        this.updateClientAttribute(clientId, 'lives', numLives);
    }
    updatePlayersAlive(clientId, playersAlive) {
        this.updateClientAttribute(clientId, 'numPlayersAlive', playersAlive);
    }
    updateAllClientsPoints(clientsAndPoints) {
        for (let i = 0; i < clientsAndPoints.length; ++i)
            this.updatePoints(clientsAndPoints[i].clientId, clientsAndPoints[i].points);
    }
    onRoomJoined() {
        this.joinedRoom = true;
    }
    updateTurnClientId(clientId) {
        this.turnClientId = clientId;
    }
    updateLocalNickname(nickname) {
        this.localNickname = nickname;
    }

    /******************************************************************/
    /************************** Retrievals ****************************/
    /******************************************************************/
    getLocalNickname() {
        return this.localNickname;
    }
    getNickname(clientId) {
        return this.getClientAttribute(clientId, 'nickname');
    }
    getPlayerIds(clientId) {
        return this.getClientAttribute(clientId, 'playerIds');
    }
    getPoints(clientId) {
        return this.getClientAttribute(clientId, 'points');
    }
    getClientAttribute(clientId, attributeName) {
        let clientInfo = this.getClientInfo(clientId);
        return clientInfo ? clientInfo[attributeName] : null;
    }
    getClientInfo(clientId) {
        let clientInfo = this.clientInfoMap[clientId];
        if (!clientInfo) {
            console.log(`ClientModel: Client ${clientId} is not stored in client model!`);
            return null;
        }
        return clientInfo;
    }
    getClientsList() {
        let clients = [];
        for (let clientId in this.clientInfoMap) {
            if (this.clientInfoMap.hasOwnProperty(clientId)) {
                clients.push(clientId);
            }
        }
        return clients;
    }
    getClientInfoList() {
        let list = [];
        let clientIds = this.getClientsList();
        for (let i = 0; i < clientIds.length; ++i)
            list.push(this.getClientInfo(clientIds[i]));
        return list;
    }
    clientMeetsPoints(clientId, points=0) {return this.getPoints(clientId) >= points;}
    anyClientMeetsPoints(points=0) {
        let clientIds = this.getClientsList();
        for (let i = 0; i < clientIds.length; ++i) {
            if (this.clientMeetsPoints(clientIds[i], points)) return true;
        }
        return false;
    }
    getTurnClientId() {return this.turnClientId;}
    turnClientMeetsPoints(points=0) {return this.clientMeetsPoints(this.getTurnClientId(), points);}
    getClientInfoMap() {return this.clientInfoMap;}
    getLocalClientsList() {return this.localClientIds;}
    hasJoinedRoom() {return this.joinedRoom;}
}

    return ClientModel;
});

