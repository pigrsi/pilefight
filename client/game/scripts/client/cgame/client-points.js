define(['cgame/client-game-base', 'client/game-status'], function(ClientGameBase, GameStatus) {
class ClientPoints extends ClientGameBase {
    constructor(clientMaster, userActions, asSpectator=false, onGameComplete) {
        super(clientMaster, userActions, asSpectator, onGameComplete);
        this.startedAddingOrbPoints = false;
        this.pointsGoal = 9999;
    }
    
    updateGameStatus() {
        // let players = [];
        // let clientIds = this.clientMaster.getClientsList();
        // for (var i = 0; i < clientIds.length; ++i) {
        //     let info = this.clientMaster.getClientInfoById(clientIds[i]);
        //     players.push({nickname: info.nickname, points: info.points});
        // }
        // GameStatus.setPlayersInfo(players);
		GameStatus.setTurnsLeftForCurrentPlayer(2 - this.clientMaster.getNumMovesUsedByTurnPlayer());
    }

	processGameInfo(gameInfo) {
    	super.processGameInfo.apply(this, arguments);
    	this.pointsGoal = gameInfo.pointsGoal;
	}

    actCountingPoints() {
		if (!this.startedAddingOrbPoints) {
			this.clientMaster.startAddingOrbPoints();
			this.startedAddingOrbPoints = true;
		}

		this.clientMaster.updateClientPoints();
		this.updateGameStatus();
		if (this.clientMaster.finishedAddingOrbPoints()) {
			this.startedAddingOrbPoints = false;
			this.changeState('IDLE');
		}
	}

	actDecidingMove() {
		super.actDecidingMove.apply(this, arguments);
	}

	onConfirmTurnPlayer() {
		this.clientMaster.checkGoalholesActive(this.pointsGoal);
		super.onConfirmTurnPlayer.apply(this, arguments);
	}
}
return ClientPoints;
});