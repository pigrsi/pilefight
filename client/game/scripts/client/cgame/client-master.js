define(['cgame/client-world', 'cgame/client-model', 'shared/global-info', 'client/game-status'], function(ClientWorld, ClientModel, GlobalInfo, GameStatus) {
class ClientMaster {
    constructor(eventManager, clientWorld, worldNetwork, nickname) {
        this.eventManager = eventManager;
        this.clientWorld = clientWorld;

        this.blockedSignals = [];
        this.freeSignals = [];
        this.numSyncsLeft = 0;

        this.reset(nickname);
    }

    reset(nickname) {
        this.mapName = '';
        this.stateEvent = null;
        this.respawnPrepareDone = false;
        this.numMovesUsedByTurnPlayer = 0;
        this.model = new ClientModel();
        this.model.updateLocalNickname(nickname);
        this.currentlyJoiningNickname = null;
        this.waitingForJoinResponse = false;
    }

    sendJoinRequest(nickname) {
        if (!nickname) nickname = this.model.getLocalNickname();
        this.currentlyJoiningNickname = nickname;
        this.waitingForJoinResponse = true;
        this.eventManager.sendNotion({notion: 'JOIN'});
    }

    getCurrentlyJoiningNickname() {return this.currentlyJoiningNickname;}

    isWaitingForJoinResponse() {return this.waitingForJoinResponse;}

    roomJoined() {
        this.model.onRoomJoined();
        this.waitingForJoinResponse = false;
    }

    startAddingOrbPoints() {this.clientWorld.startAddingOrbPoints();}

    finishedAddingOrbPoints() {return this.clientWorld.finishedAddingOrbPoints();}

    loadMap() {this.clientWorld.loadMap(this.mapName);}

    moveHasBeenDecided() {return this.clientWorld.getPlayerMoveInfo().moveReady;}

    sendMoveToServer() {
        let info = this.clientWorld.getPlayerMoveInfo();
        this.eventManager.sendNotion({notion: 'PLAYER_MOVE', move: {playerId: this.clientWorld.getTurnPlayer().id, moveType: info.moveType, velocity: info.velocity, targetId: info.targetId}});
    }

    sendNotion(notion) {this.eventManager.sendNotion(notion);}

    prepareForRespawn() {this.respawnPrepareDone = this.clientWorld.prepareForRespawn();}

    getTurnPlayerId() {return this.clientWorld.getTurnPlayer().id;}



    turnIsOver() {return this.clientWorld.canMoveOntoNextTurn();}

    respawnIsPrepared() {
        return this.respawnPrepareDone;
    }

    moveEndCleanUp() {
        this.respawnPrepareDone = false;
        this.clientWorld.moveEndCleanUp();
    }

    performMove(actionInfo) {
        this.numMovesUsedByTurnPlayer++;
        this.clientWorld.performMove(actionInfo.ids[0], actionInfo.actionType, actionInfo.launchVelocity, actionInfo.targetId);
    }

    informWorldOfNextTurnPlayers(playerIds) {
        if (this.playerBelongsToLocalClient(playerIds[0]))
            this.clientWorld.tellPlayersDecideMove(playerIds);
        else
            this.clientWorld.setRemoteTurnPlayer(playerIds[0]);
    }

    applySync(sync) {
        // Get first Sync in list
        this.decrementNumSyncsLeft();
        let syncLatenessDegree = 0;
        if (this.numSyncsLeft > 1 && this.numSyncsLeft < 4) syncLatenessDegree = 3;
        else if (this.numSyncsLeft >= 4) syncLatenessDegree = 3;
        this.clientWorld.setSyncLatenessDegree(syncLatenessDegree);
        this.syncClientsInfo(sync.clientsInfo);
        this.clientWorld.syncDestroyedTiles(sync.destroyedTileIds);
        this.clientWorld.pushItems(sync.items, sync.groundItems);
        this.clientWorld.pushPlayersInfo(sync.playersInfo);
        this.clientWorld.updateMisc(sync.lastGeneratedItemId, sync.respawnIndex);
        this.model.updateTurnClientId(sync.turnClientId);
        this.numMovesUsedByTurnPlayer = sync.numMovesUsedByTurnPlayer;
        return sync.actionInfo;
    }

    getNumMovesUsedByTurnPlayer() {return this.numMovesUsedByTurnPlayer;}

    getClientInfoById(clientId) {return this.model.getClientInfo(clientId);}

    onSignal(signal) {
        if (signal.blocked) this.blockedSignals.push(signal);
        else this.freeSignals.push(signal);

        if (signal.signal === 'SYNC') this.numSyncsLeft++;
    }

    clearBlockedSignalsForGameEnd() {
        // Only keeping last two blocked signals so client can end the game quickly on their end
        while (this.blockedSignals.length > 2)
            this.blockedSignals.shift();
    }

    eatStateEvent() {
        let e = this.stateEvent;
        this.stateEvent = '';
        return e;
    }

    setMapName(mapName) {
        GlobalInfo.MAP_NAME = mapName;
        this.mapName = mapName;
    }

    mapNameReceived() {return !!this.mapName;}

    flagClientAsDisconnected(clientId) {
        let playerIds = this.model.getPlayerIds(clientId);
        if (playerIds != null) this.clientWorld.flagPlayersAsDisconnected(playerIds);
    }

    getNumSyncsLeft() {return this.numSyncsLeft;}
    decrementNumSyncsLeft() {this.numSyncsLeft = Math.max(this.numSyncsLeft-1, 0);}

    checkGoalholesActive(requiredPoints) {return this.clientWorld.checkGoalholesActive(this.model.anyClientMeetsPoints(requiredPoints), this.model.turnClientMeetsPoints(requiredPoints));}

    /******************************************************************/
    /************************ Model retrieval *************************/
    /******************************************************************/
    syncClientsInfo(clientsInfo) {
        this.model.syncClientsInfo(clientsInfo);
        this.updateGameStatusClientInfo();
    }
    getClientsList() {return this.model.getClientsList();}
    playerBelongsToLocalClient(id) {return this.model.playerBelongsToLocalClient(id);}
    hasJoinedRoom() {return this.model.hasJoinedRoom();}
    getNickname(clientId) {return this.model.getNickname(clientId);}

    /******************************************************************/
    /************************ Model updating **************************/
    /******************************************************************/
    updateClientPoints() {
        let clientsAndPoints = [];
        let clientIds = this.model.getClientsList();
        for (let i = 0; i < clientIds.length; ++i) {
            let playerIds = this.model.getPlayerIds(clientIds[i]);
            let points = this.clientWorld.getPointsForPlayerIds(playerIds);
            clientsAndPoints.push({clientId: clientIds[i], points: points});
        }
        this.model.updateAllClientsPoints(clientsAndPoints);
        this.updateGameStatusClientInfo();
    }
    updateGameStatusClientInfo() {
        let clientIds = this.model.getClientsList();
        for (let i = 0; i < clientIds.length; ++i) {
            let numPlayersAlive = 0;
            let playerIds = this.model.getPlayerIds(clientIds[i]);
            for (var k = 0; k < playerIds.length; ++k) {
                numPlayersAlive += this.clientWorld.playerIsAlive(playerIds[k]) ? 1 : 0;
            }
            this.model.updatePlayersAlive(clientIds[i], numPlayersAlive);
        }
        GameStatus.setClientsInfo(this.model.getClientInfoList());
    }
    addLocalClientId(clientId) {
        this.model.setClientIdAsLocal(clientId);
    }
}

return ClientMaster;
});