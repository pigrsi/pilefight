define(['cgame/client-game-base', 'client/game-status'], function(ClientGameBase, GameStatus) {
class ClientLives extends ClientGameBase {
    constructor(clientMaster, userActions) {
        super(clientMaster, userActions);
    }
    
    updateGameStatus() {
        let players = [];
        let clientIds = this.clientMaster.getClientsList();
        for (let i = 0; i < clientIds.length; ++i) {
            let info = this.clientMaster.getClientInfoById(clientIds[i]);
            players.push({nickname: info.nickname, points: null, lives: info.lives});
        }
        GameStatus.setPlayersInfo(players);
		GameStatus.setTurnsLeftForCurrentPlayer(2 - this.clientMaster.getNumMovesUsedByTurnPlayer());
    }
}
return ClientLives;
});