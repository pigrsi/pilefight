define(['client/connection-events', 'events/external-listener'], function(ConnectionEvents, ExternalListener) {
    class ClientConnection {
        constructor(obj) {
            this.port = obj.port;
            this.gameId = obj.gameId;
        }

        registerListener(listenerFunction) {
            this.listenerFunction = listenerFunction;
        }

        emitEvent(eventType) {
            if (!this.listenerFunction) return;
            this.listenerFunction(eventType);
        }

        emit(messageType, data) {
            this.sock.emit(messageType, data);
        }

        connect() {
            console.log("Connecting to server...");
            this.sock = io(`${location.protocol}//${window.location.hostname}:${this.port}/play`, {query: {token:this.gameId}});
            this.sock = this.sock.connect();

            let that = this;
            this.sock.on('connect', function() {
                console.log("Connected");
                that.emitEvent(ConnectionEvents.CONNECTED);
            });

            this.sock.on('error', function(error) {
                that.emitEvent(ConnectionEvents.CONNECTION_ERROR);
                that.connectionError();
            });
        }

        registerListen(messageType) {
            let that = this;
            this.sock.on(messageType, function(data) {
                ExternalListener.messageReceived(messageType, data, that.gameId);
            });
        }

        connectionError() {
            alert("Failed to join game, please try searching again.");
            window.location.replace(`${location.protocol}//${window.location.hostname}/`);
        }
    }

    return ClientConnection;
});