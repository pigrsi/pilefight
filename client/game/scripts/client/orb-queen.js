define(['util/util'], function(Util) {
class OrbQueen {
	constructor(viewer, world) {
		this.viewer = viewer;
		this.world = world;
		this.counter = 0;
	}
	
	start(orbs) {
		this.orbs = orbs;
		this.running = true;
	}
	
	update() {
		if (!this.currentOrb) {
			if (this.orbs.length <= 0) {
				this.reset();
				return true;
			}
			this.currentOrb = this.orbs.shift();
			this.itemTree = Util.getItemsStandingOnTree(this.currentOrb);
			if (this.treeContainsAPlayer(this.itemTree)) {
				this.orbState = 'PAUSE';
				this.viewer.suggestCameraFollow('ORB_POINTS', this.currentOrb, false);
			} else {
				this.currentOrb = false;
				return this.update();
			}
		}
		
		this.updateOrbPoints();
		
		return false;
	}
	
	treeContainsAPlayer(tree) {
		for (let i = 0; i < tree.length; ++i)
			if (tree[i].name === 'Monkey') return true;
		return false;
	}
	
	updateOrbPoints() {
		switch (this.orbState) {
			case 'PAUSE':
				if (this.counter++ >= 30) {
					this.changeState('CHECK_ITEMS_BELOW');
					this.treeIndex = 0;
					this.path = [];
				}
				break;
			case 'CHECK_ITEMS_BELOW':
				if (this.counter-- <= 0) {
					if (this.actCheckItemsBelow()) {
						this.currentOrb = null;
					}
				}
				break;
		}
	}
	
	actCheckItemsBelow() {
		if (this.path.length === 0 && this.treeIndex >= this.itemTree.length-1) {
			return true;
		}
		
		if (this.treeIndex === 0) {
			// Select the orb initially
			this.addToOrbPath(this.currentOrb);
			this.treeIndex++;
			this.counter = 5;
		} else if (this.itemIsStandingOn(this.lastInOrbPath(), this.itemTree[this.treeIndex])) {
			let item = this.itemTree[this.treeIndex];
			this.addToOrbPath(item);
			this.treeIndex++;
			this.counter = item.itemsStandingOn.length > 0 ? 5 : 15;
			if (item.name === 'Monkey') {
				this.counter = 45;
				item.addPoints((this.path.length - 1) * this.world.determinePointsFor('ORB'), false, true);
			}
		} else {
			// Backtrack
			this.popOrbPath();
			this.counter = 5;
		}
		
		return false;
	}
	
	itemIsStandingOn(item, standingOn) {
		return item.itemsStandingOn.indexOf(standingOn) !== -1;
	}
	
	addToOrbPath(item) {
		this.path.push(item);
		item.glowingFromOrb = true;
	}
	
	popOrbPath() {
		let item = this.path.pop();
		if (item) item.glowingFromOrb = false;
		return item;
	}
	
	lastInOrbPath() {return this.path[this.path.length-1];}
	
	nextOrb() {
		this.currentOrb = null;
		--this.orbs.length;
	}
	
	changeState(state) {
		this.orbState = state;
		this.counter = 0;
	}
	
	reset() {
		this.running = false;
		this.orbs = null;
		this.itemTree = null;
		this.counter = 0;
	}
}

return OrbQueen;
});