define(['client/game-status', 'shared/global-info'], function(GameStatus, GlobalInfo) {
var HtmlKing = {};

HtmlKing.playerContainer = document.getElementById('containerPlayers');

HtmlKing.init = function() {
    GameStatus.registerListener(HtmlKing.onGameStatusEvent);
    if (GlobalInfo.NEED_SAFETY) document.body.style = "background-color: #FFFFFF";
}

HtmlKing.onGameStatusEvent = function(eventName, args) {
    if (GlobalInfo.NEED_SAFETY) return;

    switch(eventName) {
        case 'CLIENTS_INFO_UPDATED':
            HtmlKing.updateClientBoxes(args[0]);
            break;
    }
}

HtmlKing.updateClientBoxes = function(clientsInfo) {
    for (var i = 0; i < clientsInfo.length; ++i) {
        let info = clientsInfo[i];
        let existingBox = document.getElementById(info.clientId);
        existingBox ? HtmlKing.updateClientBox(existingBox, info) : HtmlKing.createClientBox(info);
    }
}

HtmlKing.updateClientBox = function(clientBox, clientInfo) {
    var playersAliveDiv = clientBox.children[0];
    playersAliveDiv.innerHTML = '';
    for (var i = 0; i < clientInfo.numPlayersAlive; ++i)
        playersAliveDiv.innerHTML += '•';
    
    var statsDiv = clientBox.children[2];
    //statsDiv.innerHTML = 'Respawns: ' + clientInfo.lives;
    statsDiv.innerHTML = 'Points: ' + clientInfo.points;
}

HtmlKing.createClientBox = function(clientInfo) {
    var boxDiv = document.createElement('div');
    boxDiv.className = 'playerBox checkered';
    boxDiv.id = clientInfo.clientId;
    
    var playersAliveDiv = document.createElement('div');
    playersAliveDiv.className = 'playersAlive';
    playersAliveDiv.innerHTML = '';
    for (var i = 0; i < clientInfo.numPlayersAlive; ++i)
        playersAliveDiv.innerHTML += '•';

    var nickDiv = document.createElement('div');
    nickDiv.className = 'nickname';
    nickDiv.innerHTML = clientInfo.nickname;

    var statsDiv = document.createElement('div');
    statsDiv.className = 'statistic';
    if (!isNaN(clientInfo.lives))
        statsDiv.innerHTML = 'Respawns: ' + clientInfo.lives;

    boxDiv.appendChild(playersAliveDiv);
    boxDiv.appendChild(nickDiv);
    boxDiv.appendChild(statsDiv);
    HtmlKing.playerContainer.appendChild(boxDiv);
}

// HtmlKing.update = function() {
    // let clientsInfo = GameStatus.getClientsInfo();
    // for (var i = 0; i < clientsInfo.length; ++i) {
        // let info = clientsInfo[i];
        // if (!document.getElementById(info.clientId)) {
            // HtmlKing.createClientBox(info);
        // }
    // }
// }

    HtmlKing.safetyPlease = function() {
        document.getElementById('html').innerHTML = HtmlKing.DEBUG_initialBody;
    };

return HtmlKing;
});