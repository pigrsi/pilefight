define([], function() {
  class UserActions {
    constructor() {
      this.listeners = [];
    }

    registerListener(listener) {
      this.listeners.push(listener);
    }

    emit(action, args=[]) {
      for (let i = 0; i < this.listeners.length; ++i)
        this.listeners[i].onUserAction(action, args);
    }

    addLocalPlayer() {
      this.emit('add-local-player');
    }

    toggleReady() {
      this.emit('toggle-ready');
    }
  }

  return UserActions;
});