define([], function() {
GameStatus = {};
// {nickname: String, points: Int}
GameStatus.listenCallbacks = [];
GameStatus.playersInfo = [];
GameStatus.clientsInfo = [];
GameStatus.countdownInfo = {startTime: 0, lengthSeconds: 0};
GameStatus.rankingsInfo = [];
GameStatus.gameOver = false;
GameStatus.turnsLeftForCurrentPlayer = 0;

GameStatus.setPlayersInfo = function(info) {GameStatus.playersInfo = info;};
GameStatus.getPlayersInfo = function() {return GameStatus.playersInfo;};
GameStatus.setClientsInfo = function(info) {GameStatus.clientsInfo = info; GameStatus.onUpdatedClientsInfo();};
GameStatus.getClientsInfo = function() {return GameStatus.clientsInfo;};
GameStatus.setCountdownInfo = function(endTime) {
    GameStatus.countdownInfo.endTime = endTime;
};
GameStatus.getCountdownInfo = function() {return GameStatus.countdownInfo;};
    GameStatus.setRankingsInfo = function(rankingsInfo) {GameStatus.rankingsInfo = rankingsInfo; GameStatus.gameOver = true;};
    GameStatus.getRankingsInfo = function() {return GameStatus.rankingsInfo;};
    GameStatus.setTurnsLeftForCurrentPlayer = function(turns) {GameStatus.turnsLeftForCurrentPlayer = turns;};
    GameStatus.getTurnsLeftForCurrentPlayer = function() {return GameStatus.turnsLeftForCurrentPlayer;};

GameStatus.changeClientPoints = function(clientId, points) {
    for (let i = 0; i < GameStatus.clientsInfo.length; ++i) {
        if (GameStatus.clientsInfo[i].clientId === clientId) {
            GameStatus.clientsInfo[i].points = points;
            GameStatus.onUpdatedClientsInfo();
            return;
        }
    }
}

GameStatus.registerListener = function(eventCallback) {
    GameStatus.listenCallbacks.push(eventCallback);
};

GameStatus.onUpdatedClientsInfo = function() {
    GameStatus.emitEvent('CLIENTS_INFO_UPDATED', [GameStatus.clientsInfo]);
}

GameStatus.emitEvent = function(eventName, args) {
    for (var i = 0; i < GameStatus.listenCallbacks.length; ++i)
        GameStatus.listenCallbacks[i](eventName, args);
};

return GameStatus;
});