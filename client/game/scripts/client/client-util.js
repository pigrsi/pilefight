define([], function() {
    return {
        showElement(element, show=true) {
            element.hidden = !show;
        },

        showNicknameFormIfNecessary(nickname) {
            if (!nickname) {
                ClientUtil.showElement(document.getpElementById('setName'), true);
                return true;
            }
            return false;
        },

        getGameId() {
            if (this.roomIdIsLegit(window.location.search)) {
                return window.location.search.split('?')[1];
            } else {
                console.log("Could not join game. Room ID was invalid.");
                this.joinError();
            }
        },

        joinError() {
            alert("Failed to join game, please try searching again.");
            window.location.replace(`${location.protocol}//${window.location.hostname}/`);
        },

        isPrivateGame(gameId) {
            return gameId && gameId[0] === 's';
        },

        getCookie(name) {
            var value = "; " + document.cookie;
            var parts = value.split("; " + name + "=");
            if (parts.length == 2) return parts.pop().split(";").shift();
        },

        roomIdIsLegit(roomId) {
            if (!roomId.includes('?')) return false;
            if (roomId[2] !== 'e' && roomId[3] !== 'e') return false;
            return true;
        },

        getRandomName() {
            return defaultNames[Math.floor(Math.random() * defaultNames.length)];
        },

        getPortOffset(roomId) {
            let offsetString = '';
            for (let i = 1; i < roomId.length; ++i) {
                if (roomId[i] !== 'e')
                    offsetString += roomId[i];
                else
                    break;
            }
            result = parseInt(offsetString);
            if (result === NaN) console.log("Invalid port offset.");
            return result;
        }
    };
});