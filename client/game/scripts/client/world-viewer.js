define(['shared/physics', 'hud/hud', 'drawing/draw-king', 'drawing/animator-creator', 'util/util', 'hud/mini-cam', 'hud/camera', 'client/orb-queen', 'client/game-status'],
    function (Physics, Hud, DrawKing, AnimatorCreator, Util, MiniCam, Camera, OrbQueen, GameStatus) {
        var WorldViewer = {};
        var World;
        var WorldNetwork;
        var scene;
        WorldViewer.mapIsLoaded = false;
        WorldViewer.updateCounter = 0;
        WorldViewer.pausePeriod = 0;
        WorldViewer.items = [];
        WorldViewer.pointerPos = {x: 0, y: 0};

// called by scene
        WorldViewer.create = function (hudScene, world, worldNetwork) {
            scene = this;
            World = world;
            WorldNetwork = worldNetwork;
            WorldNetwork.registerListener(WorldViewer.onWorldNetworkEvent);
            World.registerListener(WorldViewer.onWorldEvent);
            WorldViewer.gameGraphics = scene.add.graphics();
            WorldViewer.graphicsFG = scene.add.graphics();
            WorldViewer.graphicsFG.depth = 30;
            WorldViewer.camera = new Camera(this.cameras.main, scene);
            WorldViewer.miniCam = new MiniCam(scene, hudScene, WorldViewer.camera);
            WorldViewer.orbQueen = new OrbQueen(WorldViewer, World);
            let objectMover = World.objectMover;
            objectMover.setExternalTweener(WorldViewer.addTween);
            objectMover.setFunctionToStopTweens(WorldViewer.forgetTweens);
            AnimatorCreator.init(WorldViewer, scene, WorldViewer.gameGraphics);

            DrawKing.init(world, scene, WorldViewer.graphicsFG, WorldViewer.gameGraphics);
        }

        WorldViewer.onMapLoaded = function () {
            WorldViewer.mapIsLoaded = true;

            WorldViewer.createTileset('template_big', 'grassy-big');
            WorldViewer.applyTileImageToTiles('tileset-template_big');

            scene.sound.add('dingdong').play();
        }

        WorldViewer.createTileset = function (tilesetName, imageName) {
            DrawKing.createTileset(scene, tilesetName, imageName, WorldNetwork.mapKing.getTilesetInfoByName(tilesetName));
        }

        WorldViewer.applyTileImageToTiles = function (tilesetName) {
            DrawKing.applyTileImageToTiles(scene, World.tiles, tilesetName);
        }

        WorldViewer.update = function (time, delta) {
            WorldViewer.gameGraphics.clear();
            Hud.clearGraphics();
            delta /= 1000;
            WorldViewer.tickCameraMovement(delta);
            Hud.draw();
            Input.update();
            //Hud.draw();
            WorldViewer.drawWorld.call(this);
            if (WorldViewer.pausePeriod-- <= 0) {
                WorldViewer.updateWorld(delta);
            }
            var i = 0;
            while (i < World.desiredSpeedUpDegree) {
                // Speed up the game!
                WorldViewer.updateWorld(delta);
                ++i;
            }

            if (WorldViewer.orbPointsAddedCallback) {
                if (WorldViewer.updateOrbQueen()) {
                    WorldViewer.orbPointsAddedCallback();
                    WorldViewer.orbPointsAddedCallback = null;
                }
            }
        }

        WorldViewer.updateOrbQueen = function () {
            if (!WorldViewer.orbQueen.running) {
                let items = World.getItems();
                let orbs = [];
                for (let i = 0; i < items.length; ++i)
                    if (items[i].name === 'Orb') orbs.push(items[i]);

                // Sort from left to right
                orbs = Util.insertionSort(orbs, function (a, b) {
                    return a.x - b.x;
                });
                WorldViewer.orbQueen.start(orbs);
            }

            return WorldViewer.orbQueen.update();
        }

        WorldViewer.updatePointer = function (pointerPos) {
            WorldViewer.pointerPos = pointerPos;
        }

        WorldViewer.tickCameraMovement = function (delta) {
            WorldViewer.camera.tickMovement(delta);
            if (WorldViewer.miniCam.isVisible()) {
                let hoveredOver = WorldViewer.miniCam.pointerIsOverCam(WorldViewer.pointerPos);
                WorldViewer.miniCam.update(delta, hoveredOver);
            }
            WorldViewer.checkMinicamFollow(World.items);
        }

        WorldViewer.checkMinicamFollow = function () {
            if (WorldViewer.miniCam.isFollowing()) return;
            for (let i = 0; i < World.items.length; ++i) {
                let item = World.items[i];
                if (!WorldViewer.miniCam.canStartFollowing(item)) continue;
                WorldViewer.miniCam.followItem(item);
                return;
            }
        }

        WorldViewer.stopCameraFollow = function () {
            WorldViewer.camera.stopFollowing();
        }

        WorldViewer.suggestCameraFollow = function (eventName, item, followAfterTween = true) {
            // for now just accepting every suggestion...
            let duration = 300;
            let mainCam = false;
            let duration0 = ['PLAYER_JUMPED', 'EXPLOSION_SPAWNED', 'ITEM_THROWN'];
            let duration200 = ['FORCIBLY_MOVED', 'MOVER_ADDED', 'BUMP', 'CRATE_SPAWNED', 'CRATE_OPENING',
                'PLAYER_DEATH', 'ITEM_BUBBLE_OPENED', 'PLAYER_TELEPORT', 'PLAYER_PICKUP', 'ITEM_FIRED', 'BLOOT_DROP'];
            let duration1000 = ['TURN_PLAYER_CHANGED', 'ORB_POINTS'];
            if (duration0.indexOf(eventName) !== -1) duration = 0;
            else if (duration200.indexOf(eventName) !== -1) duration = 200;
            else if (duration1000.indexOf(eventName) !== -1) duration = 1000;

            let onMainCam = ['ORB_POINTS', 'PLAYER_JUMPED', 'PLAYER_TELEPORT', 'FORCIBLY_MOVED', 'ITEM_THROWN', 'BLOOT_DROP', 'TURN_PLAYER_CHANGED', 'ITEM_BUBBLE_OPENED'];
            if (onMainCam.indexOf(eventName) !== -1) mainCam = true;
            if (mainCam) {
                WorldViewer.camera.followItem(item, followAfterTween, duration);
            } else if (WorldViewer.miniCam.canStartFollowing(item)) {
                WorldViewer.miniCam.followItem(item);
            }
        }

        WorldViewer.cameraIsFollowing = function (object) {
            return WorldViewer.camera.isFollowing(object);
        }

        WorldViewer.drawWorld = function () {
            if (!WorldViewer.mapIsLoaded && World.mapIsLoaded()) WorldViewer.onMapLoaded.call(this);
            WorldViewer.graphicsFG.clear();
            //WorldViewer.checkDestroyedTiles();
            WorldViewer.drawObjects.call(this);
            WorldViewer.drawOcean.call(this);
        }

        WorldViewer.drawObjects = function () {
            DrawKing.update();
        }

        WorldViewer.drawOcean = function () {
            let c = WorldViewer.camera;
            WorldViewer.graphicsFG.fillStyle(0x440066, 0.6);
            if (World.killY < c.cameraBottom)
                WorldViewer.graphicsFG.fillRect(c.cameraLeft - 64, World.killY, c.cameraWidth + 128, c.cameraBottom - World.killY + 64);
        }

        WorldViewer.forgetTweens = function () {
            scene.tweens.killAll();
        }

        WorldViewer.addTween = function (target, properties, values, configValues, callback = null, callbackArgs = null) {
            let tweenConfig = {
                targets: target,
                duration: configValues.duration,
                ease: configValues.ease,
                delay: configValues.delay,
                yoyo: configValues.yoyo,
                onComplete: callback,
                onCompleteParams: callbackArgs
            };
            for (var i = 0; i < properties.length; ++i) {
                tweenConfig[properties[i]] = values[i];
            }
            return scene.tweens.add(tweenConfig);
        }

        WorldViewer.updateWorld = function (delta) {
            // Physics is based around 60 ticks p/s.
            // But the rate you call the world game loop doesn't affect
            // the physics, so client framerate doesn't matter.
            WorldViewer.updateCounter += delta;
            while (WorldViewer.updateCounter >= 1 / World.TICK_RATE) {
                World.update();
                WorldViewer.updateCounter -= 1 / World.TICK_RATE;
            }
        }

        WorldViewer.onPointerUp = function () {
            return false;
        }

        WorldViewer.onPointerDown = function (pointerWorldPos, pointerWindowPos) {
            WorldViewer.camera.setVelocity(0, 0);
            if (WorldViewer.miniCam.isVisible() && WorldViewer.miniCam.pointerIsOverCam(pointerWindowPos)) {
                WorldViewer.camera.followItem(WorldViewer.miniCam.getFollowing(), true, 200);
                return true;
            }
            WorldViewer.checkPlayerSelected(pointerWorldPos);
            if (WorldNetwork.getTurnPlayerMove().moveType === 'THROW')
                WorldViewer.checkThrowItemSelected(pointerWorldPos);
            return true;
        }

        WorldViewer.checkPlayerSelected = function (pointerPos) {
            let turnPlayers = WorldNetwork.getTurnPlayersAllowed();
            for (var i = 0; i < turnPlayers.length; ++i) {
                let player = turnPlayers[i];
                if (WorldViewer.pointerIsOnItem(pointerPos, player) && player !== WorldNetwork.getTurnPlayer()) {
                    WorldNetwork.setTurnPlayer(player, true);
                    break;
                }
            }
        }

        WorldViewer.checkThrowItemSelected = function (pointerPos) {
            let throwables = WorldNetwork.getThrowableItems();
            for (var i = 0; i < throwables.length; ++i) {
                let item = throwables[i];
                if (WorldViewer.pointerIsOnItem(pointerPos, item)) {
                    WorldNetwork.selectItemToThrow(item);
                    break;
                }
            }
        }

        WorldViewer.pointerIsOnItem = function (pointerPos, item) {
            let circle = {x: item.x + item.halfWidth, y: item.y + item.halfHeight, radius: item.halfWidth + 4};
            return Util.pointInCircle(circle, pointerPos.x, pointerPos.y);
        }

        WorldViewer.onPointerDrag = function (pointerDeltaX, pointerDeltaY) {
            if (pointerDeltaX !== 0 && pointerDeltaY !== 0) WorldViewer.camera.stopFollowing();
            WorldViewer.camera.setVelocity(pointerDeltaX * 1.5, pointerDeltaY * 1.5);
        }

        WorldViewer.moveButtonPressed = function (moveType) {
            if (!World.allItemsAreStill()) return;
            switch (moveType) {
                case 'JUMP':
                case 'THROW':
                case 'SKIP':
                    WorldNetwork.changeMoveType(moveType);
            }
        }

        WorldViewer.zoomCameraTo = function (zoomTo) {
            WorldViewer.cameraControlPressed('SET_ZOOM', 0, 0, zoomTo);
        }

        WorldViewer.cameraControlPressed = function (cameraControlType, scrollX = 0, scrollY = 0, zoom = 1, zoomFocusX = 0, zoomFocusY = 0) {
            WorldViewer.camera.controlPressed(cameraControlType, scrollX, scrollY, zoom, zoomFocusX, zoomFocusY);
        }

        WorldViewer.onWorldNetworkEvent = function (eventName, args) {
            switch (eventName) {
                case 'TURN_PLAYER_CHANGED':
                    WorldViewer.suggestCameraFollow(eventName, args[0]);
                    break;
                case 'ADD_ORB_POINTS':
                    WorldViewer.orbPointsAddedCallback = args[0];
                    break;
                // case 'START_PERFORM_JUMP': case 'START_RESPAWN': WorldViewer.cameraFollow = WorldNetwork.getTurnPlayer();
                // break;
            }
        }

        WorldViewer.onWorldEvent = function (eventName, args) {
            switch (eventName) {
                case 'MOVER_ADDED':
                    // WorldViewer.suggestCameraFollow('MOVER_ADDED', args[0]);
                    break;
                case 'EXPLOSION_ADDED':
                    WorldViewer.shakeCamera(200);
                    break;
                case 'POINTS_ADDED':
                    GameStatus.changeClientPoints(args[0].clientId, args[0].clientPoints);
                  break;

            }
        }

        WorldViewer.shakeCamera = function (duration) {
            WorldViewer.camera.shake(duration);
        }

        WorldViewer.getCamera = function () {
            return WorldViewer.camera.getCamera();
        }

        WorldViewer.setCameraSize = function (width, height) {
            if (!WorldViewer.camera) return;
            WorldViewer.camera.setSize(width, height);
            WorldViewer.miniCam.resetBounds();
        }

        WorldViewer.getGameGraphics = function () {
            return WorldViewer.gameGraphics;
        }

        WorldViewer.pauseUpdatesFor = function (numTicks) {
            WorldViewer.pausePeriod = numTicks;
        }

        return WorldViewer;
    });
