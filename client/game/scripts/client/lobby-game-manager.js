define(['client/lobby', 'client/client-connection', 'client/connection-events', 'client/client-util', 'client/start'], function(Lobby, Connection, ConnectionEvents, ClientUtil, Start) {
   class LobbyGameManager {
       constructor(obj) {
           this.nickname = obj.nickname;
           this.gameId = obj.gameId;
           this.connection = new Connection({port: obj.port, gameId: obj.gameId});
           this.lobbyElement = obj.lobbyElement;
           this.gameElement = obj.gameElement;
           this.localGame = obj.localGame;
           Lobby.registerListener(this.onLobbyEvent.bind(this));
       }

       init() {
           this.connection.registerListener(this.onConnectionEvent.bind(this));
           this.connection.connect();
           this.connection.registerListen('SIGNAL');
           this.connection.registerListen('LOBBY_COMPLETE');
       }

       onConnectionEvent(eventType) {
           switch (eventType) {
               case ConnectionEvents.CONNECTED:
                   this.onConnection();
                   break;
           }
       }

       onLobbyEvent(event) {
           switch (event.type) {
               case "HOST_START":
                   this.connection.emit('START_PRIVATE', event.data.gameSettings, this.gameId);
                   this.startGame();
                   break;
               case "START_SPECTATE":
                   this.startGame();
                   break;
           }
       }

       showGameElement() {
           ClientUtil.showElement(this.lobbyElement, false);
           ClientUtil.showElement(this.gameElement, true);
       }

       startGame() {
           this.showGameElement();
           if (!Start.hasStarted()) Start.start(this.localGame, 'POINTS', this.nickname, false, this.localGame ? function(){} : this.onGameComplete.bind(this));
           else Start.resetGameMode('POINTS', this.nickname);
       }

       emit(messageType, data) {
           this.connection.emit(messageType, data);
       }

       onSignal(signal) {
           Lobby.onSignal(signal);
       }

       onLobbyComplete() {
           this.startGame();
       }

       onGameComplete() {
            console.log("Game complete");
       }

       onConnection() {
           this.connection.emit("CONNECTED", {nickname: this.nickname});

           ClientUtil.showElement(this.lobbyElement, true);
           Lobby.showLobbyElements(false);
           Lobby.init();
       }
   }

   return LobbyGameManager;
});