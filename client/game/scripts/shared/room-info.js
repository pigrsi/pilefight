define([], function() {
class RoomInfo {
	constructor(roomId, privateRoom, gameType, minClients, maxClients, livesPer, playersPer, secondsPerTurn, localGame, readyCountdown=20) {
		this.roomId = roomId;
		this.privateRoom = privateRoom;
		this.gameType = gameType;
		this.minClients = minClients;
		this.maxClients = maxClients;
		this.livesPer = livesPer;
		this.playersPer = playersPer;
		this.secondsPerTurn = secondsPerTurn;
		this.readyCountdown = readyCountdown;
		this.localGame = localGame;
		
		this.numCurrentClients = 0;
		this.started = false;
		this.finished = false;
		this.joinable = true;
	}
	
	addListener(listenerFunction) {
		if (!this.listenerFunctions) this.listenerFunctions = [];
		this.listenerFunctions.push(listenerFunction);
	}
	
	sendEvent(type) {
		if (!this.listenerFunctions) return;
		let event = {type: type, roomId: this.roomId};
		switch (type) {
			case 'SET_JOINABLE':
				event.joinable = this.joinable;
				break;
		}

		for (var i = 0; i < this.listenerFunctions.length; ++i) this.listenerFunctions[i](event);
	}
	
	setGameType(val) {this.gameType = val;}
	setMinClients(val) {this.minClients = val;}
	setMaxClients(val) {this.maxClients = val;}
	setLivesPerClient(val) {this.livesPer = val;}
	setPlayersPerClient(val) {this.playersPer = val;}
	setNumberOfCurrentClients(val) {this.numCurrentClients = val; if (val === 0) this.sendEvent("NO_CLIENTS");}
	incrementNumClients() { this.setNumberOfCurrentClients(this.numCurrentClients + 1); }
	decrementNumClients() { this.setNumberOfCurrentClients(this.numCurrentClients - 1); }
	setGameStarted(val) {this.started = val;}
	setGameFinished(val) {this.finished = val;}
	setJoinable(val) {this.joinable = val; this.sendEvent('SET_JOINABLE');}
	setSecondsPerTurn(val) {this.secondsPerTurn = val;}
	setReadyCountdown(val) {this.readyCountdown = val;}
	readyToDestroyRoom() {this.sendEvent('DESTROY_ROOM');}
}

return RoomInfo;
});