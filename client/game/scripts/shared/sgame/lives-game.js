define(['sgame/game-base', 'shared/global-info'], function(GameBase, GlobalInfo) {
class LivesGame extends GameBase {
    constructor(serverMaster, roomInfo, logger) {
        super(serverMaster, roomInfo, logger);
        this.serverMaster.init(this.roomInfo.playersPer);
        this.rankings = [];
    }
    
    actGameStart() {
        let clientIds = this.serverMaster.getClientsList();
        for (var i = 0; i < clientIds.length; ++i)
            this.serverMaster.getClientInfoMap()[clientIds[i]].lives = this.roomInfo.livesPer;
        this.serverMaster.setFirstTurnPlayer();
		this.serverMaster.moveEndCleanUp();
        super.actGameStart();
    }

    actMoveEnd() {
        this.updateRankings();
        this.serverMaster.moveEndCleanUp();
        this.changeState('DETERMINE_TURN');
    }
    
    actDetermineNextTurn() {
        let movesUsed = this.serverMaster.getMovesUsedByTurnPlayer();
        let turnIsComplete = movesUsed.numMovesUsed >= 2 || movesUsed.usedSkip || this.serverMaster.turnPlayerIsDead();
        turnIsComplete = turnIsComplete || (movesUsed.usedRespawn && !this.serverMaster.turnPlayerCanThrow());
        if (turnIsComplete) {
            this.serverMaster.cycleToNextClientTurn();
            this.turnNumber++;
        }
        
		
        // if (this.isGameOver()) {
            // // There could be no players left if multiple died at the same time
            // let lastAlive = this.getLastStandingClient();
            // if (lastAlive) this.rankings.unshift(lastAlive);
            // this.serverMaster.sendSyncForGameEnd(this.rankings);
            // this.changeState('GAME_OVER');
            // return;
        // }
        
        // if (turnIsComplete && this.turnNumber % 3 === 0)
            // this.spawnCrates(1);
        
        // Decide next client turn
        let loops = 0;
        while (turnIsComplete) {
            if (!this.clientIsOut(this.serverMaster.turnClientId)) {
                break;
            } else
                this.serverMaster.cycleToNextClientTurn();
            if (loops++ > 100) this.logger.error("LivesGame: INFINITE LOOP IN LIVES GAME?!");
        }
        
        // Decide which player is next
        let moveType = 'DECIDE_STANDARD_MOVE';
        while (turnIsComplete) {
            this.serverMaster.cyclePlayerOfTurnClient();
            if (this.serverMaster.turnPlayerIsDead()) {
                if (this.serverMaster.clientHasAnyLivesLeft(this.serverMaster.turnClientId)) {
                    moveType = 'DECIDE_RESPAWN_MOVE';
                    this.serverMaster.decrementClientLives(this.serverMaster.turnClientId);
                    break;
                }
            } else break;
        }
        
        // Determine which players can be moved this turn
        let turnId = this.serverMaster.getTurnPlayer().id;
        let playerIds = [turnId];
        if (moveType === 'DECIDE_STANDARD_MOVE') {
            let ids = this.serverMaster.getPlayersOfClient(this.serverMaster.turnClientId);
            for (var i = 0; i < ids.length; ++i) {
                if (ids[i] !== turnId && !this.serverMaster.playerIsDead(ids[i])) playerIds.push(ids[i]);
            }
        }
        
        this.serverMaster.sendSyncForNextTurn(playerIds, moveType, Date.now() + this.roomInfo.secondsPerTurn * 1000);
		
		if (moveType === 'DECIDE_RESPAWN_MOVE') {
			this.serverMaster.nextMoveIsRespawn();
            if (this.isRemoteGame()) this.serverMaster.prepareWorldForRespawn();
		}
        this.changeState('AWAIT_MOVE');
    }
    
    updateRankings() {
        let deadPlayers = this.serverMaster.getRecentlyKilledPlayers();
        for (var i = 0; i < deadPlayers.length; ++i) {
            let clientId = this.serverMaster.getClientOwnerOfPlayerById(deadPlayers[i].id);
            if (this.clientIsOut(clientId) && this.rankings.indexOf(clientId === -1)) {
                this.rankings.unshift(clientId);
            }
        }
    }
    
    isGameOver() {
        let clientIds = this.serverMaster.getClientsList();
        let activePlayers = 0;
        for (var i = 0; i < clientIds.length; ++i) {
            let id = clientIds[i];
            if (!this.clientIsOut(id))
                activePlayers++;
            if (activePlayers >= 2) return false;
        }
        return true;
    }
    
    getLastStandingClient() {
        let clientIds = this.serverMaster.getClientsList();
        for (var i = 0; i < clientIds.length; ++i) {
            let id = clientIds[i];
            if (!this.clientIsOut(id)) return id;
        }
        return null;
    }
    
    clientIsOut(id) {
        return !this.serverMaster.clientHasAnyPlayersAlive(id) && !this.serverMaster.clientHasAnyLivesLeft(id);
    }
}
return LivesGame;
});