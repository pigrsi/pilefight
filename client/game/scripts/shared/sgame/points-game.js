define(['sgame/game-base'], function (GameBase) {
  class PointsGame extends GameBase {
    constructor(serverMaster, roomInfo, logger) {
      super(serverMaster, roomInfo, logger);
      this.serverMaster.init(this.roomInfo.playersPer);
      this.pointsGoal = 0;
      this.serverMaster.addGameInfo('pointsGoal', this.pointsGoal);
    }

    actGameStart() {
      let clientIds = this.serverMaster.getClientsList();
      // Client points
      for (var i = 0; i < clientIds.length; ++i)
        this.serverMaster.getClientInfoMap()[clientIds[i]].points = 0;
      this.serverMaster.spawnRandomGroundItems(3);
      this.serverMaster.setFirstTurnPlayer();
      this.serverMaster.moveEndCleanUp();
      super.actGameStart();
    }

    actMoveEnd() {
      // Check if game is over
      if (this.isGameOver()) {
        let rankings = this.calculateRankings();
        this.serverMaster.sendSyncForGameEnd(rankings);
        this.changeState('GAME_OVER');
        return;
      }

      // Is it time to switch clients?
      let movesUsed = this.serverMaster.getMovesUsedByTurnPlayer();
      this.turnIsComplete = movesUsed.numMovesUsed >= 3 || movesUsed.usedSkip || this.serverMaster.turnPlayerIsDead();
      this.turnIsComplete = this.turnIsComplete || (movesUsed.usedRespawn /*&& !this.serverMaster.turnPlayerCanThrow()*/);

      // Work out points
      // Lose points for dying
      // let deadPlayers = this.serverMaster.getRecentlyKilledPlayers();
      // let turnPlayer = this.serverMaster.getTurnPlayer();
      // let numFriendlyKills = 0;
      // for (var i = 0; i < deadPlayers.length; ++i) {
      //   deadPlayers[i].addPoints(-1, true);
      //   if (this.serverMaster.playersBelongToSameClient(turnPlayer.id, deadPlayers[i].id))
      //     numFriendlyKills++;
      // }
      // // Gain points for killing another client's players
      // turnPlayer.addPoints(deadPlayers.length - numFriendlyKills, true);

      // Add orb when a player finishes all their turns
      if (this.turnIsComplete) {
        this.serverMaster.sendSignalToOrbCheck();
        this.serverMaster.addOrbPoints();
        this.spawnItems();
      }

      this.serverMaster.updateClientPoints();
      this.serverMaster.moveEndCleanUp();
      this.changeState('DETERMINE_TURN');
    }

    spawnItems() {
      this.serverMaster.spawnRandomGroundItems(8);
    }

    actDetermineNextTurn() {
      let nextClientId = this.serverMaster.getTurnClientId();
      if (this.turnIsComplete) {
        nextClientId = this.serverMaster.getNextClientTurn();
        this.serverMaster.setNextClientTurn(nextClientId);
        this.turnNumber++;
      }

      // if (this.isGameOver()) {
      //   let rankings = this.calculateRankings();
      //   this.serverMaster.sendSyncForGameEnd(rankings);
      //   this.changeState('GAME_OVER');
      //   return;
      // }

      // Decide which player is next
      let playerIdsAllowed = [];
      let moveType = 'DECIDE_STANDARD_MOVE';
      if (this.turnIsComplete) {
        let nextPlayerId = this.serverMaster.cyclePlayerOfClient(nextClientId);
        if (this.isRemoteGame()) this.serverMaster.setTurnPlayer(nextPlayerId);
        this.serverMaster.resetMovesUsed();
        if (this.serverMaster.playerIsDead(nextPlayerId)) {
          moveType = 'DECIDE_RESPAWN_MOVE';
          playerIdsAllowed.push(nextPlayerId);
        }
      }

      // Determine which players can be moved this turn
      if (moveType === 'DECIDE_STANDARD_MOVE') {
        let ids = this.serverMaster.getPlayersOfClient(nextClientId);
        for (var i = 0; i < ids.length; ++i) {
          if (!this.serverMaster.playerIsDead(ids[i])) playerIdsAllowed.push(ids[i]);
        }
      }

      this.serverMaster.sendSyncForNextTurn(playerIdsAllowed, moveType, Date.now() + this.roomInfo.secondsPerTurn * 1000);

      if (moveType === 'DECIDE_RESPAWN_MOVE') {
        this.serverMaster.nextMoveIsRespawn();
        if (this.isRemoteGame()) this.serverMaster.prepareWorldForRespawn();
        this.serverMaster.setRandomRespawnPoint();
      }

      if (this.isRemoteGame()) this.serverMaster.checkGoalholesActive(this.pointsGoal);

      this.changeState('AWAIT_MOVE');
    }

    isGameOver() {
      return this.serverMaster.anyGoalholeSatisfied();
    }

    calculateRankings() {
      let pointsOrder = this.serverMaster.getClientIdsOrderedByPoints();
      let turnPlayerClientId = this.serverMaster.getTurnClientId();
      // Turn client must be on top as they satisfied the goalhole
      let tpIndex = pointsOrder.indexOf(turnPlayerClientId);
      if (tpIndex !== -1) pointsOrder.splice(tpIndex, 1);
      pointsOrder.unshift(turnPlayerClientId);
      return pointsOrder;
    }
  }

  return PointsGame;
});