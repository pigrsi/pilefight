const MAPS = ['generic'];

define(['shared/global-info', 'util/countdown-timer'], function(GlobalInfo, CountdownTimer) {
class GameBase {
    constructor(serverMaster, roomInfo, logger) {
		this.serverMaster = serverMaster;
		this.roomInfo = roomInfo;
		this.logger = logger;
		GlobalInfo.GAME_MODE = this.roomInfo.gameType;
        this.state = 'INIT_WORLD';
		this.CAN_SPAWN_CRATES = false;
        this.turnNumber = 1;
        this.countdown = new CountdownTimer();
		this.counter = 0;
        this.previousNumClients = 0;
    }

    update() {
        switch (this.state) {
            case 'INIT_WORLD': this.actInitWorld(); break;
            case 'WAIT_CLIENTS': this.actWaitForClients(); break;
            case 'GAME_START': this.actGameStart(); break;
            case 'MOVE_HAS_ENDED': this.actMoveEnd(); break;
            case 'DETERMINE_TURN': this.actDetermineNextTurn(); break;
            case 'AWAIT_MOVE': this.actAwaitPlayerMove(); break;
            case 'PERFORMING_MOVE': this.actPerformingMove(); break;
            case 'GAME_OVER': this.actGameOver(); break;
            default: break;
        }
    }

    onClientDisconnection(clientId) {
        let smallId = this.serverMaster.getSmallClientId(clientId);
        this.serverMaster.sendFreeSignal({signal: 'CLIENT_DISCONNECT', clientId: smallId});
        this.serverMaster.flagClientToBeRemoved(clientId);
        if (this.state === 'PERFORMING_MOVE') return;
        if (this.state === 'AWAIT_MOVE') {
            if (this.serverMaster.getTurnClientId() === clientId) {this.serverMaster.forceSkipTurn();}
            return;
        }

        this.serverMaster.removeFlaggedClients();
    }
        
    actInitWorld() {
		let mapName = MAPS[Math.floor(Math.random() * MAPS.length)];
        this.serverMaster.addGameInfo('mapName', mapName);
        this.serverMaster.generateWorld(mapName);
        this.serverMaster.setRandomRespawnPoint();
		this.countdown.stop();
        this.changeState('WAIT_CLIENTS');
    }
    
    actWaitForClients() {
		let numClients = this.serverMaster.getNumberOfClients();
		let numReadyClients = this.serverMaster.getNumberOfReadyClients();
		if (numReadyClients <= 0) {
		    if (this.countdown.isRunning()) {
		        this.countdown.stop();
            }
        } else {
            // At least 1 client ready
            if (numClients >= 2 && numClients === numReadyClients) {
                this.startGame();
                return;
            }

            if (this.countdown.isRunning()) {
                if (numClients > this.previousNumClients) {
                    if (this.countdown.getSecondsRemaining() <= 15) {
                        this.logger.debug('GameBase: Adding 15 seconds to start countdown.');
                        this.countdown.reset(15);
                    }
                } else if (this.countdown.getSecondsRemaining() <= 0) {
                    this.startGame();
                    return;
                }
            } else {
                this.countdown.reset(this.roomInfo.readyCountdown);
            }
        }

        this.previousNumClients = numClients;
    }

    startGame() {
        this.countdown.stop();
        this.changeState('GAME_START');
        this.logger.info('GameBase: Starting game ' + this.roomInfo.roomId);
    }
	
	clientCountHasJustExceededMinimum() {
		return this.serverMaster.getNumberOfClients() >= this.roomInfo.minClients && this.roomInfo.numCurrentClients < this.roomInfo.minClients;
	}
    
    actGameStart() {
		this.roomInfo.setGameStarted(true);
		this.roomInfo.setJoinable(false);
        this.changeState('DETERMINE_TURN');
    }
    
    actAwaitPlayerMove() {
        if (!this.countdown.isRunning()) this.countdown.reset(this.roomInfo.secondsPerTurn);
        if (this.countdown.getSecondsRemaining() <= 0)
            this.serverMaster.forceSkipTurn();
        
        if (this.serverMaster.hasReceivedMoveFromClient()) {
			let move = this.serverMaster.getMoveReceivedFromClient();
			move = this.analyseMove(move);
            this.serverMaster.sendSyncForPerformMove(move);
            if (this.isRemoteGame()) this.serverMaster.performMoveReceived(move);
            this.serverMaster.updateMovesUsed(move);
            this.changeState('PERFORMING_MOVE');
            this.countdown.stop();
        }
    }
	
	analyseMove(move) {
		// Check for pebble throw
		if (move && move.moveType === 'THROW' && move.playerId === move.targetId) {
			let pebble = this.serverMaster.spawnItem('Pebble');
			pebble.disable(true);
			move.targetId = pebble.id;
		}
		return move;
	}
    
    actPerformingMove() {
		if (this.counter-- <= 0) {
			if (this.serverMaster.turnHasEnded()) 
				this.changeState('MOVE_HAS_ENDED');
			else
				this.counter = 30;
		}
    }

    actMoveEnd() {}
    actDetermineNextTurn() {}
    actGameOver() {
		this.roomInfo.setGameFinished(true);
        this.roomInfo.setJoinable(true);
	}
    
    changeState(state) {this.state = state;}
    isRemoteGame() {return !this.roomInfo.localGame;}

}
return GameBase;
});