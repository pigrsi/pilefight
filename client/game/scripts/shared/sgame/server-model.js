define(['util/util'], function (Util) {
    class ServerModel {
        constructor(logger) {
            this.logger = logger;
            this.clientInfoMap = {};
            this.turnClientId = null;
            this.nextSyncNumber = 0;
            this.mapName = null;
            this.numClientsInGame = 0;
            this.numPlayersEach = 1;
            this.nextMoveRespawn = false;
            this.moveReceivedFromClient = null;
            // with them. Remote client stuff is somewhere else.
            // Stores {id, name}
            // These client IDs are only used to refer to clients, not to communicate
            this.nextSmallClientId = 0;
            this.clientsToRemove = [];
            this.gameInfo = {};
        }

        cyclePlayerOfClient(clientId) {
            let clientInfo = this.getClientInfo(clientId);
            if (!clientInfo) return;
            clientInfo.turnCycle = (clientInfo.turnCycle + 1) % clientInfo.playerIds.length;
            return clientInfo.playerIds[clientInfo.turnCycle];
        }

        getNextClientTurn() {
            let clientIds = this.getClientsList();
            let this_ = this;
            var currentTurnIndex = clientIds.findIndex(function(id) {
                return id === this_.getTurnClientId();
            });
            return clientIds[(currentTurnIndex+1)%clientIds.length];
        }

        checkAtLeastXClientsRemaining(numRequired=0) {
            let numClientsLeft = 0;
            let clientIds = this.getClientsList();
            for (var i = 0; i < clientIds.length; ++i) {
                if (this.getNumPlayersAlive(clientId) > 0) {
                    if (++numClientsLeft >= numRequired) return true;
                }
            }
            return false;
        }

        playersBelongToSameClient(id1, id2) {
            let clientIds = this.getClientsList();
            for (var i = 0; i < clientIds.length; ++i) {
                let players = this.getPlayerIds(clientIds[i]);
                if (players.includes(id1)) return players.includes(id2);
                if (players.includes(id2)) return players.includes(id1);
            }
            return false;
        }

        getClientOwnerOfPlayerById(playerId) {
            let clientIds = this.getClientsList();
            for (var i = 0; i < clientIds.length; ++i) {
                let players = this.getPlayerIds(clientIds[i]);
                if (players.includes(playerId)) return clientIds[i];
            }
            return null;
        }

        popClientToBeRemoved() {
            return this.clientsToRemove.length > 0 ? this.clientsToRemove.pop() : null;
        }

        packClientsInfo() {
            let clientsInfo = [];
            let clientIds = this.getClientsList();
            for (var i = 0; i < clientIds.length; ++i) {
                let clientId = this.getSmallClientId(clientIds[i]);
                let playerIds = this.getPlayerIds(clientIds[i]);
                let nickname = this.getNickname(clientIds[i]);
                let points = this.getPoints(clientIds[i]);
                let lives = this.getLives(clientIds[i]);
                let ready = this.getReadyStatus(clientIds[i]);
                clientsInfo.push({clientId: clientId, playerIds: playerIds.slice(), nickname: nickname, points: points, lives: lives, ready: ready});
            }
            return clientsInfo;
        }

        addNewClientInfo(clientId, clientSmallId, clientNickname) {
            if (!!this.getClientInfo(clientId)) return;
            this.incrementNumClients();
            if (!this.getTurnClientId()) this.setTurnClientId(clientId);
            this.clientInfoMap[clientId] = {clientId: clientSmallId, playerIds: [], nickname: clientNickname, turnCycle: 0, numPlayers: this.getNumPlayersEach(), ready: false};
        }

        getNextSyncNumber() {return this.nextSyncNumber++;}
        getNextSmallClientId() {return this.nextSmallClientId++;}

        clientMeetsPoints(clientId, points=0) {
            return this.getPoints(clientId) >= points;
        }
        anyClientMeetsPoints(points=0) {
            let clientIds = this.getClientsList();
            for (let i = 0; i < clientIds.length; ++i) {
                if (this.clientMeetsPoints(clientIds[i], points)) return true;
            }
            return false;
        }
        turnClientMeetsPoints(points=0) {
            return this.clientMeetsPoints(this.getTurnClientId(), points);
        }

        /******************************************************************/
        /*************************** Updates ******************************/
        /******************************************************************/
        setClientAttribute(clientId, attribute, value) {
            this.logger.debug(`ServerModel: Updating attribute ${attribute} of client ${clientId} with value ${value}`);
            let info = this.getClientInfo(clientId);
            if (info) info[attribute] = value;
        }
        setMapName(name) {
            this.mapName = name;
            this.logger.info(`ServerModel: Map set to ${name}`);
        }
        updateAttributeFromData(data, attributeName) {
            for (let i = 0; i < data.length; ++i) {
                let info = data[i];
                this.setClientAttribute(info.clientId, attributeName, info[attributeName]);
            }
        }
        addPlayerId(clientId, playerId) {
            let playerIds = this.getPlayerIds(clientId);
            if (playerIds) playerIds.push(playerId);
        }
        toggleReady(clientId) {
            this.setClientAttribute(clientId, 'ready', !this.getReadyStatus(clientId));
        }
        setEveryClientAsReady() {
            let clientIds = this.getClientsList();
            for (let i = 0; i < clientIds.length; ++i) {
                this.setClientAttribute(clientIds[i], 'ready', true);
            }
        }
        // Don't remove 'unused' functions, they are used
        updateNumPlayersAlive(numPlayersAliveData) {this.updateAttributeFromData(numPlayersAliveData, 'numPlayersAlive');}
        addGameInfo(property, value) {this.gameInfo[property] = value;}
        setGameInfo(gameInfo) {this.gameInfo = gameInfo;}
        updateClientPoints(pointsData) {this.updateAttributeFromData(pointsData, 'points');}
        setNumPlayersEach(numPlayers) {this.numPlayersEach = numPlayers;}
        setNextMoveRespawn(isRe) {this.nextMoveRespawn = isRe;}
        incrementNumClients() {this.numClientsInGame++;}
        decrementNumClients() {this.numClientsInGame = Math.max(this.numClientsInGame-1, 0);}
        flagClientToBeRemoved(clientId) {this.clientsToRemove.push(clientId);}
        setMoveReceivedFromClient(move) {this.moveReceivedFromClient = move;}
        setTurnClientId(id) {this.turnClientId = id;}
        removeClientFromModel(clientId) {delete this.clientInfoMap[clientId];}

        /******************************************************************/
        /************************** Retrievals ****************************/
        /******************************************************************/
        getPlayerIdsOfTurnClient() {
            return this.getPlayerIds(this.turnClientId);
        }
        getPlayerIds(clientId) {return this.getClientAttribute(clientId, 'playerIds');}
        getNumPlayersAlive(clientId) {return this.getClientAttribute(clientId, 'numPlayersAlive');}
        getSmallClientId(clientId) {return this.getClientAttribute(clientId, 'clientId');}
        getNickname(clientId) {return this.getClientAttribute(clientId, 'nickname');}
        getPoints(clientId) {return this.getClientAttribute(clientId, 'points');}
        getLives(clientId) {return this.getClientAttribute(clientId, 'lives');}
        getReadyStatus(clientId) {return this.getClientAttribute(clientId, 'ready');}

        getClientAttribute(clientId, attribute) {
            this.logger.debug(`ServerModel: Getting attribute ${attribute} from client ${clientId}`);
            let clientInfo = this.getClientInfo(clientId);
            if (clientInfo) return clientInfo[attribute];
            return null;
        }
        getClientInfo(clientId) {
            let clientInfo = this.clientInfoMap[clientId];
            if (!clientInfo) {
                this.logger.error(`ServerModel: Client ${clientId} does not have any client info!`);
                return null;
            }
            return clientInfo;
        }
        getClientsList() {
            let clients = [];
            for (var clientId in this.clientInfoMap) {
                if (this.clientInfoMap.hasOwnProperty(clientId))
                    clients.push(clientId);
            }
            return clients;
        }
        getNumReadyClients() {
            let counter = 0;
            let clientIds = this.getClientsList();
            for (let i = 0; i < clientIds.length; ++i) {
                if (this.getReadyStatus(clientIds[i])) ++counter;
            }
            return counter;
        }
        getClientIdsOrderedByPoints() {
            let poinClients = [];
            let clients = this.getClientsList();
            for (let i = 0; i < clients.length; ++i) {
                poinClients.push({id: clients[i], points: this.getPoints(clients[i])});
            }
            poinClients = Util.insertionSort(poinClients, function(a, b) {return b.points - a.points;});
            // Make list of just IDs
            for (let i = 0; i < poinClients.length; ++i) {
                poinClients[i] = poinClients[i].id;
            }
            return poinClients;
        }
        getClientInfoMap() {return this.clientInfoMap;}
        getMapName() {return this.mapName;}
        getTurnClientId() {return this.turnClientId;}
        getNumClientsInGame() {return this.numClientsInGame;}
        getNumPlayersEach() {return this.numPlayersEach;}
        nextMoveIsRespawn() {return this.nextMoveRespawn;}
        getClientsToBeRemoved() {return this.clientsToRemove;}
        getMoveReceivedFromClient() {return this.moveReceivedFromClient;}
        getGameInfo() {return this.gameInfo;}
    }

    return ServerModel;
});