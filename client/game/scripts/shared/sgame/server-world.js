define(['shared/world-network'], function(WorldNetwork) {
class ServerWorld {
	constructor(worldNetwork) {
		this.worldNetwork = worldNetwork;
		this.movesUsed = {usedJump: false, usedThrow: false, usedSkip: false, usedRespawn: false, numMovesUsed: 0};
	}
	
	init(mapName) {
		// Set up game - tell client what should be spawned in initial sync
		this.generateWorld(mapName);
	}
	
	generateWorld(mapName) {
		this.worldNetwork.serverCreate(mapName);
		this.worldNetwork.spawnMapItems();
		this.worldNetwork.spawnMapObjectsFromMap();
	}

	getDestroyedTileIds() {
		return this.worldNetwork.getDestroyedTileIds();
	}

	worldIsReady() {
		return this.worldNetwork.worldIsReady();
	}

	packPlayersInfo() {
		let playersInfo = this.worldNetwork.getPlayersInfo();
		let infoToSend = [];
		for (var i = 0; i < playersInfo.length; ++i)
			infoToSend.push({id: playersInfo[i].player.id, nickname: playersInfo[i].nickname, clientId: playersInfo[i].clientId});
		return infoToSend;
	}

	packItems() {
		let items = this.worldNetwork.getItems();
		let itemData = [];
		for (var i = 0; i < items.length; ++i) {
			let item = items[i];
			let data = {name: item.name, x: item.x, y: item.y, statuses: item.statuses, statusEffects: item.getStatusEffects(), disabled: item.disabled, id: item.id};
			itemData.push(data);
		}
		return itemData;
	}

	packGroundItems() {
		let groundItems = this.worldNetwork.getGroundItems();
		let itemData = [];
		for (var i = 0; i < groundItems.length; ++i) {
			let item = groundItems[i];
			let data = {name: item.name, id: item.id, tileId: item.tileContainingId};
			itemData.push(data);
		}
		return itemData;
	}

	setFirstTurnPlayerId(playerId) {
		this.worldNetwork.setTurnPlayerById(playerId);
	}

	getLastGeneratedItemId() {
		return this.worldNetwork.getLastGeneratedItemId();
	}

	gameHasCommenced() {return false;}

	addPlayer(name, smallClientId) {return this.worldNetwork.addPlayer(name, smallClientId);}

	removePlayers(playerIds) {
		for (let i = 0; i < playerIds.length; ++i)
			this.worldNetwork.removePlayer(playerIds[i]);
	}

	removeDeadItems() {
		this.worldNetwork.removeDeadItems();
	}

	killItems(itemIds) {
		for (var i = 0; i < itemIds.length; ++i) {
			let item = this.worldNetwork.getItemById(itemIds[i]);
			if (item) this.worldNetwork.killItem(item);
		}
	}

	getDeadPlayers() {
		return this.worldNetwork.getDeadPlayers();
	}

	getRecentlyKilledPlayers() {
		return this.worldNetwork.getRecentlyKilledPlayers();
	}

	getRespawnIndex() {
		return this.worldNetwork.getRespawnIndex();
	}

	createAction(playerIds=[], type='', launchVelocity={x:0,y:0}, targetId, countdownEndTime) {
		return {ids: playerIds, actionType: type, launchVelocity: launchVelocity, targetId: targetId, countdownEndTime: countdownEndTime};
	}

	createGameOverAction(clientRankings) {
		return {actionType: 'GAME_OVER', rankings: clientRankings};
	}

	getPlayers() {return this.worldNetwork.getPlayersInfo()};

	getNumPlayers() {return this.worldNetwork.getPlayersInfo().length;}

	moveIsLegit(moveInfo) {
		// Does this move make sense and is it within the boundaries?
		return true;
	}

	readyToStartNextTurn() {
		this.worldNetwork.checkArenaIsCalm();
		return this.worldNetwork.arenaIsCompletelyCalm();
	}

	turnPlayerIsDead() {
		return this.worldNetwork.getTurnPlayer().disabled;
	}

	playerIsDead(id) {
		let player = this.worldNetwork.getItemById(id);
		return player ? (player.disabled || !player.alive) : true;
	}

	moveEndCleanUp() {
		if (this.worldNetwork.turnPlayerDidTeleportPickup) this.movesUsed.usedThrow = false;
		this.worldNetwork.onMoveEnd();
	}

	getCurrentTurnPlayerId() {return this.worldNetwork.getTurnPlayer() ? this.worldNetwork.getTurnPlayer().id : -1;}

	playerIsAlive(playerId) {return this.worldNetwork.playerIsAlive(playerId);}

	cycleToNextPlayerTurn() {
		this.worldNetwork.cycleToNextPlayerTurn();
		this.resetMovesUsed();
	}

	setNextTurnPlayer(playerId) {
		this.worldNetwork.setTurnPlayerById(playerId);
	}

	updateMovesUsed(moveType, incrementNumMoves=true) {
		switch (moveType) {
			case 'JUMP':
				this.movesUsed.usedJump = true; break;
			case 'THROW':
				this.movesUsed.usedThrow = true; break;
			case 'RESPAWN':
				this.movesUsed.usedRespawn = true; break;
			case 'SKIP': 
				this.movesUsed.usedSkip = true; break;
		}
		
		if (incrementNumMoves) this.movesUsed.numMovesUsed++;
	}

	spawnCrate(crateType) {
		this.worldNetwork.spawnCrate(crateType);
	}
	
	spawnItem(itemName) {
		return this.worldNetwork.spawnItem(itemName, 0, 0);
	}

	spawnRandomGroundItems(numItems) {
		this.worldNetwork.updateSurfaceTiles();
		for (let i = 0; i < numItems; ++i)
			this.worldNetwork.spawnRandomGroundItem();
	}

	getTurnPlayer() {
		return this.worldNetwork.getTurnPlayer();
	}

	performPlayerMove(playerId, moveType, velocity, targetId) {
		this.worldNetwork.performMove(playerId, moveType, velocity, targetId);
	}

	prepareWorldForRespawn() {
		this.worldNetwork.prepareForRespawn();
	}

	setRandomRespawnPoint() {
		this.worldNetwork.setRandomRespawnPoint();
	}

	anyGoalholeSatisfied() {
		let goalholes = this.worldNetwork.getGoalholes();
		for (let i = 0; i < goalholes.length; ++i)
			if (goalholes[i].isSatisfied()) return true;
		return false;
	}

	checkGoalholesActive(anyClientMeetsPointsRequired, turnClientMeetsPointsRequired) {
		this.worldNetwork.checkGoalholesActive(anyClientMeetsPointsRequired, turnClientMeetsPointsRequired);
	}

	turnPlayerCanThrow() {
		return this.worldNetwork.turnPlayerCanThrow();
	}
	
	addOrbPoints() {
		return this.worldNetwork.addOrbPoints();
	}

	getSumOfPlayerPoints(playerIds) {
		let sum = 0;
		for (var i = 0; i < playerIds.length; ++i) {
			sum += this.worldNetwork.getItemById(playerIds[i]).serverPoints;
		}
		return sum;
	}

	resetMovesUsed() {
		this.movesUsed.usedJump = false;
		this.movesUsed.usedThrow = false;
		this.movesUsed.usedRespawn = false;
		this.movesUsed.usedSkip = false;
		this.movesUsed.numMovesUsed = 0;
	}

	getMovesUsedByTurnPlayer() {
		return this.movesUsed;
	}

	getNumPlayersAlive(playerIds) {
		let numPlayers = 0;
		for (let i = 0; i < playerIds.length; ++i)
			numPlayers += this.worldNetwork.playerIsAlive(playerIds[i]) ? 1 : 0;
		return numPlayers;
	}
}

return ServerWorld;
});