define(['sgame/server-world', 'shared/global-info', 'sgame/server-model'], function(ServerWorld, GlobalInfo, ServerModel) {
class ServerMaster {
	constructor(eventManager, worldNetwork, logger, clients) {
		this.eventManager = eventManager;
		this.serverWorld = new ServerWorld(worldNetwork);
		this.logger = logger;
		this.model = new ServerModel(logger);

		let keys = Object.keys(clients);
		for (let i = 0; i < keys.length; ++i)
			this.onClientJoin(clients[keys[i]].nickname, keys[i], clients[keys[i]].smallId);
	}
	
	init(numPlayersEach) {
		this.model.setNumPlayersEach(numPlayersEach);
	}

	generateWorld(mapName) {
		this.model.setMapName(mapName);
		this.serverWorld.init(mapName);

		let clients = this.getClientInfoMap();
		let keys = Object.keys(clients);
		for (let i = 0; i < keys.length; ++i)
			this.createPlayersForClient(clients[keys[i]].nickname, keys[i]);
	}

	makeSync(actionInfo) {
		let sync = {};
		sync.clientsInfo = this.packClientsInfo();
		sync.turnClientId = this.model.getSmallClientId(this.getTurnClientId());
		sync.playersInfo = this.serverWorld.packPlayersInfo();
		sync.items = this.serverWorld.packItems();
		sync.groundItems = this.serverWorld.packGroundItems();
		sync.destroyedTileIds = this.serverWorld.getDestroyedTileIds();
		sync.actionInfo = actionInfo;
		sync.sequenceNo = this.model.getNextSyncNumber();
		sync.numMovesUsedByTurnPlayer = this.getMovesUsedByTurnPlayer().numMovesUsed;
		sync.lastGeneratedItemId = this.serverWorld.getLastGeneratedItemId();
		sync.respawnIndex = this.serverWorld.getRespawnIndex();
		return sync;
	}

	moveEndCleanUp() {
		this.removeFlaggedClients();
		this.serverWorld.moveEndCleanUp();
		this.serverWorld.removeDeadItems();
		this.model.setMoveReceivedFromClient(null);
		this.model.setNextMoveRespawn(false);
	}

	checkIfGameIsOver() {
		this.updateNumPlayersAlive();
		return !this.model.checkAtLeastXClientsRemaining(2);
	}

	forceSkipTurn() {
		let moveInfo = {playerId: this.getTurnPlayer().id, moveType: 'SKIP', velocity: null};
		if (this.model.nextMoveIsRespawn())
			moveInfo = {playerId: this.getTurnPlayer().id, moveType: 'RESPAWN', velocity: {x: 15, y: -15}};
		this.onMoveReceived(moveInfo, this.model.getTurnClientId());
	}

	onMoveReceived(moveInfo, clientId) {
		if (this.moveIsLegit(moveInfo, clientId))
			this.model.setMoveReceivedFromClient(moveInfo);
	}

	flagClientToBeRemoved(clientId) {this.model.flagClientToBeRemoved(clientId);}

	removeFlaggedClients() {
		this.logger.debug('ServerMaster: Removing clients flagged for disconnection');
		let clientId;
		while (!!(clientId = this.model.popClientToBeRemoved())) {
			let clientPlayers = this.model.getPlayerIds(clientId);
			if (clientPlayers != null) this.serverWorld.removePlayers(clientPlayers);
			this.model.removeClientFromModel(clientId);
			this.model.decrementNumClients();
		}
	}

	onClientNotion(notion, clientId) {
		switch (notion.notion) {
			case 'JOIN':
				this.eventManager.sendSignal({signal: 'MAP_NAME', mapName: this.getMapName()}, clientId);
				this.eventManager.sendSignal({signal: 'GAME_INFO', gameInfo: this.model.getGameInfo()}, clientId);
				this.eventManager.sendSignal({signal: "YOUR_ID", clientId: this.getSmallClientId(clientId)}, clientId);
				this.sendSyncForPlayerJoin();
				break;
			// case 'JOINED_ROOM':
				// "What are you gonna do with all those photos of my wife??"
				// Put them in the game.
				// this.onClientJoin(notion.nickname, clientId, notion.spectating);
				// break;
			case 'PLAYER_MOVE':
				this.onMoveReceived(notion.move, clientId);
				break;
			case 'TOGGLE_READY':
				this.onClientToggleReady(clientId);
				break;
		}
	}

	onClientJoin(clientNickname, clientId, clientSmallId) {
		// If game hasn't started; Create new player character; Then sync with them
		if (!clientId) clientId = 'local_' + clientNickname + clientSmallId;
			this.model.addNewClientInfo(clientId, clientSmallId, clientNickname);
	}

	onClientToggleReady(clientId) {
		if (!clientId) {
			// Local game
			this.model.setEveryClientAsReady();
		} else
			this.model.toggleReady(clientId);
	}

	createPlayersForClient(clientNickname, clientId) {
		let numPlayersEach = this.model.getNumPlayersEach();
		let smallClientId = this.model.getSmallClientId(clientId);
		for (var i = 0; i < numPlayersEach; ++i) {
			let newPlayerId = this.serverWorld.addPlayer(clientNickname, smallClientId).id;
			this.model.addPlayerId(clientId, newPlayerId);
		}
	}

	moveIsLegit(moveInfo, clientId) {
		// Make sure whoever sent it actually owns the turn player
		if (!clientId || clientId === this.model.getTurnClientId()) {
			return this.serverWorld.moveIsLegit(moveInfo);
		}
		return false;
	}

	// Model SET
	updatePlayerBasedAttribute(modelFunction, serverWorldFunction, attributeName) {
		let clientIds = this.model.getClientsList();
		let infoList = [];
		for (let i = 0; i < clientIds.length; ++i) {
			let playerIds = this.model.getPlayerIds(clientIds[i]);
			let info = {clientId: clientIds[i]};
			info[attributeName] = this.serverWorld[serverWorldFunction](playerIds);
			infoList.push(info);
		}
		this.model[modelFunction](infoList);
	}
	updateNumPlayersAlive() {this.updatePlayerBasedAttribute('updateNumPlayersAlive', 'getNumPlayersAlive', 'numPlayersAlive');}
	updateClientPoints() {this.updatePlayerBasedAttribute('updateClientPoints', 'getSumOfPlayerPoints', 'points');}

	/******************************************************************/
	/*********************** Model MODIFIERS **************************/
	/******************************************************************/
	nextMoveIsRespawn() {this.model.setNextMoveRespawn(true);}
	decrementClientLives(clientId) {this.model.getClientInfoMap()[clientId].lives--;}
	setNextClientTurn(clientId) {this.model.setTurnClientId(clientId);}
	cyclePlayerOfClient(clientId) {return this.model.cyclePlayerOfClient(clientId);}
	setGameInfo(gameInfo) {this.model.setGameInfo(gameInfo);}
	addGameInfo(property, value) {this.model.addGameInfo(property, value);}
	/******************************************************************/
	/************************** Model GET *****************************/
	/******************************************************************/
	clientHasAnyPlayersAlive(clientId) {
		this.updateNumPlayersAlive();
		return this.model.getNumPlayersAlive(clientId) > 0;
	}
	getClientInfoMap() {return this.model.getClientInfoMap();}
	getTurnClientId() {return this.model.getTurnClientId();}
	getMapName() {return this.model.getMapName();}
	getPlayersOfClient(clientId) {return this.model.getPlayerIds(clientId);}
	getNumberOfClients() {return this.model.getNumClientsInGame();}
	getNumberOfReadyClients() {return this.model.getNumReadyClients();}
	getMoveReceivedFromClient() {return this.model.getMoveReceivedFromClient();}
	getClientsList() {return this.model.getClientsList();}
	getSmallClientId(clientId) {return this.model.getSmallClientId(clientId);}
	clientHasAnyLivesLeft(clientId) {return this.model.getLives(clientId).lives > 0};
	hasReceivedMoveFromClient() {return !!this.model.getMoveReceivedFromClient();}
	playersBelongToSameClient(id1, id2) {return this.model.playersBelongToSameClient(id1, id2);}
	getClientOwnerOfPlayerById(playerId) {return this.model.getClientOwnerOfPlayerById(playerId);}
	packClientsInfo() {return this.model.packClientsInfo();}
	getNextClientTurn() {return this.model.getNextClientTurn();}
	getClientIdsOrderedByPoints() {return this.model.getClientIdsOrderedByPoints();}
	/******************************************************************/
	/************************* Server World ***************************/
	/******************************************************************/
	worldIsReady() {return this.serverWorld.worldIsReady();}
	getRecentlyKilledPlayers() {return this.serverWorld.getRecentlyKilledPlayers();}
	getDeadPlayers() {return this.serverWorld.getDeadPlayers();}
	prepareWorldForRespawn() {this.serverWorld.prepareWorldForRespawn();}
	getTurnPlayer() {return this.serverWorld.getTurnPlayer();}
	turnPlayerIsDead() {return this.serverWorld.turnPlayerIsDead();}
	playerIsDead(id) {return this.serverWorld.playerIsDead(id);}
	setFirstTurnPlayer() {this.serverWorld.setFirstTurnPlayerId(this.model.getPlayerIdsOfTurnClient()[0]);}
	turnPlayerCanThrow() {return this.serverWorld.turnPlayerCanThrow();}
	getMovesUsedByTurnPlayer() {return this.serverWorld.getMovesUsedByTurnPlayer();}
	cycleToNextPlayerTurn() {this.serverWorld.cycleToNextPlayerTurn();}
	setTurnPlayer(playerId) {this.serverWorld.setNextTurnPlayer(playerId);}
	spawnCrate(crateType) {this.serverWorld.spawnCrate(crateType);}
	spawnItem(itemName) {return this.serverWorld.spawnItem(itemName);}
	addOrbPoints() {return this.serverWorld.addOrbPoints();}
	updateMovesUsed(move) {if (move) this.serverWorld.updateMovesUsed(move.moveType);}
	performMoveReceived(move) {this.serverWorld.performPlayerMove(move.playerId, move.moveType, move.velocity, move.targetId);}
	turnHasEnded() {return this.serverWorld.readyToStartNextTurn();}
	resetMovesUsed() {this.serverWorld.resetMovesUsed()}
	spawnRandomGroundItems(numItems) {this.serverWorld.spawnRandomGroundItems(numItems);}
	setRandomRespawnPoint() {this.serverWorld.setRandomRespawnPoint();}
	anyGoalholeSatisfied() {return this.serverWorld.anyGoalholeSatisfied();}
	checkGoalholesActive(requiredPoints) {return this.serverWorld.checkGoalholesActive(this.model.anyClientMeetsPoints(requiredPoints), this.model.turnClientMeetsPoints(requiredPoints));}
	/******************************************************************/
	/*********************** Messages to client ***********************/
	/******************************************************************/
	sendSync(clientActionInfo) {
		this.eventManager.sendSync(this.makeSync(clientActionInfo));
	}
	sendBlockedSignal(signal) {
		signal.blocked = true;
		this.eventManager.sendSignal(signal);
	}
	sendFreeSignal(signal) {this.eventManager.sendSignal(signal);}
	sendSyncForNextTurn(playerIds, moveType, countdownEndTime) {
		this.sendSync(this.serverWorld.createAction(playerIds, moveType, null, null, countdownEndTime));
	}
	sendSyncForGameCountdown(countdownEndTime) {
		this.sendSyncForNextTurn(null, 'COUNTDOWN', countdownEndTime);
	}
	sendSyncForPerformMove(move) {
		if (move) this.sendSync(this.serverWorld.createAction([this.model.getMoveReceivedFromClient().playerId], move.moveType, move.velocity, move.targetId));
	}
	sendSyncForPlayerJoin() {
		this.sendSync(this.serverWorld.createAction(null, 'IDLE'));
	}
	sendSyncForGameEnd(clientRankings) {
		// Convert to small IDs as the client will be using this
		let smallIds = [];
		for (var i = 0; i < clientRankings.length; ++i) {
			smallIds.push(this.model.getSmallClientId(clientRankings[i]));
		}
		this.sendFreeSignal({signal: 'GAME_ENDED'});
		this.sendSync(this.serverWorld.createGameOverAction(smallIds));
	}
	sendSignalToOrbCheck() {
		this.sendBlockedSignal({signal: 'COUNT_POINTS'});
	}
}

return ServerMaster;
});