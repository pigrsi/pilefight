define(['shared/global-info', 'util/util'], function(GlobalInfo, Util) {
var Physics = {};
Physics.gravity = 0.275;
GlobalInfo.GRAVITY = Physics.gravity;

Physics.tick = function(objects) {
    for (var i = 0; i < objects.length; ++i) {
        objects[i].prev = {x: objects[i].x, y: objects[i].y};
		if (!objects[i].ableToMove()) continue;
        Physics.applyVelocity(objects[i]);
    }
	for (var i = 0; i < objects.length; ++i) {
		if (!objects[i].ableToMove()) continue;
        Physics.applyPhysics(objects[i]);
		Physics.clampByMaxVelocity(objects[i]);
	}
}

Physics.applyPhysics = function(obj) {
    if (obj.allowPhysics()) {
        if (obj.allowGravity && !obj.isFloatingOnWater()) Physics.applyGravity(obj);
        Physics.applyDrag(obj);
    }
    Physics.resetFrameVariables(obj);
}

Physics.applyGravity = function(obj) {
	obj.velocity.y += Physics.gravity;
	if (obj.velocity.y >= obj.terminalVelocity)
		obj.velocity.y = obj.terminalVelocity;
}

// 0-1
Physics.applyDrag = function(obj) {
    if (!obj.allowDrag) return;
    obj.setVelocityX(obj.velocity.x *= (1-obj.drag));
    if (Math.abs(obj.velocity.x) <= 0.07) obj.setVelocityX(0);
}

// Reset variables that are rechecked every frame
Physics.resetFrameVariables = function(obj) {
    obj.onFloor = false;
    obj.onCeiling = false;
    obj.standingOnItem = false;
    obj.collidedWithItem = false;
    obj.collidedWithTile = false;
}

Physics.applyVelocity = function(obj) {
	obj.setX(obj.x + obj.velocity.x);
    obj.setY(obj.y + obj.velocity.y);
}

Physics.clampByMaxVelocity = function(obj) {
	if (obj.velocity.x > obj.maxVelocity.x) obj.velocity.x = obj.maxVelocity.x;
        else if (obj.velocity.x < -obj.maxVelocity.x) obj.velocity.x = -obj.maxVelocity.x;
        
	if (obj.velocity.y > obj.maxVelocity.y) obj.velocity.y = obj.maxVelocity.y;
        else if (obj.velocity.y < -obj.maxVelocity.y) obj.velocity.y = -obj.maxVelocity.y;
}

Physics.bounceObjectX = function(obj) {
    if (obj.allowBounce) {
        obj.velocity.x *= -obj.bounce.x;
        if (obj.onBounce) obj.onBounce();
    }
    else obj.setVelocityX(0);
}

Physics.bounceObjectY = function(obj) {
    if (obj.allowBounce) {
        obj.velocity.y *= -obj.bounce.y;
        if (Math.abs(obj.velocity.y) <= 1) obj.setVelocityY(0);
        if (obj.onBounce) obj.onBounce();
    } else
        obj.setVelocityY(0);
}

Physics.overlap = function(obj1, obj2, callback) {
    let result = Util.overlap(obj1, obj2);
    if (result && callback) callback(obj1, obj2);
    return result;
}

// Overlap in the centre of 2 items
Physics.smallOverlap = function(obj1, obj2, sizeMultiplier) {
    return Physics.overlap(Physics.getSmallCollisionBox(obj1, sizeMultiplier), obj2);
}

Physics.getSmallCollisionBox = function(obj, sizeMultiplier) {
    let midX = obj.x + obj.width/2;
    let midY = obj.y + obj.height/2;
    let boxLen = {x: obj.width * sizeMultiplier, y: obj.height * sizeMultiplier};
    return {x: midX - boxLen.x/2, y: midY - boxLen.y/2, width: boxLen.x, height: boxLen.y};
}
return Physics;
});