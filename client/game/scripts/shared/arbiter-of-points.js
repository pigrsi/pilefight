define([], function() {
    ArbiterOfPoints = {
        determinePointsFor: function(event) {
            switch(event) {
                case 'PLAYER_KILL': return 15;
                case 'PLAYER_DEATH': return -0;
                case 'ORB': return 3;
                case 'EXPLOSION_ATTACK_HIT': return 10;
                case 'BUMP_STUN': return 0;
                case 'BONK_STUN': return 0;
                case 'ZIP_ATTACK': return 0;
                case 'SPAWN_PICKUP': return 0;
                case 'TELEPORT_PICKUP': return 0;
            }
            return 0;
        }
    };

    return ArbiterOfPoints;
});