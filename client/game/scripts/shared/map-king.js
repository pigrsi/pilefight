const MAP_NAMES = ['orb_test', 'babys_first_map', 'platforms', 'boosters_world', 'generic'];
const MAP_FILES = [];
for (let i = 0; i < MAP_NAMES.length; ++i) {
	MAP_FILES[i] = `text!maps/${MAP_NAMES[i]}.json`;
}
const MAP_NAMES_TO_DATA = {};

define(MAP_FILES, function() {
for (let i = 0; i < MAP_NAMES.length; ++i) {
	MAP_NAMES_TO_DATA[MAP_NAMES[i]] = arguments[i];
}	
	
class MapKing {
	constructor() {
		this.currentlyLoadedMapData = null;		
	}
	
	loadMap(mapName) {
		let mapDataJSON = MAP_NAMES_TO_DATA[mapName];
		let mapData = JSON.parse(mapDataJSON);
		this.currentlyLoadedMapData = mapData;
		this.nextTileId = 0;
		
		let markersTileset = this.getTilesetInfoByName('markers');
		this.markersGidRange = {first: markersTileset.firstgid, last: markersTileset.firstgid + markersTileset.tilecount - 1};
				
		let tileSize = mapData.tilewidth;
		let tileResult = this.getTiles(mapData, this.getLayerByName(mapData, 'tiles'));
		let objectsResult = this.getMapObjects(mapData, this.getLayerByName(mapData, 'objects'));
		let configResult = this.getConfigData(this.getLayerByName(mapData, 'config'));
		let objects = objectsResult[0];
		let items = objectsResult[1];
		let tiles = tileResult[0];
		let tileObjectsInRows = tileResult[1];
		let spawns = tileResult[2];
		let respawns = tileResult[3];
		let killYBuffer = this.getKillYBuffer(mapData);

		this.addAdjacentTiles(tileObjectsInRows);
		return {tiles: tiles, playerSpawnPositions: spawns, mapWidth: mapData.width * tileSize, mapHeight: mapData.height * tileSize, playerRespawnPositions: respawns, 
				killYBuffer: killYBuffer, items: items, objects: objects, itemOdds: configResult[0]};
	}

	getLayerByName(mapData, name) {
		for (var i = 0; i < mapData.layers.length; ++i) {
			if (mapData.layers[i].name === name) {
				return mapData.layers[i];
			}
		}
	}

	getTiles(mapData, layer) {
		let tiles = [];
		let spawns = [];
		let respawns = [];
		let tileObjectsInRows = [];
		let tileSize = mapData.tilewidth;
		
		for (var i = 0; i < layer.height; ++i) {
			tileObjectsInRows[i] = [];
		}
		
		let this_ = this;
		this.loopTilesOnLayer(layer, function(tile, row, column, tileRows) {
			// Check markers
			if (tile === this_.markersGidRange.first) { spawns.push({x: column * tileSize, y: row * tileSize}); return; }
			if (tile === this_.markersGidRange.first+1) { respawns.push({x: column * tileSize - tileSize/2, y: row * tileSize - tileSize/2}); return; }
			
			// Solid tile
			let interestingEdges = this_.findInterestingEdges(row, column, tileRows);
			tile = this_.createTile(column * tileSize, row * tileSize, tileSize, interestingEdges, tile);
			tiles.push(tile);
			tileObjectsInRows[row][column] = tile;
		});
		return [tiles, tileObjectsInRows, spawns, respawns];
	}

	getSpawnPoints(mapData, layer) {
		let spawns = [];
		let tileSize = mapData.tilewidth;
		this.loopTilesOnLayer(layer, function(tile, row, column, tileRows) {
			spawns.push({x: column * tileSize, y: row * tileSize});
		});
		return spawns;
	}

	getRespawnPoints(mapData, layer) {
		let respawns = [];
		let tileSize = mapData.tilewidth;
		this.loopTilesOnLayer(layer, function(tile, row, column, tileRows) {
			respawns.push({x: column * tileSize - tileSize/2, y: row * tileSize - tileSize/2})
		});
		return respawns;
	}
	
	getMapObjects(mapData, layer) {
		let objects = [];
		let items = [];
		if (layer) {
			let mapObjects = layer.objects;
			for (let i = 0; i < mapObjects.length; ++i) {
				let ob = mapObjects[i];
				let objectData = {name: ob.name, x: ob.x, y: ob.y, width: ob.width, height: ob.height};
				if (ob.properties) {
					for (let k = 0; k < ob.properties.length; ++k) {
						let prop = ob.properties[k];
						objectData[prop.name] = prop.value;
					}
				}
				if (ob.polygon) objectData.polygon = ob.polygon;
				
				if (this.isObjectNotItem(ob.name)) objects.push(objectData);
				else items.push(objectData);
			}
		}
		return [objects, items];
	}

	isObjectNotItem(name) {
		return name === 'Booster' || name === 'BoosterStart' || name === 'Goalhole';
	}

	getConfigData(layer) {
		let configData = [];
		for (let i = 0; i < layer.objects.length; ++i) {
			let object = layer.objects[i];
			if (object.name === 'item_odds') configData[0] = this.getItemOdds(object);
		}

		return configData;
	}

	getItemOdds(oddsObject) {
		let odds = [];
		if (oddsObject.properties) {
			for (let i = 0; i < oddsObject.properties.length; ++i)
				odds.push({name: oddsObject.properties[i].name, value: oddsObject.properties[i].value});
		}
		return odds;
	}

	generateTileId() {
		return this.nextTileId++;
	}

	loopTilesOnLayer(layer, onFoundTile) {
		let tileRows = this.splitTilesIntoRows(layer.data, layer.width);
		for (var row = 0; row < layer.height; ++row) {
			for (var column = 0; column < layer.width; ++column) {
				let tile = tileRows[row][column];
				if (this.hasTile(tile)) {
					onFoundTile(tile, row, column, tileRows);
				}
			}
		}
	}

	addAdjacentTiles(tileObjectRows) {
		for (var row = 0; row < tileObjectRows.length; ++row) {
			for (var column = 0; column < tileObjectRows[row].length; ++column) {
				let tile = tileObjectRows[row][column];
				if (tile) {
					tile.adjacents = {};
					if (!tile.interesting.left) tile.adjacents.left = tileObjectRows[row][column-1];
					if (!tile.interesting.right) tile.adjacents.right = tileObjectRows[row][column+1];
					if (!tile.interesting.top) tile.adjacents.top = tileObjectRows[row-1][column];
					if (!tile.interesting.bottom) tile.adjacents.bottom = tileObjectRows[row+1][column];
				}
			}
		}
	}

	splitTilesIntoRows(allTiles, rowLength) {
		var tileRows = [];
		var chunk = rowLength;
		for (var i = 0, j = allTiles.length; i < j; i += chunk) {
			tileRows.push(allTiles.slice(i, i + chunk));
		}
		return tileRows;
	}

	createTile(x, y, size, interestingEdges, tileNumber) {
		// prev is just so tiles can collide in same way as moving entities
		return {
				id: this.generateTileId(),
				x: x,
				y: y,
				width: size,
				height: size,
				left: x,
				right: x + size,
				top: y,
				bottom: y + size,
				interesting: interestingEdges,
				tileNumber: tileNumber,
				items: [],
				//prev: {x: x, y: y}
			};
	}

	findInterestingEdges(row, column, tileRows) {
		let interesting = {};
		interesting.left = !this.hasSolidTile(tileRows[row][column-1]);
		interesting.right = !this.hasSolidTile(tileRows[row][column+1]);
		interesting.top = tileRows[row-1] === undefined || !this.hasSolidTile(tileRows[row-1][column]);
		interesting.bottom = tileRows[row+1] === undefined || !this.hasSolidTile(tileRows[row+1][column]);
		interesting.uninteresting = !interesting.left && !interesting.right && !interesting.top && !interesting.bottom;
		return interesting;
	}

	hasTile(tile) {
		return tile !== 0;
	}
	
	hasSolidTile(tile) {
		return tile && tile < this.markersGidRange.first || tile > this.markersGidRange.last;
	}

	getKillYBuffer(mapData) {
		let defaultValue = 96;
		if (!mapData.properties) return defaultValue;
		for (let i = 0; i < mapData.properties.length; ++i) {
			if (mapData.properties[i].name === 'killYBuffer') return mapData.properties[i].value;
		}
		// default
		return defaultValue;
	}

	getTilesetInfoByName(name) {
		let mapData = this.currentlyLoadedMapData;
		for (var i = 0; i < mapData.tilesets.length; ++i) {
			if (mapData.tilesets[i].name === name) {
				return mapData.tilesets[i];
			}
		}
	}
}

return MapKing;
});