define([], function() {
    WorldUtil = {};

    WorldUtil.setReleasedItemVelocities = function(items) {
        switch (items.length) {
            case 1:
                items[0].setVelocityY(-10);
                break;
            case 2:
                items[0].setVelocityX(-5);
                items[0].setVelocityY(-10);
                items[1].setVelocityX(5);
                items[1].setVelocityY(-10);
                break;
            case 3:
                items[0].setVelocityX(-5);
                items[0].setVelocityY(-10);
                items[1].setVelocityX(0);
                items[1].setVelocityY(-10);
                items[2].setVelocityX(5);
                items[2].setVelocityY(-10);
                break;
            case 4:
                items[0].setVelocityX(-7);
                items[0].setVelocityY(-10);
                items[1].setVelocityX(-4);
                items[1].setVelocityY(-10);
                items[2].setVelocityX(4);
                items[2].setVelocityY(-10);
                items[3].setVelocityX(7);
                items[3].setVelocityY(-10);
                break;
            case 5:
                items[0].setVelocityX(-7);
                items[0].setVelocityY(-10);
                items[1].setVelocityX(-4);
                items[1].setVelocityY(-10);
                items[2].setVelocityX(0);
                items[2].setVelocityY(-10);
                items[3].setVelocityX(4);
                items[3].setVelocityY(-10);
                items[4].setVelocityX(7);
                items[4].setVelocityY(-10);
                break;
        }
    }

    return WorldUtil;
});