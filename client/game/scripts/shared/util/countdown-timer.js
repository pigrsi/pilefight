define([], function() {
    class CountdownTimer {
        constructor() {
            this.endTime = 0;
            this.running = false;
        }

        reset(endInSeconds=null) {
            this.endTime = Date.now() + (endInSeconds * 1000);
            this.running = true;
        }

        stop() {
            this.running = false;
        }

        isRunning() {
            return this.running;
        }

        getSecondsRemaining() {
            return this.getMillisecondsRemaining() / 1000;
        }

        getMillisecondsRemaining() {
            return this.endTime - Date.now();
        }

        getEndTime() {
            return this.endTime;
        }
    }

    return CountdownTimer;
});