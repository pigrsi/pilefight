define([], function() {
class Timer {
    constructor() {
        this.startTime = 0;
        this.running = false;
    }

    reset(startTime=null) {
        this.startTime = !!startTime ? startTime : Date.now();
        this.running = true;
    }
    
    stop() {
        this.running = false;
    }
    
    isRunning() {
        return this.running;
    }
    
    getMillisecondsElapsed() {
        return Date.now() - this.startTime;
    }

    getSecondsElapsed() {
        return this.getMillisecondsElapsed() / 1000;
    }
    
    getStartTime() {
        return this.startTime;
    }

    pushStartTimeForwardBy(seconds) {
        this.startTime += seconds * 1000;
    }
}

return Timer;
});