define([], function() {
var Util = {};

// Pass it an array of items
Util.findItemAt = function (items, x, y) {
    for (var i = 0; i < items.length; ++i)
        if (Util.pointInRect(items[i], x, y))
            return items[i];
    return null;
}

// Find item that satisfies given conditions
Util.findItemThat = function (items, x, y, properties, values) {
    for (var i = 0; i < items.length; ++i) {
        let item = items[i];
        if (Util.pointInRect(item, x, y)) {
            let satisfied = true;
            for (var k = 0; k < properties.length; ++k) {
                if (item[properties[k]] !== values[k]) satisfied = false;
            }
            if (satisfied) return items[i];
        }
    }
    return null;
}

Util.pointInRect = function(rect, x, y) {
    return x >= rect.x && x <= rect.x + rect.width
           && y >= rect.y && y <= rect.y + rect.height;
}

Util.overlap = function(obj1, obj2) {
	return !(obj1.x + obj1.width < obj2.x ||
		obj2.x + obj2.width < obj1.x ||
		obj1.y + obj1.height < obj2.y ||
		obj2.y + obj2.height < obj1.y);
}

Util.pointInCircle = function(circle, x, y) {
    let dx = circle.x - x;
    let dy = circle.y - y;
    let dist = dx*dx + dy*dy;
    return dist < (circle.radius * circle.radius);
}

// If circle origin is at 0,0 not centre
Util.pointInCircleTopLeft = function(circle, x, y) {
	let dx = circle.x + circle.radius - x;
	let dy = circle.y + circle.radius - y;
	let dist = dx*dx + dy*dy;
	return dist < (circle.radius * circle.radius);
}

// https://stackoverflow.com/questions/8721406/how-to-determine-if-a-point-is-inside-a-2d-convex-polygon
Util.pointInPolygon = function(points, x, y) {
	let result = false;
	let k = points.length - 1;
	for (let i = 0; i < points.length; k = i++) {
		if ((points[i].y > y) != (points[k].y > y) &&
			(x < (points[k].x - points[i].x) * (y - points[i].y) / (points[k].y-points[i].y) + points[i].x)) {
			result = !result;
		}
	}
	return result;
}

Util.objectOverlapsWithAnyOf = function(object, objectList) {
	for (let i = 0; i < objectList.length; ++i)
		if (Util.overlap(object, objectList[i])) return true;
	return false;
}

Util.sortItemsBottomToTop = function(items) {
	return Util.insertionSort(items, Util.compareItemBelowItem);
}

Util.sortQuad = function(quad) {
	return Util.insertionSort(quad, Util.compareTileTopRightOfItem);
}

Util.insertionSort = function(array, sortFunction) {
	for (let i = 0; i < array.length; i++) {
		let val = array[i];
			for (var j = i - 1; j >= 0 && sortFunction(array[j], val) > 0; j--)
				array[j + 1] = array[j]
		array[j + 1] = val;
	}
	return array;
}

Util.compareItemBelowItem = function(item1, item2) {
	 return -((item1.y+item1.height) - (item2.y+item2.height)); 
}

Util.compareTileTopRightOfItem = function(tile1, tile2) {
	if (tile1.y !== tile2.y) return tile1.y - tile2.y;
	return tile1.x - tile2.x;
}

Util.getUnitVectorTowards = function(obj1, obj2) {
    let dx = obj1.x - obj2.x;
    let dy = obj1.y - obj2.y;
    let hype = Math.sqrt(dx*dx + dy*dy);
    if (hype === 0) return {x: 0, y: 0};
    return {x: dx / hype, y: dy / hype};
}

Util.getPerpendicularVectors = function(v) {
    return {left: {x: v.y, y: -v.x}, right: {x: -v.y, y: v.x}};
}

Util.AIsRightOfB = function(a, b, allowance=0) {return a.x > b.x + b.width + allowance;}
Util.AIsLeftOfB = function(a, b, allowance=0) {return a.x + a.width < b.x - allowance;}
Util.AIsBelowB = function(a, b, allowance=0) {return a.y > b.y + b.height + allowance;}
Util.AIsAboveB = function(a, b, allowance=0) {return a.y + a.height < b.y - allowance;}

Util.circlesIntersect = function(c1, c2) {
    let dx = c1.x-c2.x;
    let dy = c1.y-c2.y;
    
    let hyp = dx*dx + dy*dy;
    return hyp < c1.radius*c1.radius + c2.radius*c2.radius;
}

Util.getHousingQuad = function(point, quadMap, quadSize) {
	let column = Util.roundDownToNearestMultiple(point.x, quadSize);
	let row = Util.roundDownToNearestMultiple(point.y, quadSize);
	let colKey = 'col' + column;
	let rowKey = 'row' + row;
	if (!quadMap[colKey]) return null;
	return quadMap[colKey][rowKey];
}

Util.roundDownToNearestMultiple = function(value, toNearest) {
	return Math.floor((value - (value % toNearest)) / toNearest);
}

// An array of an item tree. 
Util.getItemsStandingOnTree = function(item) {
	let tree = [item];
	
	for (let i = 0; i < item.itemsStandingOn.length; ++i) {
		let next = item.itemsStandingOn[i];
		tree.push.apply(tree, Util.getItemsStandingOnTree(next));
	}
	
	return tree;
}

return Util;
});