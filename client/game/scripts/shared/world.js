/* 
    Controls the game loop and physics.
    Uses nothing from Phaser.
    WorldViewer renders and does client-only stuff.
    Everything here used by both client and server.
*/
define(['util/world-util', 'shared/physics', 'entities/collisions', 'util/util', 'shared/object-mover', 'entities/pawmaw', 'entities/status-effects', 'shared/arbiter-of-points'],
function (WorldUtil, Physics, Collisions, Util, ObjectMover, PawMaw, StatusEffects, ArbiterOfPoints) {
class World {
	constructor() {
		this.items = [];
		this.explosions = [];
		this.lasers = [];
		this.tiles = [];
		this.groundItems = [];
		this.mapObjects = {};
		this.moveEndCallbacks = [];
		this.destroyedTiles = [];
		this.TICK_RATE = 60;
		this.delayedCalls = [];
		this.listenCallbacks = [];
		this.killY = 0;
		this.objectMover = new ObjectMover();	
		this.gameStateUpdatesPaused = true;
		this.desiredSpeedUpDegree = 0;

		this.addPawmawRoamToMover = this.addPawmawRoamToMover.bind(this);
	}
	
	init(fps) {
		this.fps = fps;
	}

	setMapInfo(tiles, mapWidth, killY) {
		this.tiles = tiles;
		this.createTileQuads();
		this.dangerY = killY - 96;
		this.killY = killY;
		this.pawmaw = new PawMaw(killY, mapWidth, this.addPawmawRoamToMover);
	}
	
	createTileQuads() {
		// Initial creation, only should be called once
		this.quads = [];
		this.quadMap = {};
		let quadSize = 768;
		this.quads.quadSize = quadSize;
		this.quadMap.quadSize = quadSize;
		
		for (let i = 0; i < this.tiles.length; ++i) {
			let tile = this.tiles[i];
			if (tile.interesting.uninteresting) continue;
			this.addTileToHousingQuad(tile);
		}
	}
	
	addTileToHousingQuad(tile) {
		let quadSize = this.quads.quadSize;
		let column = Util.roundDownToNearestMultiple(tile.x, quadSize);
		let row = Util.roundDownToNearestMultiple(tile.y, quadSize);
		let colKey = 'col' + column;
		let rowKey = 'row' + row;
		if (!this.quadMap[colKey]) this.quadMap[colKey] = {};
		if (!this.quadMap[colKey][rowKey]) {
			var newQuad = [tile];
			newQuad.x = column * quadSize;
			newQuad.y = row * quadSize;
			this.quadMap[colKey][rowKey] = newQuad;
			this.quads.push(newQuad);
		}
		else this.quadMap[colKey][rowKey].push(tile);
		return this.quadMap[colKey][rowKey];
	}
	
	removeTileFromQuad(tile) {
		let quad = Util.getHousingQuad(tile, this.quadMap, this.quadMap.quadSize);
		let index = quad.indexOf(tile);
		if (index !== -1) quad.splice(index, 1);
	}

	update() {
		// Anything outside this still updates during tweens
		if (this.objectMover.done) {
			if (!this.gameStateUpdatesPaused) {
				Physics.tick(this.items);
				this.getSortedItemArray();
				this.resetObjects();
				this.checkCollisions();
				this.updateObjects();
				this.checkDelayedCalls();
			}
		} else {
			this.objectMover.update();
		}
		
		if (this.pawmaw) this.pawmaw.update();
	}
	
	pauseGameStateUpdates() {
		this.gameStateUpdatesPaused = true;
	}
	
	resumeGameStateUpdates() {
		this.gameStateUpdatesPaused = false;
	}

	getSortedItemArray() {return Util.sortItemsBottomToTop(this.items);}
	
	getItems() {return this.items;}

	checkCollisions() {
		Collisions.collideBoosters(this.items, this.mapObjects['Booster']);
		Collisions.collideGoalholes(this.items, this.mapObjects['Goalhole']);
		Collisions.collideExplosions(this.items, this.explosions);
		Collisions.collideLasers(this.items, this.lasers);
		Collisions.collideQuadTiles(this.items, this.quads, this.quadMap, this.tiles);
		Collisions.collideArrayBottomsUp(this.items);
	}
	
	resetObjects() {
		for (var i = 0; i < this.items.length; ++i) {
			this.items[i].resetItemsOnHead();
			this.items[i].resetItemsStoodOn();
		}
	}

	updateObjects() {
		let toBeRemoved = [];
		for (let i = 0; i < this.items.length; ++i) {
			if (!this.items[i].isDisabledOrDead())
				this.items[i].preUpdate();
		}
		for (var i = 0; i < this.items.length; ++i) {
			if (this.items[i].readyForRemoval) {toBeRemoved.push(this.items[i]); continue;}
			if (this.items[i].disabled) continue;
			if (this.items[i].alive) this.items[i].update();
			else this.items[i].updateDead();
		}
		this.removeItems(toBeRemoved);
		for (var i = 0; i < this.items.length; ++i) {
			this.items[i].moveWithItemsStoodOn();
		}
		
		for (var i = this.explosions.length-1; i >= 0; --i) {
			if (this.explosions[i].readyForRemoval) {
				this.removeExplosion(this.explosions[i]);
				continue;
			}
			this.explosions[i].update();
		}
		this.checkItemsOutOfBounds();
	}

	startPawmawTracking(item) {
		if (!this.pawmaw) return;
		this.pawmaw.setTarget(item);
	}

	checkItemsOutOfBounds() {
		for (var i = 0; i < this.items.length; ++i) {
			let item = this.items[i];
			if (item.alive && !item.disabled && item.y+item.height > this.killY) {
				item.onLandingOnWater(this.killY);
			}
		}
	}
	
	checkDelayedCalls() {
		for (var i = this.delayedCalls.length-1; i >= 0; --i) {
			let dc = this.delayedCalls[i];
			dc.ticks--;
			if (dc.ticks <= 0) {
				dc.callback.apply(dc.context, dc.params);
				this.delayedCalls.splice(i, 1);
			}
		}
	}

	addItem(item) {
		this.items.push(item);
		this.emitEvent('ITEM_ADDED', [item]);
		return item;
	}

	addGroundItem(item, tile) {
		this.groundItems.push(item);
		tile.items.push(item);
		item.tileContainingId = tile.id;
		this.emitEvent('GROUND_ITEM_ADDED', [tile, item]);
	}

	revealGroundItem(item, tile) {
		this.addItem(item);
		let index = this.groundItems.indexOf(item);
		if (index !== -1) this.groundItems.splice(index, 1);
		index = tile.items.indexOf(item);
		if (index !== -1) tile.items.splice(index, 1);
		this.emitEvent('GROUND_ITEM_REVEALED', [tile]);
	}

	clearGroundItems() {
		this.groundItems = [];
		for (let i = 0; i < this.tiles.length; ++i) {
			let tile = this.tiles[i];
			if (tile) this.tiles[i].items = [];
		}
	}

	addMapObject(object) {
		if (!this.mapObjects[object.name])
			this.mapObjects[object.name] = [];
		this.mapObjects[object.name].push(object);
		this.emitEvent('OBJECT_ADDED', [object]);
	}

	addExplosion(explosion) {
		this.explosions.push(explosion);
		this.emitEvent('EXPLOSION_ADDED', [explosion]);
		return explosion;
	}
	
	addLaser(laser) {
		this.lasers.push(laser);
		return laser;
	}

	setItems(items) {
		this.items = items;
	}

	removeItems(items) {
		for (var i = 0; i < items.length; ++i)
			this.removeItem(items[i]);
	}

	removeItem(item) {
		let items = this.items;
		let itemIndex = items.indexOf(item);
		if (itemIndex < 0) return;
		items.splice(itemIndex, 1);
	}

	removeExplosion(explosion) {
		let exIndex = this.explosions.indexOf(explosion);
		if (exIndex < 0) return;
		this.explosions.splice(exIndex, 1);
	}
	
	removeLaser(laser) {
		let laserIndex = this.lasers.indexOf(laser);
		if (laserIndex < 0) return;
		this.lasers.splice(laserIndex, 1);
	}

	removeTile(tile) {
		if (tile.destroyed) return;
		this.releaseAllTileItems(tile);
		tile.destroyed = true;
		this.destroyedTiles.push(tile);
		this.updateTileAdjacents(tile, true);
		this.removeTileFromQuad(tile);
		this.emitEvent('TILE_DESTROYED', [tile]);	
	}

	restoreTile(tile) {
		let index = this.destroyedTiles.indexOf(tile);
		if (index !== -1) this.destroyedTiles.splice(index, 1);
		
		this.updateTileAdjacents(tile, false);
		if (!tile.interesting.uninteresting) {
			let quad = this.addTileToHousingQuad(tile);
			Util.sortQuad(quad);
		}
		tile.destroyed = false;
	}

	updateTileAdjacents(tile, becomeInteresting=true) {
		if (tile.adjacents.left) {
			tile.adjacents.left.interesting.right = becomeInteresting;
			this.updateUninteresting(tile.adjacents.left);
		}
		if (tile.adjacents.right) {
			tile.adjacents.right.interesting.left = becomeInteresting;
			this.updateUninteresting(tile.adjacents.right);
		}
		if (tile.adjacents.top) {
			tile.adjacents.top.interesting.bottom = becomeInteresting;
			this.updateUninteresting(tile.adjacents.top);
		}
		if (tile.adjacents.bottom) {
			tile.adjacents.bottom.interesting.top = becomeInteresting;
			this.updateUninteresting(tile.adjacents.bottom);
		}
	}
	
	updateUninteresting(tile) {
		let old = tile.interesting.uninteresting;
		tile.interesting.uninteresting = !tile.interesting.left && !tile.interesting.right && !tile.interesting.top && !tile.interesting.bottom;
		if (old && !tile.interesting.uninteresting && !tile.destroyed) this.addTileToHousingQuad(tile);
		else if (tile.destroyed || tile.interesting.uninteresting) this.removeTileFromQuad(tile);
	}
	
	itemIsOverlappingWithAnyTile(item) {
		// Tile does not have to be interesting
		for (let i = 0; i < this.tiles.length; ++i) {
			let tile = this.tiles[i];
			if (!tile) continue;
			if (tile.y > item.y + item.height) return false;
			if (Physics.overlap(item, tile)) return true;
		}
		return false;
	}

	addToMover(gameObject, moverType, destX, destY, duration, tweenDelay, onComplete) {    
		this.objectMover.add(gameObject, moverType, ['x', 'y'], [destX, destY], {duration: duration, delay: tweenDelay}, onComplete);
		this.emitEvent('MOVER_ADDED', [gameObject, moverType]);
	}

	addCustomToMover(gameObject, moverType, properties, values, configValues) {
		this.objectMover.add(gameObject, moverType, properties, values, configValues);
		this.emitEvent('MOVER_ADDED', [gameObject, moverType]);
	}

	addPawmawRoamToMover(pawmaw, destX, destY, duration, tweenDelay, forceEaseIn=false) {
		return this.objectMover.addPawmawMover(pawmaw, destX, destY, duration, tweenDelay, forceEaseIn);
	}

	itemJustThrown() {
		let items = this.items;
		for (var i = 0; i < items.length; ++i)
			if (!items[i].tangible || items[i].dontCollideWithIntangibles)
				items[i].startTangibleTimer();
	}

	makeAllTangible(itemList) {
		if (!itemList) itemList = this.items;
		for (var i = 0; i < itemList.length; ++i)
			itemList[i].becomeTangible();
	}

	callMoveEndCallbacks() {
		for (var i = 0; i < this.moveEndCallbacks.length; ++i) {
			let callback = this.moveEndCallbacks[i];
			callback.func.call(callback.context);
		}
		this.moveEndCallbacks = [];
	}

	allItemsAreStill() {
		if (!this.objectMover.done) return false;
		for (var i = 0; i < this.items.length; ++i) {
			let item = this.items[i];
			if (!item.alive || item.disabled) continue;
			if (!item.isStill()) return false;
		}
		return true;
	}

	// Something may need to happen only at the end of a turn, but before the next turn can start
	addMoveEndCallback(func, context) {
		this.moveEndCallbacks.push({func: func, context: context});
	}

	setUseTweens(use) {this.objectMover.usingExternalTweens = use;}

	delayedCall(ticks, callback, params, context) {
		this.delayedCalls.push({ticks: ticks, callback: callback, params: params, context: context});
	}

	killItem(item) {item.die();}

	mapIsLoaded() {
		return this.tiles.length > 0;
	}

	registerListener(eventCallback) {
		this.listenCallbacks.push(eventCallback);
	}

	emitEvent(eventName, args) {
		for (var i = 0; i < this.listenCallbacks.length; ++i)
			this.listenCallbacks[i](eventName, args);
	}

	getObjectMover() {
		return this.objectMover;
	}
	
	createStatusEffectsObject(item) {
		return new StatusEffects(item);
	}
	
	findItemBelowItem(item, extraSatisfyingProperties=null, extraSatisfyingValues=null) {
		return this.findNearbyThrowableItemAtHeight(item, item.y + item.height + item.halfHeight, extraSatisfyingProperties, extraSatisfyingValues);
	}
	
	findNearbyThrowableItemAtHeight(item, height, extraSatisfyingProperties=null, extraSatisfyingValues=null) {
		let satisfyingProperties = ['collideItems', 'alive', 'onlyCollideWithIntangibles', 'disabled', 'canBeThrown'];
		let satisfyingValues = [true, true, false, false, true];
		if (extraSatisfyingProperties) {
			satisfyingProperties.push.apply(satisfyingProperties, extraSatisfyingProperties);
			satisfyingValues.push.apply(satisfyingValues, extraSatisfyingValues);
		}
		return this.findNearbyItemAtHeightThatSatisfiesProperties(item, height, satisfyingProperties, satisfyingValues);
	}
	
	findNearbyItemAtHeightThatSatisfiesProperties(item, height, satisfyingProperties, satisfyingValues) {
		let r = null;
		if ((r=Util.findItemThat(this.items, item.x + item.halfWidth, height, satisfyingProperties, satisfyingValues))) return r;
		if ((r=Util.findItemThat(this.items, item.x + item.width, height, satisfyingProperties, satisfyingValues))) return r;
		if ((r=Util.findItemThat(this.items, item.x, height, satisfyingProperties, satisfyingValues))) return r;
		return null;
	}

	releaseAllTileItems(tile) {
		if (tile.items.length === 0) return;
		let items = tile.items.slice();
		for (let i = 0; i < items.length; ++i) {
			let item = items[i];
			item.becomeIntangible();
			item.collideItems = false;
			item.startTangibleTimer();
			item.startCollideItemsTimer();
			item.setPosition(tile.x, tile.y-item.height-1);
			this.revealGroundItem(item, tile);
		}

		WorldUtil.setReleasedItemVelocities(items);
	}

	determinePointsFor(event) {
		return ArbiterOfPoints.determinePointsFor(event);
	}

	// Leave instigator blank and points will be added to turn player
	addPointsFor(event, instigator=null, victim=null) {
		this.emitEvent('ADD_POINTS_FOR', [event, instigator, victim]);
	}

	pointsAdded(to, numPointsAdded) {
		this.emitEvent('POINTS_ADDED', [to, numPointsAdded]);
	}

	playerDied(player) {
		this.emitEvent('PLAYER_DIED', [player]);
		this.addPointsFor('PLAYER_DEATH', player, null);
		this.addPointsFor('PLAYER_KILL', null, player);
	}

	getGoalholes() {return this.mapObjects['Goalhole'];}
}

return World;
});