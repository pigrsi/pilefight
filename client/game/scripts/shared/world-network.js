// Interface to pf-world to do network/player turn things.
define(['shared/world-network-model', 'entities/spawner', 'shared/map-king', 'entities/player-respawner', 'entities/collisions', 'shared/global-info', 'util/util'],
	function(WorldNetworkModel, Spawner, MapKing, PlayerRespawner, Collisions, GlobalInfo, Util) {
class WorldNetwork {
	constructor(world, logger) {
		this.world = world;
		this.logger = logger;

		this.model = new WorldNetworkModel();
		this.playerRespawner = new PlayerRespawner(this.world.objectMover);
		this.mapKing = new MapKing();
		this.spawner = new Spawner(this.world);

		this.listenCallbacks = [];
	}

	/******************************************************************/
	/******************************************************************/
	/*************************** CREATION *****************************/
	/******************************************************************/
	/******************************************************************/
	clientCreate(mapName) {
		if (!GlobalInfo.LOCAL_GAME) {
			this.registerWithWorld();
			this.loadMap(mapName, true);
		}
		this.model.populateIdsToTiles(this.world.tiles);
		this.readyToStart = true;
		this.isClient = true;
	}

	serverCreate(mapName) {
		this.registerWithWorld();
		this.loadMap(mapName, false);
		this.readyToStart = true;
		this.isServer = true;
	}

	loadMap(mapName, isClient) {
		if (this.world.mapIsLoaded()) return;
		let mapInfo = this.mapKing.loadMap(mapName);
		this.world.setMapInfo(mapInfo.tiles, mapInfo.mapWidth, mapInfo.mapHeight + mapInfo.killYBuffer);
		if (!isClient) {
			this.model.setPlayerSpawns(mapInfo.playerSpawnPositions);
			this.model.setMapItems(mapInfo.items);
			this.model.setItemOdds(mapInfo.itemOdds);
			if (this.model.getPlayerSpawns().length === 0) this.model.addPlayerSpawn({x: 400, y: 0});
		}
		this.model.setPlayerRespawns(mapInfo.playerRespawnPositions);
		this.model.setMapObjects(mapInfo.objects);
	}

	spawnItem(itemName, x, y, id=null, args, addToWorld=true) {
		let item = this.spawner.spawn(itemName, x, y, args);
		if (!item) return false;
		if (addToWorld) this.world.addItem(item);
		if (!id) id = this.model.generateNextId();
		item.id = id;
		if (itemName === 'ItemBubble') {
			this.model.addItemBubble(item);
		}
		return item;
	}

	addPlayer(playerName, clientId) {
		let spawnPoint = this.popRandomSpawnPoint();
		let newPlayer = this.spawnItem('Monkey', spawnPoint.x+4, spawnPoint.y+8);
		newPlayer.enableSpawnProtection();
		this.model.addPlayerInfo(newPlayer, playerName, clientId);
		return newPlayer;
	}

	spawnMapItems() {
		let mapItems = this.model.getMapItems();
		for (let i = 0; i < mapItems.length; ++i) {
			let item = mapItems[i];
			let args = [];
			if (item.name === 'ItemBubble') {
				args.push({include: item.include, exclude: item.exclude, spawnLevels: item.spawnLevels, cooldown: item.cooldown, strength: item.strength, autoAdd: item.autoAdd,
					spawnItem: this.spawnItem.bind(this), getItemById: this.getItemById.bind(this)});
			}
			this.spawnItem(item.name, item.x, item.y, null, args);
		}
	}

	spawnMapObjectsFromMap() {
		if (this.mapObjectsSpawned) return;
		this.mapObjectsSpawned = true;
		let mapObjects = this.model.getMapObjects();
		for (let i = 0; i < mapObjects.length; ++i) {
			let object = mapObjects[i];
			let args = [];
			if (object.name === 'Booster' || object.name === 'BoosterStart') {
				args.push(object.polygon);
				args.push(object.power);
			} else if (object.name === 'Goalhole') {
				args.push(object.width);
			}
			this.spawnMapObject(object.name, object.x, object.y, args);
		}
	}

	spawnMapObject(objectName, x, y, args) {
		let object = this.spawner.spawn(objectName, x, y, args);
		if (!object) return false;
		this.world.addMapObject(object);
		return object;
	}

	spawnRandomGroundItem() {
		// If tile is destroyed, will not try another.
		let tile = this.model.getRandomSurfaceTile();
		if (tile.destroyed || tile.items.length >= 5 || Util.objectOverlapsWithAnyOf(tile, this.model.getPlayerList())) return null;
		let item = this.spawnItem(this.model.getRandomSurfaceItemName(), 0, 0, null, null, false);
		this.world.addGroundItem(item, tile);
		return item;
	}
	/******************************************************************/
	/******************************************************************/
	/*********************** GAME OPERATIONS **************************/
	/******************************************************************/
	/******************************************************************/
	addOrbPoints() {
		let items = this.getItems();
		let orbPoints = {};
		for (let i = 0; i < items.length; ++i) {
			if (items[i].name === 'Orb')
				items[i].addOrbPoints(true);
		}
	}
	cycleToNextAllowedTurnPlayer() {
		this.setTurnPlayerById(this.model.getIdOfNextAllowedTurnPlayer());
	}
	cycleToNextPlayerTurn() {
		let playersInfo = this.model.getPlayersInfo();
		let this_ = this;
		var currentTurnIndex = playersInfo.findIndex(function(playerInfo) {
			return playerInfo.player.id === this_.model.getTurnPlayer().id;
		});
		if (currentTurnIndex === -1) this.logger.error("WorldNetwork: Current turn player not found! This should not happen!");
		else {
			do {
				let nextTurnId = playersInfo[(currentTurnIndex+1)%playersInfo.length].player.id;
				this.model.setTurnPlayer(this.getItemById(nextTurnId));
				currentTurnIndex++;
				// If getItemById doesn't find the player, it died, go to next player
			} while (!this.model.getTurnPlayer() && currentTurnIndex < playersInfo.length * 2);

		}
	}
	setTurnPlayer(player, selectPlayer=false) {
		this.model.setTurnPlayer(player);
		if (player) {
			player.onStartDecideMove();
			this.updateThrowableItems(player);
			player.disableSpawnProtection();
		}
		this.emitEvent('TURN_PLAYER_CHANGED', [player]);
		if (selectPlayer)
			this.emitEvent('PLAYER_SELECTED', [player]);
	}
	setTurnPlayerById(playerId, selectPlayer=false) {
		this.setTurnPlayer(this.getItemById(playerId), selectPlayer);
	}
	onMoveStart() {
		this.world.resumeGameStateUpdates();
		this.turnPlayerDidTeleportPickup = false;
		this.orbPointsAdded = false;
		this.resetCalmFlags();
		for (var i = 0; i < this.world.items.length; ++i)
			this.world.items[i].onMoveStart();
	}
	onMoveEnd() {
		this.world.pauseGameStateUpdates();
		for (var i = 0; i < this.world.items.length; ++i) {
			this.world.items[i].onMoveEnd();
			if (isNaN(this.world.items[i].id)) this.world.items[i].id = this.model.generateNextId();
		}
		this.model.clearMoveType();
		this.model.setSelectedThrowItem(null);
	}
	checkGoalholesActive(anyClientMeetsPointsRequired, turnClientMeetsPointsRequired) {
		let goalholes = this.world.getGoalholes();
		let players = this.model.getPlayerList();
		for (let i = 0; i < goalholes.length; ++i) {
			goalholes[i].checkActive(anyClientMeetsPointsRequired, turnClientMeetsPointsRequired, this.getTurnPlayer());
		}
	}
	resetCalmFlags() {
		this.model.resetCalmFlags();
	}
	updateThrowableItems(throwableBy) {
		let throwableItems = [throwableBy];
		throwableItems.push.apply(throwableItems, throwableBy.getAllItemsInPileAboveHead());
		this.model.setThrowableItems(throwableItems);
	}

	prepareTangibleOfThrowableItems() {
		let throwableItems = this.model.getThrowableItems();
		let selectedItem = this.model.getSelectedThrowItem();
		for (var i = 0; i < throwableItems.length; ++i) {
			let item = throwableItems[i];
			if (item !== selectedItem)
				item.becomeIntangible();
		}
		selectedItem.becomeTangible();
	}
	performMove(playerId, moveType, velocity, targetId) {
		this.onMoveStart();
		let player = this.getItemById(playerId);
		player.disableSpawnProtection();
		if (moveType === 'JUMP') {
			player.prepareJumpNextUpdate(velocity);
			this.emitEvent('START_PERFORM_JUMP');
		} else if (moveType === 'THROW') {
			player.prepareForThrow();
			let itemToThrow = this.getItemById(targetId);
			this.updateThrowableItems(player);
			this.model.setSelectedThrowItem(itemToThrow);
			this.prepareTangibleOfThrowableItems();
			if (itemToThrow.name === 'Pebble') {
				itemToThrow.disable(false);
				player.throwPebble(itemToThrow, velocity);
			} else {
				player.throwItem(itemToThrow, velocity);
			}
			this.emitEvent('START_PERFORM_THROW');
		} else if (moveType === 'RESPAWN') {
			this.performRespawnMove(player, velocity);
		}
		this.emitEvent('PERFORMING_MOVE');
	}
	// addPlayerDeathPoints(player) {
	// 	player.addPoints(this.world.determinePointsFor('PLAYER_DEATH'));
	//
	// 	let turnPlayer = this.model.getTurnPlayer();
	// 	if (!this.model.playersHaveSameClient(player, turnPlayer)) {
	// 		turnPlayer.addPoints(this.world.determinePointsFor('PLAYER_KILL'));
	// 	}
	// }
	addPointsFor(event, instigator=null, victim=null) {
		let numPoints = this.world.determinePointsFor(event);

		if (!instigator) instigator = this.model.getTurnPlayer();
		if (instigator.name !== 'Monkey') return;
		if (victim && this.model.playersHaveSameClient(victim, instigator)) return;

		instigator.addPoints(numPoints);
	}
	/******************************************************************/
	/******************************************************************/
	/**************************** QUERIES *****************************/
	/******************************************************************/
	/******************************************************************/
	getDestroyedTileIds() {
		let ids = [];
		for (var i = 0; i < this.world.destroyedTiles.length; ++i) {
			ids.push(this.world.destroyedTiles[i].id);
		}
		return ids;
	}
	getDeadPlayers() {
		let playersInfo = this.model.getPlayersInfo();
		let deads = [];
		for (var i = 0; i < playersInfo.length; ++i) {
			if (playersInfo[i].player.disabled)
				deads.push(playersInfo[i].player);
		}
		return deads;
	}
	getItemById(id) {
		let items = this.world.items;
		for (var i = 0; i < items.length; ++i)
			if (items[i].id === id) return items[i];
		return null;
	}
	getGroundItemById(id) {
		let groundItems = this.world.groundItems;
		for (let i = 0; i < groundItems.length; ++i)
			if (groundItems[i].id === id) return groundItems[i];
		return null;
	}
	getRecentlyKilledPlayers() {
		return this.model.getRecentlyKilledPlayers();
	}
	turnPlayerCanThrow() {
		let turnPlayer = this.model.getTurnPlayer();
		return !!turnPlayer && turnPlayer.hasItemOnHead();
	}
	checkArenaIsCalm() {
		if (!this.world.allItemsAreStill()) {
			this.resetCalmFlags();
		} else {
			if (this.world.moveEndCallbacks.length !== 0) {
				this.world.callMoveEndCallbacks();
				this.resetCalmFlags();
			} else {
				this.model.setCompletelyCalm(this.model.wasPreviouslyCalm());
				this.model.setPreviouslyCalm(true);
			}
		}
	}
	playerIsAlive(playerId) {
		let player = this.getItemById(playerId);
		if (player) return player.alive && !player.disabled;
		return false;
	}
	/******************************************************************/
	/******************************************************************/
	/************************ CLIENT FUNCTIONS ************************/
	/******************************************************************/
	/******************************************************************/
	getTileById(id) {
		let tile = this.model.getTileById(id);
		if (tile !== undefined) return tile;

		let tiles = this.world.tiles;
		for (var i = 0; i < tiles.length; ++i) {
			if (!tiles[i]) continue;
			if (id < tiles[i].id) return null;
			if (tiles[i].id === id) return tiles[i];
		}
		return null;
	}
	
	playersDecideMove(turnPlayerIdsAllowed) {
		this.model.setPlayerDecidingMoveIsLocal(true);
		this.model.setTurnPlayerIdsAllowed(turnPlayerIdsAllowed);
		this.setTurnPlayerById(turnPlayerIdsAllowed[0], true);
		this.model.resetTurnPlayerMove();
		this.getTurnPlayer().onStartDecideMove();
		this.emitEvent('TURN_PLAYER_CHANGED', [this.model.getTurnPlayer()]);
		this.emitEvent('DECIDING_PLAYER');
	}

	getTurnPlayersAllowed() {
		let allowed = [];
		for (var i = 0; i < this.model.getTurnPlayerIdsAllowed().length; ++i) {
			allowed.push(this.getItemById(this.model.getTurnPlayerIdsAllowed()[i]));
		}
		return allowed;
	}

	grabMoveInfo() {
		this.model.setTargetId(this.model.getSelectedThrowItem() ? this.model.getSelectedThrowItem().id : null);
		return this.model.getTurnPlayerMove();
	}

	setRemoteTurnPlayer(playerId) {
		this.model.setPlayerDecidingMoveIsLocal(false);
		let player = this.getItemById(playerId);
		player.onStartDecideMove();
		if (player) this.model.setTurnPlayer(player);
		this.emitEvent('TURN_PLAYER_CHANGED', [player]);
	}

	// Signal for something else to add the points
	signalAddOrbPoints() {
		let this_ = this;
		this.emitEvent('ADD_ORB_POINTS', [function() {this_.orbPointsAdded = true;}]);
	}

	/******************************************************************/
	/***************************** EVENTS *****************************/
	/******************************************************************/
	registerWithWorld() {
		if (this.registeredWithWorld) return;
		this.world.registerListener(this.onWorldEvent.bind(this));
		this.spawner.setItemEventCallback(this.onItemEvent.bind(this));
		this.registeredWithWorld = true;
	}
	registerListener(eventCallback) {
		this.listenCallbacks.push(eventCallback);
	}
	emitEvent(eventName, args) {
		for (var i = 0; i < this.listenCallbacks.length; ++i)
			this.listenCallbacks[i](eventName, args);
	}
	onWorldEvent(eventName, args) {
		switch (eventName) {
			case 'PLAYER_DIED':
				this.model.addRecentlyKilledPlayer(args[0]);
				// this.addPlayerDeathPoints(args[0]);
				break;
			case 'ADD_POINTS_FOR':
				this.addPointsFor(args[0], args[1], args[2]);
				break;
		}
	}
	onItemEvent(eventName, args) {
		switch (eventName) {
			case 'TELEPORT_TO_PICKUP':
				this.turnPlayerDidTeleportPickup = true;
				break;
		}
	}
	/******************************************************************/
	/*********************** WORLD MANIPULATION ***********************/
	/******************************************************************/
	removeDeadItems() {
		for (var i = this.world.items.length-1; i >= 0; --i)
			if (!this.world.items[i].alive)
				this.removeItem(this.world.items[i].id);
	}
	removeItem(id) {this.world.removeItem(this.getItemById(id));}
	removePlayer(playerId) {this.removeItem(playerId);}
	setDesiredSpeedUpDegree(degree) {this.world.desiredSpeedUpDegree = degree;}
	killItem(item) {this.world.killItem(item);}
	/******************************************************************/
	/********************** CLIENT SELECTING MOVE *********************/
	/******************************************************************/
	selectItemToThrow(item) {
		let turnPlayer = this.model.getTurnPlayer();
		if (!item) {
			item = turnPlayer.itemsOnHead[0];
			if (!item)
				item = turnPlayer;
		}
		this.model.setSelectedThrowItem(item);
		turnPlayer.prepareForThrow(item);
		this.emitEvent('THROW_ITEM_SELECTED', [item]);
		this.prepareTangibleOfThrowableItems();
	}

	applyAimer(vector) {
		this.model.setMoveReady(true);
		this.model.setMoveVelocity(vector.x, vector.y);
	}

	changeMoveType(moveType) {
		switch (moveType) {
			case 'THROW':
				this.selectItemToThrow();
				break;
			case 'SKIP':
				this.model.setMoveReady(true);
				break;
			default:
				let throwableItems = this.model.getThrowableItems();
				for (let i = 0; i < throwableItems.length; ++i) throwableItems[i].becomeTangible();
				break;
		}
		this.model.setMoveType(moveType);
		this.emitEvent('CHANGED_MOVE_TYPE', [moveType]);
	}
	/******************************************************************/
	/******************************************************************/
	/******************* CLIENT SYNCING/HARMONIZING *******************/
	/******************************************************************/
	/******************************************************************/
	harmonizeItems(itemsData) {
		let items = [];
		for (var i = 0; i < itemsData.length; ++i) {
			let itemData = itemsData[i];
			let item = this.getItemById(itemData.id);
			if (item) this.updateExistingItem(item, itemData);
			else {
				if (itemData.name === 'Crate')
					item = this.harmonizeCrate(itemData);
				else if (itemData.name === 'ItemBubble') {
					item = this.harmonizeItemBubble(itemData);
				} else {
					item = this.spawnItem(itemData.name, itemData.x, itemData.y, itemData.id);
				}
			}
			// Don't need to sync fields on local as they share the gameobjects
			if (!GlobalInfo.LOCAL_GAME) {
				for (var k = 0; k < itemData.statuses.length; ++k) {
					if (itemData.statuses[k] === null) continue;
					item.setProperty(itemData.statuses[k].property, itemData.statuses[k].val);
				}
				if (itemData.statusEffects) {
					item.setStatusEffects(itemData.statusEffects);
				}
			}

			item.disable(itemData.disabled);
		}

		if (!GlobalInfo.LOCAL_GAME && this.world.items.length > itemsData.length) {
			// Should happen rarely
			this.removeItemsNotInData(itemsData);
		}
	};

	removeItemsNotInData(itemsData) {
		for (var i = 0; i < this.world.items.length; ++i) {
			let item = this.world.items[i];
			let itemIsValid = false;
			for (var k = 0; k < itemsData.length; ++k) {
				if (itemsData[k].id === item.id) {itemIsValid = true; break;}
			}
			if (!itemIsValid) item.flagForRemoval();
		}
	}

	// Should be done after tiles are harmonized
	harmonizeGroundItems(groundItemsData) {
		this.world.clearGroundItems();

		for (let i = 0; i < groundItemsData.length; ++i) {
			let itemData = groundItemsData[i];
			let tile = this.getTileById(itemData.tileId);
			if (!tile) continue;
			let item = this.getGroundItemById(itemData.id);
			if (!item) item = this.spawnItem(itemData.name, 0, 0, itemData.id, null, false);
			this.world.addGroundItem(item, tile);
		}
	}

	harmonizeCrate(itemInfo) {
		let size = itemInfo.statuses[0].val;
		let contentsInfo = itemInfo.statuses[1].val;
		let contents = [];
		for (var i = 0; i < contentsInfo.length; ++i) {
			let item = this.spawner.spawn(contentsInfo[i].name, 0, 0);
			item.id = contentsInfo[i].id;
			contents.push(item);
		}
		return this.spawnItem('Crate', itemInfo.x, itemInfo.y, itemInfo.id, [size, contents]);
	}

	harmonizeItemBubble(itemInfo) {
		let bubble = this.spawnItem(itemInfo.name, itemInfo.x, itemInfo.y, itemInfo.id, [{getItemById: this.getItemById.bind(this)}]);
		return bubble;
	}

	harmonizePlayersWithData(playersInfo) {
		this.model.clearPlayersInfo();
		for (let i = 0; i < playersInfo.length; ++i) {
			let p = this.getItemById(playersInfo[i].id);
			if (!p) {this.logger.error("WorldNetwork: Sync error: Could not find player!"); continue;}
			this.model.addPlayerInfo(p, playersInfo[i].nickname, playersInfo[i].clientId);
		}
	}

	harmonizeDestroyedTilesWithIds(destroyedTileIds) {
		// Check if any tiles were wrongfully destroyed
		let restoreTiles = [];
		for (var i = 0; i < this.world.destroyedTiles.length; ++i) {
			let foundTile = false;
			for (var k = 0; k < destroyedTileIds.length; ++k) {
				if (this.world.destroyedTiles[i].id === destroyedTileIds[k]) foundTile = true;
			}
			if (!foundTile) {
				restoreTiles.push(this.world.destroyedTiles[i]);
			}
		}

		for (var i = 0; i < restoreTiles.length; ++i) {
			this.world.restoreTile(restoreTiles[i]);
		}

		for (var i = 0; i < destroyedTileIds.length; ++i) {
			let tile = this.getTileById(destroyedTileIds[i]);
			if (tile) this.world.removeTile(tile);
		}
	}

	harmonizeMisc(lastGeneratedItemId, respawnIndex) {
		this.model.setNextId(lastGeneratedItemId);
		if (!GlobalInfo.LOCAL_GAME) this.model.setPlayerRespawnIndex(respawnIndex);
	}

	updateExistingItem(item, itemData) {
		item.setVelocity(0, 0);
		// this.world.addToMover(item, 'ITEM_SYNC', itemData.x, itemData.y, 300, 0);
		item.disabled = itemData.disabled;
		item.setPosition(itemData.x, itemData.y);
	}
	/******************************************************************/
	/*************************** RESPAWNING ***************************/
	/******************************************************************/
	setRandomRespawnPoint() {this.model.setRandomRespawnPoint();}

	prepareForRespawn() {
		let turnPlayer = this.model.getTurnPlayer();
		if (!turnPlayer) return true;
		if (turnPlayer.disabled) {
			this.playerRespawner.start(turnPlayer, this.model.getPlayerRespawnPoint(), this.world.killY);
			this.emitEvent('START_RESPAWN');
			this.changeMoveType('RESPAWN');
			this.world.startPawmawTracking(turnPlayer);
		} else {
			// Returns true when finished
			return this.playerRespawner.updatePrepareRespawn();
		}
	}

	performRespawnMove(player, velocity) {
		this.playerRespawner.startThrowSequence(player, velocity, this.respawnLaunchDone.bind(this));
		this.world.pawmaw.shakeTweenStarted();
	}

	respawnLaunchDone() {this.world.startPawmawTracking(null);}
	/******************************************************************/
	/************************* ALTERING MODEL *************************/
	/******************************************************************/
	updateSurfaceTiles() {this.model.updateSurfaceTiles(this.world.tiles);}

	/******************************************************************/
	/*************************** RETRIEVALS ***************************/
	/******************************************************************/
	getPlayersInfo() {return this.model.getPlayersInfo();}
	getTurnPlayer() {return this.model.getTurnPlayer();}
	getTurnPlayerMove() {return this.model.getTurnPlayerMove();}
	getThrowableItems() {return this.model.getThrowableItems();}
	getSelectedThrowItem() {return this.model.getSelectedThrowItem();}
	localPlayerIsDecidingMove() {return this.model.localPlayerIsDecidingMove();}
	getLastGeneratedItemId() {return this.model.getLastGeneratedItemId();}
	arenaIsCompletelyCalm() {return this.model.isCompletelyCalm();}
	finishedAddingOrbPoints() {return this.orbPointsAdded;}
	worldIsReady() {return this.readyToStart;}
	getItems() {return this.world.items;}
	getGroundItems() {return this.world.groundItems;}
	getRespawnIndex() {return this.model.getRespawnIndex();}
	getGoalholes() {return this.world.getGoalholes(); }
	/******************** MODIFYING RETRIEVALS ************************/
	popRandomSpawnPoint() {return this.model.popRandomSpawnPoint();}

}

return WorldNetwork;
});