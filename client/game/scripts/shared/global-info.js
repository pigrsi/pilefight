define([], function() {
// Everyone can access this to pass bits of game information that might be distant
// WARNING - THERE IS ONLY ONE OF THESE FOR EVERY GAME
// Client can use this for its game, but server can only use it as a global thing between ALL games
var GlobalInfo = {};

GlobalInfo.LOCAL_GAME = null;
GlobalInfo.GAME_MODE = '';
GlobalInfo.WINDOW_WIDTH = 1024;
GlobalInfo.WINDOW_HEIGHT = 520;
GlobalInfo.ROOM_NAME = '';
GlobalInfo.MAP_NAME = '';
GlobalInfo.GRAVITY = 0;
GlobalInfo.NEED_SAFETY = false;

return GlobalInfo;
});