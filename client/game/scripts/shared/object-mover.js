define([], function() {
// Moves items to a position while physics is disabled
class ObjectMover {
	constructor() {
		this.moveList = [];
		this.tweensRemaining = 0;
		this.usingExternalTweens = false;
		this.done = false;
	}
	
	update() {
		if (!this.usingExternalTweens)
			this.moveObjectsToDestination();
    
		this.checkMovesDone();
	}
	
	moveObjectsToDestination() {
		for (var i = 0; i < this.moveList.length; ++i) {
			let moveData = this.moveList[i];
			for (var k = 0; k < moveData.properties.length; k++) {
				let property = moveData.properties[k];
				moveData.target[property] = moveData.values[k];
			}
		}
	}
	
	forgetAllMovers() {
		this.moveList = [];
		this.done = false;
		if (this.usingExternalTweens) {
			this.stopAllTweens();
			this.tweensRemaining = 0;
		}
	}
	
	checkMovesDone() {
		for (var i = this.moveList.length - 1; i >= 0; --i) {
			let nextMove = this.moveList[i];
			let gameObject = nextMove.target;
			let allPropertiesEqual = true;
			for (var k = 0; k < nextMove.properties.length; ++k) {
				let property = nextMove.properties[k];
				if (nextMove.target[property] !== nextMove.values[k]) {
					allPropertiesEqual = false;
					break;
				}
			}
        
			if (allPropertiesEqual) {
				let move = this.moveList[i];
				if (!!move && move.callback)
					move.callback.apply(this, move.args);
				this.moveList.splice(i, 1);
				//console.log("this: Item removed: Movelist length: " + this.moveList.length);
			}
		}
		this.done = this.moveList.length === 0 && this.tweensRemaining === 0;
	}
	
	// Tweens should call this when done
	tweenDone(tween, targets, tweenInfo) {
		if (this.tweensRemaining > 0)
			this.tweensRemaining--;
		
		if (tweenInfo.callback)
			tweenInfo.callback.apply(this, tweenInfo.args);
	}
	
	setFunctionToStopTweens(tweenStopper) {
		this.stopAllTweens = tweenStopper;
	}

	// Pass it a function that sets the tweens
	setExternalTweener(callbackOnAdd) {
		this.addTween = callbackOnAdd;
		this.usingExternalTweens = true;
	}

	add(object, moverType, properties, values, configValues, onCompleteCallback, callbackArgs) {    
		configValues.ease = moverTypeToEase[moverType];
		//console.log("this: Item added, a " + object.name + "; Movelist length: " + this.moveList.length);
		if (this.addTween) {
			let tweenCallbackInfo = {callback: onCompleteCallback, args: callbackArgs};
			this.addTween(object, properties, values, configValues, this.tweenDone.bind(this), [tweenCallbackInfo]);
			this.tweensRemaining++;
		} else {
			// If yoyo, it doesn't have to move as it will end up where it started anyway
			if (configValues.yoyo) {
				for (var i = 0; i < properties.length; ++i)
					values[i] = object[properties[i]];
			}
			this.moveList.push({target: object, properties: properties, values: values, callback: onCompleteCallback, args: callbackArgs});
		}
		this.done = false;
	}

	// Doesn't block the game when added
	addPawmawMover(pawmaw, destX, destY, duration=0, tweenDelay=0, forceEaseIn=false) {
		if (!this.addTween) return false;
		let ease = null;
		if (forceEaseIn)
			ease = 'Quad.easeInOut';
		else 
			ease = pawmawEases[Math.floor(Math.random() * pawmawEases.length)];
		return this.addTween(pawmaw, ['x', 'y'], [destX, destY], {duration: duration, ease: ease, delay: tweenDelay});
	}
}

var moverTypeToEase = {
    BEING_PICKED_UP: 'Circ.easeInOut', PICKING_UP: 'Circ.easeInOut', PUSHED_UP: 'Circ.easeInOut',
    RESPAWN_START: 'Quad.easeOut', RESPAWN_PERFORM: 'Sine.easeInOut', RESPAWN_PERFORM_FINAL: 'Quad.easeOut',
    SPAWN_1: 'Quint.easeOut', SPAWN_2: 'Cubic.easeIn', ITEM_SYNC: 'Back.easeIn', WHIZ_TO_STACK: 'Back.easeInOut'
}

var pawmawEases = [
    'Quad.easeOut', 'Cubic.easeOut', 'Quart.easeOut', 'Quint.easeOut', 'Sine.easeOut', 'Expo.easeOut', 'Circ.easeOut',
    'Quad.easeInOut', 'Cubic.easeInOut', 'Quart.easeInOut', 'Quint.easeInOut', 'Sine.easeInOut', 'Expo.easeInOut', 'Circ.easeInOut'
];

return ObjectMover;
});