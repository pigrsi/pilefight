define([], function() {
    ExternalListener = {};
	ExternalListener.eventManagers = {};
	ExternalListener.addEventManager = function(events, gameId) {
		this.eventManagers[gameId] = events;
	};

    ExternalListener.messageReceived = function(messageType, data, gameId, clientId=null) {
		if (!this.eventManagers[gameId]) return;
        this.logger.debug(`Received: ${messageType} event for game ${gameId}`);
        this.eventManagers[gameId].emitLocal(messageType, data, clientId);
    };
    return ExternalListener;
});