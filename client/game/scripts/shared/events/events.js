define([], function() {
class Events {
	constructor(roomId) {
		this.roomId = roomId;
		this.members = [];
		this.emitMessageExternal = null;
	}
	
	setEmitMessageExternalCallback(callback) {
		this.emitMessageExternal = callback;
	}

	// Called by event listener
	register(eventName, callback) {
		this.members.push({eventName: eventName, callback: callback});
	}

	unregister(eventName) {
		for (let i = this.members.length-1; i >= 0; --i) {
			if (this.members[i].eventName === eventName) {
				this.members.splice(i, 1);
			}
		}
	}

	// If online, send to client/server. Otherwise send to self
	emit(eventName, arg, wrapWithSenderId, sendTo=null) {
		if (this.emitMessageExternal) {
			this.emitMessageExternal(eventName, arg, this.roomId, sendTo);
		} else {
			if (wrapWithSenderId)
				arg = {data: arg, senderId: null};
			this.emitLocal(eventName, arg);
		}
	}

	// Just pass to other event listener
	emitLocal(eventName, arg) {
		let index = this.members.findIndex(function(member) {
				return member.eventName === eventName;
			});
		if (index >= 0) this.members[index].callback.call(this, arg);
	}

	// watch out for the order of parameters here
	send(type, wrapWithSenderId, data, sendTo) {this.emit(type, data, wrapWithSenderId, sendTo);}
	// Client to server
	sendNotion(notion) {this.send('NOTION', true, notion)};

	// Server to client
	sendSync(sync) {this.send('SIGNAL', false, {signal: 'SYNC', sync: sync, blocked: true});}
	sendSignal(signal, sendTo) {this.send('SIGNAL', false, signal, sendTo);}
	sendLobbyComplete() {this.send('LOBBY_COMPLETE', false);}
}

return Events;
});