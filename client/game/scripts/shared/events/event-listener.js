define([], function() {
class EventListener {
	constructor(events) {
		this.events = events;
		this.clientPostbox = null;
		this.serverPostbox = null;

		this.events.register('SIGNAL', this.onSignal.bind(this));
	}
	
	/* SERVER RECEIVES THESE EVENTS */

	// For GAMEPLAY messages
    registerListenEventsServer(sendTo) {
        if (!sendTo) return;
        this.serverPostbox = sendTo;
        this.events.register('NOTION', this.onClientNotion.bind(this));
    }
    unregisterListenEventsServer() {
    	this.serverPostbox = null;
    	this.events.unregister('NOTION');
	}
    // For GameRunner messages (lives on top of the game)
	registerGameRunnerServer(runner) {
		this.gameRunner = runner;
		this.events.register('disconnect', this.onClientDisconnect.bind(this));
		this.events.register('CONNECTED', this.onClientConnect.bind(this));
		this.events.register('START_PRIVATE', this.onLobbyStart.bind(this));
	}

	onClientNotion(message) {
    	this.serverPostbox.onClientNotion(message.data, message.senderId);
	}
    onClientDisconnect(message) {
		// if (this.serverPostbox)
		// 	this.serverPostbox.onClientDisconnect(message.senderId);
		// if (this.gameRunner)
		this.gameRunner.onClientDisconnect(message.senderId);
    }
	onLobbyStart(message) {
		if (!this.gameRunner) return;
		this.gameRunner.lobbyComplete(message.data, message.senderId);
	}
	onClientConnect(message) {
		this.gameRunner.onClientConnect(message.data, message.senderId);
	}

    
    /* CLIENT RECEIVES THESE EVENTS */
    registerListenEventsClient(sendTo) {
        if (!sendTo) return;
        this.clientPostbox = sendTo;
    }
	registerGameRunnerClient(runner) {
		this.gameRunner = runner;
		this.events.register('LOBBY_COMPLETE', this.onLobbyComplete.bind(this));
	}

	onSignal(signal) {
		if (this.gameRunner) this.gameRunner.onSignal(signal);
		if (this.clientPostbox) this.clientPostbox.onSignal(signal);
	}
	onLobbyComplete() {this.gameRunner.onLobbyComplete();}
}

return EventListener;
});