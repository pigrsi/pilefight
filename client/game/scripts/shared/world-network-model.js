define([], function() {
   class WorldNetworkModel {
       constructor() {
           this.playersInfo = []; // {player: Monkey, name: "Benny"}
           this.turnPlayer = null;
           this.turnPlayerMove = {moveReady: false, moveType: '', velocity: {x:0, y:0}, targetId: null};
           this.turnPlayerIdsAllowed = [];
           this.throwableItems = [];
           this.selectedThrowItem = null;
           this.playerDecidingMoveIsLocal = false;
           this.calm = {previously: false, completely: false};
           this.nextId = 0;
           this.respawnIndex = 0;
           this.itemBubbles = [];
           this.recentlyKilledPlayers = [];
           this.surfaceTiles = [];
           this.idsToTiles = {};
           this.itemOdds = [];

           this.mapData = {
               playerSpawns: [],
               playerRespawns: [],
               mapItems: [],
               mapObjects: []
           };

       }

       populateIdsToTiles(tiles) {
            for (let i = 0; i < tiles.length; ++i)
                this.idsToTiles[tiles[i].id.toString()] = tiles[i];
       }

       updateSurfaceTiles(allTiles) {
           this.surfaceTiles = [];
           for (let i = 0; i < allTiles.length; ++i) {
               if (allTiles[i].interesting.top) this.surfaceTiles.push(allTiles[i]);
           }
       }

       getTileById(id) {
           return this.idsToTiles[id.toString()];
       }

       getIdOfNextAllowedTurnPlayer() {
           let index = this.getTurnPlayerIdsAllowed().indexOf(this.getTurnPlayer().id) + 1;
           return this.getTurnPlayerIdsAllowed()[index % this.getTurnPlayerIdsAllowed().length];
       }

       cycleToNextPlayerTurn() {
           let this_ = this;
           var currentTurnIndex = this.getPlayersInfo().findIndex(function(playerInfo) {
               return playerInfo.player.id === this_.turnPlayer.id;
           });
           if (currentTurnIndex === -1) this.logger.error("WorldNetwork: Current turn player not found! This should not happen!");
           else {
               let playersInfo = this.getPlayersInfo();
               do {
                   let nextTurnId = playersInfo[(currentTurnIndex+1)%playersInfo.length].player.id;
                   this.setTurnPlayer(this.getItemById(nextTurnId));
                   currentTurnIndex++;
                   // If getItemById doesn't find the player, it died, go to next player
               } while (!this.getTurnPlayer() && currentTurnIndex < playersInfo.length * 2);

           }
       }

       popRandomSpawnPoint() {
           let playerSpawns = this.getPlayerSpawns();
           let spawnIndex = Math.floor(Math.random() * playerSpawns.length);
           let spawnPos = playerSpawns[spawnIndex];
           if (playerSpawns.length > 1)
               playerSpawns.splice(spawnIndex, 1);
           return spawnPos;
       }

       setItemOdds(itemOdds) {
           if (!itemOdds) return;
           let sum = 0;
           let finalOdds = [];
           for (let i = 0; i < itemOdds.length; ++i)
               sum += itemOdds[i].value;
           let nextLower = 0;
           for (let i = 0; i < itemOdds.length; ++i) {
               finalOdds.push({name: itemOdds[i].name, lower: nextLower});
               nextLower += itemOdds[i].value / sum;
           }

           this.itemOdds = finalOdds;
       }

       // Turn player move
       resetTurnPlayerMove() {
           this.setMoveReady(false);
           this.clearMoveType();
       }
       clearMoveType() {this.turnPlayerMove.moveType = '';}
       setMoveType(moveType) {this.turnPlayerMove.moveType = moveType;}
       setMoveReady(ready=true) {this.turnPlayerMove.moveReady = ready;}
       setMoveVelocity(x, y) {this.turnPlayerMove.velocity.x = x; this.turnPlayerMove.velocity.y = y;}
       setTargetId(targetId) {this.turnPlayerMove.targetId = targetId;}

       // Turn player retrievals
       getTurnPlayerMove() {return this.turnPlayerMove;}

       // Map data updates
       setPlayerSpawns(spawns) {this.mapData.playerSpawns = spawns;}
       setPlayerRespawns(respawns) {this.mapData.playerRespawns = respawns;}
       setPlayerRespawnIndex(index) {this.respawnIndex = index;}
       addPlayerSpawn(spawn) {this.mapData.playerSpawns.push(spawn);}
       setMapItems(items) {this.mapData.mapItems = items;}
       setMapObjects(objects) {this.mapData.mapObjects = objects;}

       // Map data retrievals
       getPlayerSpawns() {return this.mapData.playerSpawns;}
       getPlayerRespawns() {return this.mapData.playerRespawns;}
       getPlayerRespawnPoint() {return this.mapData.playerRespawns[this.respawnIndex];}
       getMapItems() {return this.mapData.mapItems;}
       getMapObjects() {return this.mapData.mapObjects;}

       // Updates
       setTurnPlayer(player) {this.turnPlayer = player;}
       addPlayerInfo(playerObject, nickname, clientId) {
           this.playersInfo.push({player: playerObject, nickname: nickname, clientId: clientId});
           playerObject.clientId = clientId;
       }
       setTurnPlayerIdsAllowed(ids) {this.turnPlayerIdsAllowed = ids;}
       setThrowableItems(items) {this.throwableItems = items;}
       setSelectedThrowItem(item) {this.selectedThrowItem = item;}
       clearPlayersInfo() {this.playersInfo = [];}
       setPlayerDecidingMoveIsLocal(isLocal) {this.playerDecidingMoveIsLocal = isLocal;}
       setPreviouslyCalm(calm) {this.calm.previously = calm;}
       setCompletelyCalm(calm) {this.calm.completely = calm;}
       resetCalmFlags() {this.setPreviouslyCalm(false); this.setCompletelyCalm(false);}
       setNextId(id) {this.nextId = id;}
       addItemBubble(itemBubble) {this.itemBubbles.push(itemBubble);}
       addRecentlyKilledPlayer(player) {this.recentlyKilledPlayers.push(player);}
       setRandomRespawnPoint() {this.respawnIndex = Math.floor(Math.random() * this.mapData.playerRespawns.length)};

       // Retrievals
       generateNextId() {
           let id = this.nextId.toString();
           ++this.nextId;
           return id;
       };
       getPlayerList() {
           let info = this.getPlayersInfo();
           let list = [];
           for (let i = 0; i < info.length; ++i)
               list.push(info[i].player);
           return list;
       }
       playersHaveSameClient(player1, player2) {return player1.clientId === player2.clientId;}
       getLastGeneratedItemId() {return this.nextId;}
       getPlayersInfo() {return this.playersInfo;}
       getTurnPlayer() {return this.turnPlayer;}
       getTurnPlayerIdsAllowed() {return this.turnPlayerIdsAllowed;}
       getThrowableItems() {return this.throwableItems;}
       getSelectedThrowItem() {return this.selectedThrowItem;}
       getRandomSurfaceTile() {return this.surfaceTiles[Math.floor(Math.random() * this.surfaceTiles.length)];}
       getRandomSurfaceItemName() {
           let rand = Math.random();
           for (let i = 0; i < this.itemOdds.length; ++i) {
               if (rand >= this.itemOdds[i].lower && (i === this.itemOdds.length-1 || rand < this.itemOdds[i+1].lower))
                   return this.itemOdds[i].name;
           }
           return 'Pebble';
       }
       localPlayerIsDecidingMove() {return this.playerDecidingMoveIsLocal;}
       isCompletelyCalm() {return this.calm.completely;}
       wasPreviouslyCalm() {return this.calm.previously;}
       getRecentlyKilledPlayers() {return this.recentlyKilledPlayers;}
       getRespawnIndex() {return this.respawnIndex;}
}

   return WorldNetworkModel;
});