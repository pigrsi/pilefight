define(['entities/explosion'], function(Explosion) {
   class Goalhole {
       constructor(x, y, width, pointsRequired, world) {
           this.name = 'Goalhole';
           this.x = x;
           this.y = y;
           this.width = width;
           this.pointsRequired = pointsRequired;
           this.radius = width/2;
           this.world = world;
           this.bounding = {left: x, right: x+width, top: y, bottom: y+width};
           this.satisfied = false;
           this.turnPlayerHasEnoughPoints = false;
           this.active = false;
       }

       onCollideItem(item) {
           if (!this.isActive()) return;
           if (item.isDisabledOrDead()) return;
           if (item.name === 'Orb') {
               if (this.turnPlayerHasEnoughPoints) this.satisfied = true;
               else if (this.turnPlayer && !this.turnPlayer.isDisabledOrDead()) {
                   let xOffset = (this.turnPlayer.x+this.turnPlayer.halfWidth > this.x + this.radius) ? -16 : 16;
                   this.world.addExplosion(new Explosion(this.turnPlayer.x+this.turnPlayer.halfWidth+xOffset, this.turnPlayer.y + this.turnPlayer.halfHeight, 96, 2, this.world));
               }
           }
           item.setVelocity(0);
           item.die();
       }

       checkActive(anyClientHasEnoughPoints, turnClientHasEnoughPoints, turnPlayer){
           this.active = anyClientHasEnoughPoints;
           this.turnPlayerHasEnoughPoints = turnClientHasEnoughPoints;
           this.turnPlayer = turnPlayer;
       }

       isActive() {return this.active;}

       isSatisfied() {
           return this.satisfied;
       }
   }

   return Goalhole;
});