define(['items/item', 'entities/explosion'], function(Item, Explosion) {
class TimeBomb extends Item {
    constructor(x, y, world) {
        super(x, y, 48, 48, world);
        this.name = 'TimeBomb';
        this.mass = 6;
        this.power = 4;
        this.turnCountdown = 3;
        this.explosive = true;
		this.explodeRadius = 300;
        this.statuses = [{property: 'turnCountdown', val: this.turnCountdown}];
    }
    
    countdown() {
        this.turnCountdown = Math.max(0, this.turnCountdown - 1);
    }
	
	checkExplode() {
		if (this.turnCountdown === 0)
            this.explode();
	}
    
    onMoveStart() {
        this.world.addMoveEndCallback(this.checkExplode, this);
    }
    
    // explode() {
        // if (this.alive)
            // this.world.addExplosion(new Explosion(this.x+this.halfWidth, this.y+this.halfHeight, 300, 2, this.world));
        // this.die();
    // }
    
    bump(bumpedBy, otherVelocity, toRight) {
        super.bump(bumpedBy, otherVelocity, toRight);
        this.countdown();
    }
    
    onMoveEnd() {
        super.onMoveEnd();
        this.statuses[0].val = this.turnCountdown;
    }
}
return TimeBomb;
});