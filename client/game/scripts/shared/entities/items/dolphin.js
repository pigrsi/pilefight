define(['items/item'], function(Item) {
class Dolphin extends Item {
    constructor(x, y, world) {
        super(x, y, 48, 48, world);
        this.name = 'Dolphin';
        this.mass = 3;
        this.power = 3;
		this.addStatusEffect('Float');
		this.customCollision = this.onItemCollide;
		this.timeTakenToExplode = 450;
		this.explosive = true;
		this.deployed = false;
    }
	
	preUpdate() {
		if (this.deployed) {
			if (this.wasInWater && !this.isFloatingOnWater()) this.setVelocityY(this.velocity.y * 1.5);
			this.wasInWater = this.isFloatingOnWater();
		}
		super.preUpdate();
	}

	update() {
		if (this.deployed) {
			this.power = 5 + 3*(this.width/48);
			this.minimumBumpVelocity.x = this.velocity.x === 0 ? 0 : Math.abs(this.velocity.x) + 4;
			this.setVelocityX(this.launchSpeedX);
		}
		super.update();
	}
	
	onLandingOnWater(waterHeight) {
		if (!this.deployed && !this.StandingOnItem) this.deploy();
		if (this.deployed) {
			if (this.width < 48*6) {
				let growth = 0.75;
				this.x -= growth/2;
				this.y -= growth/2;
				this.setSize(this.width+growth, this.height+growth);
			}
		}
		return super.onLandingOnWater.apply(this, arguments);
	}
	
	explode() {
		this.explodeRadius = this.width * 1.25;
		super.explode();
	}
	
	launchAsAttack(velocity, thrownBy) {
		super.launchAsAttack.apply(this, arguments);
		this.collideTiles = false;
    }
	
	deploy() {
		if (this.deployed) return;
		this.deployed = true;
		this.collideTiles = false;
		this.launchSpeedX = this.velocity.x;
		this.startExplodeTimer();
	}
	
	bump(bumpedBy, otherVelocity, toRight) {
		if (!this.deployed)
			return super.bump.apply(this, arguments);
	}
	
	canPickUpItem() {return !this.deployed && super.canPickUpItem()};
	canBePickedUp() {return !this.deployed && super.canBePickedUp()};
	onItemCollide(item) {
		if (!this.deployed || item.name === 'ItemBubble') return false;
		item.bump(this, this.velocity, this.velocity.x >= 0);
		return true;
	}
	
	onMoveEnd() {
		super.onMoveEnd();
		this.collideTiles = true;	
	}
}
return Dolphin;
});