define(['items/shooter', 'items/bubble'], function(Shooter, Bubble) {
class BubbleGun extends Shooter {
    constructor(x, y, world) {
        super(x, y, 48, 48, world);
        this.name = 'BubbleGun';
        this.mass = 3;
        this.power = 3;
		this.maxSpawns = 3;
    }
    
	spawnProjectile() {
        let velocity = {x: 2, y: 1.5};
        switch (this.numSpawned) {
            case 1: velocity.x = 4; velocity.y = 1; break;
            case 2: velocity.x = 6; velocity.y = 0.5; break;
        }
        let xMod = 0;
        if (this.lastNonZeroXVelocity < 0) {
            velocity.x *= -1;
            xMod = -64;
        }
        let bubble = this.world.addItem(new Bubble(this.x + this.halfWidth + xMod, this.y - 8, this, this.world));
        bubble.setVelocity(velocity);
    }
}
return BubbleGun;
});




