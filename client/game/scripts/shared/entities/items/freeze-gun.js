define(['items/shooter', 'items/snowball'], function(Shooter, Snowball) {
class FreezeGun extends Shooter {
    constructor(x, y, world) {
        super(x, y, 48, 48, world);
        this.name = 'FreezeGun';
        this.mass = 5;
        this.power = 4;
    }
    
    spawnProjectile() {
        let velocity = {x: 15, y: -4};
        let xMod = 0;
        if (this.lastNonZeroXVelocity < 0) {
            velocity.x *= -1;
            xMod = -96;
        }
        let snowball = this.world.addItem(new Snowball(this.x + this.width + xMod, this.y - 8, this.world));
        snowball.launchAsAttack(velocity);
		snowball.setStun(true, 180);
    }
}
return FreezeGun;
});