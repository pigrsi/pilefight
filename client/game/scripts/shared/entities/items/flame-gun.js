define(['items/shooter', 'items/flame'], function(Shooter, Flame) {
class FlameGun extends Shooter {
    constructor(x, y, world) {
        super(x, y, 48, 48, world);
        this.name = 'FlameGun';
        this.mass = 4;
        this.power = 3;
		this.maxSpawns = 5;
		this.explosive = true;
		this.cooldownBetweenShots = 4;
		this.timeTakenToExplode = 180;
    }
	
	update() {
		super.update();
		if (this.catchingFire) {
			if (this.catchFireIn-- === 0) {
				this.addStatusEffect('OnFire');
			}
		}
	}
    
    spawnProjectile() {
        let velocity = {x: 5, y: -6};
        switch (this.numSpawned) {
            case 1: velocity.x = 6; velocity.y = -6.2; break;
            case 2: velocity.x = 7; velocity.y = -6.4; break;
            case 3: velocity.x = 8; velocity.y = -6.6; break;
            case 4: velocity.x = 9; velocity.y = -6.8; break;
        }
        let xMod = 0;
        if (this.lastNonZeroXVelocity < 0) {
            velocity.x *= -1;
            xMod = -96;
        }
        let flame = this.world.addItem(new Flame(this.x + this.width + xMod, this.y, this.world));
        flame.setVelocity(velocity);
		
		if (this.numSpawned >= 4) {
			this.startCatchFire();
		}
    }
	
	startCatchFire() {
		if (this.catchingFire) return;
		this.catchFireIn = 90;
		this.catchingFire = true;
	}
	
	isStill() {
		return !this.catchingFire && super.isStill();
	}
}
return FlameGun;
});