define(['items/item'], function(Item) {
class Monkey extends Item {
    constructor(x, y, world, onEvent) {
        super(x, y, 40, 40, world);
        this.name = 'Monkey';
        this.canPluck = false;
        this.clientPoints = 0;
        this.serverPoints = 0;
        this.teleportToPickupBonusTimeLimit = 30;
        this.timeSinceTeleported = this.teleportToPickupBonusTimeLimit;
        this.onEvent = onEvent;
        this.preventShowTrajectory = true;
		this.jumpVelocity = {x: 0, y: 0};
        this.mass = 3;
        this.power = 3;
        this.nextPluckTiles = [];
        // v When picking up something, put all items in this array, to avoid picking up glitch thing
        // Where another item is picked up before the stack settles
        this.itemsAtLastPickup = [];
        this.clearLastPickupArrayCountdown = 0;
    }
    
    update() {
        super.update();
        if (this.skipPluckTileNextFrame) {this.skipPluckTileNextFrame = false;}
        else if (this.nextPluckTiles.length > 0) this.pluckItemsFromGround(this.nextPluckTiles);

		if (this.jumpNextUpdate) {this.jump(this.jumpVelocity); this.jumpNextUpdate = false;}
        if (this.timeSinceTeleported < this.teleportToPickupBonusTimeLimit) this.timeSinceTeleported++;
        if (this.itemsAtLastPickup.length > 0 && --this.clearLastPickupArrayCountdown <= 0) this.itemsAtLastPickup = [];
        this.updated();
    }
    
    // Just for animator
    updated() {}
    
    updateDead() {
        super.updateDead();
        this.canPluck = false;
    }
    
    onLandingOnItem(item) {
        if (!this.canPluckItem() || !item.canBePickedUp()) super.onLandingOnItem(item);
    }
    
    afterLandingOnItem() {
		// pickUpItem is here and not in onLandingOnItem so that we pick up the item closest to our centre instead of the first one we collide with below us.
        if (!this.canPluckItem()) super.afterLandingOnItem();
        else {
            let itemBelow = this.getItemBelow();
            if (itemBelow) this.pickUpItem(itemBelow);
        }
    }

    hitTileFromAbove(tile) {
        if (tile.items.length > 0 && this.canPluckFromGround()) this.nextPluckTiles.push(tile);
        return false;
    }

    onPushedUpByTile(tile) {
        this.hitTileFromAbove(tile);
        return false;
    }
	
	pickUpItem(item) {
		if (!item.canBePickedUp()) return;
        this.setVelocity(0);
		
		this.world.addToMover(this, 'PICKING_UP', item.x+item.halfWidth-this.halfWidth, item.y-(this.height-item.height), 600, 80);
        this.world.addToMover(item, 'BEING_PICKED_UP', item.x, this.y, 600, 60);

        // Not setting this on player. If an item is on a ledge by one pixel, setting this here will make player fall off the edge.
        // It's safe to not turn on because we know there was just an item of the same size in that spot.
		// this.justTeleported = true;
		item.justTeleported = true;

        this.shiftUpItemsOnHead(item.x+item.halfWidth, this.y, item);
		
		// Aftermath
		item.setVelocity(this.velocity.x, this.velocity.y);
		this.canPluck = false;
        this.skipPluckTileNextFrame = true;
        item.onPlucked();
		if (this.timeSinceTeleported < this.teleportToPickupBonusTimeLimit)
            this.onEvent('TELEPORT_TO_PICKUP', [this]);
	}

    pluckItemsFromGround(tileChoices) {
        // Get tile closest to centre
        let tile;
        let smallestDist = 1000;
        for (let i = 0; i < tileChoices.length; ++i) {
            let currentTile = tileChoices[i];
            let currentDist = Math.abs((currentTile.x+currentTile.width/2) - (this.x+this.halfWidth));
            if (!tile || currentDist < smallestDist) {
                smallestDist = currentDist;
                tile = currentTile;
            }
        }

        let item = tile.items[0];
        item.setPosition(tile.x, tile.y);
        this.world.revealGroundItem(item, tile);
        this.world.addToMover(item, 'BEING_PICKED_UP', this.x + this.halfWidth - item.halfWidth, this.y - item.height, 600, 60);
        item.justTeleported = true;

        this.clearLastPickupArrayCountdown = 10;

        this.shiftUpItemsOnHead(this.x+this.halfWidth, this.y - item.height, item);

        item.setVelocity(this.velocity.x, this.velocity.y);
        item.onPlucked();
        this.nextPluckTiles = [];
        this.skipPluckTileNextFrame = true;
    }

    shiftUpItemsOnHead(itemX, startY, pluckedItem) {
        this.I_AM_DONE_WITH_THIS = true;
        pluckedItem.I_AM_DONE_WITH_THIS = true;

        if (this.itemsOnHead.length !== 0 || pluckedItem.itemsOnHead.length !== 0) {
            let itemsAbove = this.itemsAtLastPickup.slice();
            itemsAbove.push.apply(itemsAbove, pluckedItem.getAllItemsInPileAboveHead(this));
            itemsAbove.push.apply(itemsAbove, this.getAllItemsInPileAboveHead());
            this.itemsAtLastPickup = [pluckedItem];
            let nextY = startY;
            //if (itemsAbove[0]) nextY -= (itemsAbove[0].height - 0.001);
            for (let i = 0; i < itemsAbove.length; ++i) {
                if (itemsAbove[i].I_AM_DONE_WITH_THIS) continue;
                nextY -= (itemsAbove[i].height - 0.001);
                this.world.addToMover(itemsAbove[i], 'BEING_PICKED_UP', itemX-itemsAbove[i].halfWidth, nextY, 600, 60);
                itemsAbove[i].justTeleported = true;
                itemsAbove[i].I_AM_DONE_WITH_THIS = true;
                itemsAbove[i].setVelocity(this.velocity.x, this.velocity.y);
                this.itemsAtLastPickup.push(itemsAbove[i]);
            }
            for (let i = 0; i < itemsAbove.length; ++i) itemsAbove[i].I_AM_DONE_WITH_THIS = null;
        }

        this.I_AM_DONE_WITH_THIS = false;
        pluckedItem.I_AM_DONE_WITH_THIS = false;
    }
	
	prepareJumpNextUpdate(velocity) {
		this.jumpNextUpdate = true;
		this.jumpVelocity = velocity;
	}
    
    jump(velocity) {		
        this.setVelocity(velocity);
        this.canPluck = true;
		this.stopStandingOnItems();
        this.allowDrag = false;
        this.jumped = true;
    }

    // teleportTo(x, y, keepPile=false) {
    //     this.timeSinceTeleported = 0;
    //     super.teleportTo.apply(this, arguments);
    // }

    zipTo(x, y, keepPile=false) {
        this.timeSinceTeleported = 0;
        super.zipTo.apply(this, arguments);
    }
    
    throwItem(item, velocity) {
        item.launchAsAttack(velocity, this);
		if (item.name === 'Monkey') item.setStun(true, 75);
        this.world.itemJustThrown();
        return item;
    }
	
	throwPebble(pebble, velocity) {
		pebble.setPosition(this.x + this.halfWidth, this.y + this.halfHeight);
		this.throwItem(pebble, velocity);
	}
    
    hasItemOnHead() {
        return this.itemsOnHead.length !== 0;
    }
    
    prepareForThrow(item=null) {
        this.becomeIntangible(false, 0.8);
    }
    
    launchAsAttack(velocity, thrownBy) {
        super.launchAsAttack(velocity, thrownBy);
        this.canPluck = false;
    }
    
    canPluckItem() {return this.canPluck && !this.isStunned() && !this.isFrozen() && !this.isZipping();}

    canPluckFromGround() {return !this.isStunned() && !this.isFrozen() && !this.isOnFire() && !this.jumpNextUpdate;}
    
    onMoveEnd() {
        super.onMoveEnd();
        this.jumped = false;
        this.statuses[0] = {property: 'clientPoints', val: this.serverPoints};
        this.canPluck = false;
    }

    moveWithItemsStoodOn() {
        if (this.jumped) return;
        return super.moveWithItemsStoodOn.apply(this, arguments);
    }
    
    die() {
        this.world.playerDied(this);
        this.disable(true);
    }

    onCaughtInExplosion() {
        super.onCaughtInExplosion.apply(this, arguments);
        this.world.addPointsFor('EXPLOSION_ATTACK_HIT', null, this);
    }

    enableSpawnProtection() {
        this.addStatusEffect('Shield');
    }

    disableSpawnProtection() {
        this.removeStatusEffect('Shield');
    }

    onStartDecideMove() {
        this.disableSpawnProtection();
    }

    addPoints(points, onServer=true, onClient=true) {
        if (onServer) this.serverPoints = Math.max(0, this.serverPoints + points);
        if (onClient) {
            this.clientPoints = Math.max(0, this.clientPoints + points);
            this.world.pointsAdded(this, points);
        }

    }
}
return Monkey;
});