define(['items/item'], function(Item) {
class BlootBlock extends Item {
    constructor(x, y, world) {
        super(x, y, 48, 48, world);
        this.name = 'BlootBlock';
        this.mass = 8;
        this.power = 6;
        this.customCollision = this.onCollide;
        this.maxVelocity.y = 25;
        this.launchVelocityY = 0;
        this.gravity = 0.5;
        this.numBounces = 0;
        this.blootDropCountdown = 45;
		this.tilesToBreak = [];
        this.victims = [];
    }
    
    update() {
        if (this.blootDropping) {
			this.breakFlaggedTiles();
            this.blootDrop();
            this.updateVictims();
        } else {
            if (this.launched) {
                if (this.numBounces >= 3) {
                    if (this.velocity.y >= 8 || this.isStandingOnSomething()) {
                        this.startBlootDrop();
                    }
                } else if (this.isStandingOnSomething()) {
                    this.blootBounce();
                }
                this.velocity.y += this.gravity;
            }
            super.update();
        }
    }
	
	breakFlaggedTiles() {
		if (this.tilesToBreak.length === 0) return;
		for (let i = 0; i < this.tilesToBreak.length; ++i) {
			this.world.removeTile(this.tilesToBreak[i]);
		}
		this.tilesToBreak.length = 0;
	}
	
	blootBounce() {
		this.numBounces++;
		this.velocity.y = this.launchVelocityY + Math.min((0.3 * this.launchVelocityY * this.numBounces), -20*this.numBounces*0.3);
	}
    
    updateVictims() {
        for (var i = 0; i < this.victims.length; ++i) {
            this.victims[i].y = this.y + this.height;
            this.victims[i].x = this.victims[i].prev.x;
        }
    }
    
    blootDrop() {
        if (this.blootDropCountdown-- > 0) {
            this.velocity.y = 0;
            return;
        }
        this.velocity.y += 1;
    }
    
    onCollide(other) {
        if (!this.blootDropping) return false;
        if (other.ableToMove() && !other.beingForciblyMoved && other.y + other.height > this.y + this.height) {
            this.addVictim(other);
            return true;
        }
        return false;
    }
	
	onLandingOnItem(item) {
		if (this.blootDropping) return true;
		return super.onLandingOnItem(item);
	}
    
    onTileCollide(tile) {
        if (!this.blootDropping) return super.onTileCollide(tile);
        this.flagTileToBeBroken(tile);
        //this.velocity.y = -10;
        return true;
    }
	
	flagTileToBeBroken(tile) {
		this.tilesToBreak.push(tile);
	}
    
    startBlootDrop() {
        this.blootDropping = true;
        this.maxVelocity.y = 20;
        this.setVelocity(0, 0);
        this.setSize(96, 96);
		this.statusEffects.clear();
        this.x -= 24;
        this.y -= 24;
    }
    
    addVictim(item) {
        this.victims.push(item);
        item.setBeingForciblyMoved(true);
    }
    
    bump(bumpedBy, otherVelocity, toRight) {
        if (!this.blootDropping) super.bump(bumpedBy, otherVelocity, toRight);
    }
	
	canHaveStatusEffects() {
		return !this.blootDropping && super.canHaveStatusEffects();
	}
    
    launchAsAttack(velocity, thrownBy) {
        super.launchAsAttack(velocity, thrownBy);
        this.allowGravity = false;
        this.launched = true;
        this.allowDrag = false;
        // velocity.y = Math.min(-10, velocity.y);
        this.setVelocity(velocity);
        this.launchVelocityY = -Math.abs(velocity.y);
        super.launchAsAttack(velocity, thrownBy);
    }
    
}
return BlootBlock;
});