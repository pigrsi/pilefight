define(['items/item'], function(Item) {
class Orb extends Item {
    constructor(x, y, world) {
        super(x, y, 48, 48, world);
        this.name = 'Orb';
        this.mass = 8;
        this.power = 10;
		this.customCollision = this.onCollide;
    }
		
	addOrbPoints(onServer) {
		this.addPointsForLevel(this.itemsStandingOn, 1, onServer);
	}
	
	addPointsForLevel(itemsAtLevel, level, onServer) {
		for (let i = 0; i < itemsAtLevel.length; ++i) {
			let item = itemsAtLevel[i];
			if (item.name === 'Monkey')
				item.addPoints(level * this.world.determinePointsFor('ORB'), onServer, !onServer);
			this.addPointsForLevel(item.itemsStandingOn, level + 1, onServer);
		}
	}
	
	onCollide(item) {
		if (item.name !== 'Pebble' || this.bouncedPebble === item) return false;
		item.setVelocity(item.velocity.x * -1, item.velocity.y * -1);
		item.power = 5;
		item.becomeTangible();
		this.bouncedPebble = item;
		return true;
	}
}
return Orb;
});