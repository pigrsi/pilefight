define(['items/item'], function(Item) {
class Bread extends Item {
	constructor(x, y, world) {
        super(x, y, 48, 48, world);
        this.name = 'Bread';
        this.mass = 1;
        this.power = 1;
        this.customCollision = this.onCollideItem;
		this.addStatusEffect('Bread');
    }
}

return Bread;
});