define(['items/item'], function(Item) {
class Snowball extends Item {
	constructor(x, y, world) {
		super(x, y, 48, 48, world);
        this.name = 'Snowball';
        this.mass = 3;
        this.power = 3;
		this.bounce.y = 0;
        this.customCollision = this.onCollideItem;
	}
	
	onCollideItem(item) {
		if (!this.launchedAsAttack) return false;
		if (!item.canHaveStatusEffects()) return false;
		this.inflictFrozen(item);
		return false;
	}
	
	inflictFrozen(item) {
		item.addStatusEffect('Frozen');
		this.die();
	}
}

return Snowball;
});