define(['items/item'], function(Item) {
class Bomb extends Item {
    constructor(x, y, world) {
        super(x, y, 48, 48, world);
        this.name = 'Bomb';
        this.mass = 5.5;
        this.power = 4.5;
        this.explosive = true;
        this.explodeCountdown = 110;
    }
    
    update() {
        if (this.triggered && (this.explodeCountdown-- <= 0 || super.isStill()))
            this.explode();
        super.update();
    }
    
    isStill() {
        return this.triggered ? false : super.isStill();
    }

    launchAsAttack(velocity, thrownBy) {
        this.triggered = true;
        super.launchAsAttack(velocity, thrownBy);
    }
    
    bump(bumpedBy, otherVelocity, toRight) {
        super.bump(bumpedBy, otherVelocity, toRight);
        // this.triggered = true;
        // if (this.explodeCountdown > 20) this.explodeCountdown = 20;
    }
}
return Bomb;
});