define(['items/item'], function(Item, Explosion) {
class Missile extends Item {
    constructor(x, y, world) {
        super(x, y, 48, 48, world);
        this.name = 'Missile';
        this.mass = 7.5;
        this.power = 6;
        this.fired = false;
        this.deathTimer = 180;
        this.rocketCountdown = 60;
        this.explosive = true;
        this.lastNonZeroXVelocity = 0;
    }
    
    update() {
        if (this.fired) {
            if (this.blastingOff) {
                if (this.alive) this.checkCrashed();
                this.velocity.y -= 0.005;
            } else {
                if (this.rocketCountdown-- <= 0)
                    this.blastOff();
            }
        }
        
        if (!this.blastingOff && this.velocity.x !== 0)
            this.lastNonZeroXVelocity = this.velocity.x;
        super.update();
    }
    
    launchAsAttack(velocity, thrownBy) {
        super.launchAsAttack(velocity, thrownBy);
        this.fired = true;
    }
    
    blastOff() {
        this.allowGravity = false;
        this.allowDrag = false;
        this.blastingOff = true;
        this.maxVelocity.x = 30;
        this.velocity.x = this.lastNonZeroXVelocity > 0 ? 30 : -30;
        this.velocity.y = 0;
    }
    
    shouldUseDrag() {
        return !this.blastingOff && super.shouldUseDrag();
    }
    
    checkCrashed() {
        if (this.velocity.x * this.lastNonZeroXVelocity <= 0 || this.deathTimer-- <= 0)
            this.explode();
    }
    
    isStill() {
        if (!this.fired) return super.isStill();
        return false;
    }
    
}
return Missile;
});