define(['items/item'], function(Item) {
class Flame extends Item {
	constructor(x, y, world) {
		super(x, y, 48, 48, world);
        this.name = 'Flame';
        this.mass = 1;
        this.power = 1;
		this.allowBounce = false;
        this.customCollision = this.onCollideItem;
		this.drag = 1;
		this.statusEffects.addImmunity('OnFire');
		this.statusEffects.addImmunity('Frozen');
		this.immuneToExplosions = true;
	}
	
	update() {
		super.update();
	}
	
	onCollideItem(item) {
		this.inflictFlame(item);
		return true;
	}

	inflictFlame(item) {
		if (!item.isOnFire()) item.addStatusEffect('OnFire', 300);
	}
	
	canBePickedUp() {return false;}
	canPickUpItem() {return false;}
	
	onMoveEnd() {
		//this.die();
	}
}
return Flame;

});