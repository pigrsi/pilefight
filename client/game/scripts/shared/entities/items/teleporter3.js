define(['items/teleporter2'], function(Teleporter2) {
class Teleporter3 extends Teleporter2 {
    constructor(x, y, world) {
        super(x, y, world);
        this.name = 'Teleporter3';
        this.customCollision = this.onCollide;
    }

    onCollide(other) {
        if (!this.thrownBy || !other.canMove) return false;
        this.thrownBy = other;
        return true;
    }
}
return Teleporter3;
});