define(['items/item', 'entities/laser'], function(Item, Laser) {
class LaserEmitter extends Item {
    constructor(x, y, world) {
        super(x, y, 48, 48, world);
        this.name = 'LaserEmitter';
        this.mass = 5;
        this.power = 5;
		this.lasers = [];
		this.lasersOn = false;
		this.statuses.push({property: 'lasersOn', val: this.lasersOn});
    }
	
	update() {
		super.update();
		if (this.lasersOn) {
			this.lasers[0].setPosition(this.x+this.halfWidth, this.y);
			this.lasers[1].setPosition(this.x+this.halfWidth, this.y+this.height);
		}
	}
	
	fireLasers() {
		if (!this.alive) return;
		let laser;
		laser = new Laser(this.x+this.halfWidth, this.y, 'up', 500, this, this.world);
		this.world.addLaser(laser);
		this.lasers.push(laser);
		laser = new Laser(this.x+this.halfWidth, this.y+this.height, 'down', 500, this, this.world);
		this.world.addLaser(laser);
		this.lasers.push(laser);
		
		this.lasersOn = true;
	}
	
	turnOffLasers() {
		for (let i = 0; i < this.lasers.length; ++i) {
			this.world.removeLaser(this.lasers[i]);
		}
		this.lasers.length = 0;
		this.lasersOn = false;
	}
	
	addStatusEffect(effect, duration) {
		super.addStatusEffect.apply(this, arguments);
		if (effect !== 'Stunned') this.turnOffLasers();
	}
	
	onMoveEnd() {
		this.statuses[0].val = this.lasersOn;
		super.onMoveEnd();
	}
	
	bump() {
		super.bump.apply(this, arguments);
	}
	
	setStun(stun, duration) {
		if (stun && duration > 0) this.turnOffLasers();
		return super.setStun.apply(this, arguments);
	}
	
	launchAsAttack() {
		super.launchAsAttack.apply(this, arguments);
		this.world.addMoveEndCallback(this.fireLasers, this);
	}
	
	setProperty(property, value) {
		if (property === 'lasersOn' && value !== this.lasersOn) {
			if (!this.lasersOn) this.fireLasers();
			else this.turnOffLasers();
		}
		super.setProperty.apply(this, arguments);
	}
	
	onPlucked() {
		this.turnOffLasers();
	}
	
	// canBePickedUp() {return !this.lasersOn && super.canBePickedUp();}
	// canPickUpItem() {return !this.lasersOn && super.canPickUpItem();}
}

return LaserEmitter;
});