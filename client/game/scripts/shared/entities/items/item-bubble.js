define(['items/item'], function(Item) {
class ItemBubble extends Item {
    constructor(x, y, world, getItemsForSpawnLevel, args) {
        super(x, y, 96, 96, world);
        this.name = 'ItemBubble';
		this.collideTiles = false;
		this.allowGravity = false;
		this.canBeThrown = false;
		this.customCollision = this.onCollide;
		this.spawnItem = args.spawnItem;
		this.getItemById = args.getItemById;
		this.cooldown = args.cooldown ? args.cooldown : 2;
		this.canMove = false;
		this.turnsUntilRespawn = 0;
		this.canDecrementRespawnCounter = true;
		// Client does not need to know about cooldown, set it to null for them
		this.strength = parseInt(args.strength);
		this.prevStrength = this.strength;
		if (isNaN(this.strength)) this.strength = 0;
		this.autoAdd = args.autoAdd === undefined ? true : args.autoAdd;
		this.statuses[1] = {property: 'cooldown', val: null};
		this.statuses[2] = {property: 'strength', val: this.strength};
		this.statuses[3] = {property: 'autoAdd', val: this.autoAdd};
		this.spawnables = this.determineSpawnables(args, getItemsForSpawnLevel);
		this.disable(true);
    }
	
	onCollide(item) {
		if (item === this.bubbledItem) return true;
		if (this.canBeOpenedByItem(item)) {
			this.prepareReleaseItem(item);
		} else {
			return false;
		}
        return true;
    }
	
	canHaveStatusEffects() {return false;}
	
	canBeOpenedByItem(item) {
		if (!item) return false;
		switch(this.strength) {
			case 0: return true;
			case 1: return item.name !== 'Pebble';
			case 2: return item.pileContainsItem('Monkey');
			default: return false;
		}
	}
	
	ableToMove() {return false;}
	
	prepareReleaseItem(itemThatHitBubble) {
		this.itemThatHitBubble = itemThatHitBubble; 
	}
	
	releaseItem(itemThatPoppedBubble) {
		if (!this.bubbledItem) return;		
		let item = this.bubbledItem;
		item.managedByOtherItem = false;
		item.collideItems = true;
		item.immuneToExplosions = false;
		item.canBeThrown = true;
		item.allowGravity = true;
		item.canMove = true;
		item.setVelocity(0, 0);
		this.itemThatHitBubble = false;
		if (this.autoAdd) {
			this.addItemToStack(item, itemThatPoppedBubble);
		} else {
			this.world.addToMover(this, '', this.x, this.y, 250, 0);
		}
		this.disable(true);
	}
	
	addItemToStack(item, itemThatPoppedBubble) {
		if (!itemThatPoppedBubble || !item || itemThatPoppedBubble.isDisabledOrDead()) return;
		let addTo = itemThatPoppedBubble.thrownBy ? itemThatPoppedBubble.thrownBy : itemThatPoppedBubble;
		if (itemThatPoppedBubble.name === 'Monkey') addTo = itemThatPoppedBubble;
		if (!addTo.canPickUpItem() || !addTo.collideItems) return;
		let top = addTo.getTopOfPile();
		let onComplete = function() {top.bubbledItem = item; item.justTeleported = true; item.setVelocity(top.velocity.x, top.velocity.y);}
		this.world.addToMover(item, 'WHIZ_TO_STACK', top.x+top.halfWidth-item.halfWidth, top.y - item.height, 600, 0, onComplete);
	}
	
    update() {
        super.update();
		if (this.itemThatHitBubble) this.releaseItem(this.itemThatHitBubble);
		if (this.strength !== this.prevStrength) this.onStrengthChange();
		this.setItemPosition();
    }
	
	checkItemChanged() {
		if (this.itemId) {
			if (this.bubbledItem && this.itemId === this.bubbledItem.id) {
				this.itemId = null;
				return;
			}
			this.holdItem(this.getItemById(this.itemId));
			this.itemId = null;
		}
	}
	
	onStrengthChange() {
		// For animator
	}
	
	reset() {
		let itemName = this.getRandomItemName();
		this.disable(false);

		if (this.spawnItem) {
			this.holdItem(this.spawnItem(itemName, 0, 0));
			this.statuses[0] = {property: 'itemId', val: this.bubbledItem.id};
		}
	}
	
	holdItem(item) {
		this.disable(false);
		this.bubbledItem = item;
		item.allowGravity = false;
		item.managedByOtherItem = true;
		item.canMove = false;
		this.setItemPosition();
		
		item.immuneToExplosions = true;
		item.canBeThrown = false;
		item.collideItems = false;
	}
	
	getRandomItemName() {
		return this.spawnables[Math.floor(Math.random() * this.spawnables.length)];
	}
	
	setItemPosition() {
		if (!this.bubbledItem) return;
		this.bubbledItem.setPosition(this.x+this.halfWidth-this.bubbledItem.halfWidth, this.y+this.halfHeight-this.bubbledItem.halfHeight);
	}
	
	determineSpawnables(settings, spawnLevelToItemList) {
		let spawnLevels = settings.spawnLevels ? settings.spawnLevels.split(',') : [];
		let include = settings.include ? settings.include.split(',') : [];
		let exclude = settings.exclude ? settings.exclude.split(',') : [];
		let spawnables = [];
		for (let i = 0; i < spawnLevels.length; ++i) {
			spawnables.push.apply(spawnables, spawnLevelToItemList(spawnLevels[i]));
		}
		for (let i = 0; i < include.length; ++i) {
			if (spawnables.indexOf(include[i]) === -1)
				spawnables.push(include[i]);
		}
		for (let i = 0; i < exclude.length; ++i) {
			let index = spawnables.indexOf(exclude[i]);
			if (index !== -1) spawnables.splice(index, 1);
		}
		return spawnables.length > 0 ? spawnables : ['Rock'];
	}
	
	onMoveStart() {
		this.checkItemChanged();
		this.canDecrementRespawnCounter = true;
	}
	
	onMoveEnd() {
		super.onMoveEnd();
		if (this.disabled && this.cooldown !== null && this.canDecrementRespawnCounter) {
			if (--this.turnsUntilRespawn <= 0) {
				this.turnsUntilRespawn = this.cooldown;
				this.reset();
			}
			this.canDecrementRespawnCounter = false;
		}
	}
	
	disable(setDisable) {
		super.disable(setDisable);
		this.statuses[0] = null;
		this.canMove = false;
	}
		
	isImmuneToExplosions() {return true;}
	canPickUpItem() {return false;}
	canBePickedUp() {return false;}
	bounceOnRisingCollision() {return true;}
	canBePushed() {return false;}
	isStill() {return true;}
	bump(bumpedBy, otherVelocity, toRight) {return false;}
	
}

return ItemBubble;
});