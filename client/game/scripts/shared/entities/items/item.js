define(['entities/game-object', 'entities/explosion'], function (GameObject, Explosion) {
  class Item extends GameObject {
    constructor(x, y, w, h, world) {
      super(x, y, w, h, world);
      this.name = 'Item';
      if (world)
        this.statusEffects = world.createStatusEffectsObject(this);
      this.tangible = true;
      this.dontCollideWithIntangibles = false;
      this.onlyCollideWithIntangibles = false;
      this.dontCollideThisTurn = false;
      this.collideItems = true;
      this.justBecameTangible = false;
      this.canBeThrown = true;
      this.justTeleported = false;
      this.standingOnItem = false;
      this.lastBonkedItem = null;
      this.immuneToExplosions = false;
      this.timeTakenToExplode = 90;
      this.explosive = false;
      this.explodeRadius = 200;
      this.alive = true;
      this.facingLeft = false;
      this.maxVelocity = {x: 50, y: 50};
      this.terminalVelocity = 30;
      this.timeUnderwater = 0;
      this.isUnderwater = false;
      this.mass = 3;
      this.power = 3;
      this.fixedStunDuration = false;
      this.minimumBumpVelocity = {x: 0, y: 0};
      this.stunDuration = 30;
      this.zippingTo = {};
      // this.afterZipVelocity = {};
      this.itemsStandingOn = [];
      this.itemsOnHead = [];
      this.statuses = [];
    }

    preUpdate() {
      this.justBecameTangible = false;
      this.allowDrag = this.shouldUseDrag();
      this.allowBounce = this.shouldBounce();
      this.justTeleported = false;
      this.floatingOnWater = false;
      this.checkHitCeiling();
    }

    update() {
      super.update();
      this.statusEffects.update();
      if (this.standingOnItem) {
        this.afterLandingOnItem();
      }
      if (this.aboutToExplode && this.explodeTimer-- <= 0) this.explode();
      if (this.velocity.x !== 0) this.facingLeft = this.velocity.x < 0;
      this.sandwiched = this.velocity.y === 0 && this.itemsOnHead.length !== 0 /*&& this.itemsStandingOn.length !== 0*/;
      this.checkDrowned();
    }

    updateDead() {
      this.velocity = {x: 0, y: 0};
    }

    checkHitCeiling() {
      if (this.isOnCeiling()) {
        if (this.itemsStandingOn.length > 0) {
          for (let i = 0; i < this.itemsStandingOn.length; ++i)
            this.itemsStandingOn[i].itemAboveHasHitCeiling(this);
        }
      }
    }

    itemAboveHasHitCeiling(itemAbove) {
      // if (this.velocity.y < 0) this.setVelocityY(0);
      // if (!this.isOnCeiling()) this.y = itemAbove.y + itemAbove.height;
      this.onCeiling = true;
      // if (this.y <= itemAbove.y + itemAbove.height)
      this.setVelocityY(Math.max(0, itemAbove.velocity.y-0.001));
      this.checkHitCeiling();
    }

    isSandwichedInPile() {
      return this.sandwiched;
    }

    isSlidingOutOfPile() {
      return this.hasStatusEffect('SlideFromSandwich');
    }

    hasBumpedEffect() {
      return this.hasStatusEffect('Bumped');
    }

    slideOutOfPile() {
      this.addStatusEffect('SlideFromSandwich');
    }

    addBumpedEffect(duration = 2) {
      this.addStatusEffect('Bumped', duration);
    }

    setStandingOnItem(isStanding) {
      this.standingOnItem = isStanding;
    }

    setBeingForciblyMoved(beingMoved) {
      this.beingForciblyMoved = beingMoved;
    }

    moveWithItemsStoodOn() {
      if (this.itemsStandingOn.length === 0) return;
      if (!this.canBePickedUp()) return;
      if (this.isSlidingOutOfPile()) return;
      if (this.hasBumpedEffect()) return;
      if (this.launchedAsAttack) return;
      let xV = 0;
      let yV = null;
      let yVOffset = 0;
      for (let i = 0; i < this.itemsStandingOn.length; ++i) {
        let item = this.itemsStandingOn[i];
        if (item.isSlidingOutOfPile()) continue;
        if (!item.canPickUpItem()) continue;
        if (this.velocity.x !== 0 && item.velocity.x === 0) continue;
          xV = Math.abs(item.velocity.x) > xV ? item.velocity.x : xV;
        if (yV === null || item.velocity.y < yV) {
          yV = item.velocity.y;
          if (item.hasBumpedEffect()) yV -= 2;
        }
      }
      if (this.isOnCeiling()) yV = this.velocity.y;
      this.setVelocity(xV, yV);
    }

    resetItemsStoodOn() {
      this.itemsStandingOn.length = 0;
    }

    resetItemsOnHead() {
      this.itemsOnHead.length = 0;
    }

    afterLandingOnItem() {
    }

    stopStandingOnItems() {
      this.resetItemsStoodOn();
      this.standingOnItem = false;
    }

    onLandingOnItem(item) {
      this.itemsStandingOn.push(item);
      this.statusEffects.afterLandingOnItem(item);
      if (!this.launchedAsAttack) return false;
      return this.bonkOtherItem(item);
    }

    onLandedOnByItem(item) {
      this.itemsOnHead.push(item);
      return false;
    }

    bonkOtherItem(other) {
      if (!other) return false;
      if (!this.allowBounce) return false;
      if (this.velocity.y <= other.velocity.y + 5) return false;

      let oldVelocity = {x: this.velocity.x, y: this.velocity.y};

      this.setY(other.y - this.height);
      other.bonk(this);
      this.setVelocityY(-Math.abs(oldVelocity.y * this.bounce.y * 2));

      this.lastBonkedItem = other;
      return true;
    }

    bonk(other) {
      let extra = {x: other.velocity.x / 10 * other.power, y: other.velocity.y / 10 * other.power};
      let damper = 1 - (0.065 * this.mass);
      this.setVelocity(this.velocity.x + extra.x * damper, this.velocity.y + extra.y * damper);
      let mag = Math.sqrt(other.velocity.x * other.velocity.y + other.velocity.y * other.velocity.y);

      let stunDuration = this.calculateAdjustedStunDuration(other, this.velocity, mag);
      this.setStun(true, stunDuration);
      if (this.name === 'Monkey' && stunDuration > 0) this.world.addPointsFor('BONK_STUN', null, this);
    }

    onBounce() {}

    getItemBelow() {
      return this.world.findItemBelowItem(this);
    }

    getAllItemsInPileAboveHead(ignoreBranchFromThisItem = null) {
      // IgnoreBranch - won't add items above that item. But that item itself will be included.
      let allItemsAbove = [];
      allItemsAbove.push.apply(allItemsAbove, this.itemsOnHead);
      for (let i = 0; i < this.itemsOnHead.length; ++i) {
        let item = this.itemsOnHead[i];
        if (item !== ignoreBranchFromThisItem)
          allItemsAbove.push.apply(allItemsAbove, item.getAllItemsInPileAboveHead());
      }
      return allItemsAbove;
    }

    pileContainsItem(itemName) {
      if (this.name === itemName) return true;
      if (this.pileContainsItemBelow(itemName)) return true;
      if (this.pileContainsItemAbove(itemName)) return true;
      return false;
    }

    pileContainsItemBelow(itemName) {
      for (let i = 0; i < this.itemsStandingOn.length; ++i) {
        if (this.itemsStandingOn[i].name === itemName) return true;
        if (this.itemsStandingOn[i].pileContainsItemBelow(itemName)) return true;
      }
      return false;
    }

    pileContainsItemAbove(itemName) {
      for (let i = 0; i < this.itemsOnHead.length; ++i) {
        if (this.itemsOnHead[i].name === itemName) return true;
        if (this.itemsOnHead[i].pileContainsItemAbove(itemName)) return true;
      }
      return false;
    }

    canBePickedUp() {
      return !this.beingForciblyMoved && this.statusEffects.canBePickedUp();
    }

    canPickUpItem() {
      return !this.isStunned(); /*return !this.launchedAsAttack;*/
    }

    canBePushed() {
      return this.ableToMove();
    }

    canHaveStatusEffects() {
      return true;
    }

    isImmuneToExplosions() {
      return this.immuneToExplosions;
    }

    launchAsAttack(velocity, thrownBy) {
      if (thrownBy) this.thrownBy = thrownBy;
      this.launchedAsAttack = true;
      this.dontCollideWithIntangibles = true;
      this.setVelocity(velocity.x, velocity.y);
      this.addBumpedEffect();
      this.allowDrag = false;
    }

    zipBump(bumpedBy, toRight) {
      if (this.hasBumpedEffect()) return;
      if (this.isZipping()) return;
      let oldVelocity = {x: this.velocity.x, y: this.velocity.y};
      let zipVelocity = {x: 4, y: 4};
      let bumpVelocity = {x: 6, y: -12};
      let adjustedStunDuration = this.calculateAdjustedStunDuration(bumpedBy, zipVelocity, bumpVelocity.x);
      if (!toRight) bumpVelocity.x *= -1;
      this.setVelocity(bumpVelocity.x, bumpVelocity.y);
      this.standingOnItem = false;
      this.addBumpedEffect();
     // this.setStun(true, adjustedStunDuration);
      this.statusEffects.afterBump(bumpedBy, oldVelocity);
      if (this.name === 'Monkey') this.world.addPointsFor('ZIP_ATTACK', null, this);
    }

    bump(bumpedBy, otherVelocity, toRight) {
      if (this.isShielded()) {
        bumpedBy.becomeTangible();
        otherVelocity = {x: 0, y: 0};
      }

      let oldVelocity = {x: this.velocity.x, y: this.velocity.y};
      let bumpVelocity = this.calculateBumpVelocity(bumpedBy, otherVelocity);
      let adjustedStunDuration = this.calculateAdjustedStunDuration(bumpedBy, otherVelocity, bumpVelocity.x);

      if (this.isStunned()) {
        bumpVelocity.x = Math.max(Math.abs(this.velocity.x * 0.75), bumpVelocity.y);
        bumpVelocity.y *= 1.5;
      }

      if (bumpedBy.isShielded()) {
        bumpVelocity.x = Math.max(Math.abs(this.velocity.x) * 0.75, bumpVelocity.x);
        bumpVelocity.y = Math.min(Math.abs(this.velocity.y) * 0.75, bumpVelocity.y);
      }

      if (!toRight) bumpVelocity.x *= -1;
      this.setVelocityX(bumpVelocity.x);
      // this.allowDrag = false;
      this.standingOnItem = false;
      if (this.isSandwichedInPile()) this.slideOutOfPile();
      else {
        this.setVelocityY(bumpVelocity.y);
        this.addBumpedEffect();
      }

      this.setStun(true, adjustedStunDuration);
      if (adjustedStunDuration > 0) {
        if (this.name === 'Monkey') this.world.addPointsFor('BUMP_STUN', null, this);
      }

      this.statusEffects.afterBump(bumpedBy, oldVelocity);
    }

    calculateBumpVelocity(bumpedBy, otherVelocity) {
      let speed = Math.abs(otherVelocity.x);

      let power = bumpedBy.power;

      // Minimum bump possible
      let minBump = {x: 0.5, y: -3};

      let extra = {x: speed * (power / 10), y: -speed * (power / 14)};
      let massDamper = 1 - (0.065 * this.mass);

      let bump = {x: massDamper * (minBump.x + extra.x), y: massDamper * (minBump.y + extra.y)};
      bump.x = Math.max(bump.x, minBump.x);
      bump.y = Math.min(bump.y, minBump.y);

      bump.x = Math.max(bump.x, bumpedBy.minimumBumpVelocity.x);
      bump.y = Math.min(bump.y, -bumpedBy.minimumBumpVelocity.y);
      return bump;
    }

    calculateAdjustedStunDuration(bumpedBy, otherVelocity, bumpVelocityX) {
      if (bumpedBy.fixedStunDuration) return bumpedBy.stunDuration;
      if (bumpVelocityX < 4) return 0;
      let speed = Math.abs(otherVelocity.x);

      let power = bumpedBy.power;
      let stunDuration = bumpedBy.stunDuration;
      let extraStunFromSpeedPercentage = 1 + (0.035 * speed);
      let extraStunPercentage = (1 + power / 7.5);
      let adjustedStunDuration = stunDuration * extraStunPercentage * extraStunFromSpeedPercentage;
      let stunDamper = 1 - (0.03 * this.mass);
      return adjustedStunDuration * stunDamper;
    }

    bounceOnRisingCollision() {
      return false;
    }

    shouldUseDrag() {
      return this.isStandingOnSomething() && !this.isOnCeiling();
    }

    shouldBounce() {
      return true;
    }

    isStandingOnSomething() {
      return this.isOnFloor() || this.standingOnItem;
    }

    startTangibleTimer() {
      this.world.delayedCall(45, this.becomeTangible, [], this);
    }

    startCollideItemsTimer() {
      this.world.delayedCall(45, this.setCollideItems, [true], this);
    }

    becomeIntangible(propagate = false, alpha = 0.5) {
      this.tangible = false;
      this.alpha = alpha;
    }

    becomeTangible() {
      if (!this.tangible)
        this.justBecameTangible = true;
      this.tangible = true;
      this.alpha = 1;
      this.dontCollideWithIntangibles = false;
      this.onlyCollideWithIntangibles = false;
    }

    blastItemAway(other) {
      if (other.x <= this.x) {
        // blast left
        other.x = this.x - other.width - 0.0001;
        other.launchAsAttack({x: -12, y: -1});
        other.checkTileCrushNextFrame = true;
      } else {
        // blast right
        other.x = this.x + this.width + 0.0001;
        other.launchAsAttack({x: 12, y: -1});
        other.checkTileCrushNextFrame = true;
      }
    }

    getTopOfPile() {
      if (this.itemsOnHead.length !== 0)
        return this.itemsOnHead[0].getTopOfPile();
      return this;
    }

    onTileCollide(tile) {
      return false;
    }

    collidingWithSomething() {
      return this.collidedWithItem || this.collidedWithTile;
    }

    crushed() {
      this.canMove = false;
      this.die();
    }

    die() {
      this.alive = false;
      this.statusEffects.clear();
      // Item may not want to be removed straight after dying
      this.flagForRemoval();
    }

    startExplodeTimer() {
      if (this.aboutToExplode) return false;
      this.aboutToExplode = true;
      this.explodeTimer = this.timeTakenToExplode;
      if (this.hasStatusEffect('OnFire')) {
        this.removeStatusEffect('OnFire');
        this.addStatusEffect('OnFire');
      }
    }

    explode() {
      if (!this.alive) return;
      this.world.addExplosion(new Explosion(this.x + this.halfWidth, this.y + this.halfHeight, this.explodeRadius, 2, this.world));
      this.die();
    }

    onCaughtInExplosion() {
      if (this.explosive) {
        this.addStatusEffect('OnFire');
      }
    }

    // teleportTo(x, y, keepPile = false) {
    //   this.x = x;
    //   this.y = y;
    //   this.prev.x = x;
    //   this.prev.y = y;
    //   this.justTeleported = true;
    //   if (keepPile) {
    //     if (this.itemsOnHead.length !== 0) {
    //       let itemsAbove = this.getAllItemsInPileAboveHead();
    //       let nextY = this.y;
    //       for (let i = 0; i < itemsAbove.length; ++i) {
    //         nextY -= itemsAbove[i].height;
    //         itemsAbove[i].teleportTo(this.x, nextY);
    //         itemsAbove[i].setVelocity(this.velocity.x, this.velocity.y);
    //       }
    //     }
    //   }
    // }

    zipTo(x, y, velocityAfter, keepPile=false) {
      this.zippingTo = {x: x, y: y};
      // this.afterZipVelocity = {x: velocityAfter.x, y: velocityAfter.y};
      this.addStatusEffect('Zipping');
      if (keepPile) {
        if (this.itemsOnHead.length !== 0) {
          let itemsAbove = this.getAllItemsInPileAboveHead();
          let nextY = y;
          for (let i = 0; i < itemsAbove.length; ++i) {
            nextY -= itemsAbove[i].height;
            itemsAbove[i].zipTo(x+this.halfWidth-itemsAbove[i].halfWidth, nextY, this.velocity);
          }
        }
      }
    }

    isStill() {
      if (this.managedByOtherItem) return true;
      if (this.aboutToExplode) return false;
      return !this.canMove
          || (this.velocity.x === 0 && this.velocity.y === 0 && (this.isStandingOnSomething() || this.isFloatingOnWater()))
          || (this.standingOnItem && !this.beingForciblyMoved);
    }

    onPlucked() {
    }

    onMoveStart() {
    }

    onMoveEnd() {
      this.launchedAsAttack = false;
      this.lastBonkedItem = null;
      this.beingForciblyMoved = false;
      this.dontCollideThisTurn = false;
      this.thrownBy = false;
      this.setStun(false);
      this.removeStatusEffect('Boosting');
      this.becomeTangible();
    }

    collidesWithItems() {
      return this.collideItems && !this.dontCollideThisTurn;
    }

    onLandingOnWater(waterHeight) {
      this.statusEffects.afterLandingOnWater(waterHeight);
      this.isUnderwater = this.y > waterHeight;
      if (this.velocity.y >= 0 && this.y > waterHeight + 256 && !this.canFloat() && !this.isPiloting())
        this.die();
    }

    checkDrowned() {
      if (this.isUnderwater) {
        this.isUnderwater = false;
        if (this.timeUnderwater++ > 300) this.die();
      } else {
        this.timeUnderwater = 0;
      }
    }

    disable(setDisable = true) {
      super.disable(setDisable);
      if (setDisable) {
        this.statusEffects.clear();
      }
    }

    setStatusEffects(effects) {
      this.statusEffects.clear();
      for (let i = 0; i < effects.length; ++i)
        this.addStatusEffect(effects[i].name, effects[i].duration);
    }

    setStun(stun, duration) {
      if (stun) {
        let remainingDuration = this.statusEffects.getEffectDurationRemaining('Stunned');
        remainingDuration = !remainingDuration ? 0 : remainingDuration;
        this.addStatusEffect('Stunned', duration + remainingDuration);
      } else {
        this.removeStatusEffect('Stunned');
      }
    }

    hasStatusEffect(effect) {
      return this.statusEffects.hasEffect(effect);
    }

    isStunned() {
      return this.hasStatusEffect('Stunned');
    }

    isFrozen() {
      return this.hasStatusEffect('Frozen');
    }

    isOnFire() {
      return this.hasStatusEffect('OnFire');
    }

    addStatusEffect(effectName, duration) {
      return this.statusEffects.addEffect(effectName, duration);
    }

    removeStatusEffect(effectName) {
      return this.statusEffects.removeEffect(effectName);
    }

    isFloatingOnWater() {
      return this.floatingOnWater;
    }

    setCollideItems(set) {
      this.collideItems = set;
    }

    canFloat() {
      return this.statusEffects.hasEffect('Float');
    }

    isPiloting() {
      return this.statusEffects.hasEffect('Piloting');
    }

    isShielded() {
      return this.statusEffects.hasEffect('Shield');
    }

    isZipping() {
      return this.statusEffects.hasEffect('Zipping');
    }

    getStatusEffects() {
      if (!this.statusEffects) return [];
      return this.statusEffects.effects;
    }

    shouldCheckTileCrush() {
      return this.justTeleported || this.checkTileCrushNextFrame;
    }

    isDisabledOrDead() {
      return this.disabled || !this.alive;
    }

    setProperty(property, value) {
      this[property] = value;
    }

    allowPhysics() {
      return !this.isDisabledOrDead();
    }

    hitTileFromLeft(tile) {
      return false;
    }

    hitTileFromRight(tile) {
      return false;
    }

    hitTileFromAbove(tile) {
      return false;
    }

    hitTileFromBelow(tile) {
      return false;
    }

    onContinuedTileOverlap(tile) {
      return false;
    }

    onPushedUpByTile(tile) {
      return false;
    }
  }

  return Item;
});