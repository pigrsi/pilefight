define(['items/item'], function(Item) {
class Floatie extends Item {
    constructor(x, y, world) {
        super(x, y, 48, 48, world);
        this.name = 'Floatie';
        this.mass = 2;
        this.power = 2;
    }
	
	onLandingOnItem(item) {
		if (item.width === this.width && item.canHaveStatusEffects() && !item.hasStatusEffect('Float')) {
			item.addStatusEffect('Float');
			this.die();
		} else super.onLandingOnItem.apply(this, arguments);
	}
    
}
return Floatie;
});