define(['items/item'], function(Item) {
class Rock extends Item {
    constructor(x, y, world) {
        super(x, y, 48, 48, world);
        this.name = 'Rock';
        this.mass = 9;
        this.power = 7.5;
    }
    
    update() {
        super.update();
    }
}
return Rock;
});