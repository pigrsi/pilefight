define(['items/item'], function(Item) {
class Crate extends Item {
    constructor(x, y, world, size, contents) {
        super(x, y, size.width, size.height, world);
        this.name = 'Crate';
        this.mass = 10;
        this.power = 10;
        this.contents = contents;
        let contentsInfo = [];
        for (var i = 0; i < contents.length; ++i)
            contentsInfo.push({name: contents[i].name, id: contents[i].id});
        this.statuses.push({property: 'size', val: size});
        this.statuses.push({property: 'contentsInfo', val: contentsInfo});
        this.customCollision = this.onCollide;
        this.canBeOpened = false;
        this.readyToOpen = false;
        this.waitATick = true;
    }
    
    update() {
        super.update();
        if (this.readyToOpen) {
            // Wait until next frame so mover has a chance to pause the game
            if (this.waitATick) this.waitATick = false;
            else this.open();
        }
    }
    
    onCollide(item) {
        if (this.name === item.name) return true;
        if (this.canBeOpened) {
            this.setCrateReadyToOpen();
            this.world.addToMover(this, '', this.x, this.y, 250, 0);
        }
        return true;
    }
    
    setCrateReadyToOpen() {
        this.readyToOpen = true;
    }
    
    open() {
        for (var i = 0; i < this.contents.length; ++i) {
            let item = this.contents[i];
            item.setPosition(this.x + this.halfWidth - item.halfWidth, this.y + this.halfHeight - item.halfHeight);
            this.setItemPositions(item, i);
            item.becomeIntangible();
            item.onlyCollideWithIntangibles = true;
            item.startTangibleTimer();
            this.world.addItem(item);
        }
        this.die();
    }
    
    setItemPositions(item, itemNumber) {
        if (this.width > 100) {
            // 4 items
            switch (itemNumber) {
                case 0: item.setVelocity(-5, -10); item.x -= 32; item.y -= 32; break;
                case 1: item.setVelocity(5, -10); item.x += 32; item.y -= 32; break;
                case 2: item.setVelocity(-10, -5); item.x -= 32; item.y += 32; break;
                case 3: item.setVelocity(10, -5); item.x += 32; item.y += 32; break;
            }
        } else if (this.height > 80) {
            // 3
            switch (itemNumber) {
                case 0: item.setVelocity(0, -8); item.y -= 32; break;
                case 1: item.setVelocity(-7, -7); item.x -= 24; item.y += 24; break;
                case 2: item.setVelocity(7, -7); item.x += 24; item.y += 24; break;
            }
        } else if (this.width > 80) {
            // 2
            switch (itemNumber) {
                case 0: item.setVelocity(-2, -7); item.x -= 24; break;
                case 1: item.setVelocity(2, -7); item.x += 24; break;
            }
        } else {
            // 1
            item.setVelocity(0, -8);
        }
    }
    
    onMoveStart() {
        this.canBeOpened = true;
        super.onMoveStart();
    }
    
    onMoveEnd() {
        this.canBeOpened = false;
        super.onMoveEnd();
    }
}
return Crate;
});