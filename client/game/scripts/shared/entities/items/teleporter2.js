define(['items/teleporter'], function(Teleporter) {
class Teleporter2 extends Teleporter {
    constructor(x, y, world) {
        super(x, y, world);
        this.name = 'Teleporter2';
        this.teleportCountdown = 60;
    }
    
    update() {
        super.update();
        if (this.thrownBy && this.teleportCountdown-- <= 0 && !(!this.isStill() && this.collidedWithTile)) this.teleportThrower();
    }
    
    isStill() {
        if (this.thrownBy) return false;
        return super.isStill();
    }
    
    launchAsAttack(velocity, thrownBy) {
        super.launchAsAttack(velocity, thrownBy);
        if (thrownBy) {
            this.collideTiles = false;
        }
    }
    
    die() {
        if (this.thrownBy)
            this.teleportThrower();
        super.die();
    }
}
return Teleporter2;
});