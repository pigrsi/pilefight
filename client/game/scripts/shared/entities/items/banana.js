define(['items/item'], function(Item) {
class Banana extends Item {
    constructor(x, y, world) {
        super(x, y, 48, 48, world);
        this.name = 'Banana';
        this.mass = 1;
        this.power = 1;
        this.allowBounce = false;
        this.customCollision = this.onCollide;
		this.stunDuration = 150;
    }
    
    update() {
        super.update();
    }
    
    onCollide(other) {
		this.fixedStunDuration = false;
        if (other.name === this.name) return true;
        if (other.velocity.x === 0) return false;
        if (this.prev.y + this.height <= other.prev.y + 1) return false;
		this.fixedStunDuration = true;
        other.velocity.x *= 3;
        if (other.velocity.x > 0 && other.velocity.x < 3) other.velocity.x = 3;
        if (other.velocity.x < 0 && other.velocity.x > -3) other.velocity.x = -3;
        other.velocity.y -= Math.max(10 - other.velocity.y, 1);
		other.setStun(true, this.stunDuration);
        this.die();
        return true;
    }
    
}
return Banana;
});