define(['items/item'], function(Item) {
class Bumper extends Item {
	constructor(x, y, world) {
        super(x, y, 48, 48, world);
        this.name = 'Bumper';
        this.mass = 3;
        this.power = 5;
		this.activated = false;
		this.justActivated = false;
		this.customCollision = this.onCollideItem;
		this.statuses.push({property: 'activated', val: this.activated});
		this.hitsThisTurn = 0;
    }
	
	update() {
        super.update();
		if (this.justActivated) this.justActivated = false;

		if (this.activated) {
			if (this.hitsThisTurn >= 10) this.die();
		} else if (!this.activated && this.thrownBy)
			if (this.activateIn-- <= 0) this.activate();
    }
	
	
	launchAsAttack(velocity, thrownBy) {
		super.launchAsAttack(velocity, thrownBy);
		if (!this.activated) {
			this.startActivationCountdown();
		}
	}
	
	canHaveStatusEffects() {
		return !this.activated && super.canHaveStatusEffects();
	}
	
	onCollideItem(item) {
		if (!this.activated) return false;
		
		if (this.justActivated)
			this.blastItemAway(item);
		
		let dx = this.x+this.halfWidth - (item.x+item.halfWidth);
		let dy = this.y+this.halfHeight - (item.y+item.halfHeight);
		let mag = Math.sqrt(dx*dx + dy*dy);
		// Make sure that "circles" collide. Radii = width/2
		if (mag > Math.abs(this.halfWidth + item.halfWidth)) return true;
		
		let unit = {x: -dx/mag, y: -dy/mag};
		
		mag = Math.sqrt(item.velocity.x*item.velocity.x + item.velocity.y*item.velocity.y);
		let multiplier = 1.5;
		item.setVelocity(unit.x * mag * multiplier, unit.y * mag * multiplier);
		
		item.addBumpedEffect();
		item.setStun(true, 30);
		++this.hitsThisTurn;
		
		return true;
	}
	
	canPickUpItem() {return !this.activated && super.canPickUpItem();}
	
	canBePickedUp() {return !this.activated && super.canBePickedUp();}
	
	activate() {
		if (this.activated) return;
		this.justActivated = true;
		this.collideTiles = false;
		this.activated = true;
		this.statuses[0].val = this.activated;
		this.canMove = false;
		this.setVelocity(0, 0);
		let multiplier = 2.5;
		this.x -= (this.width*multiplier - this.width)/2;
		this.y -= (this.height*multiplier - this.height)/2;
		this.setSize(this.width*multiplier, this.height*multiplier);
		this.statusEffects.clear();
	}
	
	startActivationCountdown() {
		this.activateIn = 75;
	}

	isStill() {
		return !(this.thrownBy && !this.activated) && super.isStill();
	}
	
	setProperty(property, value) {
		if (!this.activated && property === 'activated' && value === true)
			this.activate();
		super.setProperty(property, value);
	}

	onMoveEnd() {
		super.onMoveEnd.apply(this, arguments);
		this.hitsThisTurn = 0;
	}
}

return Bumper;
});