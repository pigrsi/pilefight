define(['items/item'], function(Item) {
class Pebble extends Item {
    constructor(x, y, world) {
        super(x, y, 12, 12, world);
        this.name = 'Pebble';
        this.mass = 1;
        this.power = 0.01;
		this.bounce = {x: 0.2, y: 0.1};
		// this.collideTiles = false;
		this.skim = false;
    }
	
	canPickUpItem() {
		return false;
	}
	
	onMoveEnd() {
		super.onMoveEnd();
		this.die();
	}
	
	bounceOnRisingCollision() {
		return true;
	}
    
    update() {
        super.update();
    }
	
	onLandingOnWater(waterHeight) {
		if (
		this.skim &&
		this.prev.y < waterHeight &&
		Math.abs(this.velocity.x) > 6 &&
		this.velocity.y > 5 && this.velocity.y < Math.abs(this.velocity.x)) {
			this.setVelocityY(-this.velocity.y / 2);
		} else {
			super.onLandingOnWater(waterHeight);
		}
	}
}

return Pebble;
});