define(['items/item'], function(Item) {
class Shooter extends Item {
    constructor(x, y, width, height, world) {
        super(x, y, width, height, world);
        this.name = 'Shooter';
        this.mass = 3;
        this.power = 3;
        this.resetStillTimer();
        this.spawnCooldown = 0;
        this.numSpawned = 0;
        this.numToSpawn = 0;
        this.lastNonZeroXVelocity = 0;
		
		this.maxSpawns = 1;
		this.cooldownBetweenShots = 30;
    }
	
	spawnProjectile() {
		// Override this
	}
    
    update() {
        super.update();
        if (this.triggered) {
            if (this.velocity.x !== 0) this.lastNonZeroXVelocity = this.velocity.x;
			if (this.isStill()) this.stillTimer--;
			else this.resetStillTimer();
            if (super.isStill() && !this.shooting) this.startShooting();
        }
        
        if (this.shooting) {
            if (this.spawnCooldown-- <= 0) {
                this.resetSpawnCooldown();
                this.spawnProjectile();
                this.numSpawned++;
            }
            if (this.numSpawned >= this.numToSpawn) {
                this.stopShooting();
            }
        }
    }
    
    launchAsAttack(velocity, triggeredBy) {
        super.launchAsAttack(velocity, triggeredBy);
        this.triggered = true;
        this.numToSpawn = Math.max(this.numToSpawn, this.maxSpawns);
    }
    
    isStill() {
        if (!this.triggered) return super.isStill();
        if (super.isStill()) {
            this.stillTimer <= 0;
        } else {
            this.resetStillTimer();
            return false;
        }
    }
    
    startShooting() {
        this.shooting = true;
        this.triggered = false;
        this.numSpawned = 0;
    }
    
    stopShooting() {
        this.shooting = false;
        this.spawnCooldown = 0;
        this.resetStillTimer();
        this.numSpawned = 0;
        this.numToSpawn = 0;
        this.triggered = false;
    }
    
    resetSpawnCooldown() {
        this.spawnCooldown = this.cooldownBetweenShots;
    }
    
    resetStillTimer() {
        this.stillTimer = 60;
    }
    
    onMoveEnd() {
        super.onMoveEnd();
        this.numToSpawn = 0;
    }
    
    liteTrigger() {
        this.triggered = true;
        this.numToSpawn = Math.min(this.numToSpawn+1, this.maxSpawns);
        this.shooting = false;
    }
    
    setBeingForciblyMoved(beingMoved) {
        super.setBeingForciblyMoved(beingMoved);
        if (beingMoved)
            this.liteTrigger();
    }
    
    bump(bumpedBy, otherVelocity, toRight) {
        super.bump(bumpedBy, otherVelocity, toRight);
        this.liteTrigger();
    }
}
return Shooter;
});