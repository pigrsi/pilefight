define(['items/item'], function(Item) {
class Pow extends Item {
    constructor(x, y, world) {
        super(x, y, 48, 48, world);
        this.name = 'Pow';
        this.mass = 7;
        this.power = 7;
    }
    
    update() {
		super.update();
		if (this.launchedAsAttack && this.isStandingOnSomething()) {
			this.bonkTheWorld();
		}
    }
	
	bonkTheWorld() {
		let items = this.world.getItems();
		for (let i = 0; i < items.length; ++i) {
			if (items[i] !== this && items[i].isOnFloor())
				this.bonkItem(items[i]);
		}
		this.die();
	}
	
	bonkItem(item) {
		let dx = (item.x+item.halfWidth) - (this.x+this.halfWidth);
		let dy = (item.y+item.halfHeight) - (this.y+this.halfHeight);
		let distance = Math.sqrt(dx*dx + dy*dy);
		//let unit = {x: dx / distance, y: dy / distance};
		let xMag = dx > 0 ? 1 : -1;
		
		let power = 18.5;
		power = power - (18.5 * distance/3000);
		power = Math.max(5, power);
		
		item.setVelocity(power/3 * xMag,  -power);
	}
}
return Pow;
});