define(['items/item', 'entities/collisions'], function(Item, Collisions) {
class Destruction extends Item {
	constructor(x, y, world) {
        super(x, y, 48, 48, world);
        this.name = 'Destruction';
        this.mass = 5;
        this.power = 7;
		this.state = 'NOT_DEPLOYED';
		this.drag = 0.1;
		this.bounce.x = 0.76;
		this.bounce.y = 0.6;
		this.customCollision = this.onItemCollide;
		this.hp = 50;
		this.tileBreakSpeed = 8;
		this.tilesToBreak = [];
		this.statuses[0] = {property: 'hp', val: this.hp};
    }
	
	update() {
		super.update();
		this.allowGravity = true;
		
		// Don't take forever to slow to 0
		if (this.isStandingOnSomething() && this.velocity.x !== 0 && Math.abs(this.velocity.x) <= 3) this.setVelocityX(this.velocity.x * 0.95);
		if (this.tilesToBreak.length !== 0) {
			for (let i = 0; i < this.tilesToBreak.length; ++i)
				this.breakTile(this.tilesToBreak[i]);
			this.tilesToBreak.length = 0;
		}
		
		if (this.isNotDeployed()) this.updateNotDeployed();
		else this.updateDeployed();
	}
	
	updateNotDeployed() {
		if (this.launchedAsAttack && this.isStandingOnSomething())
			this.deploy(this.thrownBy);
	}
	
	updateDeployed() {
		this.justDeployed = false;
		if (this.pilotIsMoving())
			this.setVelocity(this.pilot.velocity.x * 1.25, this.pilot.velocity.y * 1.25);
		
		if (this.brokeTileBelow) {
			this.brokeTileBelow = false;
			this.velocity.y *= this.bounce.y * -1;
		}

		this.updatePilot();
		
		this.minimumBumpVelocity.x = this.velocity.x === 0 ? 0 : Math.abs(this.velocity.x) + 4;
		
		if (this.hp <= 0) { this.die(); }
	}
	
	die() {
		if (this.pilot) this.ejectPilot();
		super.die();
	}
	
	onLandingOnItem(item) {
		if (!this.isDeployed()) return super.onLandingOnItem.apply(this, arguments);
        return this.bonkOtherItem(item);
    }
	
	bonkOtherItem(item) {
		if (!this.isDeployed()) return false;
		if (item.isStunned()) return false;
		if (this.velocity.y <= 0) return false;
		item.setVelocity(-this.velocity.x, -4);
		item.setStun(true, 250);
	}
	
	updatePilot() {
		if (!this.pilot) return;
		this.pilot.setPosition(this.x+70-this.pilot.halfWidth, this.y+68-this.pilot.halfHeight);
		this.pilot.setVelocity(0, 0);
		this.pilot.standingOnItem = true;
	}
	
	deploy(pilot) {
		let duration = 500;
		let delay = 250;
		let size = {x: 138, y: 162}; // deployed size
		let dest = {x: this.x-(size.x-this.width)/2, y: this.y-(size.y-this.height)};
		let start = {x: dest.x, y: dest.y - 2000};
		this.setPosition(start.x, start.y);
		this.setSize(size.x, size.y);
		this.world.addToMover(this, 'DEPLOYING_DESTRUCTION', dest.x, dest.y, duration, delay);
		
		if (pilot) {	
			let pilotStart = {x: start.x+70-pilot.halfWidth, y: start.y+68-pilot.halfHeight};
			let pilotDest = {x: dest.x+70-pilot.halfWidth, y: dest.y+68-pilot.halfHeight};
			pilot.setPosition(pilotStart.x, pilotStart.y);
			this.world.addToMover(pilot, 'DEPLOYING_DESTRUCTION_PILOT', pilotDest.x, pilotDest.y, duration, delay);
			this.setPilot(pilot);
		}
		
		this.statusEffects.clear();
		this.addStatusEffect('Float');
		if (this.pilot) this.pilot.addStatusEffect('Float');
		this.prev.x = dest.x; this.prev.y = dest.y;
		this.mass = 15;
		this.power = 15;
		this.minimumBumpVelocity.y = -7;
		this.justDeployed = true;
		this.canBeThrown = false;
		this.state = 'DEPLOYED';
	}
	
	setPilot(item) {
		this.pilot = item;
		this.pilot.setVelocity(0, 0);
		this.pilot.addStatusEffect('Piloting');
	}
	
	ejectPilot() {
		if (!this.pilot) return;
		this.pilot.removeStatusEffect('Piloting');
		this.pilot.setY(this.y - 4 - this.pilot.height);
		this.pilot.setVelocity(0, -20);
		this.pilot = null;
	}
	
	onLandingOnWater(waterHeight) {
		if (this.isNotDeployed() && this.launchedAsAttack) this.deploy(this.thrownBy);
		super.onLandingOnWater.apply(this, arguments);
	}
	
	onItemCollide(item) {
		return !!(this.isDeployed() && item === this.pilot);
	}
	
	bump(bumpedBy, otherVelocity, toRight) {
		let powerMultiplier = bumpedBy.power / 10;
		this.hp -= Math.floor(otherVelocity.x / 8 * powerMultiplier);
		if (this.isNotDeployed()) return super.bump.apply(this, arguments);
	}
	
	onCaughtInExplosion() {
		super.onCaughtInExplosion();
		this.takeDamage(15);
	}
	
	canHaveStatusEffects() {return this.isNotDeployed();}
	
	pilotIsMoving() {
		return this.pilot && (this.pilot.velocity.x !== 0 || this.pilot.velocity.y !== 0);
	}
	
	onTileCollide(tile) {
        if (!this.isDeployed()) return super.onTileCollide(tile);
		if (this.justDeployed) {
			if (tile.y <= this.prev.y + this.height + 1) this.flagTileToBeBroken(tile);
			return true;
		} 

		return super.onTileCollide(tile);
    }
	
	takeDamage(amount=1) {
		this.hp -= amount;
	}
	
	flagTileToBeBroken(tile) {
		this.tilesToBreak.push(tile);
	}
	
	breakTile(tile) {
		this.world.removeTile(tile);
		this.takeDamage(1);
	}
	
	hitTileFromLeft(tile) {
		if (this.isDeployed() && this.velocity.x + this.velocity.y >= this.tileBreakSpeed) {
			this.flagTileToBeBroken(tile);
			return true;
		}
		return false;
	}
	hitTileFromRight(tile) {
		if (this.isDeployed() && this.velocity.x - Math.abs(this.velocity.y) <= -this.tileBreakSpeed) {
			this.flagTileToBeBroken(tile);
			return true;
		}
		return false;
	}
	hitTileFromAbove(tile) {
		if (this.isDeployed() && this.velocity.y >= this.tileBreakSpeed) {
			this.flagTileToBeBroken(tile);
			this.brokeTileBelow = true;
			return true;
		}
		return false;
	}
	hitTileFromBelow(tile) {
		if (this.isDeployed() && this.velocity.y - Math.abs(this.velocity.x) <= -this.tileBreakSpeed) {
			this.flagTileToBeBroken(tile);
			return true;
		}
		return false;
	}
	onContinuedTileOverlap(tile) {
		// Don't do continued-overlap if I'm about to destroy the tile.
		return this.tilesToBreak.indexOf(tile) !== -1;
	}
	
	onMoveEnd() {
		super.onMoveEnd();
		this.statuses[0].val = this.hp;
	}
	
	canBePickedUp() {return this.isNotDeployed() && super.canBePickedUp()};
	canPickUpItem() {return this.isNotDeployed() && super.canPickUpItem()};
	
	isNotDeployed() {return this.state === 'NOT_DEPLOYED';}
	isDeployed() {return this.state === 'DEPLOYED';}
	
}

return Destruction;
});
