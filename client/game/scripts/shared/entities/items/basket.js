define(['items/item', 'items/banana'], function(Item, Banana) {
class Basket extends Item {
    constructor(x, y, world) {
        super(x, y, 48, 48, world);
        this.name = 'Basket';
        this.mass = 5;
        this.power = 5;
        this.customCollision = this.onCollide;
    }
    
    update() {
        super.update();
        if (this.thrown && this.collidedWithTile)
            this.breakBasket();
    }
    
    bonkOtherItem(other) {
        this.breakBasket();
        this.collideItems = false;
        super.bonkOtherItem(other);
    }
    
    onCollide(other) {
        // if (other.name === 'Banana') return false;
        if (this.thrown) {
            this.breakBasket();
            return true;
        }
        return false;
    }
    
    breakBasket() {
        if (!this.alive) return;
        this.die();
        this.spawnBanana({x: -5, y: -4});
        this.spawnBanana({x: 0, y: -4});
        this.spawnBanana({x: 5, y: -4});
    }
    
    spawnBanana(velocity) {
        let banana = this.world.addItem(new Banana(this.x, this.y-8, this.world));
        banana.setVelocity(velocity);
    }
    
    launchAsAttack(velocity, thrownBy) {
        super.launchAsAttack(velocity, thrownBy);
        this.thrown = true;
    }
    
}
return Basket;
});