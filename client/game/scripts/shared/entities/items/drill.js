define(['items/item'], function(Item) {
class Drill extends Item {
	constructor(x, y, world) {
		super(x, y, 48, 48, world);
        this.name = 'Drill';
        this.mass = 4;
        this.power = 6;
		this.drilling = false;
	}
	
	update() {
		super.update();
		if (this.drilling) {
			this.setVelocityY(0);
			if (!this.world.itemIsOverlappingWithAnyTile(this))
				this.stopDrilling();
		}
		if (this.spinning) {
			if (this.velocity.x === 0) this.stopSpinning();
		}
	}
	
	hitTileFromLeft() {
		this.startDrilling();
		return true;
	}
	
	hitTileFromRight() {
		this.startDrilling();
		return true;
	}
	
	onContinuedTileOverlap(tile) {return this.drilling;}
	
	startSpinning() {
		this.spinning = true;
	}
	
	stopSpinning() {
		this.spinning = false;
	}
	
	startDrilling() {
		this.drilling = true;
		this.allowGravity = false;
		this.setVelocityY(0);
		if (this.velocity.x >= 0)
			this.setVelocityX(Math.max(this.velocity.x, 6));
		else
			this.setVelocityX(Math.min(this.velocity.x, -6));
		this.setSize(this.width, 24);
		this.startSpinning();
		this.y += 12;
	}
	
	stopDrilling() {
		this.drilling = false;
		this.allowGravity = true;
		this.setSize(this.width, 48);
		this.y -= 12;
	}
	
	isStill() {
		return !this.spinning && super.isStill();
	}
	
	canBePickedUp() {return !this.drilling && super.canBePickedUp();}
	canPickUpItem() {return !this.drilling && super.canPickUpItem();}
}

return Drill;
});