define(['items/item'], function(Item) {
class Bubble extends Item {
    constructor(x, y, spawnedBy, world) {
        super(x, y, 32, 32, world);
        this.name = 'Bubble';
        this.mass = 1;
        this.power = 1;
        this.spawnedBy = spawnedBy;
        this.allowGravity = false;
        this.allowBounce = true;
        this.collideTiles = false;
        this.customCollision = this.onCollide;
        this.originalTicksUntilDeath = 75;
        this.ticksUntilDeath = this.originalTicksUntilDeath;
    }
    
    update() {
        super.update();
        this.velocity.y -= 0.15;
        if (this.ticksUntilDeath-- <= 0)
            this.die();
        if (this.bubbledItem) {
            if (this.bubbledItem.isDisabledOrDead()) this.die();
            else {
                this.bubbledItem.x = this.x + this.halfWidth - this.bubbledItem.halfWidth;
                this.bubbledItem.y = this.y + this.halfHeight - this.bubbledItem.halfHeight;
            }
        }
    }
    
    onCollide(other) {
        if (other.name === this.name || other === this.bubbledItem || other.beingForciblyMoved) return true;
        if (other === this.spawnedBy && this.ticksUntilDeath > this.originalTicksUntilDeath - 30) return true;
        if (!this.bubbledItem && other.canBePickedUp()) { this.bubbleLiftItem(other); return true; }
        return true;
    }
	
	onLandedOnByItem(item) {
		if (this.velocity.y < 0) this.setVelocityY(this.velocity.y * this.bounce.y * -1);
		return true;
	}
    
    die() {
        if (this.bubbledItem) {
			this.bubbledItem.collideItems = true;
            this.bubbledItem.setBeingForciblyMoved(false);
            this.bubbledItem.velocity.x = this.velocity.x;
            this.bubbledItem.velocity.y = -2;
        }
        super.die();
    }
        
    bubbleLiftItem(item) {
        this.bubbledItem = item;
        this.bubbledItem.setBeingForciblyMoved(true);
        this.collideTiles = true;
		this.bubbledItem.collideItems = false;
        this.setSize(item.width + 8, item.height + 8);
    }
    
}
return Bubble;
});