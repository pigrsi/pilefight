define(['items/item'], function(Item) {
class Teleporter extends Item {
    constructor(x, y, world) {
        super(x, y, 48, 48, world);
        this.name = 'Teleporter';
        this.drag = 0.4;
        this.thrownBy = null;
        this.stillTimer = 60;
        this.mass = 2;
        this.power = 1;
    }
    
    update() {
        super.update();
        if (this.thrownBy && this.isStill()) this.teleportThrower();
        this.updated();
    }

    // for animator
    updated(){};
    
    // bonkOtherItem(other) {
    //     return false;
    // }
    
    // shouldBounce() {
    //     return super.shouldBounce() && !(this.thrownBy && this.getItemBelow());
    // }
    
    isStill() {
        if (!this.thrownBy) return super.isStill();
        if (super.isStill()) {
            if (this.stillTimer <= 0) return true;
            else {
                this.stillTimer--;
                return false;
            }
        } else {
            this.stillTimer = 60;
            return false;
        }
    }
    
    teleportThrower() {
        let o = this.thrownBy;
        //o.teleportTo(this.x, this.y, true);
        o.zipTo(this.x+this.halfWidth-o.halfWidth, this.y+this.halfHeight-o.halfHeight, this.velocity, true);
        o.canPluck = true;
        this.thrownBy = null;
        this.collideItems = false;
        this.die();
    }
    
    launchAsAttack(velocity, thrownBy) {
        super.launchAsAttack(velocity, thrownBy);
    }
    
    die() {
        if (this.thrownBy)
            this.teleportThrower();
        super.die();
    }
}
return Teleporter;
});