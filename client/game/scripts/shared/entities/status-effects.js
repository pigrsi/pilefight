define([], function() {
	class StatusEffects {
		constructor(item) {
			this.item = item;
			this.effects = [];
			this.immunities = [];
		}
		
		isOutOfSandwich() {
			if (this.item.velocity.x === 0) return true;
			if (!this.item.isSandwichedInPile()) return true;
			return false;
		}
		
		addEffect(effectName, duration=null) {
			if (!this.original) this.setOriginalValues();
			if (!this.item.canHaveStatusEffects()) return;
			if (this.immunities.indexOf(effectName) !== -1) return;
			let index = this.getIndexOfEffect(effectName);
			if (index !== -1)
				this.removeEffect(effectName);
			this.effects.push({name: effectName, duration: duration});
			this.setEffectInitialValues(effectName, duration);
		}
		
		setEffectInitialValues(effectName, duration=null) {
			switch (effectName) {
				case 'SlideFromSandwich':
					this.initiallyMovingRight = this.item.velocity.x > 0;
					break;
				case 'Stunned':
					if (this.hasEffect('Frozen')) this.removeEffect('Stunned');
					break;
				case 'Frozen':
					this.removeEffect('Stunned');
					break;
				case 'OnFire':
					if (this.item.explosive) {this.item.startExplodeTimer();}
					if (this.hasEffect('Frozen')) this.removeEffect('Frozen');
					break;
				case 'Piloting':
					this.item.collideItems = false;
					this.item.allowGravity = false;
					break;
				case 'Zipping':
					this.item.collideTiles = false;
					this.item.allowGravity = false;
					break;
			}
		}
		
		removeEffect(effectName) {
			let index = this.getIndexOfEffect(effectName);
			if (index === -1) return;
			
			switch (effectName) {
				case 'Frozen':
					this.item.drag = this.original.drag;
					this.item.bounce.x = this.original.bounce.x;
					this.item.mass = this.original.mass;
					break;
				case 'Stunned':
					this.item.drag = this.original.drag;
					this.item.bounce.x = this.original.bounce.x;
					this.item.bounce.y = this.original.bounce.y;
					break;
				case 'SlideFromSandwich':
					this.item.drag = this.original.drag;
					this.sandwich = null;
					break;
				case 'Bread':
					this.item.mass = this.original.mass;
					this.item.power = this.original.power;
					break;
				case 'Piloting':
					this.item.collideItems = this.original.collideItems;
					this.item.allowGravity = this.original.allowGravity;
					break;
				case 'Zipping':
					this.item.collideTiles = this.original.collideTiles;
					this.item.allowGravity = this.original.allowGravity;
					this.item.justTeleported = true;
					//this.item.setVelocity(this.item.afterZipVelocity.x, this.item.afterZipVelocity.y);
					// this.item.afterZipVelocity = {x: 0, y: 0};
					break;
			}
			
			this.effects.splice(index, 1);
		}
		
		update() {
			// Do something using my effects
			for (let i = this.effects.length-1; i >= 0; --i) {
				let effect = this.effects[i];
				switch (effect.name) {
					case 'Frozen':
						//this.item.drag = 0.015;
						this.item.drag = 0;
						this.item.mass = this.original.mass + 1.5;
						this.item.bounce.x = 0;
						if (this.item.isStandingOnSomething() && this.item.velocity.x !== 0) {
							if (this.item.velocity.x > 0 && this.item.velocity.x < 14) this.item.velocity.x += 0.1;
							else if (this.item.velocity.x < 0 && this.item.velocity.x > -14) this.item.velocity.x -= 0.1;
						}
						break;
					case 'Stunned':
						this.item.bounce.x = this.original.bounce.x * 2;
						this.item.bounce.y = this.original.bounce.y * 2;
						this.item.drag = 0.01;
						if (effect.duration > 300) effect.duration = 300;
						break;
					case 'Boosting':
						if (this.item.isStandingOnSomething()) this.removeEffect('Boosting');
						break;
					case 'SlideFromSandwich':
						this.item.drag = this.original.drag / 2;
						if (this.isOutOfSandwich()) this.removeEffect('SlideFromSandwich');
						break;
					case 'OnFire':
						if (this.hasEffect('Stunned')) {
							if (this.item.velocity.x !== 0) {
								if (effect.duration !== null) effect.duration--;
							}
						} else {
							if (this.item.isStandingOnSomething()) {
								this.addEffect('Stunned', 75);
								let velocity = {x: 5, y: -10};
								if (this.item.facingLeft) velocity.x *= -1;
								this.item.setVelocity(this.item.velocity.x + velocity.x, this.item.velocity.y + velocity.y);
							}
						}
						break;
					case 'Bread':
						this.item.mass = 1;
						this.item.power = 1;
						break;
					case 'Zipping':
						this.updateZipping();
						break;
				}
			
				if (effect && effect.duration !== null && effect.duration-- <= 0) {
					this.removeEffect(effect.name);
				}
			}
		}

		updateZipping() {
			let item = this.item;
			let to = item.zippingTo;
			if (!to) { this.removeEffect('Zipping'); return; }
			let dx = to.x - item.x;
			let dy = to.y - item.y;
			let dist = Math.sqrt(dx*dx + dy*dy);
			let speed = 60;
			if (dist <= speed * 1.5) {
				item.setPosition(to.x, to.y);
				this.removeEffect('Zipping');
			} else {
				let moveBy = {x: speed * dx / dist, y: speed * dy / dist};
				item.x += moveBy.x;
				item.y += moveBy.y;
				item.setVelocity(moveBy.x / 5, moveBy.y / 5);
			}
		}
		
		afterBump(bumpedBy, oldVelocity) {
			if (this.hasEffect('Frozen'))
				if (oldVelocity.x !== 0 &&
				((oldVelocity.x > 0 && this.item.velocity.x < 0) || (oldVelocity.x < 0 && this.item.velocity.x > 0)))
				this.item.setVelocityX(0);
			if (this.hasEffect('Bread')) {
				bumpedBy.addStatusEffect('Bread');
			}
			if (this.hasEffect('OnFire')) {
				bumpedBy.addStatusEffect('OnFire', 60);
			}
		}

		canBePickedUp() {
			return !this.hasEffect('Stunned') &&
				!this.hasEffect('Frozen') &&
				!this.hasEffect('Shield');
		}
		
		afterLandingOnItem(itemLandedOn) {
			if (this.hasEffect('Bread')) {
				itemLandedOn.addStatusEffect('Bread');
			}
			if (itemLandedOn.hasStatusEffect('OnFire')) {
				this.addEffect('OnFire', 60);
			}
		}
		
		afterLandingOnWater(waterHeight) {
			if (this.hasEffect('Float') && !this.hasEffect('Piloting')) {
				let item = this.item;
				if (waterHeight > item.y + item.halfHeight) return;
				item.floatingOnWater = true;
				let ySpeed = item.velocity.y - (item.velocity.y > 0 ? 0.4 : 0.15);
				item.setVelocityY(ySpeed);
				let xSlow = 0.995;
				if (Math.abs(item.velocity.y) < 1 && Math.abs(waterHeight-(item.y+item.halfHeight)) < 2) {
					item.setVelocityY(0);
					xSlow = 0.75;
				}
				item.setVelocityX(item.velocity.x * xSlow);
				if (Math.abs(item.velocity.x) < 0.1) item.setVelocityX(0);
			}
		}
		
		hasEffect(effectName) {
			return this.getIndexOfEffect(effectName) !== -1;
		}
		
		getIndexOfEffect(effectName) {
			for (let i = 0; i < this.effects.length; ++i) {
				if (this.effects[i].name === effectName) return i;
			}
			return -1;
		}
		
		getEffectDurationRemaining(effectName) {
			let index = this.getIndexOfEffect(effectName);
			if (index === -1) return null;
			return this.effects[index].duration;
		}
		
		addImmunity(effect) {
			if (this.immunities.indexOf(effect) === -1)
				this.immunities.push(effect);
		}
		
		clear() {
			for (let i = this.effects.length-1; i >= 0; --i) {
				this.removeEffect(this.effects[i].element);
			}
		}
		
		setOriginalValues() {
			this.original = {
				drag: this.item.drag,
				bounce: {x: this.item.bounce.x, y: this.item.bounce.y},
				mass: this.item.mass,
				power: this.item.power,
				collideItems: this.item.collideItems,
				collideTiles: this.item.collideTiles,
				allowGravity: this.item.allowGravity
			}
		}
	}

	return StatusEffects;
});