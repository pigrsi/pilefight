define(['entities/game-object'], function(GameObject) {
class Laser extends GameObject {
    constructor(x, y, direction, lifetime, emitter, world) {
        super(x, y, 8, 0, world);
		if (!direction) direction = 'up';
		this.direction = direction;
        this.name = 'Laser';
        this.lifetime = lifetime;
		this.emitter = emitter;
    }
	
	onCollideItem(item) {
		if (item === this.emitter) return true;
		if (item.name === 'Pebble') return true;
		
		if (item.prev.x + item.halfWidth < this.x) item.setX(this.x - this.width/2 - item.width - 0.01);
		else item.setX(this.x + this.width/2 + 0.01);
		
		item.setVelocityX(item.velocity.x * -1);
		if (item.velocity.x <= 0 && item.velocity.x > -7) item.setVelocityX(-7);
		else if (item.velocity.x > 0 && item.velocity.x < 7) item.setVelocityX(7);
		
		item.setVelocityY(item.velocity.y * 2);
		
		item.addStatusEffect('OnFire', 75);
		item.setStun(true, 60);
		return true;
	}
	
	pointingUp() {return this.direction === 'up';}
	pointingDown() {return this.direction === 'down';}
}
return Laser;
});