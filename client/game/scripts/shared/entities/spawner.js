let e = 'items/';
let n = 'entities/';
define([e+'monkey', e+'rock', e+'teleporter', e+'teleporter2', e+'teleporter3', e+'missile', e+'basket', e+'banana', e+'bubble-gun', e+'bloot-block', e+'bomb', e+'time-bomb', e+'crate', e+'pebble', e+'item-bubble',
		n+'booster', e+'pow', e+'bumper', e+'snowball', e+'freeze-gun', e+'flame', e+'bread', e+'flame-gun', e+'destruction', e+'floatie', e+'dolphin', e+'laser-emitter', e+'drill', e+'orb', n+'goalhole'],
function(Monkey, Rock, Teleporter, Teleporter2, Teleporter3, Missile, Basket, Banana, BubbleGun, BlootBlock, Bomb, TimeBomb, Crate, Pebble, ItemBubble, Booster, Pow, Bumper,
		Snowball, FreezeGun, Flame, Bread, FlameGun, Destruction, Floatie, Dolphin, LaserEmitter, Drill, Orb, Goalhole) {
class Spawner {
	constructor(world) {
		this.world = world;
	}
	
	setItemEventCallback(eventCallback) {
		this.eventCallbackForItems = eventCallback;
	}

	spawnRandomItems(numItems) {
		let items = [];
		for (var i = 0; i < numItems; ++i) {
			let nameIndex = Math.floor(Math.random() * randomItems.length);
			items.push(this.spawn(randomItems[nameIndex], 0, 0));
		}
		return items;
	}

	spawn(objectName, x, y, args) {
		switch (objectName) {
			case 'Monkey': return new Monkey(x, y, this.world, this.eventCallbackForItems);
			case 'Rock': return new Rock(x, y, this.world);
			case 'Teleporter': return new Teleporter(x, y, this.world);
			case 'Teleporter2': return new Teleporter2(x, y, this.world);
			case 'Teleporter3': return new Teleporter3(x, y, this.world);
			case 'Missile': return new Missile(x, y, this.world);
			case 'Basket': return new Basket(x, y, this.world);
			case 'Banana': return new Banana(x, y, this.world);
			case 'BubbleGun': return new BubbleGun(x, y, this.world);
			case 'BlootBlock': return new BlootBlock(x, y, this.world);
			case 'Bomb': return new Bomb(x, y, this.world);
			case 'TimeBomb': return new TimeBomb(x, y, this.world);
			case 'Crate': return new Crate(x, y, this.world, args[0], args[1]);
			case 'Pebble': return new Pebble(x, y, this.world);
			case 'ItemBubble': return new ItemBubble(x, y, this.world, getItemsForSpawnLevel, args[0]);
			case 'Booster': return new Booster(x, y, args[0], args[1]);
			case 'BoosterStart': return new Booster(x, y, args[0], args[1], true);
			case 'Pow': return new Pow(x, y, this.world);
			case 'Bumper': return new Bumper(x, y, this.world);
			case 'Snowball': return new Snowball(x, y, this.world);
			case 'FreezeGun': return new FreezeGun(x, y, this.world);
			case 'Flame': return new Flame(x, y, this.world);
			case 'Bread': return new Bread(x, y, this.world);
			case 'FlameGun': return new FlameGun(x, y, this.world);
			case 'Destruction': return new Destruction(x, y, this.world);
			case 'Floatie': return new Floatie(x, y, this.world);
			case 'Dolphin': return new Dolphin(x, y, this.world);
			case 'LaserEmitter': return new LaserEmitter(x, y, this.world);
			case 'Drill': return new Drill(x, y, this.world);
			case 'Orb': return new Orb(x, y, this.world);
			case 'Goalhole': return new Goalhole(x, y, args[0], 5, this.world);
		}
	}
}

return Spawner;
});

let randomItems = ['Teleporter2', 'Missile', 'Basket', 'Banana', 'BubbleGun', 'BlootBlock', 'Bomb', 'TimeBomb'];
let itemLevels = [];
itemLevels[0] = ['Basket', 'BlootBlock', 'Bomb', 'BubbleGun', 'Bumper', 'Destruction', 'Dolphin', 'Drill', 'FlameGun', 'Floatie', 'FreezeGun', 'LaserEmitter', 'Missile',
	'Pow', 'Rock', 'Teleporter', 'Teleporter2', 'TimeBomb'];
itemLevels[1] = ['level1item'];
itemLevels[2] = ['level2item'];

function getItemsForSpawnLevel(level) {
	return itemLevels[level] ? itemLevels[level] : [];
}