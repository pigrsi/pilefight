define([], function () {
class GameObject {
    constructor(x, y, w, h, world) {
        this.name = 'GameObject';
        this.disabled = false;
        this.setPosition(x, y);
        this.prev = {x: x, y: y};
        this.velocity = {x: 0, y: 0};
        this.setSize(w, h);
        this.alpha = 1;
        this.setPhysicsValues();
        this.world = world;
        this.readyForRemoval = false;
    }
    
    setPhysicsValues() {
		this.canMove = true;
        this.allowDrag = false;
        this.drag = 0.2;
        this.allowBounce = true;
        this.bounce = {x: 0.2, y: 0.3};
        this.mass = 1;
        this.allowGravity = true;
        this.collidedWithItem = false;
        this.collidedWithTile = false;
        this.collideTiles = true;
		this.onFloor = true;
		this.onCeiling = false;
    }

    update() {
    }
    
    isOnCeiling() {return this.onCeiling;}
    isOnFloor() {return this.onFloor;}
	ableToMove() {return this.canMove;}
        
    setPosition(x, y) {this.x = x; this.y = y;}
    setX(x) {this.x = x;}
    setY(y) {this.y = y;}    
    setVelocity(velocityX, velocityY) {
        if (!isNaN(velocityX)) {
            this.velocity.x = velocityX;
            if (!isNaN(velocityY))
                this.velocity.y = velocityY;
            else
                this.velocity.y = velocityX;
        } else {
            this.velocity.x = velocityX.x;
            this.velocity.y = velocityX.y;
        }
    }
    setVelocityX(x) {this.velocity.x = x;}
    setVelocityY(y) {this.velocity.y = y;}
    setSize(w, h) {this.width = w; this.height = h; this.halfWidth = this.width/2; this.halfHeight = this.height/2;}
    allowPhysics() {return true;}
    
    disable(setDisable=true) {this.disabled = setDisable;}
    flagForRemoval() {this.readyForRemoval = true;}
}
return GameObject;
});