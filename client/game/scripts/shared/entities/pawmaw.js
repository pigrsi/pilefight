define(['util/util'], function (Util) {
class PawMaw {
    constructor(waterHeight, mapWidth, addTweenFunction) {
        this.nodeDistance = 2;
        this.createNodes();
        this.target = null;
        this.waterHeight = waterHeight;
        this.roamBounds = {x: -3000, y: waterHeight + 1500, width: mapWidth+6000, height: 2000};
        this.addTween = addTweenFunction;
        this.floatOffsetCounter = 0;
        this.floatOffset = 0;
        this.alpha = 1;
    }
    
    createNodes() {
        let node = null;
        let prevNode = null;
        let nodeRadii = [24, 24, 24, 32, 48, 64, 96, 128, 156, 224, 48, 32, 24, 20, 16];
        //let nodeRadii = [24, 24, 24, 24, 24, 24, 24, 24, 24, 32, 48, 128, 156, 224, 48, 32, 24, 20, 16];
        let prevX = this.x - nodeRadii[0];
        this.halfWidth = nodeRadii[0];
        this.halfHeight = nodeRadii[0];
        let max = nodeRadii.length;
        for (var i = 0; i < max; ++i) {
            let radius = nodeRadii[i];
            node = {x: i*radius*2+this.nodeDistance, y: 5000+i*radius*2, radius: radius, next: prevNode, floatDegree: radius/700};
            if (prevNode) prevNode.prev = node;
            prevNode = node;
            if (i === 0) this.head = node;
            else if (i === max-1) this.tail = node;
            prevX -= radius;
        }
        this.x = this.head.x;
        this.y = this.head.y
    }
    
    startRoamTween() {
        let roamTo = {x: this.roamBounds.x + Math.random() * this.roamBounds.width, y: this.roamBounds.y + Math.random() * this.roamBounds.height};
        let delay = this.nextDelay;
        if (!this.nextDelay) delay = Math.random() * 10000;
        else this.nextDelay = 0;
        return this.tween = this.addTween(this, roamTo.x, roamTo.y, 10000, delay);
    }
    
    // Quick tween to dive deep into the water
    startDiveTween() {
        let roamTo = {x: this.x, y: this.roamBounds.y + this.roamBounds.height*0.75};
        this.nextDelay = 200;
        return this.tween = this.addTween(this, roamTo.x, roamTo.y, 3000, 250, true);
    }
    
    update() {
        if (this.noTweens) return;
        
        if (this.target) {
            this.x = this.target.x + this.target.width/2;
            this.y = this.target.y + this.target.height/2;
            this.setAlpha(this.alpha + 0.075);
        } else {
            this.roam();
        }
        this.updateNodes();
        this.setFloatOffset();
        this.head.x = this.x;
        this.head.y = this.y;
    }
    
    setAlpha(alpha) {
        if (alpha < 0) alpha = 0;
        else if (alpha > 1) alpha = 1;
        this.alpha = alpha;
    }
    
    roam() {
        if (!this.tween || this.tween.progress >= 1) {
            if (this.tween) {
                this.tween.stop();
                this.startRoamTween();
            }
            else {
                if (!this.startDiveTween())
                    this.noTweens = true;
            }
        }
        if (this.y < this.roamBounds.y)
            this.y += 1;
    }
    
    updateNodes() {
        let node = this.head.prev;
        while (node) {
            // Find vector from node ahead to me
            let next = node.next;
            // // Find distance that node needs to move by to be at nodeDistance
            let unit = Util.getUnitVectorTowards(node, next);
            // Multiply unit vector by correct distance
            let distanceToMove = this.nodeDistance + next.radius + node.radius;
            node.x = next.x + unit.x * distanceToMove;
            node.y = next.y + unit.y * distanceToMove;
            
            node = node.prev;
        }
    }
    
    setFloatOffset() {
        this.floatOffset = Math.cos(this.floatOffsetCounter)*0.25;
        this.floatOffsetCounter += 0.025;
        // if (false && this.tween.progress > 0 && this.tween.progress < 1) {
            // this.y += this.floatOffset;
        // } else 
        if (this.target) {
            if (!this.shakingHead) {
                let node = this.head.prev;
                node.y += this.floatOffset;
            }
        } else {
            let node = this.head;
            while (node) {
                    node.y += -5 * node.floatDegree;
                    node = node.prev;
            }
        }
    }
    
    setHeadPosition(x, y) {
        this.x = x;
        this.y = y;
    }
    
    getHead() {
        return this.head;
    }
    
    getTail() {
        return this.tail;
    }
    
    shakeTweenStarted() {
        this.shakingHead = true;
    }
    
    setTarget(object) {
        // Target can be null
        this.target = object;
        if (this.tween) {
            if (this.target) {
                this.alpha = 0;
                // Move head and update nodes to straighten out pawmaw again.
                this.head.x -= 9999;
                this.updateNodes();
                this.tween.stop();
                this.tween = null;
                this.shakingHead = false;
            } else {
                this.floatOffsetCounter = 0;
            }
        }
    }
}
return PawMaw;
});