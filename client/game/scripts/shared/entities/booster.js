define([], function () {
class Booster {
	constructor(x, y, points, power=18, startingBooster=false) {
		this.name = 'Booster';
		this.requiresBoosterEffect = !startingBooster;
		this.points = points;
		let bounding = {left: null, right: null, top: null, bottom: null};
		for (let i = 0; i < this.points.length; ++i) {
			let point = points[i];
			point.x += x;
			point.y += y;
			
			if (bounding.left === null || point.x < bounding.left) bounding.left = point.x;
			if (bounding.right === null || point.x > bounding.right) bounding.right = point.x;
			if (bounding.top === null || point.y < bounding.top) bounding.top = point.y;
			if (bounding.bottom === null || point.y > bounding.bottom) bounding.bottom = point.y;
		}
		this.bounding = bounding;
		this.calculateLaunchPower(power);
	}
	
	onCollideItem(item) {
		let isBoosting = item.hasStatusEffect('Boosting');
		if (this.requiresBoosterEffect && !isBoosting) return;
		item.setVelocity(this.launchPower.x, this.launchPower.y);
		if (!isBoosting) item.addStatusEffect('Boosting');
	}
	
	calculateLaunchPower(power) {
		let p0 = this.points[0];
		let p1 = this.points[1];
		let dy = p1.y - p0.y;
		let dx = p1.x - p0.x;
		let angle = Math.atan2(dy, dx);
		
		let x = power;
		let y = 0;
		this.launchPower = {x: x*Math.cos(angle) /*- y*Math.sin(angle)*/, y: /*y*Math.cos(angle) +*/ x*Math.sin(angle)};
	}
}

return Booster;
});