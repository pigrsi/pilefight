define(['shared/physics', 'util/util'], function(Physics, Util) {
var Collisions = {};

// Useful so items bump into lower ones first
// Pass it array already sorted
Collisions.collideArrayBottomsUp = function(items) {
    for (var i = 0; i < items.length; ++i) {
        var item1 = items[i];
		if (item1.isDisabledOrDead()) continue;
        for (var k = i+1; k < items.length; k++) {
            var item2 = items[k]; 
			if (item2.isDisabledOrDead()) continue;
            if (item1.y > item2.y + item2.height) break; 
            if (item1.collidesWithItems() && item2.collidesWithItems())
                Physics.overlap(item1, item2, Collisions.collideItems);
        }
    }
}

Collisions.collideQuadTiles = function(items, quads, quadMap, allTiles) {
	for (let i = 0; i < items.length; ++i) {
		let item = items[i];
		item.pushedUpByTile = false;
		if (item.isDisabledOrDead()) continue;
		let housingQuads = Collisions.getHousingQuads(item, quadMap);
		Collisions.collideTilesInQuads(item, housingQuads, allTiles);
	}
}

Collisions.collideTilesInQuads = function(item, quads, tiles) {
	if (item.shouldCheckTileCrush())
		if (Collisions.checkCrushedUninterestingTiles(item, tiles)) return;

	for (let i = 0; i < quads.length; ++i) {
		let tiles = quads[i];
		for (var k = 0; k < tiles.length; k++) {
            var tile = tiles[k];
            if (!tile) continue;
			if (tile.destroyed) continue;
            if (item.collideTiles) Physics.overlap(item, tile, Collisions.collideTile);
        }
	}
	item.checkTileCrushNextFrame = false;
}

Collisions.getHousingQuads = function(item, quadMap) {
	let points = [{x: item.x, y: item.y}, {x: item.x + item.width, y: item.y}, {x: item.x, y: item.y + item.height}, {x: item.x + item.width, y: item.y + item.height}];
	let quads = [];
	for (let i = 0; i < points.length; ++i) {
		let quad = Util.getHousingQuad(points[i], quadMap, quadMap.quadSize);
		if (quad && quads.indexOf(quad) === -1) quads.push(quad);
	}
	return quads;
}

Collisions.collideExplosions = function(items, explosions) {
    for (var i = 0; i < explosions.length; ++i) {
        let ex = explosions[i];
        for (var k = 0; k < items.length; ++k) {
            let item = items[k];
            if (Util.circlesIntersect({x: ex.x, y: ex.y, radius: ex.radius}, {x: item.x+item.halfWidth, y: item.y+item.halfHeight, radius: item.halfWidth})) {
                ex.onCollideItem(item);
            }
        }
    }
}

Collisions.collideLasers = function(items, lasers) {
	for (let i = 0; i < lasers.length; ++i) {
		let laser = lasers[i];
		if (laser.pointingUp()) {
			Collisions.checkLaserCollision(laser, items, function(Item, Laser) {return Item.y > Laser.y});
		} else if (laser.pointingDown()) {
			Collisions.checkLaserCollision(laser, items, function(Item, Laser) {return Item.y + Item.height < Laser.y});
		}
	}
}

Collisions.checkLaserCollision = function(laser, items, yCheck) {
	for (let k = 0; k < items.length; ++k) {
		let item = items[k];
		if (!item.collideItems) continue;
		if (!item.ableToMove()) continue;
		if (yCheck(item, laser)) continue;
		if (item.x + item.width < laser.x - laser.width/2) continue;
		if (item.x > laser.x + laser.width/2) continue;				
		laser.onCollideItem(item);
	}
}

Collisions.collideBoosters = function(items, boosters) {
	if (!boosters) return;
	for (let i = 0; i < boosters.length; ++i) {
		let booster = boosters[i];
		for (let k = 0; k < items.length; ++k) {
			let item = items[k];
			let x = item.x+item.halfWidth;
			let y = item.y+item.halfHeight;
			// Items array is bottom to top
			if (booster.bounding.top > item.y + item.height) break;
			if (x < booster.bounding.left || x > booster.bounding.right || y < booster.bounding.top || y > booster.bounding.bottom) continue;
			if (Util.pointInPolygon(booster.points, item.x+item.halfWidth, item.y+item.halfHeight)) {
				booster.onCollideItem(item);
			}
		}
	}
}

Collisions.collideGoalholes = function(items, goalholes) {
	if (!goalholes) return;
	for (let i = 0; i < goalholes.length; ++i) {
		let goalhole = goalholes[i];
		for (let k = 0; k < items.length; ++k) {
			let item = items[k];
			let x = item.x+item.halfWidth;
			let y = item.y+item.halfHeight;
			// Items array is bottom to top
			if (goalhole.bounding.top > item.y + item.height) break;
			if (x < goalhole.bounding.left || x > goalhole.bounding.right || y < goalhole.bounding.top || y > goalhole.bounding.bottom) continue;
			if (Util.pointInCircleTopLeft(goalhole, item.x+item.halfWidth, item.y+item.halfHeight))
				goalhole.onCollideItem(item);
		}
	}
}

/* ***************** TILE COLLISIONS *******************/
Collisions.collideTile = function(item, tile) {
    item.collidedWithTile = true;

	if (item.shouldCheckTileCrush()) {
		if (Collisions.checkCrushed(item, tile)) return;
	}

    // nudge is small offset to move items away from eachother
    if (item.onTileCollide(tile)) return; 
    let nudge = 0.0001;
    // VERTICAL
    if (Collisions.itemWasAboveTile(item, tile) && item.velocity.y >= 0) {
        // Land on top
		if (!item.hitTileFromAbove(tile)) {
			Physics.bounceObjectY(item);
			item.y = tile.y - item.height;
			item.onFloor = true;
		}
    } else if (Collisions.itemWasBelowTile(item, tile)) {
        // Bump on ceiling
		if (!item.hitTileFromBelow(tile)) {
			Physics.bounceObjectY(item);
			item.y = tile.y + tile.height + nudge;
			item.onCeiling = true;
		}
    }
    
    // HORIZONTAL
    if (Collisions.itemWasLeftOfTile(item, tile)) {
        // Bump on left
		if (!item.hitTileFromLeft(tile)) {
			item.x = tile.x - item.width - nudge;
			Physics.bounceObjectX(item);
		}
    } else if (Collisions.itemWasRightOfTile(item, tile)) {
        // Bump on right
		if (!item.hitTileFromRight(tile)) {
			item.x = tile.x + tile.width + nudge;
			Physics.bounceObjectX(item);			
		}
    }
    
    // Continued collision? - pop to the top
    if (Physics.overlap(item, tile)) {
		if (!item.onContinuedTileOverlap(tile)) {
			//item.velocity.y = 0;
			if (item.prev.y + item.height < tile.y + 24) {
				if (!item.onPushedUpByTile(tile)) {
					if (item.velocity.y >= 0) item.velocity.y = 0;
					item.y = tile.y - item.height + 0.1;
					item.onFloor = true;
					item.pushedUpByTile = true;
				}
			} else {
				item.velocity.y = 0;
				item.y = tile.y + tile.height + 0.1;
				item.onCeiling = true;
			}
		}
    }
}

// slow, loops all tiles
Collisions.checkCrushedUninterestingTiles = function(item, tiles) {
	for (let i = 0; i < tiles.length; ++i) {
		if (tiles[i].y > item.y + item.height) return false;
		if (tiles[i].interesting.uninteresting && Physics.overlap(item, tiles[i])) {
			item.crushed();
			return true;
		}
	}
	return false;
}

Collisions.checkCrushed = function(item, tile) {
	// First check I'm not standing on it. If so don't need to check side crush.
	if (!(tile.interesting.top && item.y+item.height >= tile.y+tile.height/2)) {
		if (tile.interesting.left) {
			if (item.x + item.width < tile.x + tile.width / 2) {
				item.x = tile.x - item.width - 0.0001;
				item.prev.x = item.x;
				return false;
			}
		}
		if (tile.interesting.right) {
			if (item.x > tile.x + tile.width / 2) {
				item.x = tile.x + tile.width + 0.0001;
				item.prev.x = item.x;
				return false;
			}
		}
	}
	Collisions.checkCrushedVertical(item, tile);
}

Collisions.checkCrushedVertical = function(item, tile) {
	if (tile.interesting.top) {
		// If a tile is not interesting at the top, there is a tile above it. If I collide with it and am above it, the tile above would squish me.
		if (item.y+item.height < tile.y+tile.height/2) {
			item.y = tile.y-item.height;
			item.prev.y = item.y;
			return false;
		}
	}
	if (tile.interesting.bottom) {
		if (item.y > tile.y+tile.height/2) {
			item.y = tile.y+tile.height;
			item.prev.y = item.y;
			return false;
		}
	}
	
	item.crushed();
	return true;
}

Collisions.itemWasAboveTile = function(item, tile){return tile.interesting.top && item.prev.y + item.height < tile.top};
Collisions.itemWasBelowTile = function(item, tile){return tile.interesting.bottom && item.prev.y > tile.bottom};
Collisions.itemWasLeftOfTile = function(item, tile){return tile.interesting.left && item.prev.x + item.width < tile.left};
Collisions.itemWasRightOfTile = function(item, tile){return tile.interesting.right && item.prev.x > tile.right};

/* ***************** ITEM COLLISIONS *******************/
Collisions.collideItems = function(item1, item2) {
    if (Collisions.itemsDontCollide(item1, item2)) return;
    if (Collisions.checkZipping(item1, item2)) return;
    if (Collisions.checkJustBecameTangibleOverlap(item1, item2)) return;
    item1.collidedWithItem = true; item2.collidedWithItem = true;
    if (Collisions.checkCustomCollision(item1, item2)) return;
    let itemVerticalOrder = Collisions.objWasAboveOther(item1, item2);
    if (itemVerticalOrder) Collisions.resolveTopCollisionItem(itemVerticalOrder);
    else {
        let itemHorizontalOrder = Collisions.getLeftRightObjOrder(item1, item2);
        if (itemHorizontalOrder)
            Collisions.resolveSideCollisionItem(itemHorizontalOrder);
    }
    Collisions.resolveContinuedCollisionItem(item1, item2);
}

Collisions.checkZipping = function(item1, item2) {
	if (item1.isZipping()) item2.zipBump(item1, Collisions.objIsOnLeftSideOfOther(item1, item2));
	else if (item2.isZipping()) item1.zipBump(item2, Collisions.objIsOnLeftSideOfOther (item2, item1));
	else return false;
	return true;
}

Collisions.checkCustomCollision = function(item1, item2) {
    let usedCustom = false;
    if (item1.customCollision)
        usedCustom = item1.customCollision(item2);
    if (item2.customCollision)
        usedCustom = usedCustom || item2.customCollision(item1);
    return usedCustom;
}

// If item1 just became tangible and item2 is inside it, blast item2 away
Collisions.checkJustBecameTangibleOverlap = function(item1, item2) {
    if (item1.justBecameTangible) {
        if (Physics.smallOverlap(item1, item2, 0.85)) {
			if (item2.ableToMove())
				item1.blastItemAway(item2);
			else
				item2.blastItemAway(item1);
		}
        return true;
    }
    if (item2.justBecameTangible) {
        if (Physics.smallOverlap(item2, item1, 0.85)) {
			if (item1.ableToMove())
				item2.blastItemAway(item1)
			else
				item1.blastItemAway(item2);
		};
        return true;
    }
    return false;
}

Collisions.itemsDontCollide = function(item1, item2) {
    if (item1.isDisabledOrDead() || item2.isDisabledOrDead()) return true;
    if (!item1.tangible && item2.dontCollideWithIntangibles) return true;
    if (!item2.tangible && item1.dontCollideWithIntangibles) return true;
    if (item1.tangible && item2.onlyCollideWithIntangibles) return true;
    if (item2.tangible && item1.onlyCollideWithIntangibles) return true;
    return false;
}

// Return pair of top and bottom objs
Collisions.objWasAboveOther = function(obj1, obj2) {
    let prev1 = {top: obj1.prev.y, bottom: obj1.prev.y+obj1.height};
    let prev2 = {top: obj2.prev.y, bottom: obj2.prev.y+obj2.height}; 
    if (prev1.bottom <= prev2.top) return {top: obj1, bottom: obj2};
    if (prev2.bottom <= prev1.top) return {top: obj2, bottom: obj1};
    return null;
}

// Return pair of left and right item
Collisions.getLeftRightObjOrder = function(obj1, obj2) {
    if (Collisions.objWasLeftOfOther(obj1, obj2)) return {left: obj1, right: obj2};
    if (Collisions.objWasLeftOfOther(obj2, obj1)) return {left: obj2, right: obj1};
    return null;
}

Collisions.objWasLeftOfOther = function(obj1, obj2) {
	return obj1.prev.x + obj1.width <= obj2.prev.x;
}

Collisions.objIsOnLeftSideOfOther = function(obj1, obj2) {
	return obj1.x + obj1.width/2 <= obj2.x + obj2.width/2;
}

Collisions.resolveTopCollisionItem = function(items) {
    let topItem = items.top, bottomItem = items.bottom;
	
	if (bottomItem.bounceOnRisingCollision() && bottomItem.velocity.y < 0)
		return Collisions.verticalItemBounce(topItem, bottomItem);
	
    topItem.setStandingOnItem(true);
	// Don't want short circuit here
    if (!topItem.onLandingOnItem(bottomItem) & !bottomItem.onLandedOnByItem(topItem)) {
        if (!topItem.isOnCeiling() && topItem.canBePushed()) {
            if (topItem.velocity.y >= 0) Physics.bounceObjectY(topItem);
            topItem.y = bottomItem.y - topItem.height + 0.001;
			if (bottomItem.velocity.y < 0 && topItem.velocity.y <= 0 && bottomItem.velocity.y < topItem.velocity.y)
				topItem.setVelocityY(bottomItem.velocity.y);
        } else {
            bottomItem.velocity.y = 0;
            bottomItem.y = topItem.y + topItem.height + 0.0001;
        }
    }
}

Collisions.verticalItemBounce = function(topItem, bottomItem) {
	let topBumpY = -1 + Math.min(-bottomItem.mass/topItem.mass, -0.15);
	let bottomBumpY = 1 + Math.max(topItem.mass/bottomItem.mass, 0.15);
	// topItem.bump(topItem.velocity.x, topBumpY);
	// bottomItem.bump(bottomItem.velocity.x, bottomBumpY);
	topItem.setVelocityY(topBumpY);
	bottomItem.setVelocityY(bottomBumpY);
}

Collisions.resolveSideCollisionItem = function(items) {
    let leftItem = items.left, rightItem = items.right;
    leftItem.x = leftItem.prev.x;
    rightItem.x = rightItem.prev.x;

	let leftV = {x: leftItem.velocity.x, y: leftItem.velocity.y};
	let rightV = {x: rightItem.velocity.x, y: rightItem.velocity.y};
	rightItem.bump(leftItem, leftV, true);
	leftItem.bump(rightItem, rightV, false);
}

// Check if they are still overlapping after the initial hit and separate
Collisions.resolveContinuedCollisionItem = function(item1, item2) {
    if (Physics.overlap(item1, item2)) {
        let items = Collisions.getHigherCenterItem(item1, item2);
        // Solve continued collision by repeating top collision.
        Collisions.resolveTopCollisionItem(items);
    }
}

Collisions.getHigherCenterItem = function(item1, item2) {
    return item1.y + item1.halfHeight <= item2.y + item2.halfHeight ? 
    {top: item1, bottom: item2} : {top: item2, bottom: item1};
}


Collisions.objOverlapsWithAnyInList = function(obj1, sortedObjList) {
    for (var i = 0; i < sortedObjList.length; ++i) {
        var obj2 = sortedObjList[i];
        if (obj1.y > obj2.y + obj2.height) return false; 
        if (Physics.overlap(obj1, obj2)) return true;
    }
    return false;
    
}
return Collisions;
});