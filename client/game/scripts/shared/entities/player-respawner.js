define([], function() {
class PlayerRespawner {
	constructor(objectMover) {
		this.respawnDone = false;
		this.launchVelocity = null;
		this.objectMover = objectMover;
	}
	    
    start(player, respawnPoint, waterHeight) {
        player.disable(false);
        player.x = respawnPoint.x - 1500;
        player.y = waterHeight + 300;
        player.setBeingForciblyMoved(true);
        player.setVelocity(0, 0);
        player.allowGravity = false;
        this.respawnDone = false;
        this.objectMover.add(player, 'RESPAWN_START', ['x'], [respawnPoint.x], {duration: 1500});
        this.objectMover.add(player, 'RESPAWN_START', ['y'], [respawnPoint.y], {duration: 2000}, function() {this.respawnDone=true;}.bind(this));
    }
	
	// Return true when finished!
    updatePrepareRespawn() {
        return this.respawnDone;
    }
	
	startThrowSequence(player, velocity, onCompleteCallback) {
        this.objectMover.add(player, 'RESPAWN_PERFORM', ['x', 'y'], [player.x, player.y+200], {duration: 200, yoyo: true});
        this.objectMover.add(player, 'RESPAWN_PERFORM', ['x', 'y'], [player.x, player.y+200], {duration: 200, delay: 400, yoyo: true});
        this.objectMover.add(player, 'RESPAWN_PERFORM_FINAL', ['x', 'y'], [player.x, player.y+300], {duration: 300, delay: 800, yoyo: true}, 
                            this.onLaunchComplete, [player, velocity, onCompleteCallback]);
    }
	
	onLaunchComplete(player, velocity, callback) {
        player.allowGravity = true;
        player.addStatusEffect('Shield');
        player.prepareJumpNextUpdate(velocity);
        callback();
    }
}

return PlayerRespawner;
});