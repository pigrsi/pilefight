define(['entities/game-object'], function(GameObject) {
class Explosion extends GameObject {
    constructor(x, y, radius, lifetime, world) {
        super(x, y, radius*2, radius*2, world);
        this.name = 'Explosion';
        this.radius = radius;
        this.lifetime = lifetime;
        this.itemsLaunched = [];
    }
    
    update() {
        if (this.lifetime-- <= 0) this.flagForRemoval();
    }
    
    onCollideItem(item) {
		if (item.isImmuneToExplosions()) return true;
        if (this.itemsLaunched.indexOf(item) !== -1) return;
        this.itemsLaunched.push(item);
        		
        let percentageDistanceFromCentre = {};
        percentageDistanceFromCentre.x = Math.min(Math.abs(item.x + item.halfWidth - this.x) / this.radius, 0.4);
        percentageDistanceFromCentre.y = Math.min(Math.abs(item.y + item.halfHeight - this.y) / this.radius, 0.4);
        
        let velocity = {};
        
        velocity.x = -20;
        if (item.x + item.halfWidth > this.x) velocity.x = 20;
        velocity.x *= (1-percentageDistanceFromCentre.x);
        
        velocity.y = -16
        if (item.y + item.halfHeight > this.y) velocity.y = 16;
        velocity.y *= (1-percentageDistanceFromCentre.x);
        
        velocity.y = -Math.abs(velocity.y);
        //item.bump(bumpedBy, otherVelocity, toRight);
        //item.launchAsAttack(velocity);
		item.setStun(true, 60 - (120 * (percentageDistanceFromCentre.x + percentageDistanceFromCentre.y)/2));
		item.setVelocity(velocity);
		item.onCaughtInExplosion();
    }
}

return Explosion;
});