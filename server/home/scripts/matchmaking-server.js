const crypto = require("crypto");

const GAME_PASS_PREFIX = 'g';
const PUBLIC_PREFIX = 'r';
const PRIVATE_PREFIX = 's';
const PORT_SUFFIX = 'e';

module.exports = {
	roomsInfo: [],
	workers: {},
	workerGameCounts: {},
	workerJoinableGames: {},
	
	init(io, workerArray, logger) {
		this.logger = logger;
		this.homeSpace = io.of('/home');
		this.monitorSpace = io.of('/monitor');
		this.setupWorkers(workerArray);
		let this_ = this;
		this.homeSpace.on('connection', function(sock) {
			this_.logger.debug("Client connected to matchmaker");
			this_.registerClientMessages(sock);
		});
		this.monitorSpace.on('connection', function(sock) {
			sock.on('monitorRequest', function() {
				this_.requestWorkerInfo();
			});
		});
	},
	
	setupWorkers(workerArray) {
		for (let i = 0; i < workerArray.length; ++i) {
			let worker = workerArray[i];
			this.workerGameCounts[worker.wid] = 0;
			this.workers[worker.wid] = worker;
			this.workerJoinableGames[worker.wid] = [];
			this.registerWorkerMessages(worker);
		}
	},
	
	registerWorkerMessages(worker) {
		let this_ = this;
		worker.on('message', function(msg) {
			switch (msg.type) {
				case 'SET_JOINABLE':
					let joinableGamesArray = this_.workerJoinableGames[worker.wid];
					if (msg.joinable)
						joinableGamesArray.push(msg.roomId);
					else {
						let index = joinableGamesArray.indexOf(msg.roomId);
						if (index !== -1) joinableGamesArray.splice(index, 1);
					}
					break;
				case 'GAME_ADDED':
					this_.workerGameCounts[worker.wid] += 1;
					break;
				case 'GAME_REMOVED':
					this_.workerGameCounts[worker.wid] -= 1;
					break;
				case 'WORKER_INFO':
					msg.workerData.workerWid = worker.wid;
					this_.workerInfoReceived(msg.workerData);
					break;
			}
		});
	},
	
	registerClientMessages(sock) {
		let this_ = this;
		sock.on('requestgame', function(info) {
			if (!info.private) {
				this_.findPublicGameForClient(sock.id);
			} else {
				this_.createPrivateGame(sock.id);
			}
		});
	},
	
	findPublicGameForClient(sockId) {
		this.logger.debug(`Finding game for client: id[${sockId}]`);
		
		let res = this.findExistingJoinableGame();
		let gamePass;
		let wid;
		let roomId;
		if (res) {
			// tell worker to create one time pass
			wid = res.wid;
			roomId = res.roomId;
		} else {
			wid = this.getWorkerIdForNewGame();
			roomId = this.getNewPublicGameId(wid);
			this.workers[wid].send({type: 'CREATE_GAME', roomId: roomId});
		}
		
		gamePass = this.getNewOneTimePass(wid);		
		this.workers[wid].send({type: 'ADD_ONE_TIME_PASS', roomId: roomId, pass: gamePass});
		this.emit("foundgame", {roomId:gamePass}, sockId);
		//setTimeout(this.emit.bind(this, "foundgame", {roomId:gamePass}, sockId), 1000);
	},
	
	createPrivateGame(sockId) {
		this.logger.debug(`Asking worker to create private game for client: id[${sockId}]`);
		
		let wid = this.getWorkerIdForNewGame();
		let roomId = this.getNewPrivateGameId(wid);
		this.workers[wid].send({type: 'CREATE_GAME', roomId: roomId, privateGame: true});
		this.emit("foundgame", {roomId: roomId}, sockId);
	},
	
	findExistingJoinableGame() {
		let keys = Object.keys(this.workerJoinableGames);
		for (let i = 0; i < keys.length; ++i) {
			let currentWid = keys[i];
			if (this.workerJoinableGames[currentWid][0])
				 return {wid: currentWid, roomId: this.workerJoinableGames[currentWid][0]};
		}
		return false;
	},
	
	getWorkerIdForNewGame() {
		// Find worker with lowest number of games running
		let leastGamesWorkerId = null;
		let this_ = this;
		Object.keys(this_.workers).forEach(function(currentWid, index) {
			 if (!leastGamesWorkerId ||
			 this_.workerGameCounts[currentWid] < this_.workerGameCounts[leastGamesWorkerId])
				leastGamesWorkerId = currentWid;
		});
		
		return leastGamesWorkerId;
	},
	
	emit(messageType, data, sendTo) {
		this.logger.debug("Sending " + messageType + " message to " + sendTo);
		if (sendTo) this.homeSpace.connected[sendTo].emit(messageType, data);
	},

	emitToMonitors(messageType, data) {
		// this.logger.debug("Sending " + messageType + "message to monitors");
		this.monitorSpace.emit(messageType, data);
	},
	
	getNewPublicGameId(wid) {
		return PUBLIC_PREFIX + wid + PORT_SUFFIX + crypto.randomBytes(5).toString('hex');
	},
	
	getNewPrivateGameId(wid) {
		return PRIVATE_PREFIX + wid + PORT_SUFFIX + crypto.randomBytes(5).toString('hex');
	},
	
	getNewOneTimePass(wid) {
		return GAME_PASS_PREFIX + wid + PORT_SUFFIX + crypto.randomBytes(5).toString('hex');
	},

	requestWorkerInfo() {
		// this.logger.debug(`Requesting worker info`);
		let keys = Object.keys(this.workers);
		for (let i = 0; i < keys.length; ++i) {
			let currentWid = keys[i];
			this.workers[currentWid].send({type: 'WORKER_INFO_REQUEST'});
		}
	},

	workerInfoReceived(workerData) {
		// this.logger.debug(`Sending worker info to monitor`);
		this.emitToMonitors('monitorInfo', {workers: [workerData]});
	},
	
	// createGame() {
		// let gameId;
		// do {
			// gameId = PUBLIC_PREFIX + crypto.randomBytes(5).toString('hex');
		// } while (!!this.roomsInfo[gameId]);
		// this.roomsInfo[gameId] = gameSetup.createGame(gameId);
		// this.logger.debug('Created new game with id: ' + gameId);
		// return gameId;
	// },


};