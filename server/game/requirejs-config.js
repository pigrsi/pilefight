const requirejs = require('requirejs');

requirejs.config({
  //By default load any module IDs from js/lib
  baseUrl: process.cwd(),
  paths: {
    server: 'server/game/scripts',
    text: 'client/game/lib/text',
    shared: 'client/game/scripts/shared',
    entities: 'client/game/scripts/shared/entities',
    util: 'client/game/scripts/shared/util',
    maps: 'client/game/assets/maps',
    events: 'client/game/scripts/shared/events',
    sgame: 'client/game/scripts/shared/sgame',
    items: 'client/game/scripts/shared/entities/items',
  }
});