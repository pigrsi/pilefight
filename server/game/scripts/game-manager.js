var requirejs = require('requirejs');
var socketIO = require('socket.io');
const gameSetup = requirejs(process.cwd() + '/server/game/scripts/game-setup.js');
var gameSpace;

module.exports = {
	oneTimePasses: {},
	roomsInfo: {},
	
	init(port, logger) {
		this.logger = logger;
		this.logger.info('Starting game hoster server on port ' + port);
		this.registerMasterMessages();
		this.initConnection(port);
	},
	
	registerMasterMessages() {
		let this_ = this;
		process.on('message', function(msg) {
			switch (msg.type) {
				case 'ADD_ONE_TIME_PASS':
					this_.addOneTimePass(msg.pass, msg.roomId); break;
				case 'CREATE_GAME':
					this_.createGame(msg.roomId, !!msg.privateGame); break;
				case 'WORKER_INFO_REQUEST':
					this_.sendInfoToMaster();
			}
		});
	},
	
	createGame(roomId, isPrivateRoom) {
		this.logger.info('Creating new game with roomId: ' + roomId);
		let roomInfo = gameSetup.createGame(this.messageClient, roomId, isPrivateRoom);
		roomInfo.addListener(this.onRoomEvent.bind(this));
		this.roomsInfo[roomId] = roomInfo;
		this.messageMaster({type: 'SET_JOINABLE', roomId: roomId, joinable: true});
		this.messageMaster({type: 'GAME_ADDED'});
	},
	
	addOneTimePass(pass, roomId) {
		this.logger.info("Adding one time pass: " + pass);
		this.oneTimePasses[pass] = {roomId: roomId};
		setTimeout(this.removeOneTimePass.bind(this, pass), 30000);
	},
	
	removeOneTimePass(pass) {
		if (!this.oneTimePasses[pass]) {
			this.logger.debug("Trying to remove pass (" + pass + ") that was already consumed.");
			return;
		}
		this.logger.info("Removing one time pass: " + pass);
		delete this.oneTimePasses[pass];
	},
	
	initConnection(port) {
		io = socketIO.listen(port);
		gameSpace = io.of('/play');
		
		let this_ = this;
		gameSpace.use(function(socket, next) {
			if (socket.handshake.query) {
				let token = socket.handshake.query.token;
				let gameId;
				let nickname;
				let success = false;
				if (token[0] === 's') {
					success = !!this_.roomsInfo[token];
					gameId = token;
				} else if (token[0] === 'g') {
					let passData = this_.oneTimePasses[token];
					success = !!passData;
					if (success) {
						gameId = passData.roomId;
						this_.removeOneTimePass(token);
					}
				}
				
				if (success) {
					this_.logger.debug("Handshake success for (" + socket.id + ")");
					socket.join(gameId);
					next();
					gameSetup.onClientHandshakeSuccess(socket, gameId);
					return;
				}
			}
				
			this_.logger.debug("Handshake fail for (" + socket.id + ")");
			next(new Error('Authentication error'));
		});
	},
	
	onRoomEvent(event) {
		switch (event.type) {
			case 'SET_JOINABLE':
				this.messageMaster({type: 'SET_JOINABLE', roomId: event.roomId, joinable: event.joinable});
				if (!event.joinable)
					this.logger.info(`Room ${event.roomId} is no longer joinable.`);
				break;
			case 'DESTROY_ROOM':
				delete this.roomsInfo[event.roomId];
				this.messageMaster({type: 'SET_JOINABLE', roomId: event.roomId, joinable: false});
				this.messageMaster({type: 'GAME_REMOVED', roomId: event.roomId});
				this.logger.info(`Room ${event.roomId} has been removed.`);
				break;
		}
	},

	sendInfoToMaster() {
		var rooms = [];
		let keys = Object.keys(this.roomsInfo);
		for (let i = 0; i < keys.length; ++i) {
			rooms.push(this.roomsInfo[keys[i]]);
		}

		this.messageMaster({type: 'WORKER_INFO', workerData: {rooms: rooms}});
	},
	
	messageMaster(message) {
		message.wid = process.wid;
		process.send(message);
	},
	
	messageClient(messageType, data, roomId, sendTo=null) {
		// this.logger.debug("To: " + (sendTo == null ? "All" : sendTo) + "    Data: " + data);
		if (sendTo)
			gameSpace.connected[sendTo].emit(messageType, data);
		else
			gameSpace.to(roomId).emit(messageType, data);
	}
};