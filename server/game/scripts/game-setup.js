const pino = require('pino');
require(process.cwd() + '/server/game/requirejs-config');

define(['server/server-loop', 'events/external-listener', 'events/events', 'shared/room-info'], function(ServerLooper, ExternalListener, Events, RoomInfo) {
	let gameSetup = {};
	ExternalListener.logger = createGameLogger('EXL');
	
	let registerListen = function(messageType, socket, gameId) {
		socket.on(messageType, function(info) {
			let dataWithSenderId = {data: info, senderId: socket.id};
			ExternalListener.messageReceived(messageType, dataWithSenderId, gameId);
		});
	};

	gameSetup.registerClientEvents = function(socket, gameId) {
		registerListen('disconnect', socket, gameId);
		registerListen('CONNECTED', socket, gameId);
		registerListen('NOTION', socket, gameId);
		registerListen('START_PRIVATE', socket, gameId);
	};
	
	gameSetup.onClientHandshakeSuccess = function(socket, gameId) {
		gameSetup.registerClientEvents(socket, gameId);
		// ExternalListener.messageReceived('CONNECTED', {data: null, senderId: socket.id}, gameId);
	};

	let gameIdsToInfo = {};
	gameSetup.createGame = function(emit, roomId, isPrivateRoom) {
		let roomInfo = new RoomInfo(roomId, isPrivateRoom, 'POINTS', 2, 8, 2, 1, 26, false);
		let logger = createGameLogger('RM-'+roomId);
		ServerLooper.addGame(emit, roomId, roomInfo, logger);
		gameIdsToInfo[roomId] = roomInfo;
		return roomInfo;
	};
	
	return gameSetup;
});

function createGameLogger(loggerName) {
	let logger = pino({name: loggerName, level: 20}, pino.destination('./server/game/logs/game.log'));
	let gameLogger = {
		debug(message) {logger.debug(message);},
		info(message) {logger.info(message);},
		warn(message) {logger.warn(message);},
		error(message) {logger.error(message);},
		fatal(message) {logger.fatal(message);},
	};
	return gameLogger;
}
