define(['shared/global-info', 'server/game-runner'], function(GlobalInfo, GameRunner) {
var ServerLooper = {};
ServerLooper.fps = 60;
GlobalInfo.LOCAL_GAME = false;

ServerLooper.addGame = function(externalEmitter, roomId, roomInfo, logger) {
	let game = new GameRunner(externalEmitter, roomId, roomInfo, logger);
	game.start();
	ServerLooper.update(game);
};

ServerLooper.update = function(game) {
    // if (!ServerLooper.gameMode) ServerLooper.gameMode = new LivesGame(false);
	if (game.gameShouldBeDestroyed) return;
    setTimeout(ServerLooper.update.bind(null, game), 1000/ServerLooper.fps);
    game.update();
};

return ServerLooper;
});