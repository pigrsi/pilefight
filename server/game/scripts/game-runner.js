define(['events/external-listener', 'events/events', 'events/event-listener', 'shared/world', 'shared/world-network', 'sgame/server-master', 'sgame/points-game'],
function(ExternalListener, Events, EventListener, World, WorldNetwork, ServerMaster, PointsGame) {
	class GameRunner {
		constructor(externalEmitter, roomId, roomInfo, logger) {
			this.externalEmitter = externalEmitter;
			this.roomId = roomId;
			this.roomInfo = roomInfo;
			this.roomInfo.addListener(this.onRoomEvent.bind(this));
			this.logger = logger;
			
			this.clients = {};
			this.host = null;
			this.gameRunning = false;
            this.nextSmallClientId = 0;

			this.startTeardownRoomTimeout();
		}
		
		start() {
			this.events = new Events(this.roomId);
			ExternalListener.addEventManager(this.events, this.roomId);
			this.eventListener = new EventListener(this.events);
			this.events.setEmitMessageExternalCallback(this.externalEmitter);
			this.eventListener.registerGameRunnerServer(this);
			
			if (!this.roomInfo.privateRoom) {
				this.startNewGame();
			}
		}
		
		lobbyComplete(gameSettings, senderId) {
			if (senderId === this.host) {
				this.roomInfo.setLivesPerClient(gameSettings.livesPerPlayer);
				this.roomInfo.setPlayersPerClient(gameSettings.playersPerClient);
				this.roomInfo.setSecondsPerTurn(gameSettings.secondsPerTurn);
				this.roomInfo.setReadyCountdown(0);
				this.startNewGame(gameSettings);
				this.events.sendLobbyComplete();
			}
		}
		
		onClientConnect(clientData, clientId) {
			if (!!this.teardownRoomTimeout) this.cancelTeardownRoomTimeout();
			let nickname = clientData.nickname;
			this.logger.info(`GameRunner: Client ${nickname} joined. ID: ${clientId}`);
			this.clients[clientId] = {nickname: nickname, smallId: this.nextSmallClientId++};
			this.events.sendSignal({signal: 'PLAYER_JOINED', nickname: nickname});
			this.events.sendSignal({signal: 'ROOM_INFO', players: this.getListOfClientInfo(), gameRunning: this.gameRunning}, clientId);
			this.roomInfo.incrementNumClients();
			if (!this.host) this.setHost(clientId);
		}
		
		onClientDisconnect(clientId) {
			this.logger.info(`GameRunner: Client ${clientId} has disconnected.`);
			if (this.gameRunning)
				this.game.onClientDisconnection(clientId);
			else
				this.lobbyDisconnection(clientId);
			this.roomInfo.decrementNumClients();
		}

		lobbyDisconnection(clientId) {
			if (!this.clients[clientId]) return;
			this.events.sendSignal({signal: 'CLIENT_DISCONNECTED_LOBBY', nickname: this.clients[clientId].nickname});
			delete this.clients[clientId];
			if (clientId === this.host) {
				let keys = Object.keys(this.clients);
				this.setHost(keys[0]);
			}
		}
		
		setHost(clientId) {
			this.logger.info("GameRunner: Setting host client to " + clientId);
			this.host = clientId;
			this.events.sendSignal({signal: 'BECOME_HOST', becomeHost: true}, clientId);
		}
		
		getListOfClientInfo() {
			let keys = Object.keys(this.clients);
			let infos = [];
			for (let i = 0; i < keys.length; ++i)
				infos.push(this.clients[keys[i]]);
			return infos;
		}
		
		startNewGame() {			
			this.gameWorld = new World();
			this.gameWorld.init(30);
			let network = new WorldNetwork(this.gameWorld, this.logger);
			let serverMaster = new ServerMaster(this.events, network, this.logger, this.clients);
			this.game = new PointsGame(serverMaster, this.roomInfo, this.logger);
			this.eventListener.registerListenEventsServer(serverMaster);
			this.gameRunning = true;
		}

		endCurrentGame() {
			this.gameWorld = null;
			this.game = null;
			this.eventListener.unregisterListenEventsServer();
			this.gameRunning = false;
		}

		teardownRoom() {
			this.cancelTeardownRoomTimeout();
			this.gameShouldBeDestroyed = true;
			this.roomInfo.readyToDestroyRoom();
		}
		
		update() {
			// TODO: I don't think the GameMode must be updated every frame!! A lot of time can possibly be saved here.
			if (this.gameRunning) {
				this.game.update();
				this.gameWorld.update();
			}
		}

		onRoomEvent(event) {
			switch (event.type) {
				case "NO_CLIENTS":
					this.startTeardownRoomTimeout();
					break;
				case "GAME_FINISHED":
					this.endCurrentGame();
					break;
			}
		}

		startTeardownRoomTimeout() {
			if (!!this.teardownRoomTimeout) return;
			this.teardownRoomTimeout = setTimeout(this.teardownRoom.bind(this), 10000);
		}

		cancelTeardownRoomTimeout() {
			if (!this.teardownRoomTimeout) return;
			clearTimeout(this.teardownRoomTimeout);
			this.teardownRoomTimeout = null;
		}
	}
	
	return GameRunner;
});