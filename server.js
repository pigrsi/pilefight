const fs = require('fs');
sendOutputToFile();

// Dependencies
const cluster = require('cluster');
var numCPUs = require('os').cpus().length;
var express = require('express');
var http = require('http');
var path = require('path');
var socketIO = require('socket.io');
const pino = require('pino');

if (cluster.isMaster) {
	masterProcess();
} else {
	childProcess();
}

function masterProcess() {
	let logger = pino({name: 'MM', level: 20}, pino.destination('./server/home/logs/server.log'));
	
	logger.debug(`Master ${process.pid} is running`);
	// let port = 6425;
	let port = 80;

	var app = express();	
	var server = http.Server(app);
	io = socketIO(server);

	app.set('port', port);
	app.use('/', express.static(process.cwd() + '/client'));

	app.get('/', function(request, response) {
		response.sendFile(path.join(process.cwd(), '/client/home/home.html'));
	});

	app.get('/game', function(request, response) {
		response.sendFile(path.join(process.cwd(), '/client/game/game.html'));
	});
	
	server.listen(port, '0.0.0.0', function() {
		logger.info('Starting main server on port ' + port);
	});
	
	let workers = [];
	numCPUs = 4;
	for (let i = 0; i < numCPUs; i++) {
		logger.debug(`Forking process number ${i}...`);
		let wid = i.toString();
		let worker = cluster.fork({basePort: 3427, wid: wid});
		worker.wid = wid;
		workers.push(worker);
	}
	
	let mm = require('./server/home/scripts/matchmaking-server.js');
	mm.init(io, workers, logger);	
}

function childProcess() {
	let wid = process.env.wid;
	let port = parseInt(process.env.basePort) + parseInt(wid);
	let logger = pino({name: 'GM-' + wid, level: 20}, pino.destination('./server/home/logs/game-manager.log'));
		
	let gameManager = require('./server/game/scripts/game-manager.js');
	gameManager.init(port, logger);
}

function sendOutputToFile() {
	var access = fs.createWriteStream('./output.log', {'flags': 'a'});
	process.stdout.write = process.stderr.write = access.write.bind(access);
	process.on('uncaughtException', function(err) {
		console.error((err && err.stack) ? err.stack : err);
	});
}