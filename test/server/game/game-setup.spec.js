require(process.cwd() + '/server/game/requirejs-config');
const requirejs = require('requirejs');
var assert = require('assert');

gameSetup = requirejs('server/game-setup');


describe('Game setup', function() {
  it('When new game is created, should return roomInfo with correct properties set', function() {
    let roomInfo = gameSetup.createGame(null, 'awakawakatwotwot', false);
    assert(roomInfo.roomId === 'awakawakatwotwot');
    assert(roomInfo.privateRoom === false);
  });
});