What I'm using:

node.js
socket.io
Express

Express is like the boss here. It's the server program that responds to client requests.

Socket.io allows back and forth messages using the socket.emit() method.

__dirname - node.js thing that is the dir the script being executed is in.

Routing is responding to HTTP requests such as GET, PUT, etc.

What I will need:
Client can connect to server (obviously)
Client can send messages to server
Server can send message to individual client
Server can broadcast messages to all clients