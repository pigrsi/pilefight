Experiment:
- If something is moving offscreen, can we show it in an extra small window on the screen?

It would confirm why a turn is taking a while to end.
Would allow player to move camera where they want.
Sometimes things just happen in two places at once.

Also, a minimap:
- Show all tiles
- Show all friendly players in green
- Show opponents in red