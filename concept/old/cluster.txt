Cluster

E.g. Two child processes

Matchmaking is done on master.

Each child process has its own port. This is so a client can specify which port to connect to, because the game
they are trying to connect to only exists on one port.

Player requests a game:
- Matchmaker tells worker with least amount of games to create a new game.
- In this message, matchmaker includes client ID, and game info.
- When game is made, worker sends back message with client ID, and the join code.
- Matchmaker sends to client, join code, but also a port offset, so it knows which port to join.
- Player connects to game.html, but connects to game server using given port and joinId.
- Player is now connected to the correct server.
- New players can join using the same code, as it contains the port offset.

When a new game is created,

[With PM2, what do I want to do...]
- One main server, that handles matchmaking.
- 
