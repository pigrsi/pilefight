Server/Client System:

Setup:
- Server is in 'WAIT_PLAYERS' state.
- Client is in 'JOIN_ROOM' state.
- Client requests to join the server room until it gets a response from the server.
- Server gives the room name for the client.
- Client acknowledges this and joins the given room.
- Server sees client ack and creates player objects for that client, and returns the IDs.
- Client stores these IDs so it knows which players it controls.
- Server sends a sync to all players, now including the new player.
- Client who joined goes 'IDLE', waiting for the first turn sync.
- When the server has enough players, it moves to the 'GAME_START' state.
- When game starting setup is done, Server moves to 'DETERMINE_TURN' state.

Gameplay:
- Server determines whose turn it is. They send a sync to all stating whose turn it is.
- Server includes with sync, 'DECIDING_MOVE' action with the ID of the turn player.
- All clients check if this is one of their players. If not, stay 'IDLE', wait for next sync.
- The turn player's client has playerDecideMove called on their arena. This tells the arena that it is time for one of the local players to have their turn.
- Client gets the decided move from the arena and sendMoves it to the server.
- Server's moveReceived gets called. Server checks if the move is legit.
- If the move is legit, Server sends a sync to all clients telling them to perform that move on which player.
- Server performs the move itself and so do all the clients.
- When the server's arena becomes calm, Server goes back to 'DETERMINE_TURN' state, and sends sync containing next move information.
- When the client's arena becomes calm, go 'IDLE'. Perform the sync and use the info in it to prepare for the next move.
- Repeat.