Each item will need something telling the game which animation it should use. But this must be a client-only thing, and the item itself cannot do it.
There must be one for each item.

Therefore, as each item is spawned, draw-king should create a new object that watches items.
All items are in similar situations ->
They are spawned.
They fall to the ground.
They bounce on the ground.
They get launched.
They bump into other items.
They fall into water and die.

But some items will have unique animations.

Rock spawned ->
Create RockAnimator, pass it the rock.
RockAnimator adds image object to scene.
RockAnimator extends ItemAnimator
ItemAnimator -> UpdateAnimFrame -> Set image animation
UpdateAnimFrame -> If item.readyForRemoval -> Then destroy image, and animator.flagForRemoval(). Draw king can remove it

Common stuff - setup
In update, set frame to idle