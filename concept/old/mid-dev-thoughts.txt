Issues I have

No benefit in getting high ground in levels:
- Could make better items appear as you go higher
- Better level design so it's safer to be higher up
- No way to attack players below. Need items that can go through tiles.
- Item dispensers always in certain locations - harder to reach places can have better dispensers


Confirmed changes:
- Missile can go through walls
- Change missile movement:
    After fired: get thrown normally for a second
    After a second: no longer affected by gravity, double the speed, does not change speed until it explodes
    

On the bright side, I still think it's fun. Just needs a lot of tweaking and thought. But for now it might be better to work on the boring stuff
so I can move onto getting the gameplay just right. I think for now working on other stuff will go a longer way toward it feeling closer to being done.
