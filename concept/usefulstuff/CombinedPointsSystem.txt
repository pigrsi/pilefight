 You get points if:
* Someone gets hurt by an item on your turn.
* Someone else dies on your turn.
* Someone gets hit by a trap you set (even on a different turn).
* You're holding an orb at the end of the turn.
* Extra bonuses: e.g. spawn pickup, teleport pickup.
You lose points if:
* You die.
Therefore:
* Two players can gain points at the same time with traps.
Balance:
* Traps and hurting other players by bumping award 5 points each.
* Explosions award 10 points.
* Orb awards 3 points per item in stack.
    * 1 item = 3 points
    * 4 items = 12 points