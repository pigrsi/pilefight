- Create black hole object - GoalHole  - can be of any size ✓ 
- Spawn and draw it. For now draw it at all times ✓
- Make black hole eat/kill any items that collide with it (round collision). ✓
- Make it only spawn when a player has 100 points (allow possibility of multiple goal holes)
- Make game end when orb is thrown in. ✓
	- Well not 'spawn', but be active. It was spawned on map creation.
	- Send a blocking signal to spawn a goalhole.

- putting an orb in too early zaps you?
- Turn yellow when ready (only if turn player has enough points. else turn black again) ✓

Set points limit on map?