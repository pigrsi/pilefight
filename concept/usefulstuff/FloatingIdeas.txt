What if teleporters "zipped" you there instead of instantly teleporting? And anything you bump into gets knocked back? 
This would mean teleporting can be used offensively AND would be more clear

Another way to get items,  pluck from the ground like on Mario 2. We look for tiles with interesting tops and put them there. You won't know what item(s) you're gonna get. There can easily be multiple ones in a spot. They won't take up space. It's more random. Will need to specify pluckable items on map somehow.

on teleport, emit a little "whoosh" that pushes things away a bit

idea, being hit hard from above (e.g. destruction) can bury you in the ground

A spelunky item that lets you see the items in the floor.
level where you have to use green teleporters to get around

You get points if:
* Someone gets hurt by an item on your turn.
	- Tell the world - world will emit, world network will add the points
* Someone else dies on your turn.
	- Done
* Someone gets hit by a trap you set (even on a different turn).
* You're holding an orb at the end of the turn.
* Extra bonuses: e.g. spawn pickup, teleport pickup.
You lose points if:
* You die.
Therefore:
* Two players can gain points at the same time with traps.
Balance:
* Traps and hurting other players by bumping award 5 points each.
* Explosions award 10 points.
* Orb awards 3 points per item in stack.
    * 1 item = 3 points
    * 4 items = 12 points